#pragma once
#include "Pin.hpp"
#include "Meta.hpp"
#include "DataView.hpp"
#include "StaticLambdaWrapper.hpp"
#include "PinDefinitions.hpp"
#include <cstdint>

namespace IO
{
	#pragma pack(push, 1)
    enum I2CDirection : uint16_t
    {
        Read = 1,
        Write = 0
    };
    /**
     * @brief Represents an I2C address
     * 
     * @tparam Bits 
     */
    template <unsigned Bits>
    class Address
    {
    private:
        static_assert(General::IsAnyOf<unsigned>(Bits, 7u, 10u), "Address must be 7 or 10.");
        //
        //
        struct
        {
            std::uint16_t m_Value   : Bits;
            std::uint16_t           : 16u - Bits;   //Padding, Unused
        } Fields;
        //
        //
        class Bit7Addr
        {
        private:
            union
            {
                struct 
                {
                    uint8_t m_Read_NWrite  : 1u;
                    uint8_t m_Address      : 7u;
                } Fields;
                uint8_t m_Value;
            };
        public:
            constexpr Bit7Addr( I2CDirection direction, uint16_t address ) noexcept :
                Fields
                {
                    (uint8_t)direction,
                    (uint8_t)address
                }
            {}
            //
            constexpr uint16_t Value() const noexcept
            {
                return m_Value;
            }
        };
        //
        //
        class Bit10Addr
        {
        private:
            union
            {
                struct 
                {
                    uint16_t        m_Address_8_0   : 8u;
                    I2CDirection    m_Direction     : 1u;
                    uint16_t        m_Address_10_9  : 2u;
                    const uint16_t  m_AddressMode   : 5u;
                } Fields;
                uint16_t m_Value;
            };
        public:
            constexpr Bit10Addr( I2CDirection direction, uint16_t address ) noexcept:
                Fields
                {
                    General::FromRange<uint16_t>( address, 0u, 8u ),
                    direction,
                    General::FromRange<uint16_t>( address, 10, 2 ),
                    0b1110 //Always this value
                }
            {}
            //
            constexpr uint16_t Value() const noexcept
            {
                return m_Value;
            }
        };
        //
        //
        static constexpr uint16_t Read7(uint16_t value) noexcept
        {
            return Bit7Addr{ I2CDirection::Read, value }.Value();
        }
        static constexpr uint16_t Write7(uint16_t value) noexcept
        {
            return Bit7Addr{ I2CDirection::Write, value }.Value();
        }
        //
        //
        static constexpr uint16_t Read10(uint16_t value) noexcept
        {
            return Bit10Addr{ I2CDirection::Read, value }.Value();
        }
        static constexpr uint16_t Write10(uint16_t value) noexcept
        {
            return Bit10Addr{ I2CDirection::Write, value }.Value();
        }
        //
        //
    public:
        constexpr Address(std::uint16_t value) noexcept :
            Fields{ value }
        {}
        //
        constexpr operator uint16_t () const noexcept
        {
            return Fields.m_Value;
        }
        //
        constexpr uint16_t Read() const noexcept
        {
            if constexpr (Bits == 7u)
                return Read7( *this );
            if constexpr (Bits == 10u)
                return Read10( *this );
        }
        constexpr uint16_t Write() const noexcept
        {
            if constexpr (Bits == 7u)
                return Write7( *this );
            if constexpr (Bits == 10u)
                return Write10( *this );
        }
        //
        template <typename F>
        constexpr bool ForEachReadByte( F and function ) noexcept
        {
            auto f{ std::forward<F>( function ) };
            auto v{ Read() };
            if constexpr (Bits == 10u)
            {
                return	f( (uint8_t)(v >> 8u) ) and
						f( (uint8_t)(v) );
            }
            if constexpr (Bits == 7u)
            {
                return	f( (uint8_t)(v) );
            }
        }
        //
        template <typename F>
        constexpr bool ForEachWriteByte( F and function ) noexcept
        {
            auto f{ std::forward<F>( function ) };
            auto v{ Write() };
            if constexpr (Bits == 10u)
            {
                return	f( (uint8_t)(v >> 8u) ) and  
						f( (uint8_t)(v) );
            }
            if constexpr (Bits == 7u)
            {
                return	f( (uint8_t)(v) );
            }
        }
    };
    //
    //
    template <typename SDA, typename SCL>
    class I2C
    {
        template <typename T, bool R>
        using DataView = Containers::DataView<T, R>;
    public:
        using write_dataview = DataView<uint8_t, true>;
        using read_dataview = DataView<uint8_t, false>;
        /**
         * @brief Construct a new I2C object
         */
        I2C() noexcept
        {
            SCL::Configure( Type::OpenDrain, Mode::Output, State::High );
            SDA::Configure( Type::OpenDrain, Mode::Output, State::High );
        }
        I2C( SDA, SCL) noexcept :
            I2C{}
        {}
        /**
         * @brief Performs a generic transaction
         * 
         * @tparam Bits The number of bits in the address
         * @tparam Callback The type of the callback.
         * @param address The address to interact with.
         * @param callback The callback which calls after the address stage of the transaction.
         * @return size_t Returns the number of bytes transacted.
         */
        template< unsigned Bits, typename Callback >
		ALWAYS_INLINE static size_t Transaction( Address<Bits> address, Callback && callback ) noexcept
        {
            size_t out;
			(
                StartCondition()
                and (out = std::forward<Callback>(callback)())
                and EndCondition()
            )
            or ReleaseBus();
            return out;
        }
        /**
         * @brief Reads data into the views from the device at the address.
         * 
         * @tparam Bits The number of bits in an address.
         * @tparam Views The views that represent the destination data (can be fragmented)
         * @param address The address to read from
         * @param views See Views
         * @return size_t The number of bytes transmitted.
         */
        template< unsigned Bits, typename ... Views >
		static size_t Read( Address<Bits> address, Views ... views ) noexcept
        {
            return Transaction
            (
                address,
                [&]()
                {
                    return SendAddress( I2CDirection::Read, address ) ? 
                        ( ReadView( views ) + ... ) : 
                        ( 0u );
                }
            );
        }
        /**
         * @brief Writes the data in the views to the bus to the device at the address.
         * 
         * @tparam Bits The number of bits in the address
         * @tparam Views The data that is transmitted (it can be fragmented)
         * @param address The address to transmit the data
         * @param views See Views.
         * @return size_t The result is the number of bytes transmitted.
         */
        template< unsigned Bits, typename ... Views >
        static size_t Write( Address<Bits> address, Views ... views ) noexcept
        {
            return Transaction
            (
                address,
                [&]()
                {
                    return SendAddress( I2CDirection::Write, address ) ?
                        ( WriteView( views ) + ... ) :
                        ( 0u );
                }
            );
        }
        /**
         * @brief 
         * 
         * @tparam T 
         */
        template <typename T>
        inline static constexpr I2CDirection GetDirection = (T::IsReadOnly_v ? I2CDirection::Write : I2CDirection::Read);
        /**
         * @brief Sends a block of data, no extra handling involved here.
         * 
         * @param data 
         * @return size_t 
         */
        static size_t Handle(write_dataview data) noexcept
        {
            return WriteView( data );
        }
        /**
         * @brief Reads a block of data, no extra handling involved here.
         * 
         * @param data 
         * @return size_t 
         */
        static size_t Handle(read_dataview data) noexcept
        {
            return ReadView( data );
        }
        template < unsigned Bits >
        static bool AutoAddress( Address<Bits> address, read_dataview ) noexcept
        {
            return SendAddress( I2CDirection::Read, address );
        }
        template < unsigned Bits >
        static bool AutoAddress( Address<Bits> address, write_dataview ) noexcept
        {
            return SendAddress( I2CDirection::Write, address );
        }
        /**
         * @brief Performs a mix of reads and writes
         * 
         * @tparam Bits 
         * @tparam Views 
         * @param address 
         * @param views 
         * @return size_t 
         */
        template < unsigned Bits, typename First, typename ... Views >
        static size_t Mixed( Address<Bits> address, First first, Views ... views ) noexcept
        {
            return Transaction
            (
                address,
                [&]() noexcept
                {
                    size_t count = 0;
                    //
                    auto handle = [&](auto view) noexcept -> bool
                    {
                        auto out{ Handle( view ) };
                        count += out;
                        return out;
                    };
                    //
                    auto address_data = [&]( Address<Bits> addr, auto view ) noexcept
                    {
                        return AutoAddress( addr, view ) and handle( view );
                    };
                    //
                    I2CDirection direction = GetDirection<decltype(first)>;
                    //
                    return
                    (
                        address_data( address, first )
                        and
                        (
                            [&]( auto view ) noexcept
                            {
                                constexpr I2CDirection current = GetDirection<decltype(view)>;
                                //
                                auto r =
                                (
                                    ( ( current == direction ) and ( handle( view ) ) ) 
                                    or 
                                    ( RestartCondition() and ( address_data( address, view ) ) )
                                );
                                //
                                direction = current;
                                return r;
                            }( views )
                            and
                            ...
                        )
                    );
                }
            );
        }
    private:
        static void Delay(volatile int delay = 8) noexcept
        {
            while(--delay);
        }
        static void WriteSDA(State state) noexcept
        {
            SDA::Write( state );
            Delay();
        }
        static void WriteSCL(State state) noexcept
        {
            SCL::Write( state );
            Delay();
        }
        /**
         * @brief 
         * 
         * @param state 
         */
        static void WriteSDA(bool state) noexcept
        {
            WriteSDA( (State)state );
        }
        static void WriteSCL(bool state) noexcept
        {
            WriteSCL( (State)state );
        }
        /**
         * @brief 
         */
        ALWAYS_INLINE static bool ReleaseBus() noexcept
        {
            WriteSCL( State::High );
            WriteSDA( State::High );
			return true;
        }
        ALWAYS_INLINE static bool StartCondition() noexcept
        {
            WriteSDA( State::Low );
            WriteSCL( State::Low );
            return true;
        }
        ALWAYS_INLINE static bool EndCondition() noexcept
        {
            WriteSDA( State::Low );
            return ReleaseBus();
        }
        ALWAYS_INLINE static void ClockHigh() noexcept
        {
            WriteSCL( State::High );
            //
            // This allows clock stretching
            while ( SCL::Read() != State::High );
        }
        ALWAYS_INLINE static void ClockLow() noexcept
        {
            WriteSCL( State::Low );
        }
        template <typename Before, typename Middle, typename After>
        static void Clock( Before && before, Middle && middle, After && after ) noexcept
        {
            std::forward<Before>( before )();
            ClockHigh();
            std::forward<Middle>( middle )();
            ClockLow();
            std::forward<After> ( after )();
        }
        ALWAYS_INLINE static bool RestartCondition() noexcept
        {
            WriteSDA( State::High );
            Clock
            (
                []{},
                []{ WriteSDA( State::Low ); },
                []{}
            );
            return true;
        }
        ALWAYS_INLINE static void WriteClock() noexcept
        {
            Clock([]{}, []{}, []{});
        }
        ALWAYS_INLINE static uint8_t ReadClock() noexcept
        {
            volatile uint8_t output = 0;
            Clock
            (
                [](){},
                [&]()
                {
                    output = SDA::Read();
                },
                [](){}
            );
            Delay();
            return output;
        }
        ALWAYS_INLINE static bool ReadACK() noexcept
        {
            return not ReadClock();
        }
        static void WriteBit( bool input ) noexcept
        {
            WriteSDA( input );          //1
            WriteClock();               //2
        }
        ALWAYS_INLINE static void WriteACK() noexcept
        {
            WriteBit( false );
            WriteSDA( true );
        }
        ALWAYS_INLINE static void WriteNACK() noexcept
        {
            WriteBit( true );
        }
        ALWAYS_INLINE static bool WriteByte( uint8_t input ) noexcept
        {
            for ( auto i{ 0u }; i < 8u; ++i )
            {
                WriteBit( input & 0x80 );
                input <<= 1u;
            }
            return ReadACK();
        }
        ALWAYS_INLINE static size_t WriteView( write_dataview dataview ) noexcept
        {
			size_t index{ 0 };
			for ( ; index < dataview.Size(); ++index )
			{
				if ( not WriteByte( dataview[ index ] ) )
					break;
			}
            return index;
        }
        /**
         * Reads a single byte and an ACK
         */
        static uint8_t ReadByte( bool nack ) noexcept
        {
            uint8_t buffer =
                ( ReadClock() << 7u ) |
                ( ReadClock() << 6u ) |
                ( ReadClock() << 5u ) |
                ( ReadClock() << 4u ) |
                ( ReadClock() << 3u ) |
                ( ReadClock() << 2u ) |
                ( ReadClock() << 1u ) |
                ( ReadClock() << 0u );
            //
            ( nack ) ? WriteNACK() : WriteACK();
            return buffer;
        }
        /**
         *  Read a block of data, this does not include the address section of a transaction
         *  returns the number of bytes Readd (based on ACK's).
         */
        ALWAYS_INLINE static size_t ReadView( read_dataview data ) noexcept
        {
            // Release clock ready for read
            WriteSDA( State::High );
            for ( size_t i = 0; i < data.Size(); ++i )
            {
                data[i] = ReadByte( i + 1 == data.Size() );
            }
            return data.Size();
        }
        template < unsigned Bits >
        ALWAYS_INLINE static bool SendAddress( I2CDirection direction, Address<Bits> address ) noexcept
        {
            switch ( direction )
            {
            case I2CDirection::Read:
				return address.ForEachReadByte ( [] (uint8_t v) { return WriteByte( v ); } );
            case I2CDirection::Write:
				return address.ForEachWriteByte( [] (uint8_t v) { return WriteByte( v ); } );
            default:
                return false;
			};
        }
    };
}