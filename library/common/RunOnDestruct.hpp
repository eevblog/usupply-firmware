#pragma once

#include <utility>
namespace General
{
    /**
     * @brief Runs a function when the object is destructed.
     * 
     * This can be used for logging, peripheral power or resource release.
     * 
     * @tparam F The function type.
     */
    template <typename F>
    class RunOnDestruct
    {
    private:
        F m_Function;
    public:
        /**
         * @brief Construct a new RunOnDestruct object.
         * 
         * @tparam Fn The function type.
         * @param function The function to run on destruction.
         */
        template <typename Fn>
        constexpr RunOnDestruct(Fn && function) : 
            m_Function{ std::forward<Fn>(function) }
        {}
        /**
         * @brief Destroy the Run On Destruct object
         */
        ~RunOnDestruct()
        {
            m_Function();
        }
    };

    template <typename F>
    RunOnDestruct(F) -> RunOnDestruct<F>;
}