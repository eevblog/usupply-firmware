
#pragma once
#include "RunOnConstruct.hpp"
#include "Meta.hpp"
#include <cstddef>

namespace General
{
    /**
     * @brief Constructs an arbitary object on the stack
     * 
     * This is similar to std::any but on the stack.
     * 
     * Objects of this type are not movable or copyable.
     * The way to handle this is by destructing the object then reconstructing it.
     * 
     * @tparam BufferSize The size of the internal buffer.
     */
    template <size_t BufferSize>
    class StackObject : RunOnConstruct
    {
        void (*m_Destroy)( const void * ){ nullptr };
        std::byte m_Storage[ BufferSize ]{ };
        /**
         * @brief Runs the destructor on the underlying object
         * 
         * @tparam T The type stored in the class.
         * @param destroy Pointer to the object which will be destroyed.
         */
        template <typename T>
        static void RunDestroy( const void * destroy ) noexcept
        {
            static_cast<const std::decay_t<T> *>( destroy )->~T();
        }
    public:
        /**
         * @brief Constructs an object from the given arguments
         * 
         * @tparam T The type of the object to construct.
         * @tparam Args The constructor argument types.
         * @param args The constructor arguments.
         */
        template <typename T, typename ... Args>
        void Construct( Type<T>, Args && ... args ) noexcept
        {
            Destruct();
            //
            new ( &this->m_Storage[0] ) T( std::forward<Args>( args ) ... );
            //
            m_Destroy = &RunDestroy<T>;
        }
        StackObject() = default;
        /**
         * @brief Construct a new Stack Object object
         * 
         * @tparam T The type of the object to construct.
         * @tparam Args The type of the arguments to construct the object with.
         * @param args The arguments.
         */
        template <typename T, typename ... Args>
        StackObject( Type<T>, Args && ... args ) noexcept :
            RunOnConstruct
            {
                [this, &args...] () noexcept
                {
                    new ( &this->m_Storage[0] ) T( std::forward<Args>( args ) ... );
                }
            },
            m_Destroy{ &RunDestroy<T> }
        {}
        /**
         * 
         */
        StackObject(StackObject &&) = delete;
        StackObject(StackObject const &) = delete;
        StackObject & operator = (StackObject &&) = delete;
        StackObject & operator = (StackObject const &) = delete;
        /**
         * @brief Get a pointer to the underlying object.
         * 
         * @return const void* The pointer to the object.
         */
        const void * Ptr() const noexcept { return static_cast<const void *>( &m_Storage[0] ); }
        /**
         * @brief Run the destructor of the stored object (if there is a stored object)
         */
        void Destruct() noexcept
        {
            if (auto destroy = m_Destroy; destroy) 
            {
                m_Destroy = nullptr;
                destroy( Ptr() );
            }
        }
        /**
         * @brief Destroy the Stack Object object
         */
        ~StackObject(){ Destruct(); }
    };
}