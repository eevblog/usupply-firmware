#pragma once

namespace General
{
	/**
	 * @brief A wrapper for a type so that it can be used as a singleton without having a global and
	 * numerous extern X calls.
	 * 
	 * @tparam T The type of the value.
	 * @tparam Key Used to uniquely key the singleton (unique per type and key).
	 */
	template <typename T, typename Key = void>
	class Singleton
	{
	private:
		inline static T m_Value = {};
	public:
		using type  = T;
		/**
		 * @brief Construct a new Singleton object
		 */
		Singleton() = default;
		/**
		 * @brief Returns the singleton value.
		 * 
		 * @return T& The value stored. 
		 */
		T & Value() { return m_Value; }
		/**
		 * @brief Returns the singleton value.
		 * 
		 * @return T& The value stored. 
		 */
		T const & Value() const noexcept { return m_Value; }
		/**
		 * @brief Implict cast to the underlying type.
		 * 
		 * @return T const &
		 */
		operator T const & () const noexcept 
		{
			return m_Value;
		}
		/**
		 * @brief Assigns to the value.
		 * 
		 * @param input The value to assign.
		 * @return Singleton& For chained operations.
		 */
		Singleton& operator=( T const& input ) noexcept 
		{
			m_Value = input;
			return *this;
		}
		/**
		 * @brief Performs an or operation on the value.
		 * 
		 * @param input The value to or with.
		 * @return Singleton& For chained operations.
		 */
		Singleton& operator|=( T const& input ) noexcept 
		{
			m_Value |= input;
			return *this;
		}
		/**
		 * @brief Performs an and operation on the value.
		 * 
		 * @param input The value to and with.
		 * @return Singleton& For chained operations.
		 */
		Singleton& operator&=( T const& input ) noexcept 
		{
			m_Value &= input;
			return *this;
		}
	};
}