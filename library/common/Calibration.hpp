#pragma once

#include "Line.hpp"
#include "Math.hpp"
#include "FILO.hpp"
#include "FIFO.hpp"
#include <array>
#include <algorithm>
#include <type_traits>

namespace General
{
	/**
	 * @brief Stores the pair of values that represent a calibration point.
	 * 
	 * These are the expected value and the measured value.
	 */
	struct Sample
	{
		float Expected = 0;
		float Measured = 0;
		/**
		 * @brief Construct a new Sample object
		 */
		Sample() = default;
		/**
		 * @brief Copy assignment is typical.
		 * 
		 * @return Sample& For chained operations.
		 */
		Sample & operator = (Sample const &) = default;
		/**
		 * @brief Construct a new Sample object
		 * 
		 * @param expected The expected value (in a perfect world).
		 * @param measured The actual measured value (in the real world).
		 */
		Sample(float expected, float measured) noexcept:
			Expected(expected),
			Measured(measured)
		{}
		/**
		 * @brief Less than operator
		 * 
		 * Performs the operation on the Expected value.
		 * This is for sortability of tests.
		 * 
		 * @param rhs The right hand side of the operation.
		 * @return bool Expected < rhs.Expected
		 */
		bool operator < (Sample const & rhs) const noexcept
		{
			return Expected < rhs.Expected;
		}
		/**
		 * @brief Greater than operator
		 * 
		 * Performs the operation on the Expected value.
		 * This is for sortability of tests.
		 * 
		 * @param rhs The right hand side of the operation.
		 * @return bool Expected > rhs.Expected
		 */
		bool operator > (Sample const & rhs) const noexcept
		{
			return Expected > rhs.Expected;
		}
		/**
		 * @brief Less than or equal operator.
		 * 
		 * Performs the operation on the Expected value.
		 * This is for sortability of tests.
		 * 
		 * @param rhs The right hand side of the operation.
		 * @return bool Expected <= rhs.Expected
		 */
		bool operator <= (Sample const & rhs) const noexcept
		{
			return Expected <= rhs.Expected;
		}
		/**
		 * @brief Greater than or equal operator.
		 * 
		 * Performs the operation on the Expected value.
		 * This is for sortability of tests.
		 * 
		 * @param rhs The right hand side of the operation.
		 * @return bool Expected >= rhs.Expected
		 */
		bool operator >= (Sample const & rhs) const noexcept
		{
			return Expected >= rhs.Expected;
		}
		/**
		 * @brief The equals operator.
		 * 
		 * Performs the operation on the Expected value.
		 * This is for sortability of tests.
		 * 
		 * @param rhs The right hand side of the operation.
		 * @return bool Expected == rhs.Expected
		 */
		bool operator == (Sample const & rhs) const noexcept
		{
			return Expected == rhs.Expected;
		}
		/**
		 * @brief Converts the calibration point to a string.
		 * 
		 * This function inserts the string result into the parameter output.
		 * The first character output starts at the index parameter and continues from there.
		 * 
		 * @tparam N The capacity of the array to print into.
		 * @param output The output of the string conversion function.
		 * @param index The index to start outputting from.
		 * @return std::size_t The number of items that were inserted into the string.
		 */
		template <std::size_t N>
		std::size_t ToString( std::array<char, N> &output, std::size_t index = 0 ) noexcept
		{
			auto const i = index;
			index += General::ftoa(Expected, output, index);
			index += General::atoa(General::MakeArray(" "), output, index);
			index += General::ftoa(Measured, output, index);
			return index - i;
		}
		/**
		 * @brief Converts the calibration point to a string.
		 * 
		 * This inserts the string output into a FIFO.
		 * 
		 * @tparam N The capacity of the FIFO to print into.
		 * @param output The output of the string conversion function.
		 * @return std::size_t The number of items that were inserted into the string.
		 */
		template <std::size_t N>
		std::size_t ToString( Containers::FIFO<char, N> & output ) noexcept
		{
			auto l = 0;
			l += General::ftoa(Expected, output);
			l += General::ctoa(',', output);
			l += General::ftoa(Measured, output);
			return l;
		}
	};
	/**
	 * @brief Tests whether a type is a sample class.
	 * 
	 * @tparam T The type to test.
	 */
	template <typename T>
	constexpr bool is_sample_v = std::is_same_v<std::decay_t<T>, Sample>;
	/**
	 * @brief This stores the current calibration points for a particular parameter (voltage perhaps)
	 * 
	 * @tparam Points The maximum number of calibration points.
	 */
	template <std::size_t Points>
	class Calibration
	{
	private:
		static_assert( Points > 1u, "Does not support single point calibration." );
		Containers::FILO<Sample, Points> m_Samples;
	public:
		/**
		 * @brief The default calibraiton constructor.
		 * 
		 * This always constructs an empty calibration object. 
		 * This hopefully prevents people typing to map this class into flash, it must be in ram.
		 */
		constexpr Calibration() = default;
		/**
		 * @brief Clears the samples from the calibration class. 
		 */
		void Clear() noexcept
		{
			m_Samples.Clear();
		}
		/**
		 * @brief Adds a sample to the FILO of samples.
		 * 
		 * @param input The sample to push into the FIFO.
		 * @return true The push was successful.
		 * @return false The push didn't complete, its likely the system is at capacity for the samples.
		 */
		bool Push( Sample const & input ) noexcept
		{
			bool out = m_Samples.Push(input);
			if (out) std::sort(m_Samples.begin(), m_Samples.end());
			return out;
		}
		/**
		 * @brief Get the number of calibration points in the class.
		 * 
		 * @return std::size_t The number points in the class.
		 */
		std::size_t Count() const noexcept
		{
			return m_Samples.Size();
		}
		/**
		 * @brief Tests whether the class has any calibration points.
		 * 
		 * @return true There are calibration points.
		 * @return false There are no calibration points.
		 */
		bool Empty() const noexcept
		{
			return m_Samples.Empty();
		}
		/**
		 * @brief Returns an iterator to the begining of the class
		 * 
		 * There are no mutable iterators in this class, it is prohibited because
		 * it would allow the calibration points to exist in a non-sorted state.
		 * 
		 * @return decltype(auto) The iterator.
		 */
		decltype(auto) begin() const noexcept
		{
			return m_Samples.cbegin();
		}
		/**
		 * @brief Returns an iterator to the end of the calibration points.
		 * 
		 * @return decltype(auto) The iterator.
		 */
		decltype(auto) end() const noexcept
		{
			return m_Samples.cend();
		}
		/**
		 * @brief Given the calibration points avaliable corrects for error in the system.
		 * 
		 * @param value The point to correct.
		 * @return float The error corrected value. 
		 */
		constexpr float operator()( float value ) const
		{
			//Only runs when over 2 samples are present.
			// 1 < 1 is false.
			for ( auto i = 1u; i < m_Samples.Size(); ++i )
			{
				auto const& s1 = m_Samples[ i - 1 ];
				auto const& s2 = m_Samples[ i ];

				if ( Between( value, s1.Expected, s2.Expected ) )
				{
					auto const expected = Line( s1.Expected, s1.Measured, s2.Expected, s2.Measured )( value );
					auto const error    = value - expected;
					return error + value;
				}
			}
			return value;
		}
	};
}