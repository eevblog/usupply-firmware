#pragma once

#include <type_traits>
#include <utility>
#include <array>

namespace General
{
	/**
	 * @brief Store a function in static storage.
	 * 
	 * @tparam T The type of the function.
	 * @tparam Key The tag Key unique construction. 
	 */
	template <typename T, typename Key = void>
	class StaticLambdaWrapper
	{
	public:
		using array_t = std::uint8_t[sizeof(std::decay_t<T>)];
	private:
		inline static array_t m_Data;
		inline static T * volatile m_DataPtr = nullptr;
		/**
		 * @brief Get a reference to the underlying function storage.
		 * 
		 * @return T& 
		 */
		static T & Function() noexcept
		{
			return *m_DataPtr;
		}
		/**
		 * @brief Run the destructor on the underlying stored object.
		 * 
		 */
		static void Destruct() noexcept
		{
			if ( m_DataPtr )
			{
				( (T*)m_Data )->~T();
				m_DataPtr = nullptr;
			}
		}
		/**
		 * @brief Construct a function object via a move.
		 * 
		 * @param function 
		 */
		static void Construct( T && function ) noexcept
		{
			Destruct();
			new( m_Data ) T{ std::move( function ) };
			m_DataPtr = (T *)&m_Data[0];
		}
		/**
		 * 
		 */
	public:
		/**
		 * @brief Deconstruct an empty lambda object.
		 */
		StaticLambdaWrapper() = default;
		/**
		 * @brief Construct a new Static Lambda Wrapper object
		 * 
		 * @param input The function object to move into the underlying storage.
		 */
		StaticLambdaWrapper( T && input ) noexcept
		{
			Construct( std::forward<T>( input ) );
		}
		/**
		 * @brief Move assign the function into the underlying storage.
		 * 
		 * @param function The function to move assign.
		 * @return StaticLambdaWrapper& Chain operation support.
		 */
		StaticLambdaWrapper & operator = ( T && function ) const noexcept
		{
			Construct( std::forward<T>( function ) );
			return *this;
		}
		/**
		 * @brief Run the underlying function with the specified arguments.
		 * 
		 * @tparam Args The argument types.
		 * @param args The arguments
		 * @return decltype(auto) The result of the function.
		 */
		template < typename ... Args >
		static auto Run( Args && ... args ) noexcept -> decltype( Function()( std::forward<Args>( args ) ... ) )
		{
			if ( m_DataPtr )
			{
				return Function()( std::forward<Args>( args ) ... );
			}
			else
			{
				if constexpr (std::is_void_v<decltype( Function()( std::forward<Args>( args ) ... ) )>)
					return;
				else
				{
					return {};
				}
			}
		}
		/**
		 * @brief Run the underlying function with the specified arguments.
		 * 
		 * @tparam Args The argument types.
		 * @param args The arguments
		 * @return decltype(auto) The result of the function.
		 */
		template < typename ... Args >
		decltype(auto) operator()( Args && ... args ) const noexcept
		{
			return Run( std::forward<Args>( args ) ... );
		}
	};
	//
	//
	template <typename T>
	StaticLambdaWrapper(T &&)->StaticLambdaWrapper<std::decay_t<T>>;
}