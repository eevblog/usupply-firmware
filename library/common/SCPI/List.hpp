#pragma once

#include "ArgAtIndex.hpp"
#include "Required.hpp"
#include "Chain.hpp"
#include "XOrMore.hpp"
#include <tuple>
#include <type_traits>

namespace Parser
{
	/**
	 * @brief Creates a pasable spaces comma.
	 * 
	 * Examples of a spaced comma are:
	 * - ", "
	 * - " ,"
	 * - " , "
	 * - ","
	 * 
	 * @return auto The type of the parser.
	 */
	constexpr auto SpacedComma() noexcept
	{
		return Chain
		(
			ZeroOrMore(" "),
			Required(","),
			ZeroOrMore(" ")
		);
	}
	/**
	 * @brief A metaclass that helps construct a comma sepeted list.
	 * 
	 * @tparam N The number of items in the list.
	 */
	template <std::size_t N>
	struct ListHelper
	{
		static constexpr auto items		= N;
		static constexpr auto commas	= items - 1u;
		static constexpr auto total		= items + commas;
		static_assert(items >= 1u, "Lists need at least one item.");
		/**
		 * @brief Constructs the appropate type of a given index, this could be one of the arugments or it could be a spaced comma.
		 * 
		 * @tparam I The index.
		 * @tparam Args The types of the items in the list.
		 * @param args The items in the list.
		 * @return auto The item at the requested index.
		 */
		template <std::size_t I, typename ... Args>
		static constexpr auto AtListIndex( Args && ... args ) noexcept
		{
			//	
			//	if		Odd items are commas
			//	else	Even items are arguments
			//	
			if constexpr (I % 2u)
				return SpacedComma();
			else
				return General::ArgAtIndex<I / 2u>(FWD(args) ... );
		}
		/**
		 * @brief Creates a comma seperated list given a set of items in the list and a sequence of integers.
		 * 
		 * @warning The integer sequence MUST be the total number of arguments + the total number of commas between the arguments.
		 * 
		 * @tparam I The indexes to process.
		 * @tparam Args The types of the items in the list.
		 * @param args The items in the list.
		 * @return auto The resulting parser type. 
		 */
		template <std::size_t ... I, typename ... Args>
		constexpr static auto Create( std::index_sequence<I...>, Args && ... args) noexcept
		{
			return Chain( FWD( AtListIndex<I>( FWD(args)... ) ) ... );
		}
		/**
		 * @brief Creates a comma seperated list given a set of items in the list and a sequence of integers.
		 * 
		 * @tparam Args The types of the items in the list.
		 * @param args The items in the list.
		 * @return auto The resulting parser type. 
		 */
		template <typename ... Args>
		constexpr static auto Create(Args && ... args) noexcept
		{
			return ListHelper::template Create( std::make_index_sequence<total>{ }, FWD(args) ... );
		}
	};
	/**
	 * @brief Creates a comma seperated list given a set of items in the list and a sequence of integers.
	 * 
	 * @tparam Args The types of the items in the list.
	 * @param args The items in the list.
	 * @return auto The resulting parser type. 
	 */
	template <typename ... Args>
	constexpr auto List(Args && ... args) noexcept
	{
		return ListHelper<sizeof...(Args)>::Create( FWD(args) ... );
	}
}