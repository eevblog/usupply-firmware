#pragma once

#include "States.hpp"
#include "ParseResult.hpp"

namespace Parser
{
	/**
	 * @brief A parser for sections that have a common start.
	 * 
	 * An example:
	 * 
	 * :POWER:SET
	 * :POWER:GET
	 * :POWER:CLEAR
	 * :POWER:PROCESS
	 * 
	 * All have :POWER: at the begining, Required(":POWER:") would be a suitable type for Key
	 * in this case.
	 * 
	 * The states in this case would be of the type:
	 * 
	 * States
	 * (
	 *    Required("SET"),
	 *    Required("GET"),
	 *    Required("CLEAR"),
	 *    Required("PROCESS")
	 * )
	 * 
	 * @tparam Key The type of the key.
	 * @tparam States The type of the states.
	 */
	template <typename Key, typename States>
	class Node
	{
	private:
		Key const m_Key;
		States const m_States;
	public:
		/**
		 * @brief Construct a new Node object.
		 * 
		 * @tparam A The type of the key.
		 * @tparam S The type of the states.
		 */
		template <typename A, typename S>
		constexpr Node(A && key, S && states) noexcept: 
			m_Key	{ FWD( key ) },
			m_States{ FWD( states ) }
		{}
		/**
		 * @brief Parse the node and return whether or not parse was successful.
		 * 
		 * @tparam N The length of the input array.
		 * @param input The input array.
		 * @param index The index to start processing.
		 * @param offset The offset from the index to start processing.
		 * @return ParseResult The result, see ParseResult docs.
		 */
		template <std::size_t N>
		constexpr ParseResult operator()( std::array<char, N> const & input, std::size_t index = 0u, std::size_t offset = 0u ) const noexcept
		{
			if (auto k{ m_Key(input, index, offset) }; k)
				if (auto s{ m_States(input, index, offset + k.Length()) }; s)
					return ParseResult(k.Length() + s.Length());

			return ParseResult(false);
		}

		using result_t = ParseResult;
	};
	/**
	 * @brief A deduction guide for the Node class.
	 * 
	 * @tparam A The type of the key.
	 * @tparam S The type of the states.
	 */
	template <typename A, typename S>
	Node(A, S)->Node<A, S>;
}