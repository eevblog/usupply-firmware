#pragma once

#include "ParseResult.hpp"
#include "Char.hpp"
#include <array>
#include <type_traits>

namespace Parser
{
	/**
	 * @brief A storage of the basic block.
	 * @warning This function automatically drops null termination character.
	 * @warning Do not rely on null termination.
	 * @tparam L The number of characters in the underlying array.
	 */
	template <std::size_t L>
	class BasicBlock
	{
	public:
		using buffer_t = std::array<char, L>;
	protected:
		static_assert(L > 0, "Cannot parse a non-existant string (length must be greater than 0).");
		buffer_t const m_Data;
	public:
		/**
		 * @brief Construct a new Basic Block object
		 * 
		 * @param input Construct from an array.
		 */
		constexpr BasicBlock( buffer_t input ) noexcept :
			m_Data( input ) 
		{}
		/**
		 * @brief Construct a new Basic Block object
		 * 
		 * @tparam N The length of the input.
		 */
		template<std::size_t N>
		constexpr BasicBlock( char const (&input)[N] ) noexcept : 
			m_Data{ General::MakeArray(input) } 
		{}
		/**
		 * @brief Construct a new Basic Block object
		 * 
		 * @param input Copy construct the basic blocks data.
		 */
		constexpr BasicBlock( BasicBlock const & input ) noexcept : 
			m_Data{ input.m_Data } 
		{}
		/**
		 * @brief Copy and move don't make sense, this would be a performance issue.
		 * 
		 * @note This is a deleted function.
		 * @return constexpr BasicBlock& 
		 */
		constexpr BasicBlock & operator=(BasicBlock &&) = delete;
		/**
		 * @brief Copy and move don't make sense, this would be a performance issue.
		 * 
		 * @note This is a deleted function.
		 * @return constexpr BasicBlock& 
		 */
		constexpr BasicBlock & operator=(BasicBlock const &) = delete;
		/**
		 * @brief Return a reference to to the underlying array.
		 * 
		 * @return buffer_t const& The underlying array.
		 */
		buffer_t const & Data() const noexcept
		{
			return m_Data;
		}
		/**
		 * @brief The type that should be returned by the Parse function.
		 */
		using result_t = ParseResult;
	};
	/**
	 * @brief Deduction guide from string literal ie. "hello, world"
	 * 
	 * This drops the null character.
	 * 
	 * @tparam N The number of characters
	 */
	template < std::size_t N >
	BasicBlock( const char(&)[N] ) -> BasicBlock< N - 1u >;

	template < std::size_t N >
	BasicBlock( std::array<char, N> ) -> BasicBlock< N >;
}