#pragma once
#include "ParseResult.hpp"
#include "AtIndex.hpp"
#include <type_traits>
#include <tuple>

namespace Parser
{
	/**
	 * @brief Extracts a list of return values(ignoring void returns) from a series of functions.
	 */
	template <typename list, typename, typename>
	struct ReturnIndexes;
	/**
	 * @brief Extracts a list of return values(ignoring void returns) from a series of functions.
	 * 
	 * @tparam list_t The list of return types.
	 * @tparam Returns The indexes of the return values (skipping voids)
	 */
	template <typename list_t, std::size_t ... Returns>
	struct ReturnIndexes < list_t, std::index_sequence<Returns...>, std::index_sequence<> >
	{
		using type = std::index_sequence<Returns...>;
	};
	/**
	 * @brief Extracts a list of return values(ignoring void returns) from a series of functions.
	 * 
	 * @tparam list_t The list of return types.
	 * @tparam Returns The indexes of the return values (skipping voids)
	 * @tparam Head The first return index.
	 * @tparam Rest The rest of the return indexes.
	 */
	template <typename list_t, std::size_t ... Returns, std::size_t Head, std::size_t ... Rest>
	struct ReturnIndexes < list_t, std::index_sequence<Returns...>, std::index_sequence<Head, Rest...> >
	{
		static constexpr auto has_return = IsValueParseResult_v<typename General::AtIndex<Head, list_t>::type>;

		using rest_t = std::index_sequence<Rest...>;
		using type = std::conditional_t
		<
			has_return, 
			typename ReturnIndexes<list_t, std::index_sequence<Returns..., Head>,	rest_t>::type, 
			typename ReturnIndexes<list_t, std::index_sequence<Returns...>,			rest_t>::type
		>;
	};
	/**
	 * @brief Chains a series of parsable blocks, this is useful for parsing parameter lists.
	 * 
	 * @tparam Args The various parsable blocks.
	 */
	template <typename ... Args>
	class Chain : std::tuple<Args...>
	{
	public:
		/**
		 * @brief The number of parsable components in the chain.
		 */
		static constexpr std::size_t count = sizeof...(Args);
		/**
		 * @brief Extracts items from the tuple specified by the index sequence.
		 * 
		 * @tparam list_t The types in the tuple.
		 * @tparam seq_t The sequence of indexes.
		 */
		template <typename list_t, typename seq_t>
		struct SliceTuple;
		/**
		 * @brief Extracts items from the tuple specified by the index sequence.
		 * 
		 * @tparam list_t The return values of the chain.
		 * @tparam I The indexes for each item in the tuple.
		 */
		template <typename list_t, std::size_t ... I>
		struct SliceTuple<list_t, std::index_sequence<I...>>
		{
			using type = std::tuple < typename General::AtIndex<I, list_t>::type::Result_t ... >;
		};
		/**
		 * @brief The type of the tuple in the base.
		 */
		using base_t			= std::tuple<Args...>;
		/**
		 * @brief A list of all the return types of the parsable components in the class (indexes in the tuple).
		 * 
		 * @note This includes the void return values.
		 */
		using returns_t			= General::TypeList<typename std::decay_t<Args>::result_t...>;
		/**
		 * @brief A list of all the non-void return indexes (indexes in the tuple).
		 * 
		 * @note This does not include the void value indexes.
		 */
		using return_indexes_t	= typename ReturnIndexes < returns_t, std::index_sequence<>, decltype(std::make_index_sequence<count>{}) >::type;
		/**
		 * @brief A list of all the non-void return types.
		 * 
		 * @note This does not include void returns.
		 */
		using return_tuple_t	= typename SliceTuple < returns_t, decltype(return_indexes_t{}) >::type;
		/**
		 * @brief Tests whether the index exists in the return indexes sequence (the items that have returned values in their ParseResult)
		 */
		template <std::size_t, std::size_t, typename>
		struct TestIndex;
		/**
		 * @brief This specialisation indicates there was no match for the index.
		 * 
		 * @tparam Match The index to find.
		 * @tparam Current The index that is currently checking.
		 */
		template <std::size_t Match, std::size_t Current>
		struct TestIndex<Match, Current, std::index_sequence<>>
		{
			static bool constexpr IsMatch			= false;
			static bool constexpr Exists			= false;
			static std::size_t constexpr MatchIndex	= 0;
		};
		/**
		 * @brief This specialisation tests the head index against the match index.
		 * 
		 * @tparam Match The match index.
		 * @tparam Current The currently scanning index.
		 * @tparam Head The item to check against.
		 */
		template <std::size_t Match, std::size_t Current, std::size_t Head>
		struct TestIndex<Match, Current, std::index_sequence<Head>>
		{
			static bool constexpr IsMatch			= (Match == Head);
			static bool constexpr Exists			= (IsMatch);
			static std::size_t constexpr MatchIndex	= (IsMatch) ? Current	: TestIndex<Match, Current + 1, std::index_sequence<>>::MatchIndex;
		};
		/**
		 * @brief This specialisation tests the head index against the match index.
		 * 
		 * @tparam Match The match index.
		 * @tparam Current The currently scanning index.
		 * @tparam Head The item to check against.
		 * @tparam Tail The remaining items that may need to be checked.
		 */
		template <std::size_t Match, std::size_t Current, std::size_t Head, std::size_t ... Tail>
		struct TestIndex<Match, Current, std::index_sequence<Head, Tail...>>
		{
			static bool constexpr IsMatch			= (Match == Head);
			static bool constexpr Exists			= (IsMatch) ? true		: TestIndex<Match, Current + 1, std::index_sequence<Tail...>>::Exists;
			static std::size_t constexpr MatchIndex	= (IsMatch) ? Current	: TestIndex<Match, Current + 1, std::index_sequence<Tail...>>::MatchIndex;
		};
		/**
		 * @brief A metaprogramming tool to get the TestIndex at the requested index.
		 * @note See docs for TestIndex.
		 * @tparam I The requested index.
		 */
		template <std::size_t I>
		using TestIndex_t = TestIndex<I, 0, return_indexes_t>;
		/**
		 * @brief Returns the item in the base tuple at the requested index.
		 * 
		 * @tparam I The index in the tuple.
		 * @return auto const & The item returned.
		 */
		template <std::size_t I>
		constexpr auto const & Base() const noexcept
		{
			return std::get<I>((base_t&)(*this));
		}
		/**
		 * @brief The number of elements in the return tuple.
		 * @note This is also a count of the non-void return values.
		 */
		static auto constexpr TupleElements = std::tuple_size_v<return_tuple_t>;
		/**
		 * @brief Runs each item in the chain in sequence.
		 * 
		 * @tparam I The indexes of the items to run.
		 * @tparam N The capacity of the input array.
		 * @param input The input array.
		 * @param index The starting index in the input array.
		 * @param offset The offset from the index to start processing.
		 * @return return_tuple_t Only if there are return values from the items in the chain.
		 * @return ParseResult If there are zero return values from the items in the chain.
		 */
		template <std::size_t ... I, std::size_t N>
		constexpr auto Run(std::index_sequence<I...>, std::array<char, N> const & input, std::size_t index, std::size_t offset = 0u ) const noexcept
		{
			return_tuple_t buffer{};
			std::size_t working_offset = 0;
			std::size_t remaining	= count;
			
			[&]
			{
				return (([&](auto & base, auto type) -> bool
				{
					if ( auto result = base(input, index, working_offset + offset) )
					{
						if constexpr (type.Exists) std::get<type.MatchIndex>(buffer) = result.Value();
						working_offset += result.Length();
						--remaining;
						return true;
					}
					return false;
				}(Base<I>(), TestIndex_t<I>{}))
				and 
				...);
			}();
			if constexpr (TupleElements > 0)
			{
				 return (remaining == 0) ?
				 	ValueParseResult<return_tuple_t>( buffer, working_offset ) :
					ValueParseResult<return_tuple_t>();
			}
			else
			{
				return (remaining == 0) ? 
					ParseResult( working_offset ) :
					ParseResult( false );
			}
		}
	public:
		/**
		 * @brief Constructs the Chain by passing in the components in the chain.
		 * 
		 * @param args The components in the chain.
		 */
		constexpr Chain( Args... args ) noexcept :
			base_t{ args ... }
		{}
		/**
		 * @brief 
		 * 
		 * @tparam N 
		 * @param input 
		 * @param index 
		 * @param offset 
		 * @return constexpr auto 
		 */
		template <std::size_t N>
		constexpr auto operator () ( std::array<char, N> const & input, std::size_t index = 0u, std::size_t offset = 0u ) const noexcept
		{
			return Run(std::make_index_sequence< count >{}, input, index, offset );
		}
		/**
		 * @brief The resulting tuple from the parse operation.
		 */
		using result_t = std::conditional_t<(TupleElements > 0u), ValueParseResult<return_tuple_t>, ParseResult>;
	};
	/**
	 * @brief Template type class deduction.
	 * 
	 * @tparam Args The different parsable componetns in the chain.
	 */
	template <typename ... Args>
	Chain(Args...)->Chain<Args...>;
}