#pragma once

#include "BasicBlock.hpp"
#include "Required.hpp"
#include <array>

namespace Parser
{
	/**
	 * @brief An optional section is one that can exist or not, but if it exists it must exist in its entirity.
	 * 
	 * @tparam L The length of the optional section.
	 */
	template <std::size_t L>
	struct Optional : public Required<L>
	{
		using base_t = Required<L>;
		using base_t::base_t;
		using result_t = ParseResult;
		/**
		 * @brief Parse the optional section.
		 * 
		 * @tparam N The length of the input array.
		 * @param input The input array.
		 * @param index The index to start processing.
		 * @param offset The offset from the start to process.
		 * @return ParseResult The result:
		 * If the section is found in its entirity then the length of the section is returned.
		 * If the section is not found, the parser returns true but with a length of zero.
		 */
		template <std::size_t N>
		inline constexpr ParseResult operator()( std::array<char, N> const & input, std::size_t index = 0u, std::size_t offset = 0u ) const noexcept
		{
			return { base_t::operator()(input, index, offset).Length() };
		}
	};
	/**
	 * @brief A deduction guide that allows construction from a C string.
	 * @warning The null terminaor will be stripped from the string, relying on the null terminator
	 *  is undefined behaviour for this library.
	 * @tparam N The length of the C string.
	 */
	template < std::size_t N >
	Optional(const char(&)[N])->Optional<N - 1u>;
	/**
	 * @brief Check if a type is an Optional<> type.
	 * 
	 * @tparam T The type to check.
	 */
	template <typename T>
	constexpr bool IsOptional_v = false;
	template <unsigned L>
	constexpr bool IsOptional_v<Optional<L>> = true;
}