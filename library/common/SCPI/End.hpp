#pragma once
#include "Macros.hpp"
#include "Parser.hpp"
#include <variant>

namespace Parser::SCPI
{
	/**
	 * @brief Check whether a type is a variant.
	 * 
	 * @tparam T The type to check.
	 */
	template <typename T>
	constexpr bool IsVarient_v = false;
	template <typename ... T>
	constexpr bool IsVarient_v<std::variant<T...>> = true;
	/**
	 * @brief Check whether a SCPI command has a terminal character at the end.
	 * 
	 * @tparam Predicate The condition to check.
	 * @tparam Callback The callback to call if this is the end.
	 */
	template <typename Predicate, typename Callback>
	class End
	{
	private:
		Predicate const m_Predicate;	//Prediacte must return something that is implicitly convertable to bool
		Callback  const m_Callback;		//Callback must take the return of predicate as its only argument

	public:
		/**
		 * @brief Construct a new End class.
		 * 
		 * @tparam P The predicate, this is checked. 
		 * @tparam C The callback, this is run when the end is encountered at the correct position.
		 */
		template <typename P, typename C>
		constexpr End( P && predicate, C && callback ) noexcept : 
			m_Predicate	{ FWD(predicate) },
			m_Callback	{ FWD(callback) }
		{}
		/**
		 * @brief Default copy constructor.
		 */
		constexpr End(End const &) noexcept = default;
		/**
		 * @brief Run the parser and check if the string is terminated correctly.
		 * 
		 * @tparam N The length of the input array.
		 * @param input The input array.
		 * @param index The index to start checking.
		 * @param offset The offset from the start to process.
		 * @return constexpr ParseResult See ParseResult docs.
		 */
		template <std::size_t N> 
		constexpr ParseResult operator () ( std::array<char, N> const & input, std::size_t index = 0u, std::size_t offset = 0u ) const noexcept
		{
			auto s{ m_Predicate(input, index, offset) };

			auto const process = [&](auto && v) -> ParseResult
			{
				if (v)
				{
					index += offset + v.Length();

					if (index == N)
					{
						m_Callback(FWD(v));
						return { v.Length() };
					}
					else if (index < N)
					{
						if ( General::IsOneOf( input[index], '!', ';', '\n', '\r' ) )
						{
							m_Callback(v);
							return { v.Length() + 1 };
						}
					}
				}
				return {};
			};

			if constexpr (IsVarient_v<std::decay_t<decltype(s)>>)
			{
				return std::visit([&](auto && v) { return process(FWD(v)); }, s);
			}
			else
			{
				return process(FWD(s));
			}
		}
	};
	/**
	 * @brief A deduction guide for the end class.
	 * 
	 * @tparam P The type of the predicate.
	 * @tparam C The type of the callback.
	 */
	template <typename P, typename C>
	End(P, C)->End<P, C>;
}