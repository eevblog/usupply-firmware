#pragma once

#include "Macros.hpp"
#include "Parser.hpp"
#include "Keyword.hpp"
#include "End.hpp"
#include <array>
#include <variant>

namespace Parser::SCPI
{
	/**
	 * @brief Using this indicates that a SCPI command does not support a query or event.
	 * 
	 * @tparam I, for consistancy with the Blank class.
	 */
	template <std::size_t I = 0>
	struct None 
	{
		using result_t = None; 
		constexpr None() = default;
	};
	/**
	 * @brief Using this indicates that a SCPI command does support a query or event but with no parameters.
	 * 
	 * Indicates that there is support for a query or event with NO parameters.
	 * 
	 * @tparam I Used to uniquely identify which of a query or event is called without parameters. 
	 */
	template <std::size_t I = 0>
	struct Blank : ParseResult
	{
		/**
		 * @brief The ID of the blank field
		 * 
		 */
		static constexpr auto Index = I;
		/**
		 * @brief The result of a match operation.
		 * 
		 */
		using result_t = Blank;
		using ParseResult::ParseResult;
		/**
		 * @brief A default constructor.
		 * 
		 */
		constexpr Blank() = default;
		/**
		 * @brief Return self as the value.
		 * 
		 * @return Blank & Return self. 
		 */
		constexpr Blank & Value() const noexcept
		{
			return *this;
		}
	};
	/**
	 * @brief Test if a type is a Blank type.
	 * 
	 * @tparam T The type to test.
	 */
	template <typename T>
	constexpr auto IsBlank_v = false;
	template <unsigned I>
	constexpr auto IsBlank_v<Blank<I>> = true;
	/**
	 * @brief Test if a type is a IsBlank_v<T> type.
	 * 
	 * @tparam T The result of IsBlank_v<T>.
	 */
	template <typename T>
	auto constexpr IsBlank(T &&) noexcept
	{
		return IsBlank_v<std::decay_t<T>>;
	}
	/**
	 * @brief Test if a type is a None type.
	 * 
	 * @tparam T The type to test.
	 */
	template <typename T>
	constexpr auto IsNone_v = false;
	template <unsigned I>
	constexpr auto IsNone_v<None<I>> = true;
	/**
	 * @brief Test if a type is a IsBlank_v<T> type.
	 * 
	 * @tparam T The result of IsBlank_v<T>.
	 */
	template <typename T>
	auto constexpr IsNone(T &&) noexcept
	{
		return IsNone_v<std::decay_t<T>>;
	}
	/**
	 * @brief Test if a type is a std::tuple type.
	 * 
	 * @tparam T The type to test.
	 */
	template <typename T>
	constexpr bool IsTuple_v = false;
	template<typename ... types>
	constexpr bool IsTuple_v<std::tuple<types...>>   = true;
	//template<typename ... types>
	//constexpr bool IsTuple_v<std::tuple<types...>&&>   = true;
	//template<typename ... types>
	//constexpr bool IsTuple_v<std::tuple<types...>&>   = true;
	//template<typename ... types>
	//constexpr bool IsTuple_v<std::tuple<types...>const && >   = true;
	//template<typename ... types>
	//constexpr bool IsTuple_v<std::tuple<types...>const &>   = true;
	/**
	 * @brief Test if a type is a IsTuple_v<T> type.
	 * 
	 * @tparam T The result of IsTuple_v<T>.
	 */
	template <typename T>
	auto constexpr IsParams(T && c) noexcept
	{
		return IsTuple_v<std::decay_t<T>>;
	}
	/**
	 * @brief Test if a type is a IsParam_v<T> type.
	 * 
	 * @tparam T The result of IsParam_v<T>.
	 */
	template <typename T>
	auto constexpr IsParam(T &&) noexcept
	{
		return IsParam_v<std::decay_t<T>>;
	}

	template <typename Key, typename...>
	class CommandInternal;
	/**
	 * @brief Builds a SCPI command with events and queries suppored based on the query and event parameters.
	 * 
	 * @tparam Key The SCPI strucutre for the underlying command.
	 * @tparam EventParams The support of event parameters for this command:
	 * If None, the command doesn't support events.
	 * If Blank, the command does support evnets, but with no parameters.
	 * If Param, the command supports events with a single paramter.
	 * If Params, the command event supports multiple parameters.
	 * @tparam QueryParams The support of query parameters for this command:
	 * If None, the command doesn't support queries.
	 * If Blank, the command does support queries, but with no parameters.
	 * If Param, the command supports queries with a single paramter.
	 * If Params, the command query supports multiple parameters.
	 */
	template <typename Key, typename EventParams, typename QueryParams>
	class CommandInternal<Key, EventParams, QueryParams> : EventParams, QueryParams
	{
	public:
		static constexpr auto HasEvent = !IsNone_v<EventParams>;
		static constexpr auto HasEventParameters = HasEvent && !IsBlank_v<EventParams>;
		static constexpr auto HasQuery = !IsNone_v<QueryParams>;
		static constexpr auto HasQueryParameters = HasQuery && !IsBlank_v<QueryParams>;
	private:
		/**
		 * @brief The keyword used to trigger the command event or query.
		 * 
		 */
		Key const m_Keyword;
		/**
		 * @brief The event parameter.
		 * 
		 * @return decltype(auto) A reference to the EventParams.
		 */
		decltype(auto) Event() const noexcept
		{
			return static_cast<EventParams const &>(*this);
		}
		/**
		 * @brief The query parameter.
		 * 
		 * @return decltype(auto) A reference to the QueryParams.
		 */
		decltype(auto) Query() const noexcept
		{
			return static_cast<QueryParams const &>(*this);
		}

		//MSVC allows explicit specialitation at class scope, GCC doesn't
		template <typename gcc_workaround, bool has_event, bool has_query>
		struct ReturnType
		{
			using event_t = std::decay_t<typename EventParams::result_t>;
			using query_t = std::decay_t<typename QueryParams::result_t>;

			using varient_t = std::variant<event_t, query_t>;
			static auto constexpr is_same = std::is_same_v<event_t, query_t>;

			using type = std::conditional_t<is_same, event_t, varient_t>;
		};
		template <typename gcc_workaround/*bool has_event, bool has_query*/>
		struct ReturnType<gcc_workaround, false, true>
		{
			using query_t = std::decay_t<typename QueryParams::result_t>;
			using type = query_t;
		};
		template <typename gcc_workaround/*bool has_event, bool has_query*/>
		struct ReturnType<gcc_workaround, true, false>
		{
			using event_t = std::decay_t<typename EventParams::result_t>;
			using type = event_t;
		};
		template <typename gcc_workaround/*bool has_event, bool has_query*/>
		struct ReturnType<gcc_workaround, false, false>
		{
			using type = ParseResult;
		};

		using result_meta = ReturnType<void, HasEvent, HasQuery>;
		using result_t = typename result_meta::type;

	public:
		/**
		 * @brief Construct the Command from a key, an event and a query.
		 * 
		 * @tparam K The type that will be forwarded into the keyword.
		 * @tparam E The type that will be forwared into the EventParams.
		 * @tparam Q The type that will be forwarded into the QueryParams.
		 */
		template <typename K,typename E,typename Q>
		constexpr CommandInternal(K && key, E && event, Q && query) noexcept : 
			EventParams	( FWD( event )	),
			QueryParams	( FWD( query )	),
			m_Keyword	( FWD( key )	)
		{}
		/**
		 * @brief Support copy construction.
		 */
		constexpr CommandInternal(CommandInternal const &) = default;
		/**
		 * @brief Processes the an input array from the index and offset onwards.
		 * 
		 * @tparam Result Stores the value from the operation and the length of the parse operation.
		 * @tparam N The length of the input array.
		 * @tparam T The kernal type to use to process parameters.
		 * @param input The input array.
		 * @param index The start index for processing.
		 * @param s_offset The offset from the start index for processing.
		 * @param kernal The kernal to use to process parameters.
		 * @param offset The offset reference.
		 * @return Result The result of the parse operation and the length of the parse operation.
		 */
		template <typename Result, std::size_t N, typename T>
		Result constexpr ProcessParameters(std::array<char, N> const & input, std::size_t index, std::size_t s_offset, T && kernal, std::size_t & offset) const noexcept
		{
			// Whitespace is required
			if ( auto w{ OneOrMore(" ")(input, index, offset) }; w )
			{
				// Add [w]hitespace length to offset
				offset += w.Length();

				// Process the parameters, these could be anything, float int or other, defined by template parameter
				if ( auto p{ kernal( FWD(input), index, offset ) }; p )
				{
					// Add [p]arameters offset to the offset
					offset += p.Length();

					// Return the value from parameters and the length of the section
					return { std::move(p.Value()), offset - s_offset };
				}
			}
			return {};
		}
		/**
		 * @brief Process the input array and parse a command.
		 * 
		 * @tparam N The length of the input.
		 * @param input The input array.
		 * @param index The start index of the processing. 
		 * @param s_offset The offset from the start index for processing.
		 * @return result_t The result if there is one, which includes the value and the length of the parse operation.
		 */
		template <std::size_t N>
		result_t constexpr operator () (std::array<char, N> const & input, std::size_t index = 0u, std::size_t s_offset = 0u) const noexcept
		{
			// Initial offset
			auto offset = s_offset;

			// Process command keyword
			if (auto k{ m_Keyword(input, index, s_offset) }; k)
			{
				// Add [k]eyword length to offset
				offset += k.Length();
				
				// Question mark is a fast shortcut to stop parsing query, this is processed first
				if constexpr ( HasQuery )
				{
					// Absorb the query character
					if ( auto q = Required{"?"}( input, index, offset ); q )
					{
						// Add [q]uestion mark length to offset (if its zero this does nothing)
						offset += q.Length();

						// Process parameters (these might not exist)
						using type = typename result_meta::query_t;
						if constexpr ( HasQueryParameters )
							return ProcessParameters<type>(input, index, s_offset, Query(), offset);
						else
							return type{ offset - s_offset };
					}
				}
				//
				if constexpr ( HasEvent )
				{
					using type = typename result_meta::event_t;
					//
					if constexpr ( HasEventParameters )
						return ProcessParameters<type>(input, index, s_offset, Event(), offset);
					else
						return type{ offset - s_offset };
				}
			}
			// Constructs it like an optional, blank everything
			return {};
		}
	};
	/**
	 * @brief A template deduction guide for the Command class.
	 * 
	 * @tparam Key The key to use to trigger the command, this MUST be parsed for the command to attempt to process an event or a query.
	 * @tparam Args 
	 */
	template <typename Key, typename ... Args>
	CommandInternal(Key&& v, Args&&...a) -> CommandInternal<General::decay_rvalue_t<decltype(v)>, General::decay_rvalue_t<decltype(a)>...>;
	/**
	 * @brief Create a triggerable SCPI command that runs a callback when an event or query is detected. 
	 * 
	 * Supported param types are:
	 * - None
	 * - Blank
	 * - Param
	 * - Params
	 * 
	 * @warning Commands must be at the end this constructs an End object in combination with CommandInternal
	 * 
	 * @tparam Key The type of the key, this should be a type from the SCPI Parser namespace.
	 * @tparam EventParams The type of the event, this should be one of the supported param types above.
	 * @tparam QueryParams The type of the query, this should be one of the supported param types above.
	 * @tparam Callback The type of the callback.
	 * @param key The key used as the first stage of the command parser, if this doesn't parse the command will not run.
	 * @param event The event.
	 * @param query The query.
	 * @param callback The function that is run when a query or event is detected.
	 * @return auto A parser that can process the constructed SCPI command. 
	 */
	template <typename Key, typename EventParams, typename QueryParams, typename Callback>
	constexpr auto Command(Key && key, EventParams && event, QueryParams && query, Callback && callback)
	{
		return End
		(
			//Process a command regardless if its at the end
			CommandInternal( FWD(key), FWD(event), FWD(query) ),

			//Only call callback if its position is correct, callback takes the value type as its parameters as a rvalue or a copy.
			[ c{ FWD(callback) } ]( auto && result )
			{
				using type = std::decay_t< decltype(result) >;
				if constexpr (IsValueParseResult_v<type>)
					c( FWD(FWD(result)).Value() );
				else
					c( FWD(result) );
			}
		);
	}
}