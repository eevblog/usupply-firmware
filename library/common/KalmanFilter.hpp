#pragma once

#include "Math.hpp"
namespace General
{
	/**
	 * @brief A basic 1D kalman filter used for stabalising noisy ADC measurements.
	 * 
	 * @tparam T The Storage type for the kalman filter.
	 */
    template <typename T>
	class KalmanFilter
	{
	private:
		struct State{ T m_Estimate, m_ErrorEstimate, m_MeasureError; };
		State 		m_State;
		State const m_Reset;
	public:
		/**
		 * @brief Copy the kalman filter based only on the state.
		 */
		constexpr KalmanFilter( KalmanFilter const & input ) noexcept :
			m_State{ input.m_State }
		{}
		/**
		 * @brief Construct the Kalman filter.
		 * 
		 * @param initial_estimate The initial estimate for the kalman filter.
		 * @param initial_error_estimate The initial error estimate for the kalman filter.
		 * @param error_in_measurement The error in measurement for the kalman filter.
		 */
		constexpr KalmanFilter(T const & initial_estimate, T const & initial_error_estimate, T const & error_in_measurement) noexcept :
			m_State		{ initial_estimate, initial_error_estimate, error_in_measurement },
			m_Reset		{ m_State }
		{}
		/**
		 * @brief Resets the estimate for the value in the kalman filter.
		 * 
		 * @param initial_estimate The estiamte to load.
		 */
		void Reset( T const & initial_estimate ) noexcept
		{
			m_State.m_Estimate		= initial_estimate;
			m_State.m_ErrorEstimate	= m_Reset.m_ErrorEstimate;
			m_State.m_MeasureError 	= m_Reset.m_MeasureError;
		}
		/**
		 * @brief A single tick of the 1D kalman filter.
		 * 
		 * @param measure The value to sample.
		 * @return T The filtered value.
		 */
		constexpr T Update( T const & measure ) noexcept
		{
			auto error = measure - m_State.m_Estimate;
			auto gain = m_State.m_ErrorEstimate / ( m_State.m_ErrorEstimate + m_State.m_MeasureError );
			m_State.m_Estimate += gain * error;
			m_State.m_ErrorEstimate *= T{ 1 } - gain;
			return m_State.m_Estimate;
		}
	};
}