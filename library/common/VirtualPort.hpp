#pragma once
#include "ReferenceBit.hpp"
#include "Singleton.hpp"

namespace IO
{
	/**
	 * @brief Used to emulate a port.
	 * 
	 * @tparam T The underlying integer type.
	 * @tparam Key The key used to uniquely define the VirtualPort.
	 */
	template <typename T, typename Key>
	struct VirtualPort
	{
		/**
		 * @brief Construct a new Virtual Port object
		 * 
		 * This class has no members so this does nothing.
		 */
		VirtualPort() = default;
		/**
		 * @brief Used to store the state of the port, ports are always by definition singleton.
		 */
		using Singleton_t = General::Singleton<T, Key>;
		/**
		 * @brief Used to store a single bit from within the singleton.
		 * 
		 * @tparam Bit The index of the bit.
		 */
		template <unsigned Bit>
		using Bit_t = General::ReferenceBit<Singleton_t, Bit>;
		/**
		 * @brief The way the pins are defined in the port, this is how each pin is linked to the 
		 * port as a whole.
		 * 
		 * @tparam Pin The index of the pin.
		 */
		template <unsigned Pin>
		using Pin_t = VirtualPin<Bit_t<Pin>>;
		/**
		 * @brief Returns the singleton of type T (perhaps uint32_t for a 32 pin port)
		 * 
		 * @return T & The singletons stored value. 
		 */
		static T & Value() noexcept
		{
			General::Singleton<T, Key> out;
			return out.Value();
		}
	};
}