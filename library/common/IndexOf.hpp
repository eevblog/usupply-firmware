#pragma once
#include "AtIndex.hpp"
#include <cstdint>
#include <cstddef>
#include <type_traits>

namespace General
{
    /**
     * @brief Get the index of the first instance of a type in a function.
     * 
     * @tparam Match The type to find. 
     * @tparam Types The types to check against.
     * @tparam Indexes The indexes to check.
     * @return std::size_t The index of the type, or if it doesn't exist, numeric limits maximum of std::size_t.
     */
    template <typename Match, typename ... Types, size_t ... Indexes>
    constexpr std::size_t IndexOfImpl( TypeList<Types...>, std::index_sequence<Indexes...> ) noexcept
    {
        size_t index = 0;
        //
        ([&] () -> bool
        {
            if constexpr (std::is_same_v<Match, Types>)
            {
                return true;
            }
            ++index;
            return false;
        }() or ...);
        //
        return index;
    }
    /**
     * @brief An inline variable for thye IndexOfImpl function.
     * 
     * This finds the index of the first instance of a type in the type list.
     * If the item is not found, the result is the numeric limits maximum of size_t.
     */
    template <typename Match, typename Types>
    constexpr size_t IndexOf_v = IndexOfImpl<Match>( Types{}, std::make_index_sequence<Types::count>{} );
}