#pragma once

#include "Convert.hpp"
#include <array>
#include <type_traits>

namespace General
{
	/**
	 * @brief Tests whether a type is a boolean type or a registered boolean type.
	 * 
	 * @tparam T The type to test.
	 */
	template <typename T>
	constexpr bool is_boolean_v = false;
	/**
	 * @brief A macro that produces boolean registered types.
	 * 
	 * The resulting enum is:
	 * - Named TrueFalseValue.
	 * - Has the underlying type of bool.
	 * 
	 * The resulting class is:
	 * - implicitly convertable from and to bool.
	 * - Convertable from a string.
	 * - Convertable to the underlying enum type.
	 */
	#define MAKE_BOOLEAN( TRUE_VAL, FALSE_VAL )                                                                        \
	enum TRUE_VAL##FALSE_VAL##Value : bool																			   \
	{																												   \
	    TRUE_VAL  = true,                                                                                              \
	    FALSE_VAL = false																							   \
	};																												   \
	class TRUE_VAL##FALSE_VAL                                                                                          \
	{                                                                                                                  \
	public:                                                                                                            \
		using enum_t = TRUE_VAL##FALSE_VAL##Value;                                                                     \
		using this_t = TRUE_VAL##FALSE_VAL;                                                                            \
	protected:                                                                                                         \
		enum_t m_Value    = FALSE_VAL;                                                                                 \
																													   \
		static constexpr auto true_length = sizeof(#TRUE_VAL);														   \
		static constexpr auto false_length = sizeof(#FALSE_VAL);													   \
		using true_array  = std::array<char, true_length - 1>;														   \
		using false_array = std::array<char, false_length - 1>;														   \
                                                                                                                       \
	public:                                                                                                            \
		static const true_array  true_str;                                                                             \
		static const false_array false_str;                                                                            \
																													   \
		using buffer_t = std::array<char, ((true_length - 1 > false_length - 1) ? true_length - 1 : false_length - 1)>;\
                                                                                                                       \
	public:                                                                                                            \
		TRUE_VAL##FALSE_VAL()                 = default;                                                               \
		TRUE_VAL##FALSE_VAL( this_t const & ) = default;                                                               \
		TRUE_VAL##FALSE_VAL( this_t && )      = default;                                                               \
                                                                                                                       \
		auto &operator=( this_t const &pInput )                                                                        \
		{                                                                                                              \
			m_Value = pInput.m_Value;                                                                                  \
			return *this;                                                                                              \
		}                                                                                                              \
		auto &operator=( this_t &&pInput )                                                                             \
		{                                                                                                              \
			m_Value = std::forward<this_t>( pInput ).m_Value;                                                          \
			return *this;                                                                                              \
		}                                                                                                              \
                                                                                                                       \
		TRUE_VAL##FALSE_VAL( bool &&value ) noexcept : m_Value( (enum_t)value ) {}                                     \
		TRUE_VAL##FALSE_VAL( bool const &value ) noexcept : m_Value( (enum_t)value ) {}                                \
		TRUE_VAL##FALSE_VAL( enum_t &&value ) noexcept : m_Value( value ) {}                                           \
		TRUE_VAL##FALSE_VAL( enum_t const &value ) noexcept : m_Value( value ) {}                                      \
                                                                                                                       \
		operator enum_t() const noexcept                                                                               \
		{                                                                                                              \
			return m_Value;                                                                                            \
		}                                                                                                              \
		void Clear() noexcept                                                                                          \
		{                                                                                                              \
			m_Value = ( enum_t ) false;                                                                                \
		}                                                                                                              \
		auto &Toggle() noexcept                                                                                        \
		{                                                                                                              \
			m_Value = ( enum_t ) !(bool)m_Value;                                                                       \
			return *this;                                                                                              \
		}                                                                                                              \
		auto &operator=( enum_t const &value ) noexcept                                                                \
		{                                                                                                              \
			m_Value = value;                                                                                           \
			return *this;                                                                                              \
		}                                                                                                              \
		operator bool() const noexcept                                                                                 \
		{                                                                                                              \
			return static_cast<std::underlying_type_t<decltype(m_Value)>>(m_Value);                                    \
		}                                                                                                              \
		auto &operator=( bool value ) noexcept                                                                   	   \
		{                                                                                                              \
			m_Value = (enum_t)value;                                                                                   \
			return *this;                                                                                              \
		}                                                                                                              \
		template <std::size_t N>                                                                                       \
		auto FromString( std::array<char, N> const &output, std::uint32_t index = 0 ) noexcept				   		   \
		{                                                                                                              \
			bool temp = false;																						   \
			auto out = atob( output, temp, true_str, false_str, index );								       		   \
			m_Value = (enum_t)temp;																					   \
			return out;																							       \
		}                                                                                                              \
	};                                                                                                                 \
	const typename TRUE_VAL##FALSE_VAL::true_array  TRUE_VAL##FALSE_VAL::true_str  = General::MakeArray( #TRUE_VAL );  \
	const typename TRUE_VAL##FALSE_VAL::false_array TRUE_VAL##FALSE_VAL::false_str = General::MakeArray( #FALSE_VAL ); \
	template <>                                                                                                        \
	auto constexpr is_boolean_v<TRUE_VAL##FALSE_VAL> = true;
	/**
	 * @brief Creates a new class called YesNo and an enum called YesNoValue
	 */
	MAKE_BOOLEAN( Yes,  No );
	/**
	 * @brief Creates a new class called OnOff and an enum called OnOffValue
	 */
	MAKE_BOOLEAN( On,   Off );
	/**
	 * @brief Creates a new class called TrueFalse and an enum called TrueFalseValue
	 * 
	 * This is different to ordinary bool because it has a member FromString.
	 */
	MAKE_BOOLEAN( True, False );
	
#undef MAKE_BOOLEAN
}