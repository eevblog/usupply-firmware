#pragma once
#include <cstdint>

namespace General
{
	struct Version
	{
		std::uint8_t const 
			Major = 0,
			Minor = 0;
		/**
		 * @brief Construct a new Version object
		 * 
		 * This just initializes everything to zero.
		 */
		constexpr Version() = default;
		/**
		 * @brief Construct a new Version object.
		 * 
		 * @param major Used to indicate a major release version.
		 * @param minor Used to indicate a minor release version.
		 */
		constexpr Version( std::uint8_t major, std::uint8_t minor = 0 ) noexcept :
		    Major{ major },
		    Minor{ minor }
		{}
		/**
		 * @brief Used to compare whether this version is greater than another.
		 * 
		 * @param rhs The other version to compare with.
		 */
		bool operator > ( Version const & version ) noexcept
		{
			return
			{
				Major > version.Major or
				(
					Major == version.Major and 
					Minor > version.Minor
				)
			};
		}
		/**
		 * @brief Used to compare whether this version is less than another.
		 * 
		 * @param rhs The other version to compare with.
		 */
		bool operator < ( Version const & version ) noexcept
		{
			return
			{
				Major < version.Major or
				(
					Major == version.Major and 
					Minor < version.Minor
				)
			};
		}
	};
}