#pragma once
#include <cstdint>

namespace LCD
{
	/**
	 * @brief A list of segment characters where the bits in each character represent the chacter segments.
	 */
	enum class SegmentCharacters : std::uint8_t
	{
		a    = 95,
		b    = 124,
		c    = 88,
		d    = 94,
		e    = 123,
		f    = 113,
		g    = 111,
		h    = 116,
		i    = 4,
		j    = 14,
		k    = 122,
		l    = 6,
		m    = 20,
		n    = 84,
		o    = 92,
		p    = 115,
		q    = 103,
		r    = 80,
		s    = 109,
		t    = 120,
		u    = 28,
		v    = 98,
		w    = 42,
		x    = 100,
		y    = 110,
		z    = 91,
		_0   = 0x3F,
		_1   = 0x6,
		_2   = 0x5B,
		_3   = 0x4F,
		_4   = 0x66,
		_5   = 0x6D,
		_6   = 0x7d,
		_7   = 0x7,
		_8   = 0x7F,
		_9   = 0x6F,
		A    = 0x77,
		B    = 0x7F,
		C    = 0x39,
		D    = 0x3F,
		E    = 0x79,
		F    = 0x71,
		G    = 0x3D,
		H    = 0x76,
		I    = 0x06,
		J    = 0x1E,
		K    = 0x57,
		L    = 0x38,
		M    = 0x76,
		N    = 0x76,
		O    = 0x3F,
		P    = 0x73,
		Q    = 0x3F,
		R    = 0x77,
		S    = 0x6D,
		T    = 0x31,
		U    = 0x3E,
		V    = 0x3E,
		W    = 0x7E,
		X    = 0x76,
		Y    = 0x66,
		Z    = 0x5B,
		Dot  = 0x80,
		None = 0x00
	};
	/**
	 * @brief Converts a character to a SegmentCharacter enum.
	 * 
	 * @param input The value to convert from.
	 * @return SegmentCharacters The result of the conversion.
	 */
	constexpr SegmentCharacters ToSegment( char input ) noexcept
	{
		switch ( input & 0x7F )
		{
		case '0': return SegmentCharacters::_0;
		case '1': return SegmentCharacters::_1;
		case '2': return SegmentCharacters::_2;
		case '3': return SegmentCharacters::_3;
		case '4': return SegmentCharacters::_4;
		case '5': return SegmentCharacters::_5;
		case '6': return SegmentCharacters::_6;
		case '7': return SegmentCharacters::_7;
		case '8': return SegmentCharacters::_8;
		case '9': return SegmentCharacters::_9;
		case 'a': return SegmentCharacters::a;
		case 'b': return SegmentCharacters::b;
		case 'c': return SegmentCharacters::c;
		case 'd': return SegmentCharacters::d;
		case 'e': return SegmentCharacters::e;
		case 'f': return SegmentCharacters::f;
		case 'g': return SegmentCharacters::g;
		case 'h': return SegmentCharacters::h;
		case 'i': return SegmentCharacters::i;
		case 'j': return SegmentCharacters::j;
		case 'k': return SegmentCharacters::k;
		case 'l': return SegmentCharacters::l;
		case 'm': return SegmentCharacters::m;
		case 'n': return SegmentCharacters::n;
		case 'o': return SegmentCharacters::o;
		case 'p': return SegmentCharacters::p;
		case 'q': return SegmentCharacters::q;
		case 'r': return SegmentCharacters::r;
		case 's': return SegmentCharacters::s;
		case 't': return SegmentCharacters::t;
		case 'u': return SegmentCharacters::u;
		case 'v': return SegmentCharacters::v;
		case 'w': return SegmentCharacters::w;
		case 'x': return SegmentCharacters::x;
		case 'y': return SegmentCharacters::y;
		case 'z': return SegmentCharacters::z;
		case 'A': return SegmentCharacters::A;
		case 'B': return SegmentCharacters::B;
		case 'C': return SegmentCharacters::C;
		case 'D': return SegmentCharacters::D;
		case 'E': return SegmentCharacters::E;
		case 'F': return SegmentCharacters::F;
		case 'G': return SegmentCharacters::G;
		case 'H': return SegmentCharacters::H;
		case 'I': return SegmentCharacters::I;
		case 'J': return SegmentCharacters::J;
		case 'K': return SegmentCharacters::K;
		case 'L': return SegmentCharacters::L;
		case 'M': return SegmentCharacters::M;
		case 'N': return SegmentCharacters::N;
		case 'O': return SegmentCharacters::O;
		case 'P': return SegmentCharacters::P;
		case 'Q': return SegmentCharacters::Q;
		case 'R': return SegmentCharacters::R;
		case 'S': return SegmentCharacters::S;
		case 'T': return SegmentCharacters::T;
		case 'U': return SegmentCharacters::U;
		case 'V': return SegmentCharacters::V;
		case 'W': return SegmentCharacters::W;
		case 'X': return SegmentCharacters::X;
		case 'Y': return SegmentCharacters::Y;
		case 'Z': return SegmentCharacters::Z;
		case '.': return SegmentCharacters::Dot;
		}
		return SegmentCharacters::None;
	}
	/**
	 * @brief A tag type that tags a cursor.
	 */
	struct CursorTag{};
	/**
	 * @brief The bitfield of the bits in a segment.
	 */
	struct SegmentBits
	{
		union
		{
			std::uint16_t flags;
			struct
			{
				std::uint8_t COL : 1;
				std::uint8_t Cursor : 1;
			};
		};
		union
		{
			SegmentCharacters data;
			struct
			{
				std::uint8_t A : 1;
				std::uint8_t B : 1;
				std::uint8_t C : 1;
				std::uint8_t D : 1;
				std::uint8_t E : 1;
				std::uint8_t F : 1;
				std::uint8_t G : 1;
				std::uint8_t DP : 1;
			};
		};
		/**
		 * @brief Initialise all segments as off.
		 */
		constexpr SegmentBits() noexcept :
		    flags( 0 ),
		    data( SegmentCharacters::None )
		{}
		/**
		 * @brief Initialise the segment with the cursor.
		 */
		constexpr SegmentBits( CursorTag const ) noexcept :
		    COL( false ),
		    Cursor( true ),
		    data( SegmentCharacters::None ) 
		{}
		/**
		 * @brief Initialise the character with a letter, and optionally a cursor (if the MSb is 1)
		 */
		constexpr SegmentBits( char input ) noexcept :
		    COL( false ),
		    Cursor( ( input & 0x80 ) > 0 ),
		    data( ToSegment( input ) )
		{}
		/**
		 * @brief Initialise the segment with the option of also initialising a colon.
		 * 
		 * @param input The letter to print on the segment.
		 * @param next The next character to print (if its a colon it will be merged into the segments).
		 */
		constexpr SegmentBits( char input, char next ) noexcept :
		    COL( next == ':' ),
		    Cursor( ( input & 0x80 ) > 0 ),
		    data( ( next == '.' ) ? ( SegmentCharacters )( (std::uint8_t)ToSegment( input ) | (std::uint8_t)ToSegment( next ) ) : ToSegment( input ) )
		{}
	};

	//A, B, C, D, E, F, G are segment definitions, see LCD.h for example
	template <typename...>
	struct SegmentDigit
	{
	};
	/**
	 * @brief Manage the segments in a single digit.
	 * 
	 * @image html seven_segment_display.png
	 * @image latex seven_segment_display.png
	 * 
	 * @tparam base_class The lcd base class (CRTP).
	 * @tparam B The b segment
	 * @tparam C The c segment.
	 */
	template <typename base_class, typename B, typename C>
	struct SegmentDigit<base_class, B, C> : public base_class
	{
		using base   = base_class;
		/**
		 * @brief Never a new LCD manager objects.
		 * 
		 * This can only be constructed as via a cast. 
		 * This has a similar pattern to CRTP.
		 */
		constexpr SegmentDigit() = delete;
		constexpr SegmentDigit( const SegmentDigit& ) = delete;
		constexpr SegmentDigit& operator=( const SegmentDigit& ) = delete;
		/**
		 * @brief Assign bits to the segment digit.
		 * 
		 * @param input The bits to assign.
		 * @return true The assignment occured.
		 * @return false The assignment failed.
		 */
		bool Assign( SegmentBits input ) noexcept
		{
			if ( !input.Cursor )
			{
				base::template Set<B>( input.B );
				base::template Set<C>( input.C );
			}
			else Clear();
			return !( input.A || input.D || input.E || input.F || input.G || input.DP || input.COL );
		}
		/**
		 * @brief Clear all the segment bits.
		 * 
		 */
		void Clear() noexcept
		{
			base::template Set<B>( false );
			base::template Set<C>( false );
		}
	};
	/**
	 * @brief Manage the segments in a single digit.
	 * 
	 * @image html seven_segment_display.png
	 * @image latex seven_segment_display.png
	 * 
	 * @tparam base_class The lcd base class (CRTP).
	 * @tparam B The b segment
	 * @tparam C The c segment.
	 * @tparam DP The dp segment.
	 */
	template <typename base_class, typename B, typename C, typename DP>
	struct SegmentDigit<base_class, B, C, DP> : public base_class
	{
		using base   = base_class;
		/**
		 * @brief Never a new LCD manager objects.
		 * 
		 * This can only be constructed as via a cast. 
		 * This has a similar pattern to CRTP.
		 */
		constexpr SegmentDigit()                = delete;
		constexpr SegmentDigit( const SegmentDigit& ) = delete;
		constexpr SegmentDigit& operator=( const SegmentDigit& ) = delete;
		/**
		 * @brief Assign segment bits to the digit.
		 * 
		 * @param input The bits to assign.
		 * @return true The assignment was successful.
		 * @return false The assignment failed.
		 */
		bool Assign( SegmentBits input ) noexcept
		{
			if ( !input.Cursor )
			{
				base::template Set<B>( input.B );
				base::template Set<C>( input.C );
				base::template Set<DP>( input.DP );
			}
			else Clear();

			return !( input.A || input.D || input.E || input.F || input.G || input.COL );
		}
		/**
		 * @brief Clear segment bits to the digit.
		 */
		void Clear() noexcept
		{
			base::template Set<B>( false );
			base::template Set<C>( false );
			base::template Set<DP>( false );
		}
	};
	/**
	 * @brief Manage the segments in a single digit.
	 * 
	 * @image html seven_segment_display.png
	 * @image latex seven_segment_display.png
	 * 
	 * @tparam base_class The lcd base class (CRTP).
	 * @tparam B The b segment
	 * @tparam C The c segment.
	 * @tparam DP The dp segment.
	 * @tparam COL The col segment.
	 */
	template <typename base_class, typename B, typename C, typename DP, typename COL>
	struct SegmentDigit<base_class, B, C, DP, COL> : public base_class
	{
		using base = base_class;
		/**
		 * @brief Never a new LCD manager objects.
		 * 
		 * This can only be constructed as via a cast. 
		 * This has a similar pattern to CRTP.
		 */
		constexpr SegmentDigit() = delete;
		constexpr SegmentDigit( const SegmentDigit& ) = delete;
		constexpr SegmentDigit& operator=( const SegmentDigit& ) = delete;

		bool Assign( SegmentBits input ) noexcept
		{
			if ( !input.Cursor )
			{
				base::template Set<B>	( input.B );
				base::template Set<C>	( input.C );
				base::template Set<DP>	( input.DP );
				base::template Set<COL>	( input.COL );
			}
			else Clear();

			return !( input.A || input.D || input.E || input.F || input.G );
		}
		void Clear() noexcept
		{
			base::template Set<B>	( false );
			base::template Set<C>	( false );
			base::template Set<DP>	( false );
			base::template Set<COL>	( false );
		}
	};
	/**
	 * @brief Manage the segments in a single digit.
	 * 
	 * @image html seven_segment_display.png
	 * @image latex seven_segment_display.png
	 * 
	 * @tparam base_class The lcd base class (CRTP).
	 * @tparam A The a segment
	 * @tparam B The b segment
	 * @tparam C The c segment.
	 * @tparam D The d segment.
	 * @tparam E The e segment.
	 * @tparam F The f segment.
	 * @tparam G The g segment.
	 */
	template <typename base_class, typename A, typename B, typename C, typename D, typename E, typename F, typename G>
	struct SegmentDigit<base_class, A, B, C, D, E, F, G> : public base_class
	{
		using base   = base_class;
		/**
		 * @brief Never a new LCD manager objects.
		 * 
		 * This can only be constructed as via a cast. 
		 * This has a similar pattern to CRTP.
		 */
		constexpr SegmentDigit() = delete;
		constexpr SegmentDigit( SegmentDigit const & ) = delete;
		constexpr SegmentDigit& operator=( SegmentDigit const & ) = delete;
		/**
		 * @brief Assigns segment bits to the segment.
		 * 
		 * @param input The bits to assign.
		 * @return true Segment was assigned.
		 * @return false The segment couldn't be assigned.
		 */
		bool Assign( SegmentBits input ) noexcept
		{
			if ( !input.Cursor )
			{
				base::template Set<A>( input.A );
				base::template Set<B>( input.B );
				base::template Set<C>( input.C );
				base::template Set<D>( input.D );
				base::template Set<E>( input.E );
				base::template Set<F>( input.F );
				base::template Set<G>( input.G );
			}
			else	Clear();

			return !( input.DP || input.COL );
		}
		/**
		 * @brief Clears all the segments.
		 * 
		 */
		void Clear() noexcept
		{
			base::template Set<A>( false );
			base::template Set<B>( false );
			base::template Set<C>( false );
			base::template Set<D>( false );
			base::template Set<E>( false );
			base::template Set<F>( false );
			base::template Set<G>( false );
		}
	};
	/**
	 * @brief Manage the segments in a single digit.
	 * 
	 * @image html seven_segment_display.png
	 * @image latex seven_segment_display.png
	 * 
	 * @tparam base_class The lcd base class (CRTP).
	 * @tparam A The a segment
	 * @tparam B The b segment
	 * @tparam C The c segment.
	 * @tparam D The d segment.
	 * @tparam E The e segment.
	 * @tparam F The f segment.
	 * @tparam G The g segment.
	 * @tparam DP The dp segment.
	 */
	template <typename base_class, typename A, typename B, typename C, typename D, typename E, typename F, typename G, typename DP>
	struct SegmentDigit<base_class, A, B, C, D, E, F, G, DP> : public SegmentDigit<base_class, A, B, C, D, E, F, G>
	{
		using base   = SegmentDigit<base_class, A, B, C, D, E, F, G>;
		/**
		 * @brief Never a new LCD manager objects.
		 * 
		 * This can only be constructed as via a cast. 
		 * This has a similar pattern to CRTP.
		 */
		constexpr SegmentDigit()                = delete;
		constexpr SegmentDigit( const SegmentDigit& ) = delete;
		constexpr SegmentDigit& operator=( const SegmentDigit& ) = delete;
		/**
		 * @brief Set or clear the decimal point segment.
		 * 
		 * @param input 
		 */
		void Decimal( bool input ) noexcept
		{
			base::template Set<DP>( input );
		}
		/**
		 * @brief Assign segemnt bits to the digit.
		 * 
		 * @param input The bits to assign.
		 * @return true The assignment was successful.
		 * @return false The assignment failed.
		 */
		bool Assign( SegmentBits input ) noexcept
		{
			if ( !input.Cursor )	Decimal( input.DP );
			else					Clear();
			base::Assign(input);
			return !( input.COL );
		}
		/**
		 * @brief Clears all the digits.
		 */
		void Clear() noexcept
		{
			base::Clear();
			Decimal( false );
		}
	};
	/**
	 * @brief Manage the segments in a single digit.
	 * 
	 * @image html seven_segment_display.png
	 * @image latex seven_segment_display.png
	 * 
	 * @tparam base_class The lcd base class (CRTP).
	 * @tparam A The a segment
	 * @tparam B The b segment
	 * @tparam C The c segment.
	 * @tparam D The d segment.
	 * @tparam E The e segment.
	 * @tparam F The f segment.
	 * @tparam G The g segment.
	 * @tparam DP The dp segment.
	 * @tparam COL The col segment.
	 */
	template <typename base_class, typename A, typename B, typename C, typename D, typename E, typename F, typename G, typename DP, typename COL>
	struct SegmentDigit<base_class, A, B, C, D, E, F, G, DP, COL> : public SegmentDigit<base_class, A, B, C, D, E, F, G, DP>
	{
		using base   = SegmentDigit<base_class, A, B, C, D, E, F, G, DP>;
		/**
		 * @brief Never a new LCD manager objects.
		 * 
		 * This can only be constructed as via a cast. 
		 * This has a similar pattern to CRTP.
		 */
		constexpr SegmentDigit() = delete;
		constexpr SegmentDigit( const SegmentDigit& ) = delete;
		constexpr SegmentDigit& operator=( const SegmentDigit& ) = delete;
		/**
		 * @brief Assign a colon to the segment.
		 * 
		 * @param input True is enables the segment, false disables.
		 */
		void Colon( bool input ) noexcept
		{
			base::template Set<COL>( input );
		}
		/**
		 * @brief Assign segemnt bits to the digit.
		 * 
		 * @param input The bits to assign.
		 * @return true The assignment was successful.
		 * @return false The assignment failed.
		 */
		bool Assign( SegmentBits input ) noexcept
		{
			if ( !input.Cursor )	Colon( input.COL );
			else					Clear();
			base::Assign(input);
			return true;
		}
		/**
		 * @brief Clear segment bits to the digit.
		 */
		void Clear() noexcept
		{
			base::Clear();
			Colon( false );
		}
	};
}