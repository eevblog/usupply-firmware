#pragma once

#include "HT1622.hpp"
#include "SPIBitbanging.hpp"
#include <array>

namespace LCD
{
	/**
	 * @brief This is the tone which is output by the LCD controller.
	 */
	enum class BuzzerTone
	{
		High,
		Low,
		Off
	};
	/**
	 * @brief These are just constant definitions.
	 * 
	 */
	static constexpr unsigned rows       = 32;
	static constexpr unsigned commons    = 8;
	static constexpr unsigned segments   = rows * commons;
	static constexpr unsigned data_count = ( segments / Parts::HT1622::data_bits );

	//Represents an individual LCD segment
	template <unsigned common, unsigned segment>
	struct LCD_Segment
	{
		static constexpr auto address = Parts::HT1622::GetAddress( common, segment );

		template <bool input = true>
		static constexpr Parts::HT1622::DataInt bit = ( input ) ? Parts::HT1622::GetBit( common ) : (Parts::HT1622::DataInt)0u;

		static constexpr Parts::HT1622::DataInt set_bit( bool input ) noexcept
		{
			return ( ( input ) ? Parts::HT1622::GetBit( common ) : (Parts::HT1622::DataInt)0u );
		}

		static constexpr Parts::HT1622::DataInt mask = (Parts::HT1622::DataInt)0xf ^ bit<true>;
		static_assert( address < data_count, "Common index or segment count is too large." );
	};

	template <typename SPI_Kernal>
	class LCD
	{
	private:
		using HT1622_t = Parts::HT1622::HT1622<SPI_Kernal>;
		using array_t  = std::array<Parts::HT1622::DataInt, data_count>;

		HT1622_t	m_Controller;
		array_t		m_Data = {};
	public:
		LCD() = default;
		inline void Clear() noexcept
		{
			m_Data.fill( (Parts::HT1622::DataInt)0 );
		}
		template <typename s, bool input>
		inline void Set() noexcept
		{
			m_Data[ s::address ] &= (Parts::HT1622::DataInt)s::mask;
			m_Data[ s::address ] |= (Parts::HT1622::DataInt)s::template bit<input>;
		}
		template <typename s>
		inline void Set( bool input ) noexcept
		{
			m_Data[ s::address ] &= (Parts::HT1622::DataInt)s::mask;
			m_Data[ s::address ] |= (Parts::HT1622::DataInt)s::set_bit( input );
		}
		void Update() noexcept
		{
			m_Controller.template WriteBulk<data_count>( 0u, m_Data );
		}
		void Buzzer( BuzzerTone const& input ) noexcept
		{
			switch ( input )
			{
			case BuzzerTone::Low:
				m_Controller.Command( Parts::HT1622::Commands::TONE_2K );
				break;
			case BuzzerTone::High:
				m_Controller.Command( Parts::HT1622::Commands::TONE_4K );
				break;
			case BuzzerTone::Off:
				m_Controller.Command( Parts::HT1622::Commands::TONE_OFF );
				break;
			};
		}
	};
}