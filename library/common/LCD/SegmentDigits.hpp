#pragma once
#include "AtIndex.hpp"
#include "Convert.hpp"
#include "SegmentDigit.hpp"
#include "MenuItem.hpp"
#include "Time.hpp"

#include <array>
#include <cstdint>
#include <string>

namespace LCD
{
	/**
	 * @brief Manages a the set of segment digits on the LCD screen.
	 * 
	 * @tparam  The types of the digits. These MUST be SegmentDigit types.
	 */
	template <typename...>
	struct SegmentDigits;
	/**
	 * @brief Manages a the set of segment digits on the LCD screen.
	 * 
	 * @tparam base_class The base LCD class.
	 * @tparam digits The digit types.
	 */
	template <typename base_class, typename... digits>
	struct SegmentDigits<base_class, digits...> : private base_class
	{
		/**
		 * @brief A list of all the digit types.
		 */
		using list_t = General::TypeList<digits...>;
		/**
		 * @brief The number of digits.
		 */
		static constexpr auto count = sizeof...( digits );
		/**
		 * @brief An array type of the required size for the segments.
		 */
		using string_array_t = char[ count ];
		/**
		 * @brief Never a new LCD manager objects.
		 * 
		 * This can only be constructed as via a cast. 
		 * This has a similar pattern to CRTP.
		 */
		SegmentDigits() = delete;
		SegmentDigits( SegmentDigits const & ) = delete;
		SegmentDigits &operator=( SegmentDigits const & ) = delete;

		//Should be replaced with a fully indexed compile time solution based on index re-specialisation
		// and inheritance, inheriting fully specialised overloads of functions.
		template <unsigned c>
		using array_t = std::array<SegmentBits, c>;
		/**
		 * @brief Get the type at the requested index.
		 * 
		 * @tparam index 
		 */
		template <unsigned index>
		using AtIndex_t = typename General::AtIndex<index, list_t>::type;
		/**
		 * @brief Gets the first segment that supports a decimal point.
		 * 
		 * @tparam i The scanning index
		 * @tparam T The scanning type.
		 */
		template <unsigned i, typename T>
		struct FirstDecimal;
		template <unsigned i, typename head>
		struct FirstDecimal<i, General::TypeList<head>>
		{
			static constexpr unsigned index = ( head::has_decimal ) ? i : ( i + 1 );
		};
		template <unsigned i, typename head, typename... tail>
		struct FirstDecimal<i, General::TypeList<head, tail...>>
		{
			static constexpr unsigned index = ( head::has_decimal ) ? i : FirstDecimal<( i + 1 ), General::TypeList<tail...>>::index;
		};
		/**
		 * @brief Gets the index of the next decimal point
		 * 
		 * @tparam i The index to start checking for a decimal point.
		 */
		template <unsigned i = 0u>
		static constexpr auto next_decimal = FirstDecimal<i, list_t>::index;
		/**
		 * @brief Assign an array of characters to the segment digits.
		 * 
		 * @tparam c The number of elements in the array.
		 * @param input The input array.
		 * @return true The assignment succeeded.
		 * @return false The assignment failed or partially failed.
		 */
		template <size_t c>
		bool Set( array_t<c> const &input ) noexcept
		{
			static constexpr auto i       = c - 1;
			auto &                segment = ( (AtIndex_t<i> &)( *this ) );

			if constexpr ( c > 1u )
				if ( segment.Assign( input[ i ] ) )
					return ( Set<i>( (array_t<i> const &)input ) );

			if constexpr ( c == 1u )
				return ( segment.Assign( input[ i ] ) );

			return false;
		}
		/**
		 * @brief Clears all segments.
		 * 
		 * @tparam count The number of characters to clear.
		 */
		template <unsigned c = count>
		void Clear() noexcept
		{
			if constexpr ( c > 0 )
			{
				auto &segment = ( (AtIndex_t<c - 1> &)( *this ) );
				Clear<c - 1>();
				return segment.Clear();
			}
		}
		/**
		 * @brief Determines whether a character in a string represents a character that doesn't print in a whole segment.
		 * 
		 * Each printable character represents one segment. Some characters are special, these are printed between segments.
		 * Examples are the colon or decimal point.
		 * 
		 * @param input The input character.
		 * @return true The character should be skipped when iterating segments.
		 * @return false The character should not be skipped.
		 */
		static bool ShouldSkip( char input ) noexcept
		{
			return ( input == '.' || input == ':' );
		}
		/**
		 * @brief Assigns a character array to the segment digits.
		 * 
		 * @tparam N The capacity of the assigning array.
		 * @param str The array.
		 * @return true The assignment was successful.
		 * @return false Characters were dropped during print.
		 */
		template <unsigned N>
		bool operator = ( std::array<char, N> const &str ) noexcept
		{
			unsigned       str_index = 0;
			unsigned       buf_index = 0;
			array_t<count> buffer{};

			while ( buf_index < count && str_index < N )
			{
				const char current = str[ str_index++ ];
				if ( ShouldSkip( current ) )
					continue;

				buffer[ buf_index++ ] = ( str_index < N ) ? SegmentBits( current, str[ str_index ] ) : SegmentBits( current );
			}
			auto N_Count = N;
			while (!Set(buffer) && --N_Count)
				General::ShiftRight( buffer, 0U );

			return buf_index > 0;
		}
		/**
		 * @brief Assigns a time to the segment digits
		 * 
		 * This prints the time digits and the colons nessary to display time.
		 * 
		 * @param input The time to set.
		 * @return true The assignment was successful.
		 * @return false The assignment failed (digits likely truncated).
		 */
		bool operator = ( General::Time const &input ) noexcept
		{
			General::hh_mm_ss buffer;
			input.ToString( buffer );
			return ( *this = buffer );
		}
		/**
		 * @brief Assign a menu item to the segment.
		 * 
		 * This could be any format and relies on the menu items Render function to
		 * format the output.
		 * 
		 * @tparam N The maximum characters capacity of the menu item.
		 * @param input The menu item.
		 * @return true Render successful.
		 * @return false Render failed (digits likely truncated).
		 */
		template <unsigned N>
		bool operator = ( Menu::RawMenuItem<N> & input ) noexcept
		{
			using buffer_t = typename Menu::RawMenuItem<N>::buffer_t;
			buffer_t buffer{};
			if ( input.Render( buffer, true ) > 0 )
			{
				*this = buffer;
				return true;
			}
			return false;
		}
		/**
		 * @brief Print a float to the segment digits.
		 * 
		 * @param value The value to print.
		 * @return true The value printed successfully.
		 * @return false The value failed to print (digits likely truncated).
		 */
		bool operator = ( float value ) noexcept
		{
			//Extra place for a decimal point
			std::array<char, count + 1> buffer;
			General::fixed_ftoa<count + 1>( value, buffer );
			return ( *this = buffer );
		}
		/**
		 * @brief See bool operatpr = ( float value ) noexcept
		 * 
		 * @param value 
		 * @return true 
		 * @return false 
		 */
		bool operator = ( double value ) noexcept
		{
			return ( *this = (float)value );
		}
		/**
		 * @brief Prints an unsigned number to the segment digits.
		 * 
		 * Aligns the output to the right.
		 * 
		 * @param value The value to print
		 * @return true The print was successful.
		 * @return false The print failed (digits likely truncated).
		 */
		bool operator = ( unsigned value ) noexcept
		{
			std::array<char, count> buffer;
			General::fixed_utoa<count>( value, buffer );
			return ( *this = buffer );
		}
		/**
		 * @brief Prints a signed number to the segment digits.
		 * 
		 * Output is aligned to the right.
		 * 
		 * @param value The value to print.
		 * @return true The print succeeded.
		 * @return false The print failed (digits likely truncated).
		 */
		bool operator = ( int value ) noexcept
		{
			std::array<char, count> buffer;
			General::fixed_itoa<count>( value, buffer );
			return ( *this = buffer );
		}
	};
}