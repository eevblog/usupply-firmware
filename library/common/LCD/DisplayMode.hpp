#pragma once
#include "AtIndex.hpp"

namespace LCD::Block
{
	/**
	 * @brief The type of the display that is active.
	 */
	enum class Display
	{
		Min,
		Max,
		Min_Max,
		Off
	};
	/**
	 * @brief Manages the display mode LCD segments in a CRTP.
	 * 
	 * @tparam base_class The LCD class.
	 * @tparam MIN Segment for min.
	 * @tparam MAX Segment for max.
	 */
	template <typename base_class, typename MIN, typename MAX>
	struct DisplayMode : public base_class
	{
		/**
		 * @brief Never a new LCD manager objects.
		 * 
		 * This can only be constructed as via a cast. 
		 * This has a similar pattern to CRTP.
		 */
		constexpr DisplayMode() = delete;
		constexpr DisplayMode( DisplayMode const & ) = delete;
		constexpr DisplayMode& operator=( DisplayMode const & ) = delete;
		/**
		 * @brief Assigns the mode to the LCD segments
		 * 
		 * @param input The mode to assign.
		 * @return DisplayMode & For chained operations.
		 */
		constexpr DisplayMode & Assign (Display input) noexcept
		{
			switch ( input )
			{
			case Display::Min:
				base_class::template Set<MIN>(true);
				base_class::template Set<MAX>(false);
				break;
			case Display::Max:
				base_class::template Set<MIN>(false);
				base_class::template Set<MAX>(true);
				break;
			case Display::Min_Max:
				base_class::template Set<MIN>(true);
				base_class::template Set<MAX>(true);
				break;
			default:
				base_class::template Set<MIN>(false);
				base_class::template Set<MAX>(false);
				break;
			}
			return *this;
		}
		/**
		 * @brief Clears the segments.
		 * 
		 * @return DisplayMode & For chained operations.
		 */
		constexpr DisplayMode & Clear() noexcept
		{
			Assign(Display::Off);
			return *this;
		}
		/**
		 * @brief Assigns the display mode to the segments.
		 * 
		 * @param input The display mode to assign.
		 * @return DisplayMode & For chained operations.
		 */
		DisplayMode & operator = (Display const input)
		{
			Assign(input);
			return *this;
		}
	};
}