#pragma once

#include "IntegerConvert.hpp"

namespace General
{
	struct TimeHours;
	struct TimeMinutes;
	struct TimeSeconds;

	//HH:mm
	using hh_mm    = std::array<char, 5>;

	//HH:mm.ss
	using hh_mm_ss = std::array<char, 8>;
	/**
	 * @brief A data structure that container hours, minutes and seconds.
	 * 
	 */
	class Time
	{
	protected:
		std::int8_t m_Hours;
		std::int8_t m_Minutes;
		std::int8_t m_Seconds;

		void NonZeroMinutes() noexcept
		{
			while ( m_Minutes <= (std::int8_t)0 )
			{
				--m_Hours;
				m_Minutes += (std::int8_t)60;
			}
		}
		void NonZeroSeconds() noexcept
		{
			while ( m_Seconds < (std::int8_t)0 )
			{
				NonZeroMinutes();
				--m_Minutes;
				m_Seconds += (std::int8_t)60;
			}
		}
		void PaddMinutes() noexcept
		{
			while ( m_Minutes >= (std::int8_t)60 )
			{
				++m_Hours;
				m_Minutes -= (std::int8_t)60;
			}
		}
		void PaddSeconds() noexcept
		{
			while ( m_Seconds >= (std::int8_t)60 )
			{
				++m_Minutes;
				PaddMinutes();
				m_Seconds -= (std::int8_t)60;
			}
		}

		Time &Sanity() noexcept
		{
			NonZeroMinutes();
			NonZeroSeconds();
			PaddSeconds();
			PaddMinutes();
			return ( *this );
		}

	public:
		/**
		 * @brief Construct a new Time object
		 * 
		 */
		Time() noexcept:
		    m_Hours( (std::int8_t)0 ),
		    m_Minutes( (std::int8_t)0 ),
		    m_Seconds( (std::int8_t)0 )
		{}
		/**
		 * @brief Construct a new Time object
		 * 
		 * @param hours
		 * @param minutes 
		 * @param seconds 
		 */
		Time( std::int8_t hours, std::int8_t minutes, std::int8_t seconds ) noexcept:
		    m_Hours( hours ),
		    m_Minutes( minutes ),
		    m_Seconds( seconds )
		{
			Sanity();
		}
		/**
		 * @brief Returns a TimeSeconds object, which inherits from Time.
		 * 
		 * The result has the same underlying storage as the Time object.
		 * 
		 * @return TimeSeconds& The Time object
		 */
		TimeSeconds & Seconds() noexcept
		{
			return (TimeSeconds &)( *this );
		}
		/**
		 * @brief Returns a TimeMinutes object, which inherits from Time.
		 * 
		 * The result has the same underlying storage as the Time object.
		 * 
		 * @return TimeMinutes& The Time object
		 */
		TimeMinutes & Minutes() noexcept
		{
			return (TimeMinutes &)( *this );
		}
		/**
		 * @brief Returns a TimeHours object, which inherits from Time.
		 * 
		 * The result has the same underlying storage as the Time object.
		 * 
		 * @return TimeHours& The Time object
		 */
		TimeHours & Hours() noexcept
		{
			return (TimeHours &)( *this );
		}
		/**
		 * @brief Returns a TimeSeconds object, which inherits from Time.
		 * 
		 * The result has the same underlying storage as the Time object.
		 * 
		 * @return TimeSeconds& The Time object
		 */
		TimeSeconds const & Seconds() const noexcept
		{
			return (TimeSeconds &)( *this );
		}
		/**
		 * @brief Returns a TimeMinutes object, which inherits from Time.
		 * 
		 * The result has the same underlying storage as the Time object.
		 * 
		 * @return TimeMinutes& The Time object
		 */
		TimeMinutes const & Minutes() const noexcept
		{
			return (TimeMinutes &)( *this );
		}
		/**
		 * @brief Returns a TimeHours object, which inherits from Time.
		 * 
		 * The result has the same underlying storage as the Time object.
		 * 
		 * @return TimeHours& The Time object
		 */
		TimeHours const & Hours() const noexcept
		{
			return (TimeHours &)( *this );
		}
		/**
		 * @brief Decrement the time structure.
		 * 
		 * @return Time& 
		 */
		Time & operator-=( Time const & ) noexcept;
		/**
		 * @brief Increment the time structure.
		 * 
		 * @return Time& 
		 */
		Time & operator+=( Time const & ) noexcept;
		/**
		 * @brief Converts the time structure to a string.
		 * 
		 * @tparam BufferSize The capacity of the output string.
		 * @tparam NullTerminated Whether or not to add a null terminator to the string.
		 * @param output The output string.
		 * @return std::size_t The number of bytes written.
		 */
		template <unsigned BufferSize, bool NullTerminated = false>
		std::size_t ToString(std::array<char, BufferSize> & output) const noexcept
		{
			static constexpr auto L = 2u;
			static constexpr auto N = 8u + (unsigned)NullTerminated;
			static constexpr auto S = ':';

			std::size_t length = 0u;
			length += General::utoa( (std::int8_t)m_Hours, output, length, ' ' );
			if ( length < N ) output[ length++ ] = S;
			length += General::fixed_utoa<L>( (std::int8_t)m_Minutes, output, length, '0' );
			if ( length < N ) output[ length++ ] = S;
			length += General::fixed_utoa<L>( (std::int8_t)m_Seconds, output, length, '0' );

			if constexpr ( NullTerminated )
				if ( length < N ) 
					output[ length ] = '\0';

			return length;
		}
	};
	/**
	 * @brief An object that deals with an assignable hour object that supports chained time operations.
	 * 
	 * The eventual purpose of this is to provide to and from string capibilities.
	 */
	struct TimeHours : Time
	{
		constexpr TimeHours() = delete;
		constexpr TimeHours( const TimeHours & ) = delete;
		constexpr TimeHours &operator=( const TimeHours & ) = delete;
		/**
		 * @brief Get the number of characters needed for the hours tostirng component.
		 * 
		 * @return std::size_t The number of characters.
		 */
		std::size_t Length() const noexcept
		{
			if ( (std::int8_t)0 <= m_Hours && m_Hours <= (std::int8_t)9 ) return 1u;
			return 2u;
		}
		/**
		 * @brief Assign a value to the hours component of the time.
		 * 
		 * @param value The value to assign
		 * @return Time & For chained operations. 
		 */
		Time &operator=( std::int8_t value ) noexcept
		{
			m_Hours = value;
			Sanity();
			return *this;
		}
		/**
		 * @brief Increment by a value to the hours component of the time.
		 * 
		 * @param increment The value to assign
		 * @return Time & For chained operations. 
		 */
		Time &operator+=( std::int8_t increment ) noexcept
		{
			m_Hours += increment;
			Sanity();
			return *this;
		}
		/**
		 * @brief Decrement by a value to the hours component of the time.
		 * 
		 * @param increment The value to assign
		 * @return Time & For chained operations. 
		 */
		Time &operator-=( std::int8_t increment ) noexcept
		{
			m_Hours -= increment;
			Sanity();
			return *this;
		}
		//##################################################################################
		/**
		 * @brief Add a value to the class.
		 * 
		 * @param lhs The class to add to.
		 * @param increment The amount to add.
		 * @return Time & 
		 */
		friend Time &operator+( TimeHours &lhs, std::int8_t const increment ) noexcept
		{
			return ( lhs += increment );
		}
		/**
		 * @brief Subtract a value from the class.
		 * 
		 * @param lhs The class to subtract from.
		 * @param increment The amount to decrement from.
		 * @return Time & 
		 */
		friend Time &operator-( TimeHours &lhs, std::int8_t const increment ) noexcept
		{
			return ( lhs -= increment );
		}
		/**
		 * @brief Implicity cast to the compatible types.
		 * 
		 * @tparam T The type to convert to 
		 * @return T The value.
		 */
		template <typename T>
		operator T() const noexcept
		{
			return m_Hours;
		}
	};
	/**
	 * @brief An object that deals with an assignable minutes object that supports chained time operations.
	 * 
	 * The eventual purpose of this is to provide to and from string capibilities.
	 */
	struct TimeMinutes : Time
	{
		constexpr TimeMinutes() = delete;
		constexpr TimeMinutes( const TimeMinutes & ) = delete;
		constexpr TimeMinutes &operator=( const TimeMinutes & ) = delete;
		/**
		 * @brief The number of characters required 
		 * 
		 * @return std::size_t
		 */
		std::size_t Length() const noexcept
		{
			if ( (std::int8_t)0 <= m_Minutes && m_Minutes <= (std::int8_t)9 ) return 1u;
			return 2u;
		}
		/**
		 * @brief Assigns a value to the minutes component of the underlying time structure.
		 * 
		 * @param value The minutes to assign.
		 * @return Time& For chained operations.
		 */
		Time &operator=( std::int8_t const value ) noexcept
		{
			m_Minutes = value;
			Sanity();
			return (Time &)( *this );
		}
		/**
		 * @brief Increments the value to the minutes component of the underlying time structure.
		 * 
		 * @param increment The minutes to increment.
		 * @return Time& For chained operations.
		 */
		Time &operator+=( std::int8_t const increment ) noexcept
		{
			m_Minutes += increment;
			Sanity();
			return (Time &)( *this );
		}
		/**
		 * @brief Decrements the value to the minutes component of the underlying time structure.
		 * 
		 * @param increment The minutes to decrement.
		 * @return Time& For chained operations.
		 */
		Time &operator-=( std::int8_t const increment ) noexcept
		{
			m_Minutes -= increment;
			Sanity();
			return (Time &)( *this );
		}
		/**
		 * @brief Adds the value to the minutes component of the underlying time structure.
		 * 
		 * @param lhs The left hand side of the addition.
		 * @param increment The minutes to increment.
		 * @return Time& For chained operations.
		 */
		friend Time &operator+( TimeMinutes &lhs, std::int8_t const increment ) noexcept
		{
			return ( lhs += increment );
		}
		/**
		 * @brief Subtracts the value to the minutes component of the underlying time structure.
		 * 
		 * @param lhs The left hand side of the subtraction operation.
		 * @param increment The minutes to increment.
		 * @return Time& For chained operations.
		 */
		friend Time &operator-( TimeMinutes &lhs, std::int8_t const increment ) noexcept
		{
			return ( lhs -= increment );
		}
		/**
		 * @brief Implicitly cast to the minutes component if possible.
		 * 
		 * @tparam T The type to cast to.
		 * @return T The minutes component of the underlying time structure.
		 */
		template <typename T>
		operator T() const noexcept
		{
			return m_Minutes;
		}
	};
	/**
	 * @brief An object that deals with an assignable seconds object that supports chained time operations.
	 * 
	 * The eventual purpose of this is to provide to and from string capibilities.
	 */
	struct TimeSeconds : Time
	{
		constexpr TimeSeconds()                       			= delete;
		constexpr TimeSeconds( const TimeSeconds & ) 			= delete;
		constexpr TimeSeconds &operator=( const TimeSeconds & ) = delete;
		/**
		 * @brief The number of characters needed to print this component of the time structure.
		 * 
		 * @return std::size_t The number of characters required.
		 */
		std::size_t Length() const noexcept
		{
			if ( (std::int8_t)0 <= m_Seconds && m_Seconds <= (std::int8_t)9 ) return 1u;
			return 2u;
		}
		/**
		 * @brief Assigns a value to the seconds component of the underlying time data structure.
		 * 
		 * @param value The value to assign to the seconds.
		 * @return Time& For chained operations. 
		 */
		Time &operator=( std::int8_t value ) noexcept
		{
			m_Seconds = value;
			Sanity();
			return (Time &)( *this );
		}
		/**
		 * @brief Increments by value to the seconds component of the underlying time data structure.
		 * 
		 * @param value The value to increment the seconds.
		 * @return Time& For chained operations. 
		 */
		Time &operator+=( std::int8_t value ) noexcept
		{
			m_Seconds += value;
			Sanity();
			return (Time &)( *this );
		}
		/**
		 * @brief Decrements by value to the seconds component of the underlying time data structure.
		 * 
		 * @param value The value to decrement the seconds.
		 * @return Time& For chained operations. 
		 */
		Time &operator-=( std::int8_t value ) noexcept
		{
			m_Seconds -= value;
			Sanity();
			return (Time &)( *this );
		}
		/**
		 * @brief Increments by value to the seconds component of the underlying time data structure.
		 * 
		 * @param lhs The left hand side of the addition operation.
		 * @param increment The value to increment the seconds.
		 * @return Time& For chained operations. 
		 */
		friend Time &operator+( TimeSeconds & lhs, std::uint8_t const increment ) noexcept
		{
			return ( lhs += (std::int8_t)(increment & 0x7f) );
		}
		/**
		 * @brief Decrements by value to the seconds component of the underlying time data structure.
		 * 
		 * @param lhs The left hand side of the substraction operation.
		 * @param increment The value to decrement the seconds.
		 * @return Time& For chained operations. 
		 */
		friend Time &operator-( TimeSeconds & lhs, std::uint8_t const increment ) noexcept
		{
			return ( lhs -= (std::int8_t)(increment & 0x7f) );
		}
		/**
		 * @brief Implicitly cast to the seconds component if possible.
		 * 
		 * @tparam T The type to cast to.
		 * @return T The seconds component of the underlying time structure.
		 */
		template <typename T>
		operator T() const noexcept
		{
			return m_Seconds;
		}
	};
	/**
	 * @brief Subtract one time structure(lhs) from another rhs.
	 * 
	 * @param rhs The value to subtract with.
	 * @return Time& For chained operations.
	 */
	Time &Time::operator-=( Time const & rhs ) noexcept
	{
		Hours() -= rhs.m_Hours;
		Minutes() -= rhs.m_Minutes;
		Seconds() -= rhs.m_Seconds;
		return *this;
	}
	/**
	 * @brief Adds one time structure(lhs) from another rhs.
	 * 
	 * @param rhs The value to add with.
	 * @return Time& For chained operations.
	 */
	Time &Time::operator+=( Time const & rhs ) noexcept
	{
		Seconds() += rhs.m_Seconds;
		Minutes() += rhs.m_Minutes;
		Hours() += rhs.m_Hours;
		return *this;
	}
	/**
	 * @brief Tests whether a type is a time data structure.
	 * 
	 * @tparam T The type to test.
	 */
	template <typename T>
	constexpr bool IsTime_v = false;
	template<>
	constexpr bool IsTime_v<Time> = true;
}