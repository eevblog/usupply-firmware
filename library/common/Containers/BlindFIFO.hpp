#pragma once
#include "Macros.hpp"
#include <array>
#include <utility>

namespace Containers
{
	/**
	 * @brief A fifo that conducts no bounds checks whatsoever.
	 * 
	 * @tparam T The element type.
	 * @tparam N The capacity of the FIFO.
	 */
	template <typename T, std::size_t N>
	class BlindFIFO
	{
	private:
		std::array<T, N>    m_Data;
		std::size_t         m_Index = 0, m_Length = 0;
		//
		constexpr void Wrap()
		{
			while ( m_Index >= N ) m_Index -= N;
		}
		constexpr void Reduce()
		{
			--m_Length;
		}
		constexpr void Increment()
		{
			++m_Index;
			Wrap();
		}
		constexpr static std::size_t Index( std::size_t input )
		{
			return input % N;
		}
		template <typename R>
		constexpr void SingleEmplace( R && data )
		{
			if (m_Length < N) ++m_Length;
			m_Data[ Index( m_Index ) ] = FWD(data);
			Increment();
		}
		//
		constexpr std::size_t Start() const noexcept
		{
			return Index(m_Index + N - m_Length);
		}
		constexpr std::size_t Endpoint() const noexcept
		{
			return Index(m_Index + N - 1);
		}
	public:
		constexpr BlindFIFO() noexcept = default;
		/**
		 * @brief Construct the BlindFIFO.
		 * 
		 * Constructs the blind fifo with inital values.
		 * 
		 * @tparam Args the types of the arguements.
		 * @param args The arguements. 
		 */
		template <typename ... Args>
		constexpr BlindFIFO(Args && ... args) noexcept :
			m_Data	{ FWD(args) ...		},
			m_Index	{ 0 },
			m_Length{ sizeof...(Args)	}
		{
			static_assert(sizeof...(Args) > N, "Too many arguments for construction.");
		}
		/**
		 * @brief Inserts some items into the BlindFIFO.
		 * 
		 * @tparam Args The item types that are being inserted.
		 * @param args The items to insert.
		 */
		template <typename ... Args>
		constexpr void Emplace( Args && ... args ) noexcept
		{
			return ( SingleEmplace( FWD( args ) ), ... );
		}
		/**
		 * @brief Checks if an item exists in the FIFO.
		 * 
		 * @param data The data to compare with.
		 * @return true One of the items in the fifo is the equal( as per operator== ) to data.
		 * @return false No items in the fifo are the equal( as per operator== ) to data.
		 */
		constexpr bool Exists( T const & data ) noexcept
		{
			for ( auto const & item : ( *this ) )
				if ( item == data )
					return true;
			return false;
		}
		/**
		 * @brief Remove the item at the front of the FIFO.
		 * 
		 * @note If the FIFO is empty this does nothing.
		 */
		constexpr void Pop() noexcept
		{
			if (Empty()) return;
			Reduce();
		}
		/**
		 * @brief Removes all items from the fifo.
		 * 
		 * @note No destructors are called, items are left as is here. The head and tail indexes are reset.
		 */
		constexpr void Clear() noexcept
		{
			m_Index = 0;
			m_Length = 0;
		}
		/**
		 * @brief Get the number of items in the FIFo.
		 * 
		 * @return std::size_t The number of items in the FIFO.
		 */
		constexpr std::size_t Count() const noexcept
		{
			return m_Length;
		}
		/**
		 * @brief The space remaining in the FIFO.
		 * 
		 * @return std::size_t Number of free slots in the FIFO. 
		 */
		constexpr std::size_t Space() const noexcept
		{
			return N - Count();
		}
		/**
		 * @brief Get the maximum number of items the FIFO can store.
		 * 
		 * @return constexpr std::size_t 
		 */
		constexpr std::size_t Capacity() const noexcept
		{
			return N;
		}
		/**
		 * @brief Test whether the fifo is empty.
		 * 
		 * @return true The fifo is empty.
		 * @return false The fifo has items.
		 */
		constexpr bool Empty() const noexcept
		{
			return Count() == 0;
		}
		/**
		 * @brief Test whether the fifo is full.
		 * 
		 * @return true The fifo is full.
		 * @return false The fifo has empty slots. 
		 */
		constexpr bool Full() const
		{
			return Count() == N;
		}
		/**
		 * @brief Get a reference to the underlying data strucutre (std::array)
		 * 
		 * @return std::array<T, N> const& The underlying storage.
		 */
		constexpr auto Data() const noexcept -> std::array<T, N> const &
		{
			return m_Data;
		}
		/**
		 * @brief Get the item at the front of the fifo.
		 * 
		 * @return T& The item at the front of the fifo. 
		 */
		constexpr T & Front() noexcept
		{
			return m_Data[ Start() ];
		}
		/**
		 * @brief Get the item at the back of the fifo.
		 * 
		 * @return T& The item at the back of the fifo.
		 */
		constexpr T & Back() noexcept
		{
			return m_Data[ Endpoint() ];
		}
		/**
		 * @brief Get the item at the front of the fifo.
		 * 
		 * @return T& The item at the front of the fifo. 
		 */
		constexpr T const & Front() const noexcept
		{
			return m_Data[ Start() ];
		}
		/**
		 * @brief Get the item at the back of the fifo.
		 * 
		 * @return T& The item at the back of the fifo.
		 */
		constexpr T const & Back() const noexcept
		{
			return m_Data[ Endpoint() ];
		}
		/**
		 * @brief Get the item at the requested index in the fifo.
		 * 
		 * @param index The requested index.
		 * @return T& The item.
		 */
		constexpr T & operator[]( std::size_t index ) noexcept
		{
			return m_Data[ Index( m_Index + N - m_Length + index ) ];
		}
		/**
		 * @brief Get the item at the requested index in the fifo.
		 * 
		 * @param index The requested index.
		 * @return T& The item.
		 */
		constexpr T const & operator[]( std::size_t index ) const noexcept
		{
			return m_Data[ Index( m_Index + N - m_Length + index ) ];
		}
        /**
         * @brief A function that runs a callback with the slices that have been processed.
         * 
         * This is useful with a DMA, if a DMA has only partially processed its data.
         * 
		 * @todo Fix bug in implementation and uncomment this code.
		 * 
         * @tparam Data A view of the buffer that the DMA operates on.
         * @tparam C A callback which is called with the slices that the DMA has completed processing.
         * @param remaining The number of items the DMA still has to send.
         * @param d See Data.
         * @param callback See C.
         */
        template <typename Data, typename C>
        void Contigious(size_t remaining, Data const & d, C && callback) noexcept
        {
            // auto count = previous_remaining - remaining;
            // previous_remaining = remaining;
            // //
            // if (not count) return;
            // //
            // auto current = (previous + count) % N;
            // //
            // auto c{ std::forward<C>(callback) };
            // if (current > previous)
            // {
            //     size_t len = (current - previous) % ( N + 1 );
            //     c( View{ &d[previous], len } );
            // }
            // else
            // {
            //     size_t rhs_len = N - previous;
            //     size_t lhs_len = current;
            //     //
            //     c( View{ &d[previous], rhs_len } );
            //     c( View{ &d[0],		   lhs_len } );
            // }
            // //
            // previous = current;
        }
	};
}