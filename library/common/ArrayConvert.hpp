#pragma once

#include "Char.hpp"
#include "FIFO.hpp"

#include <array>
#include <cmath>
#include <type_traits>

namespace General
{
	/**
	 * @brief A utility alias that results in a C style array from C++ style template arguments.
	 * 
	 * This can be helpful in argument deduction.
	 * 
	 * @tparam T The item type for the array.
	 * @tparam N The number of items in the array.
	 * @return The C style array type.
	 */
	template <typename T, std::size_t N>
	using CArray = T[ N ];
	/**
	 * @brief Constructs an std::array from a C style array.
	 * 
	 * This function allows arbitary indexes to be included in the std::array.
	 * No bounds checks are included.
	 * 
	 * @tparam T The type of the items.
	 * @tparam N The number of items in the array.
	 * @tparam IA The indexes to include in the output array.
	 * @param input The input C style array.
	 * @return std::array<char, sizeof...( IA )> The requested std::array.
	 */
	template <typename T, std::size_t N, std::size_t... IA>
	constexpr auto MakeArrayInternal( CArray<T, N> const & input, std::index_sequence<IA...> ) noexcept -> std::array<char, sizeof...( IA )> 
	{
		return {input[ IA ]...};
	}
	/**
	 * @brief This function converts C style array to std::array
	 * 
	 * @tparam T The item type.
	 * @tparam N The number of items.
	 * @param input The C style array to convert to std::array.
	 * @return decltype( auto ) The 
	 */
	template <typename T, std::size_t N>
	constexpr decltype( auto ) MakeArray( CArray<T, N> const &input ) noexcept
	{
		return MakeArrayInternal( input, std::make_index_sequence<N - 1>{} );
	}
	template <typename T, std::size_t N>
	constexpr decltype( auto ) MakeArray( std::array<T, N> const &input ) noexcept
	{
		return input;
	}
	/**
	 * @brief Tests whether a type is an array.
	 * 
	 * @tparam T The type to test.
	 */
	template <class T>
	struct IsArray : std::is_array<T>
	{};
	template <class T, std::size_t N>
	struct IsArray<std::array<T, N>> : std::true_type
	{};
	template <class T, std::size_t N>
	struct IsArray<Containers::FIFO<T, N>> : std::true_type
	{};
	template <class T>
	struct IsArray<T const> : IsArray<T>
	{};
	template <class T>
	struct IsArray<T volatile> : IsArray<T>
	{};
	template <class T>
	struct IsArray<T volatile const> : IsArray<T>
	{};
	template <class T>
	struct IsArray<T const &> : IsArray<T>
	{};
	template <class T>
	struct IsArray<T volatile &> : IsArray<T>
	{};
	template <class T>
	struct IsArray<T volatile const &> : IsArray<T>
	{};
	/**
	 * @brief Gets the properties of an array.
	 * 
	 * The properties retrieved are:
	 * - type : array element type.
	 * - count : The number of elements.
	 * 
	 * @tparam T 
	 */
	template <typename T>
	struct ArrayProperties
	{
		template <class Array>
		using array_element_t = std::decay_t<decltype( std::declval<Array>()[ 0 ] )>;

		template <typename t_t, std::size_t N>
		static constexpr std::size_t array_size( std::array<t_t, N> input ) noexcept
		{
			return N;
		}
		static constexpr std::size_t count = array_size( T() );
		using type                      = array_element_t<T>;
	};
	/**
	 * @brief Reverse the items in an array.
	 * 
	 * @tparam T The type of the elements.
	 * @tparam N The number of items in the array.
	 * @param input The array to reverse.
	 * @param start The first item of the range to reverse.
	 * @param length The number of items in the range to reverse.
	 */
	template <typename T, std::size_t N>
	void Reverse( std::array<T, N> &input, std::size_t start, std::size_t length ) noexcept
	{
		int i = start, j = ( length + start ) - 1;
		while ( i < j ) std::swap( input[ i++ ], input[ j-- ] );
	}
	/**
	 * @brief Move all the items in the array forward by a specified offset.
	 * 
	 * @tparam T The element type.
	 * @tparam N The number of elements in the array.
	 * @tparam length_N The range to deal with when shifting.
	 * @param input The array to modify.
	 * @param offset The offset to move the items.
	 * @param padding The value to place in the empty array slots.
	 */
	template <typename T, std::size_t N, std::size_t length_N = N>
	void ShiftRight( std::array<T, N> & input, std::size_t offset = 0u, T padding = T() ) noexcept
	{
		auto const stop = std::min( N, length_N + offset );
		for ( auto i = stop - 1u; i > offset; i-- )
			std::swap( input[ i - 1u ], input[ i ] );
		input[ offset ] = padding;
	}
	/**
	 * @brief Move all the items in the array backwards by a specified offset.
	 * 
	 * @tparam T The item type.
	 * @tparam N The number of items in the input array.
	 * @tparam length_N The number of items to mutate within the array. 
	 * @param input The array to mutate.
	 * @param offset The first index to modify.
	 * @param padding The value to assign to empty slots in the array.
	 */
	template <typename T, std::size_t N, std::size_t length_N = N>
	void ShiftLeft( std::array<T, N> & input, std::size_t offset = 0u, T padding = (T)0 ) noexcept
	{
		auto const stop = length_N + offset;
		for ( auto i = offset; i < stop - 1u; ++i )
			std::swap( input[ i ], input[ i + 1u ] );
		input[ stop - 1u ] = padding;
	}
	/**
	 * @brief Removes an item in an array and shifts the array so that values remain contigious.
	 * 
	 * @tparam T The type of the items in the array.
	 * @tparam N The number of items in the input array.
	 * @param input The input 
	 * @param position The index of the item to remove
	 * @param padding The value to place in the empty slot in the array.
	 */
	template <typename T, std::size_t N>
	void RemoveAtIndex( std::array<T, N> & input, std::size_t position, T const padding = (T)0 ) noexcept
	{
		if ( position < N )
		{
			auto const start = position + 1;
			if ( start < N )
			{
				auto const length = N - start;
				for ( std::size_t i = position; i < N - 1u; ++i )
				{
					std::swap( input[ i ], input[ i + 1u ] );
				}
				input[ N - 1u ] = padding;
			}
			else
			{
				input[ position ] = padding;
			}
		}
	}
	/**
	 * @brief Converts an array to an array.
	 * 
	 * This can be used to insert one array into another.
	 * 
	 * @tparam N The number of items in the output array.
	 * @tparam C The number of items in the input array.
	 * @param input The input array, not modified.
	 * @param output The output array, this is what is mutated from the offset onwards.
	 * @param offset The offset for the output array.
	 * @param padding The value to use as padding when padding is required.
	 * @return std::size_t The number of items inserted.
	 */
	template <std::size_t N, std::size_t C>
	std::size_t atoa( std::array<char, C> const & input, std::array<char, N> & output, std::size_t offset = 0u, char padding = ' ' ) noexcept
	{
		//Unused at this point, reserved for future use.
		(void)padding;
		//
		auto length = std::min( C + offset, N );
		for ( auto i = offset; i < length; ++i )
		{
			auto const from_index = i - offset;
			auto const to_index   = i;
			auto const item       = input[ from_index ];
			if ( item == '\0' )
			{
				length = i;
				break;
			}
			output[ to_index ] = item;
		}
		return length - offset;
	}
	/**
	 * @brief Converts an array to an FIFO.
	 * 
	 * This can be used to insert one array into another.
	 * 
	 * @tparam N The number of items in the output array.
	 * @tparam C The number of items in the input array.
	 * @param input The items to insert into the FIFO.
	 * @param output The FIFO to insert into.
	 * @param offset As FIFO always inserts at the end, this does nothing. Its implemented for consistancy among overloads.
	 * @return std::size_t The number of items that were successfully inserted.
	 */
	template <std::size_t N, std::size_t C>
	std::size_t atoa( std::array<char, C> const & input, Containers::FIFO<char, N> & output, std::size_t offset = 0u ) noexcept
	{
		size_t i = 0;

		for ( ; i < C; ++i )
			if ( !output.Push( input[ i ] ) )
				return i;

		return i;
	}
	/**
	 * @brief Inserts items from one FIFO into another FIFO.
	 * 
	 * @tparam N The capacity of the output FIFO.
	 * @tparam C The capacity of the input FIFO.
	 * @param input The input FIFO.
	 * @param output The output FIFO.
	 * @param offset As FIFO always inserts at the end, this does nothing. Its implemented for consistancy among overloads.
	 * @return std::size_t The number of items successfully inserted.
	 */
	template <std::size_t N, std::size_t C>
	std::size_t atoa( Containers::FIFO<char, C> const &input, Containers::FIFO<char, N> &output, size_t offset = 0 ) noexcept
	{
		std::size_t length{0u};
		for ( std::size_t i = 0; i < input.Size(); ++i)
		{
			if ( output.Push( input[i] ) )
				++length;
			else
				return length;
		}
		return length;
	}
	/**
	 * @brief Adds single character to a fifo. 
	 * 
	 * This is implemented for consistancy of implementation.
	 * 
	 * @tparam N Capacity of the FIFO.
	 * @param input The input character to insert.
	 * @param output The output FIFO.
	 * @param offset The offset in the output container.
	 * @return std::size_t 1 on success, 0 on failure.
	 */
	template <std::size_t N>
	std::size_t ctoa( char input, std::array<char, N> & output, size_t offset = 0 ) noexcept
	{
		if (offset < N)
		{
			output[offset] = input;
			return 1;
		}
		return 0;
	}
	/**
	 * @brief Adds single character to a fifo. 
	 * 
	 * This is implemented for consistancy of implementation.
	 * 
	 * @tparam N Capacity of the FIFO.
	 * @param input The input character to insert.
	 * @param output The output FIFO.
	 * @param offset The offset in the output container (does nothing for FIFO).
	 * @return std::size_t 1 on success, 0 on failure.
	 */
	template <std::size_t N>
	std::size_t ctoa( char input, Containers::FIFO<char, N> & output, size_t offset = 0 ) noexcept
	{
		return output.Push( input ) ? 1 : 0;
	}
}