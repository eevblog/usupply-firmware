#pragma once

#include "FIFO.hpp"
#include "Meta.hpp"
#include <cmath>

namespace General
{
	/**
	 * @brief An expressive function that takes the input as a percentage and outputs a decimal.
	 * 
	 * @tparam T The type of the input.
	 * @param input The input.
	 * @return T input / 100.
	 */
	template <typename T>
	constexpr auto Percent( T input ) noexcept -> T
	{
		return input / (T)100;
	}
	/**
	 * @brief Checks whether a floating point value is close to an expected value.
	 * 
	 * @tparam T The floating point type (double, long double, float, short double etc...)
	 * @param value The value
	 * @param expected The expected value.
	 * @param tolerance The tolerance of the comparison.
	 * @return true The value is within the specified tolerance.
	 * @return false The value is outside of the specified tolerance.
	 */
	template <typename T>
	constexpr bool WithinTolerance( T const & value, Mirror_t<T> const expected, Mirror_t<T> tolerance ) noexcept
	{
		auto tol = Percent( tolerance );
		auto min = expected * ( (T)1 - tol );
		auto max = expected * ( (T)1 + tol );
		return ( min <= value ) && ( value <= max );
	}
	/**
	 * @brief Returns the absolute value of a value.
	 * 
	 * @tparam T The value type.
	 * @param value The value 
	 * @return constexpr auto 
	 */
	template <typename T>
	constexpr auto Abs( T const &value ) noexcept -> T
	{
		return ( value < 0 ) ? -value : value;
	}
	/**
	 * @brief Performs a division and rounds to the nearst whole number.
	 * 
	 * @tparam T The type of the elements.
	 * @param numerator The numerator of the division.
	 * @param denominator The denominator of the division.
	 * @return T The rounded division.
	 */
	template <typename T>
	constexpr auto RoundedDivide( T const & numerator, Mirror_t<T> const & denominator ) noexcept -> T
	{
		static_assert( std::is_signed_v<T> || std::is_unsigned_v<T> || std::is_floating_point_v<T>, "The rounded divide function only works with integers." );

		if constexpr ( std::is_floating_point_v<T> )
			return std::round( numerator / denominator );

		if constexpr ( std::is_signed_v<T> )
			return ( ( numerator < 0 ) ^ ( denominator < 0 ) ) ? ( ( numerator - denominator / 2 ) / denominator ) : ( ( numerator + denominator / 2 ) / denominator );

		if constexpr ( std::is_unsigned_v<T> )
			return ( numerator + ( denominator / 2u ) ) / denominator;
	}
	/**
	 * @brief Performs a division and rounds up the result to the next whole number.
	 * 
	 * @tparam T The type of the elements.
	 * @param numerator The numerator.
	 * @param denominator The denominator.
	 * @return T The result of the division with the remainder rounded up.
	 */
	template <typename T>
	constexpr auto CeilDivide( T const &numerator, Mirror_t<T> const &denominator ) noexcept -> T
	{
		static_assert( std::is_signed_v<T> || std::is_unsigned_v<T> || std::is_floating_point_v<T>, "The rounded divide function only works with integers." );

		if constexpr ( std::is_floating_point_v<T> )
			return std::ceil( numerator / denominator );

		if constexpr ( std::is_unsigned_v<T> )
			return ( numerator + ( denominator - 1u ) ) / denominator;

		if constexpr ( std::is_signed_v<T> )
		{
			if ( ( numerator < 0 ) ^ ( denominator < 0 ) )
				return ( numerator / denominator );
			else
			{
				//Answer is positive, might as well use abs
				auto const num   = Abs( numerator );
				auto const den = Abs( denominator );
				return ( ( num + ( den - 1 ) ) / den );
			}
		}
	}
	/**
	 * @brief Divides the numerator and denominator and discards any remainder.
	 * 
	 * @tparam T The type of the elements
	 * @param numerator The numerator of the division.
	 * @param denominator The denomintor of the division.
	 * @return T The result of the division.
	 */
	template <typename T>
	constexpr auto FloorDivide( T const &numerator, Mirror_t<T> const &denominator ) noexcept -> T
	{
		static_assert( std::is_signed_v<T> || std::is_unsigned_v<T> || std::is_floating_point_v<T>, "The rounded divide function only works with integers." );

		if constexpr ( std::is_floating_point_v<T> )
			return std::floor( numerator / denominator );

		if constexpr ( std::is_unsigned_v<T> )
			return numerator / denominator;

		if constexpr ( std::is_signed_v<T> )
		{
			bool const negative_result = ( numerator < 0 ) ^ ( denominator < 0 );
			if ( negative_result )
				return -( ( Abs( numerator ) + denominator - 1 ) / Abs( denominator ) );
			else
				return ( numerator / denominator );
		}
	}
	/**
	 * @brief Compile time function to calculate the mode of all elements in an array.
	 * 
	 * @tparam Container The container type.
	 * @tparam T The element type.
	 * @tparam N The container capacity.
	 * @param input The container.
	 * @return T The mode of all the elements in the container.
	 */
	template <template <typename, unsigned> class Container, typename T, unsigned N>
	constexpr T Mode( Container<T, N> const &input ) noexcept
	{
		T    output{};
		auto max_count = 0u;

		for ( auto i = 0u; i < input.Size(); ++i )
		{
			auto count = 1u;
			for ( auto u = 0u; u < input.Size(); ++u )
				if ( i != u )
					if ( input[ i ] == input[ u ] )
						++count;
			if ( count > max_count )
			{
				output    = input[ i ];
				max_count = count;
			}
		}

		return output;
	}
	/**
	 * @brief Compile time function to calculate the sum of all elements in a container.
	 * 
	 * @tparam Container The container type.
	 * @tparam T The element type.
	 * @tparam N The capacity of the container.
	 * @param input The container.
	 * @return T The sum of all elements in the container.
	 */
	template <template <typename, unsigned> class Container, typename T, unsigned N>
	constexpr T Sum( Container<T, N> const &input ) noexcept
	{
		auto output = (T)0;
		for ( auto i = 0u; i < input.Size(); ++i )
			output += input[ i ];
		return output;
	}
	/**
	 * @brief Compile time function to calculate the average of all elements in a container.
	 * 
	 * @tparam Container The container type.
	 * @tparam T The container element type.
	 * @tparam N The capacity of the container.
	 * @param input The container.
	 * @return T The average of the elements in the container.
	 */
	template <template <typename, unsigned> class Container, typename T, unsigned N>
	constexpr T Average( Container<T, N> const &input ) noexcept
	{
		return RoundedDivide<T>( Sum( input ), (T)N );
	}
	/**
	 * @brief Compile time function to calculate Base ^ input of an integer.
	 * 
	 * @tparam T The integer type.
	 * @tparam Base The base to use for the function.
	 * @param input The input.
	 * @return T The result, input ^ Base.
	 */
	template <typename T = unsigned, T Base = (T)10>
	constexpr auto Power( T input ) noexcept -> T
	{
		T output = (T)1;
		while ( input-- > 0 ) output *= Base;
		return output;
	}
	/**
	 * @brief Compile time function that calculates 2 ^ input of an integer.
	 * 
	 * @tparam T The value type.
	 * @param value The value.
	 * @return T The result, 2 ^ input.
	 */
	template <typename T>
	constexpr auto Power2( T const &value ) noexcept -> T
	{
		return std::forward<T>( MaskBit<T>( value ) );
	}
	/**
	 * @brief A compile time function that calculations 10 ^ input of an integer.
	 * 
	 * @tparam T The integer type.
	 * @param input The exponent of the power function.
	 * @return T 10 ^ input.
	 */
	template <typename T = unsigned>
	constexpr auto Power10( T input ) noexcept -> T
	{
		return Power<T, 10u>( input );
	}
	/**
	 * @brief A compile time version of log used for integers only.
	 * 
	 * @tparam T The type of the input.
	 * @param input The input value.
	 * @return T The Log base 10 of the input integer.
	 */
	template <typename T = unsigned>
	constexpr auto Log10( T input ) noexcept -> T
	{
		T items[] = {10u, 100u, 1000u, 10000u, 100000u, 1000000u, 10000000u, 100000000u, 1000000000u};
		T output  = 0u;

		for ( auto item : items )
		{
			if ( item >= input ) return output;
			++output;
		}

		return output;
	}
	/**
	 * @brief An expressive function that converts GHz to Hz.
	 * 
	 * @tparam T The type of the input. 
	 * @param input The value in GHz
	 * @return T The value in Hz.
	 */
	template <typename T>
	constexpr auto GHz( T input ) noexcept -> T
	{
		return input * (T)1'000'000'000;
	}
	/**
	 * @brief An expressive function that converts MHz to Hz.
	 * 
	 * @tparam T The type of the input. 
	 * @param input The value in MHz
	 * @return T The value in Hz.
	 */
	template <typename T>
	constexpr auto MHz( T input ) noexcept -> T
	{
		return input * (T)1'000'000;
	}
	/**
	 * @brief An expressive function that converts KHz to Hz.
	 * 
	 * @tparam T The type of the input. 
	 * @param input The value in KHz
	 * @return T The value in Hz.
	 */
	template <typename T>
	constexpr auto KHz( T input ) noexcept -> T
	{
		return input * (T)1'000;
	}
	/**
	 * @brief An expressive function to indicate that the result should read as a frequency.
	 * 
	 * @tparam T The type of the input.
	 * @param input The value.
	 * @return T The value.
	 */
	template <typename T>
	constexpr auto Hz( T input ) noexcept -> T
	{
		return input;
	}
	/**
	 * @brief Gets the minimum of two given arguments.
	 * 
	 * @tparam T The type of the arguments
	 * @param lhs An argument.
	 * @param rhs An argument.
	 * @return T The minimum of lhs and rhs.
	 */
	template <typename T>
	constexpr auto Minimum( T const &lhs, T const &rhs ) noexcept -> T
	{
		return ( lhs > rhs ) ? rhs : lhs;
	}
	/**
	 * @brief Gets the maximum of two given arguments.
	 * 
	 * @tparam T The type of the arguments
	 * @param lhs An argument.
	 * @param rhs An argument.
	 * @return T The maximum of lhs and rhs.
	 */
	template <typename T>
	constexpr auto Maximum( T const &lhs, T const &rhs ) noexcept -> T
	{
		return ( lhs > rhs ) ? lhs : rhs;
	}
	/**
	 * @brief Tests whether a value is within a defined range.
	 * 
	 * @tparam T The type of the value.
	 * @param value The value to test.
	 * @param minimum The minimum value.
	 * @param maximum The maximum value.
	 * @return true If the value is between the values.
	 * @return false If the value is not between the values.
	 */
	template <typename T>
	constexpr auto Between( T const &value, Mirror_t<T> const &minimum, Mirror_t<T> const &maximum ) noexcept -> bool
	{
		return ( minimum <= value ) && ( value <= maximum );
	}
	/**
	 * @brief Ensures that a value is outside of a defined range.
	 * 
	 * When given a value between minimum and maximum the result is the closest of minimum and maximum.
	 * If the value is exactly in the middle, the result is the minimum.
	 * 
	 * @tparam T The type of the value.
	 * @param value The value.
	 * @param minimum The minimum value.
	 * @param maximum The maximum value.
	 * @return T The unclamped value (see brief).
	 */
	template <typename T>
	constexpr T UnClamp( T const & value, Mirror_t<T> const & minimum, Mirror_t<T> const & maximum ) noexcept
	{
		if ( Between<T>( value, minimum, maximum ) )
		{
			auto mid = ( minimum + maximum ) / T{2};
			return ( value > mid ) ? maximum : minimum;
		}
		return value;
	}
}