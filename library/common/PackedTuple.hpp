#pragma once

#pragma pack(push,1)
/**
 * @brief A type that can always be inherited from given an input type.
 * 
 * @tparam T The type to store.
 * @tparam V An index for the 
 */
template <typename T, std::size_t V>
struct InheritT
{
    T Value;
};
/**
 * @brief A helper class to allow classes to inherit from duplicate classes.
 * 
 * @tparam Args The types to inherit from.
 */
template <typename ... Args>
struct UniqueInheritImpl;
/**
 * 
 */
template <std::size_t ... I, typename ... Endpoints>
struct UniqueInheritImpl<std::index_sequence<I...>, Endpoints ...> : InheritT< Endpoints, I > ... {};
/**
 * 
 */
template <typename ... Args>
using UniqueArgs = UniqueInheritImpl<std::make_index_sequence<sizeof...(Args)>, Args...>;
/**
 * @brief An inheritable tuple.
 * 
 * @tparam Args The values to store.
 */
template <typename ... Args>
struct PackedTuple : UniqueArgs<Args...>
{

};
template<typename...Args>
PackedTuple(Args...)->PackedTuple<Args...>;