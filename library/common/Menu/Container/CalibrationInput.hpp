#pragma once

#include "Calibration.hpp"
#include "Labelled.hpp"
#include "RawInput.hpp"
#include <optional>

namespace Menu
{
	/**
	 * @brief The calibration input state.
	 */
	enum CalibrationState : unsigned
	{
		Expected         = 0,
		SelectedExpected = 1,
		Measured         = 2,
		SelectedMeasured = 3
	};
	/**
	 * @brief A menu item that allows the input of calibration data points.
	 * 
	 * @tparam BufferSize 
	 * @tparam N_LABEL 
	 * @tparam N_EXP 
	 * @tparam N_MEAS 
	 * @tparam calibration_function_t 
	 */
	template <unsigned BufferSize, unsigned N_LABEL, unsigned N_EXP, unsigned N_MEAS, typename calibration_function_t>
	class CalibrationInput : public SelectableMenuItem<BufferSize>
	{
	private:
		/**
		 * @brief This stores the input state machine.
		 */
		CalibrationState m_State = Expected;
	
		using base_t        = SelectableMenuItem<BufferSize>;
		using buffer_t      = typename SelectableMenuItem<BufferSize>::buffer_t;
		using exp_array_t   = std::array<char, N_EXP>;
		using meas_array_t  = std::array<char, N_MEAS>;
		using label_array_t = std::array<char, N_LABEL>;

		General::Sample                         m_Sample;
		Label<BufferSize, N_LABEL>              m_Label;
		Label<BufferSize, N_EXP>                m_ExpectedLabel;
		Label<BufferSize, N_MEAS>               m_MeasuredLabel;
		RawInput<BufferSize, NumberInputKernal> m_Input;
		/**
		 * @brief Get the current item in the calibration input state machine
		 * 
		 * @return RawMenuItem<BufferSize>& The current item.
		 */
		RawMenuItem<BufferSize> & Item() noexcept
		{
			switch ( m_State )
			{
			case SelectedExpected:
			case SelectedMeasured:
				return m_Input;
			case Expected:
				return m_ExpectedLabel;
			case Measured:
				return m_MeasuredLabel;
			default:
				return m_Label;
			};
		}
		/**
		 * @brief Get the current calibration value.
		 * 
		 * @return float The value to get.
		 */
		float GetValue() noexcept
		{
			buffer_t buffer;
			float    value;
			m_Input.Render( buffer, false );
			General::atof( buffer, value );
			return value;
		}
		/**
		 * @brief The callback that is called once a calibration point is entered.
		 */
		calibration_function_t m_Callback;
	public:
		/**
		 * @brief Construct a new Calibration Input object
		 * 
		 * @param label The default label of the calibration input.
		 * @param expected_label The label of the expected field input.
		 * @param actual_label The label of the actual value field input.
		 * @param callback the callback to run when a calibration point is input.
		 */
		CalibrationInput( label_array_t const &label, exp_array_t const &expected_label, meas_array_t const &actual_label, calibration_function_t && callback ) noexcept:
		    m_Label			( label ),
		    m_ExpectedLabel	( expected_label ),
		    m_MeasuredLabel	( actual_label ),
		    m_Callback		( std::forward<calibration_function_t>(callback) )
		{}
		/**
		 * @brief Construct a new Calibration Input object
		 * 
		 * @tparam Args The input arguement types passed into the other constructor, see docs.
		 * @param args The input arguments passed into the other constructor.
		 */
		template <typename... Args>
		CalibrationInput( General::SizeT<BufferSize>, Args &&... args ) noexcept:
			CalibrationInput{ std::forward<Args>( args )... }
		{}
		/**
		 * @brief Reset the calibration input, state machine and the calibration values.
		 */
		virtual void Reset() noexcept
		{
			base_t::Deselect();
			m_Sample = {0, 0};
			m_State  = Expected;
			m_Input.Reset();
		}
		/**
		 * @brief The navigation to use when the calibration input is selected.s
		 * 
		 * @param input The keycode to process.
		 * @return NavigateReturn See NavigateReturn docs.
		 */
		virtual NavigateReturn SelectedNavigate( KeyCodes input ) noexcept
		{
			switch ( m_State )
			{
			case Expected:
				m_Input.Reset();
				if ( auto out = m_Input.Navigate( input ); out )
				{
					m_State = SelectedExpected;
					return Handled;
				}
				break;
			case SelectedExpected:
				if ( auto out = m_Input.Navigate( input ); !out )
				{
					if ( input == KeyCodes::Enter )
					{
						m_Sample.Expected = GetValue();
						m_State           = Measured;
						return Handled;
					}
					else break;
				}
				else
					return out;
			case Measured:
				m_Input.Reset();
				if ( auto out = m_Input.Navigate( input ); out )
				{
					m_State = SelectedMeasured;
					return Handled;
				}
				break;
			case SelectedMeasured:
				if ( auto out = m_Input.Navigate( input ); !out )
				{
					if ( input == KeyCodes::Enter )
					{
						m_Sample.Measured = GetValue();
						m_State           = Expected;
						m_Callback(m_Sample);
						base_t::Deselect();
					}
					else break;
				}
				else
					return out;
			};

			return Unhandled;
		}
		/**
		 * @brief The render function to use when the menu item is selected.
		 * 
		 * @param output The render target.
		 * @param use_cursor Whether or not to use cursors. 
		 * @return unsigned The number of characters rendered.
		 */
		virtual unsigned SelectedRender( buffer_t &output, bool use_cursor ) noexcept
		{
			return Item().Render( output, use_cursor );
		}
		/**
		 * @brief The render function to use when the menu item is not selected.
		 * 
		 * @param output The render target.
		 * @param use_cursor Whether or not to use cursors. 
		 * @return unsigned The number of characters rendered.
		 */
		virtual unsigned DeselectedRender( buffer_t &output, bool use_cursor ) noexcept
		{
			return m_Label.Render( output, use_cursor );
		}
	};
	/**
	 * @brief A template deduction guide for the CalibrationInput menu item.
	 * 
	 * @tparam BufferSize The length of the render target (in characters).
	 * @tparam N_LABEL The number of characters in the default label.
	 * @tparam N_EXP The number of characters in the expected value label.
	 * @tparam N_MEAS The number of characters in the measured value label.
	 * @tparam calibration_function_t The callback function to run when a point is complete.
	 */
	template <unsigned BufferSize, unsigned N_LABEL, unsigned N_EXP, unsigned N_MEAS, typename calibration_function_t>
	CalibrationInput(
	    General::SizeT<BufferSize>,
	    std::array<char, N_LABEL> const &,
	    std::array<char, N_EXP> const &,
	    std::array<char, N_MEAS> const &, 
		calibration_function_t const & )
	    ->CalibrationInput<BufferSize, N_LABEL, N_EXP, N_MEAS, calibration_function_t>;
}