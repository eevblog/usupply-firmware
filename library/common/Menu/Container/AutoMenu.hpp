#pragma once
#include "RawMenu.hpp"
#include <type_traits>

namespace Menu
{
	/**
	 * @brief A class used to deduce the possible template parameters in a RawMenu.
	 * 
	 * @tparam BufferSize The render target length( number of characters ).
	 * @tparam Items the different menu items in the menu.
	 */
	template < unsigned BufferSize, typename... Items >
	class AutoMenu : public RawMenu< BufferSize, sizeof...( Items ) >
	{
	private:
		static constexpr auto count = sizeof...(Items);
		using base_tuple_t			= std::tuple<Items...>;
		using base_menu_t			= RawMenu<BufferSize, count>;
		base_tuple_t m_Tuple;
		/**
		 * @brief Gets pointers to each of the items in the menu.
		 * 
		 * @tparam I The index of the item to get a pointer.
		 * @return decltype(auto) The pointer type.
		 */
		template < unsigned I >
		decltype(auto) Get() noexcept
		{
			return &( std::get<I>( m_Tuple ) );
		}
		/**
		 * @brief Construct a new Auto Menu object
		 * 
		 * @tparam Indexes 
		 * @param mover 
		 */
		template < unsigned ... Indexes >
		AutoMenu( std::index_sequence<Indexes...>, AutoMenu && mover ) noexcept: 
			base_menu_t	{ Get<Indexes>() ...		},
			m_Tuple		{ std::move(mover.m_Tuple)	}
		{}
		/**
		 * @brief Construct a new Auto Menu object
		 * 
		 * @tparam Indexes 
		 * @param items 
		 */
		template < unsigned ... Indexes >
		AutoMenu( std::index_sequence<Indexes...>, Items && ... items ) noexcept: 
			base_menu_t	{ Get<Indexes>() ...				},
			m_Tuple		{ std::move(items) ...	}
		{}
	public:
		/**
		 * @brief Construct a new Auto Menu object
		 * 
		 * @param input The raw menu item to move into this one.
		 */
		AutoMenu( AutoMenu && input ) noexcept: AutoMenu( std::make_index_sequence<count>{}, std::forward<AutoMenu>( input ) ) {}
		/**
		 * @brief Construct a new Auto Menu object
		 * 
		 * @param input The items contained in the AutoMenu.
		 */
		AutoMenu( Items && ... input ) noexcept: AutoMenu( std::make_index_sequence<count>{}, std::forward<Items>( input )... ) {}
		/**
		 * @brief Construct a new Auto Menu object
		 * 
		 * @note This is used for template arguemnt deduction, this is the same as the other Items... constructor.
		 * @param input The Items contained in the AutoMenu.
		 */
		AutoMenu( General::SizeT<BufferSize>, Items&&... input ) noexcept: AutoMenu( std::forward<Items>( input )... ) {}
	};
	/**
	 * @brief The template deduction guide.
	 * 
	 * @tparam BufferSize The render target length.
	 * @tparam Items The types of the items in the AutoMenu.
	 */
	template <unsigned BufferSize, typename... Items>
	AutoMenu(General::SizeT<BufferSize>, Items&&... input)->AutoMenu<BufferSize, std::remove_reference_t<Items>...>;
	/**
	 * @brief An AutoMenu combined with the Labelled class.
	 * 
	 * This a menu that when deselected displays its label, and when selected displays its menu items.
	 * 
	 * @tparam BufferSize The render target length
	 * @tparam N The length of the label.
	 * @tparam Items The AutoMenu items.
	 */
	template <unsigned BufferSize, unsigned N, typename... Items>
	struct LabelledAutoMenu : Labelled<BufferSize ,AutoMenu<BufferSize, Items ...>, N>
	{
		/**
		 * @brief The base class type.
		 */
		using base_t = Labelled<BufferSize, AutoMenu<BufferSize, Items ...>, N>;
		/**
		 * @brief Inherit all the functions and constructors from the base class.
		 */
		using base_t::base_t;
		/**
		 * @brief Construct a new Labelled Auto Menu object
		 * 
		 * @tparam Args The items used to construct the Labelled<BufferSize, AutoMenu<BufferSize, Items ...>, N> base class.
		 * @param items The items used to construct the Labelled<BufferSize, AutoMenu<BufferSize, Items ...>, N> base class.
		 */
		template <typename... Args>
		LabelledAutoMenu( General::SizeT<BufferSize>, Args&&... items ) noexcept : base_t{ std::forward<Args>( items )... } {}
	};
	/**
	 * @brief The template deduction guide.
	 * 
	 * @tparam BufferSize The render target length.
	 * @tparam N The length of the label.
	 * @tparam Items The types of the items in the AutoMenu.
	 */
	template <unsigned BufferSize, unsigned N, typename... Items>
	LabelledAutoMenu( General::SizeT<BufferSize>, std::array<char, N> const&, Items&&... )->LabelledAutoMenu<BufferSize, N, std::remove_reference_t<Items>...>;
}