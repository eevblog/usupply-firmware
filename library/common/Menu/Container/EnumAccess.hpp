#pragma once

#include "Meta.hpp"
#include "MenuItem.hpp"
#include <tuple>

namespace Menu
{
	/**
	 * @brief Associates a key value with a particular menu item.
	 * 
	 * @tparam Menu The underlying menu.
	 * @tparam Key_t The key type.
	 */
	template <typename Menu, typename Key_t>
	struct KeyedItem : public Menu
	{
		using Menu::Menu;
		/**
		 * @brief Tests whether the item is the same 
		 * 
		 * @tparam T The type of the key to compare.
		 * @param key The key to compare with the item key.
		 * @return true The item has the same key.
		 * @return false The item doesn't have the same key.
		 */
		template <typename T>
		constexpr bool Match( T const & key ) const noexcept
		{
			using key_t		= typename std::decay_t<Key_t>::type;
			using input_t	= std::decay_t<T>;

			if constexpr (std::is_same_v<input_t, key_t>) return ( key == Key_t::value );
			else return false;
		}
		/**
		 * @brief Construct a new Keyed Item object
		 * 
		 * @tparam Args The types to construct the base class menu item with.
		 * @param args The arguments to construct the base class menu item with.
		 */
		template <typename ... Args>
		KeyedItem(Key_t, Args && ... args) noexcept: 
			Menu( std::forward<Args>( args ) ... ) 
		{}
	};
	/**
	 * @brief A deduction guide that allows ommision of the template arguments from the top level code.
	 * 
	 * @tparam key The items key.
	 * @tparam Menu The underlying menu item.
	 */
	template <auto key, typename Menu>
	KeyedItem(General::Key<key>, Menu)->KeyedItem<Menu, General::Key<key>>;
	/**
	 * @brief Manages a set of KeyedItems so that a shortcut results in a particular menu item being active.
	 * 
	 * @tparam BufferSize The render target size.
	 * @tparam The different keyed items.
	 */
	template <std::size_t BufferSize , typename... Items>
	class EnumAccess;
	/**
	 * @brief Manages a set of KeyedItems so that a shortcut results in a particular menu item being active.
	 * 
	 * @tparam BufferSize The render target size.
	 * @tparam Key The type of the key used for all the menu items.
	 * @tparam Items The keyed items.
	 * @tparam Keys The keys for each item.
	 */
	template <std::size_t BufferSize, typename Key, typename... Items, typename ... Keys>
	class EnumAccess<BufferSize, Key, KeyedItem< Items, Keys > ... >: 
		public RawMenuItem<BufferSize>
	{
		using base_t	= RawMenuItem<BufferSize>;
		using tuple_t	= std::tuple<KeyedItem< Items, Keys > ...>;
		using Key_t		= Key;
	public:
		using buffer_t	= typename base_t::buffer_t;
	private:
		tuple_t		m_Items;
		/**
		 * @brief This key is a reference to an external key.
		 * 
		 * This is used to determine which menu item is active. 
		 */
		Key const &	m_Key;
		/**
		 * @brief Gets a menu item at the specified index.
		 * 
		 * @tparam I The index to retrieve
		 * @return auto& A reference to the item to retrieve.
		 */
		template <std::size_t I>
		auto & Get() noexcept
		{
			return std::get<I>( m_Items );
		}
		/**
		 * @brief Gets the menu item that has the requested key.
		 * 
		 * Each item is compared with the key stored in the member variable m_Key.
		 * 
		 * @tparam I The maximum index to check.
		 * @return RawMenuItem<BufferSize>& The requested item.
		 */
		template <std::size_t I = sizeof...( Items ) - 1>
		RawMenuItem<BufferSize> & AtKey() noexcept
		{
			auto & item = Get<I>();
			if constexpr (I == 0)
				return item;
			else
			{
				return (item.Match(m_Key))? item : AtKey<I - 1>();
			}
		}
	public:
		/**
		 * @brief Construct a new Enum Access object
		 * 
		 * @tparam ItemsT The keyed menu item types.
		 * @param key The key used used to match against the keyed items.
		 * @param items The menu items.
		 */
		template <typename ... ItemsT>
		EnumAccess( Key const & key, ItemsT &&... items ) noexcept:
			m_Items	{ std::forward<ItemsT>( items )... },
			m_Key	{ key }
		{}
		/**
		 * @brief Construct a new Enum Access object
		 * 
		 * @tparam ItemT See docs for other constructor.
		 * @param key See docs for other constructor.
		 * @param items See docs for other constructor.
		 */
		template <typename ... ItemT>
		EnumAccess( General::SizeT<BufferSize>, Key const& key, ItemT &&... items ) noexcept: 
			EnumAccess( key, std::forward<ItemT>( items )... ) 
		{}
		/**
		 * @brief Renders the currently active item into the render target.
		 * 
		 * @param buffer The render target.
		 * @param use_cursor Whether or not to use cursors.
		 * @return unsigned The number of characters rendered.
		 */
		virtual unsigned Render( buffer_t & buffer, bool use_cursor ) noexcept
		{
			return AtKey().Render( buffer, use_cursor );
		}
	};
	/**
	 * @brief A deduction guide to allow template arguments to be omitted from construction.
	 * 
	 * @tparam BufferSize The render target size.
	 * @tparam Key The key type to use.
	 * @tparam Items The different KeyedMenu item types.
	 * @tparam Key_t The key type.
	 */
	template <std::size_t BufferSize, typename Key, typename... Items, typename ... Key_t>
	EnumAccess( General::SizeT<BufferSize>, Key, KeyedItem< Items, Key_t > ...  )->EnumAccess<BufferSize, Key, KeyedItem< Items, Key_t > ... >;
}