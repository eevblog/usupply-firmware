#pragma once

#include "Labelled.hpp"
#include "RawInput.hpp"
#include "RawValue.hpp"

namespace Menu
{
	/**
	 * @brief A RawEdittable menu item is one that displays a value by default and when selected allows editting of the item.
	 * 
	 * @tparam BufferSize The render target length.
	 * @tparam T The type that the edittable is representing (float, double, bool etc...)
	 */
	template <unsigned BufferSize, typename T>
	class RawEditable : public RawMenuItem<BufferSize>
	{
	private:
		using base_t   = RawMenuItem<BufferSize>;
		using buffer_t = typename base_t::buffer_t;
		using kernal_t = typename GetKernal<std::decay_t<T>>::type;

		RawValue<BufferSize, T>			m_Value;
		RawInput<BufferSize, kernal_t>	m_Input;
	public:
		/**
		 * @brief Construct a new Raw Editable object
		 * 
		 * @param value A reference to the value that will be edittable.
		 */
		RawEditable( T & value ) noexcept :
			m_Value( value )
		{
			Load();
		}
		/**
		 * @brief Construct a new Raw Editable object
		 * 
		 * @tparam SizeT<BufferSize> The render target information.
		 * @param value A reference to the value that will be editted.
		 */
		RawEditable(General::SizeT<BufferSize>, T & value) noexcept:
			RawEditable(value)
		{}
		/**
		 * @brief Load the input control from the value.
		 * 
		 * The loaded value will display when the item is selected.
		 */
		void Load() noexcept
		{
			buffer_t buffer{};
			if ( m_Value.Render( buffer, false ) > 0 )
				m_Input.Load( buffer );
		}
		/**
		 * @brief Load the value from the input control.
		 * 
		 * The loaded value will display when the item is not selected.
		 */
		void Save() noexcept
		{
			buffer_t buffer{};
			if (m_Input.Render(buffer, false) > 0)
				m_Value.Load( buffer );
		}
		/**
		 * @brief Reset the input and edittable controls.
		 */
		virtual void Reset() noexcept
		{
			m_Value.Reset();
			m_Input.Reset();
		}
		/**
		 * @brief Handle navigation.
		 * 
		 * When the item is selected editing can occur, otherwise it displays the referenced value.
		 * 
		 * @param input The keycode to process.
		 * @return NavigateReturn See docs for NavigateReturn.
		 */
		virtual NavigateReturn Navigate( KeyCodes input ) noexcept
		{
			if ( m_Input.Navigate( input ) )
				return Handled;
			else if (input == KeyCodes::Enter)
			{
				Save();
				return Finished;
			}
			return Unhandled;
		}
		/**
		 * @brief Render the edittable value to the render target.
		 * 
		 * @param output The render target.
		 * @param use_cursor Whether or not the edittable control uses cursor.
		 * @return unsigned The number of characters rendered.
		 */
		virtual unsigned Render( buffer_t& output, bool use_cursor ) noexcept
		{
			return m_Input.Render( output, use_cursor );
		}
		/**
		 * @brief Implicitly cast to a reference to the stored value.
		 * 
		 * @return T const & The stored value.
		 */
		operator T const &() const
		{
			return m_Value;
		}
	};
	/**
	 * @brief A deduction guide that allows automatic deduction of render target properties and the type of the refernce value.
	 * 
	 * @tparam BufferSize The render target information.
	 * @tparam T The referenced value's type.
	 */
	template <unsigned BufferSize, typename T>
	RawEditable(General::SizeT<BufferSize>, T&)->RawEditable<BufferSize, T>;
	/**
	 * @brief This is the same as raw edittable only with a default label.
	 * 
	 * This control will display a label instead of the value when not selected.
	 * 
	 * @tparam BufferSize THe length of the render target (in characters)
	 * @tparam T The type of the referenced value.
	 * @tparam N The length of the label.
	 */
	template <unsigned BufferSize, typename T, unsigned N>
	struct LabelledEditable : Labelled<BufferSize ,RawEditable<BufferSize, T>, N>
	{
		using base_t = Labelled<BufferSize ,RawEditable<BufferSize, T>, N>;
		using base_t::base_t;
		/**
		 * @brief Construct a new Labelled Editable object
		 * 
		 * @tparam Args The argument types to construct the labelled control.
		 * @param args The arguments to construct the labelled control.
		 */
		template <typename... Args>
		LabelledEditable( General::SizeT<BufferSize>, Args&&... args ) noexcept:
			base_t( std::forward<Args>( args )... )
		{}
	};
	/**
	 * @brief A deduction guide that allows automatic deduction of class template arguments.
	 * 
	 * The deduced types are:
	 * - Render target length (in characters)
	 * - The label length.
	 * - The referenced type.
	 * 
	 * @tparam BufferSize The render target information.
	 * @tparam N The label length.
	 * @tparam T The referenced value's type.
	 */
	template <unsigned BufferSize, typename T, unsigned N>
	LabelledEditable( General::SizeT<BufferSize>, std::array<char, N> const&, T& )->LabelledEditable<BufferSize, T, N>;
}