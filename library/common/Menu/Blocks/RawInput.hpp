#pragma once

#include "RawInputKernals.hpp"

namespace Menu
{
	/**
	 * @brief Manages a menuitem that supports input from a keypad or up-down buttons to allow users to input data and values.
	 * 
	 * @tparam BufferSize The number of characters in the render target.
	 * @tparam Kernal The underlying kernal, this determines the type of accepted inputs.
	 */
	template <unsigned BufferSize, typename Kernal>
	class RawInput : public RawMenuItem<BufferSize>
	{
	public:
		using array_t = std::conditional_t<Kernal::IsBlock, typename Kernal::type, Containers::FILO<char, BufferSize>>;
	protected:
		using base_t   = RawMenuItem<BufferSize>;
		using buffer_t = typename base_t::buffer_t;
		array_t  m_Data;
		unsigned m_Cursor;
		/**
		 * @brief A mutable member of RawInput, this doesn't change the underlying data in the strucutre
		 * only the way the data is displayed.
		 */
		mutable bool m_Blinky;
		/**
		 * @brief Determine whether the cursor is at the end of the render target.
		 * 
		 * @return true The cursor is at or beyond the end.
		 * @return false The cursor is before the end.
		 */
		bool AtEnd() const noexcept
		{
			return ( m_Cursor >= m_Data.Size() );
		}
		/**
		 * @brief Get the character at the cursor position.
		 * 
		 * @return char & The character
		 */
		char & Current() noexcept
		{
			if ( AtEnd() ) return m_Data.Back();
			return m_Data[ m_Cursor ];
		}
		/**
		 * @brief Increment the cursor position if possible.
		 * 
		 * Increments the cursor and skips if a skip character is found.
		 *  skipping swollows skipped characters on increment
		 * 
		 * @note Skip characters are defined by the Kernal, see kernal docs for information.
		 */
		void Increment() noexcept
		{
			if ( m_Cursor < m_Data.Size() )
			{
				++m_Cursor;
				while ( Kernal( Current() ).Skip() )
					if ( !m_Data.RemoveAt( m_Cursor ) || AtEnd() ) break;
			}
		}
		/**
		 * @brief Decrements the cursor and skips if a skip character is found.
		 */
		void Decrement() noexcept
		{
			if ( m_Cursor > 0u )
			{
				--m_Cursor;
				if ( Kernal( Current() ).Skip() )
					return Decrement();
			}
		}
		/**
		 * @brief Places a character at the current position in the buffer
		 * does nothing if buffer is full.
		 * 
		 * @param input The character to put at the current buffer position.
		 */
		void Put( char input ) noexcept
		{
			//Cursor tracks the end
			if ( AtEnd() )
			{
				//Insert new character and move cursor (if possible)
				if ( m_Data.Push( input ) )
					m_Cursor = m_Data.Size();
			}
			else
			{
				//Set current character and move cursor
				m_Data[ m_Cursor ] = input;
				Increment();
			}
		}
		/**
		 * @brief Clears every character after the cursor position.
		 * 
		 * @param clear_symbol The clear character.
		 */
		void ClearFromCursor(char clear_symbol = ' ') noexcept
		{
			//if (AtEnd()) return;
			for ( auto i = m_Cursor; i < m_Data.Capacity(); ++i )
				m_Data[ i ] = clear_symbol;
		}
		/**
		 * @brief Gets whether or not the cursor should be blinking.
		 * 
		 * @return true Render shuold blink the cursor.
		 * @return false Render should not blink the cursor.
		 */
		bool Blinky() const noexcept
		{
			return ( m_Blinky = !m_Blinky );
		}
		/**
		 * @brief General Navigation, inrements a to b, or 1 to 2 etc...
		 * also moves cursor forward and back characters
		 * @note Behaviour is determined by kernal.
		 * 
		 * @return NavigateReturn The result of the input (whether it was handled or not).
		 */
		NavigateReturn Up() noexcept
		{
			if constexpr ( Kernal::AllowUpDown )
			{
				if constexpr ( Kernal::IsBlock )
					m_Data = !m_Data;
				else
					Current() = ++Kernal( Current() );

				return Handled;
			}
			return Unhandled;
		}
		/**
		 * @brief The control input for the down arrow.
		 * @note Behaviour is determined by kernal.
		 * 
		 * @return NavigateReturn The result of the input (whether it was handled or not).
		 */
		NavigateReturn Down() noexcept
		{
			if constexpr ( Kernal::AllowUpDown )
			{
				if constexpr ( Kernal::IsBlock )
					m_Data = !m_Data;
				else
					Current() = --Kernal( Current() );

				return Handled;
			}
			return Unhandled;
		}
		/**
		 * @brief Handle left arrow input.
		 * @note Behaviour is determined by kernal.
		 * 
		 * @return NavigateReturn The result of the input (whether it was handled or not).
		 */
		NavigateReturn Left() noexcept
		{
			if constexpr ( Kernal::AllowLeftRight )
			{
				Decrement();
				return Handled;
			}
			return Unhandled;
		}
		/**
		 * @brief Handle left arrow input.
		 * @note Behaviour is determined by kernal.
		 * 
		 * @return NavigateReturn The result of the input (whether it was handled or not).
		 */
		NavigateReturn Right() noexcept
		{
			if constexpr ( Kernal::AllowLeftRight )
			{
				Increment();
				if ( AtEnd() ) Current() = Kernal();
				return Handled;
			}
			return Unhandled;
		}

	public:
		/**
		 * @brief Construct a new Raw Input object
		 * 
		 */
		RawInput() noexcept:
		    m_Cursor( 0 ),
		    m_Blinky( false ) 
		{}
		/**
		 * @brief Resets the input state.
		 * 
		 * The reset includes:
		 * - The blinky state.
		 * - The cursor position.
		 * - The data in the input.
		 */
		virtual void Reset() noexcept
		{
			m_Blinky = false;
			m_Cursor = 0;
			m_Data.Clear();
		}
		/**
		 * @brief Loads the value in the RawBuffer from a character array.
		 * 
		 * @param input The character array to load from.
		 */
		virtual void Load( buffer_t const& input ) noexcept
		{
			if constexpr ( Kernal::IsBlock )
				m_Data.FromString( input );
			else
			{
				Reset();
				for ( auto c : input )
				{
					if constexpr ( Kernal::AllowNumbers )
						if ( General::IsNumber( c ) )
							m_Data.Push( c );

					if constexpr ( Kernal::AllowLetters )
						if ( General::IsLetter( c ) )
							m_Data.Push( c );

					if constexpr ( Kernal::AllowDecimal )
						if ( ( c == '.' ) )
							m_Data.Push( c );
				}
			}
		}
		/**
		 * @brief Performs nagivation of the input, this could be typing a character or pressing up or down etc...
		 * 
		 * @param input The keycode for the key that was pressed, if you have multiple keys, just call this multiple times.
		 * @return NavigateReturn The navigate return state, see docs for NavigateReturn.
		 */
		virtual NavigateReturn Navigate( KeyCodes input ) noexcept
		{
			if constexpr ( Kernal::IsBlock )
			{
				switch ( input )
				{
				case KeyCodes::Up: return Up();
				case KeyCodes::Down: return Down();
				default: break;
				};
			}
			else
			{
				//Numbers
				if constexpr ( Kernal::AllowNumbers )
				{
					switch ( input )
					{
					//Allow signs
					case KeyCodes::Subtract: Put( '-' ); return Handled;
					case KeyCodes::Add: Put( '+' ); return Handled;

					// 0 to 9
					case KeyCodes::Numpad0:
					case KeyCodes::Num0: Put( '0' ); return Handled;
					case KeyCodes::Numpad1:
					case KeyCodes::Num1: Put( '1' ); return Handled;
					case KeyCodes::Numpad2:
					case KeyCodes::Num2: Put( '2' ); return Handled;
					case KeyCodes::Numpad3:
					case KeyCodes::Num3: Put( '3' ); return Handled;
					case KeyCodes::Numpad4:
					case KeyCodes::Num4: Put( '4' ); return Handled;
					case KeyCodes::Numpad5:
					case KeyCodes::Num5: Put( '5' ); return Handled;
					case KeyCodes::Numpad6:
					case KeyCodes::Num6: Put( '6' ); return Handled;
					case KeyCodes::Numpad7:
					case KeyCodes::Num7: Put( '7' ); return Handled;
					case KeyCodes::Numpad8:
					case KeyCodes::Num8: Put( '8' ); return Handled;
					case KeyCodes::Numpad9:
					case KeyCodes::Num9: Put( '9' ); return Handled;
					default: break;
					};
				}

				//Letters
				if constexpr ( Kernal::AllowLetters )
				{
					switch ( input )
					{
					case KeyCodes::A: Put( 'A' ); return Handled;
					case KeyCodes::B: Put( 'B' ); return Handled;
					case KeyCodes::C: Put( 'C' ); return Handled;
					case KeyCodes::D: Put( 'D' ); return Handled;
					case KeyCodes::E: Put( 'E' ); return Handled;
					case KeyCodes::F: Put( 'F' ); return Handled;
					case KeyCodes::G: Put( 'G' ); return Handled;
					case KeyCodes::H: Put( 'H' ); return Handled;
					case KeyCodes::I: Put( 'I' ); return Handled;
					case KeyCodes::J: Put( 'J' ); return Handled;
					case KeyCodes::K: Put( 'K' ); return Handled;
					case KeyCodes::L: Put( 'L' ); return Handled;
					case KeyCodes::M: Put( 'M' ); return Handled;
					case KeyCodes::N: Put( 'N' ); return Handled;
					case KeyCodes::O: Put( 'O' ); return Handled;
					case KeyCodes::P: Put( 'P' ); return Handled;
					case KeyCodes::Q: Put( 'Q' ); return Handled;
					case KeyCodes::R: Put( 'R' ); return Handled;
					case KeyCodes::S: Put( 'S' ); return Handled;
					case KeyCodes::T: Put( 'T' ); return Handled;
					case KeyCodes::U: Put( 'U' ); return Handled;
					case KeyCodes::V: Put( 'V' ); return Handled;
					case KeyCodes::W: Put( 'W' ); return Handled;
					case KeyCodes::X: Put( 'X' ); return Handled;
					case KeyCodes::Y: Put( 'Y' ); return Handled;
					case KeyCodes::Z: Put( 'Z' ); return Handled;
					default: break;
					};
				}

				//Decimal point
				if constexpr ( Kernal::AllowDecimal )
				{
					if ( input == KeyCodes::Decimal )
					{
						//Prevent a number starting with a decimal point, it is confusing
						if ( m_Cursor == 0 ) Put( '0' );
						Put( '.' );
						ClearFromCursor();
						//Clear characters after decimal
						return Handled;
					}
				}

				//Controls are always enabled
				//Navigation Keys
				switch ( input )
				{
				case KeyCodes::Up:		return Up();
				case KeyCodes::Down:	return Down();
				case KeyCodes::Left:	return Left();
				case KeyCodes::Right:	return Right();
				default:
					if (IsCancel(input))
					{
						m_Data.Clear();
						m_Cursor = 0;
					}
				};
			}
			return Unhandled;
		}
		/**
		 * @brief Renders the RawInput to the render target.
		 * 
		 * @param output The render target.
		 * @param use_cursor Whether or not to show cursor in the RawInput.
		 * @return unsigned The length that was rendered.
		 */
		virtual unsigned Render( buffer_t& output, bool use_cursor ) noexcept
		{
			if constexpr (Kernal::IsBlock)
			{
				UNUSED(use_cursor); //Suppress unused error message
				return General::fixed_btoa<BufferSize>( static_cast<std::underlying_type_t<typename decltype(m_Data)::enum_t>>(m_Data), output, m_Data.true_str, m_Data.false_str, 0 );
			}
			else
			{
				if ( use_cursor )
				{
					for ( auto i = 0u; i < BufferSize; ++i )
						output[ i ] = m_Data[ i ];

					if ( m_Cursor < BufferSize && Blinky() )
						output[ m_Cursor ] |= 0x80;
				}
				else
				{
					for ( auto i = 0u; i < BufferSize; ++i )
						output[ i ] = m_Data[ i ];
					m_Cursor = 0u;
					return BufferSize;
				}
				return m_Data.Size();
			}
		}
	};
	/**
	 * @brief A metaprogramming tool to choose the input kernal for the RawInput.
	 * 
	 * @tparam T The type used for the choice.
	 * @tparam void A meta-parameter.
	 */
	template<typename T, typename = void>
	struct GetKernal;
	template<typename T>
	struct GetKernal<T, std::enable_if_t<std::is_floating_point_v<T> || std::is_integral_v<T>>> 
	{
		using type = NumberInputKernal;
	};
	template <typename T>
	struct GetKernal<T, std::enable_if_t<General::is_boolean_v<T>>>
	{
		using type = BooleanInputKernal<T>;
	};
}