#pragma once

#include "Action.hpp"
#include "Labelled.hpp"
#include "RawEditable.hpp"

namespace Menu
{
	/**
	 * @brief 
	 * 
	 * @tparam BufferSize 
	 * @tparam ItemCount 
	 */
	template <unsigned BufferSize, unsigned ItemCount>
	class RawMenu : public RawMenuItem<BufferSize>
	{
		static_assert( ItemCount > 0, "Menu cannot be empty." );

	private:
		using base_t   = RawMenuItem<BufferSize>;
		using buffer_t = typename base_t::buffer_t;
		using item_t   = base_t* const;

		std::array<item_t, ItemCount> m_Items;
		int                           m_Current;

		auto Item()
		{
			return m_Items[ m_Current ];
		}
	public:
		/**
		 * @brief Construct a new Raw Menu object
		 * 
		 * @tparam Args 
		 * @param pItems 
		 */
		template <typename... Args>
		RawMenu( Args&&... pItems ) : m_Items{std::forward<Args>( pItems )...}, m_Current( 0 ) {}
		/**
		 * @brief Construct a new Raw Menu object
		 * 
		 * @tparam Args 
		 * @param input 
		 */
		template <typename... Args>
		RawMenu( General::SizeT<BufferSize>, Args&&... args ) : RawMenu( std::forward<Args>( args )... ) {}
		/**
		 * @brief 
		 * 
		 * @param input 
		 * @return NavigateReturn 
		 */
		virtual NavigateReturn Navigate( KeyCodes input ) noexcept
		{
			if ( Item()->Navigate( input ) )
				return Handled;

			else if ( input == KeyCodes::Up )
			{
				m_Current = ( m_Current + ItemCount + 1 ) % ItemCount;
				return Handled;
			}
			else if ( input == KeyCodes::Down )
			{
				m_Current = ( m_Current + ItemCount - 1 ) % ItemCount;
				return Handled;
			}
			return Unhandled;
		}
		virtual unsigned Render( buffer_t& output, bool use_cursor ) noexcept
		{
			return Item()->Render( output, use_cursor );
		}
	};
	/**
	 * @brief 
	 * 
	 * @tparam BufferSize 
	 * @tparam Args 
	 */
	template <unsigned BufferSize, typename... Args>
	RawMenu( General::SizeT<BufferSize>, Args&&... )->RawMenu<BufferSize, sizeof...( Args )>;
	/**
	 * @brief 
	 * 
	 * @tparam BufferSize 
	 * @tparam N 
	 * @tparam ItemCount 
	 */
	template <unsigned BufferSize, unsigned N, unsigned ItemCount>
	struct LabelledMenu : Labelled<BufferSize, RawMenu<BufferSize, ItemCount>, N>
	{
		using base_t = Labelled<BufferSize, RawMenu<BufferSize, ItemCount>, N>;
		using base_t::base_t;
		/**
		 * @brief Construct a new Labelled Menu object
		 * 
		 * @tparam Args 
		 * @param pInput 
		 */
		template <typename... Args>
		LabelledMenu( General::SizeT<BufferSize>, Args&&... pInput ) :
		    base_t( std::forward<Args>( pInput )... )
		{}
	};
	/**
	 * @brief 
	 * 
	 * @tparam BufferSize 
	 * @tparam N 
	 * @tparam Args 
	 */
	template <unsigned BufferSize, unsigned N, typename... Args>
	LabelledMenu( General::SizeT<BufferSize>, std::array<char, N> const&, Args&&... )->LabelledMenu<BufferSize, N, sizeof...( Args )>;
}