#pragma once

#include "Char.hpp"
#include "FILO.hpp"
#include "MenuItem.hpp"
#include <cmath>

namespace Menu
{
	/**
	 * @brief A RawInput kernal that only allows numbers and a decimla point as inputs.
	 * 
	 * This also supports cursor navigation using left and right.
	 */
	struct NumberInputKernal
	{
	private:
		char m_Value;
	public:
		using type                           = void;
		/**
		 * @brief Whether or not to allow number inputs 0-9.
		 */
		static constexpr auto AllowNumbers   = true;
		/**
		 * @brief Whether or not to allow decimal point inputs.
		 */
		static constexpr auto AllowDecimal   = true;
		/**
		 * @brief Whether or not to allow letters as inputs a-z or A-Z.
		 */
		static constexpr auto AllowLetters   = false;
		/**
		 * @brief Whether or not to treat the input as a single block
		 * 
		 * A block is something that can have a single button result in multple characters,
		 *  such as boolean True and False strings.
		 */
		static constexpr auto IsBlock        = false;
		/**
		 * @brief Whether or not to allow up and down keypresses.
		 */
		static constexpr auto AllowUpDown    = false;
		/**
		 * @brief Whether or not to allow cursor movement using left and right.
		 */
		static constexpr auto AllowLeftRight = true;
		/**
		 * @brief Construct a new Number Input Kernal object
		 * 
		 * @param value The inital value of the number kernal
		 */
		NumberInputKernal( char value = '0' ) noexcept:
		    m_Value( value )
		{}
		/**
		 * @brief Increment the number that is being processed (a single character)
		 * 
		 * @return NumberInputKernal& For chained operations.
		 */
		NumberInputKernal & operator++() noexcept
		{
			m_Value = General::FromNumber( General::ToNumber( m_Value ) + (std::int8_t)1 );
			return *this;
		}
		/**
		 * @brief Decrement the number that is being processed.
		 * 
		 * @return NumberInputKernal& For chained operations.
		 */
		NumberInputKernal & operator--() noexcept
		{
			m_Value = General::FromNumber( General::ToNumber( m_Value ) - (std::int8_t)1 );
			return *this;
		}
		/**
		 * @brief Decimal points are the only skippable characters for the NumberInputKernal.
		 * 
		 * @return true The current character is a decimal point.
		 * @return false The current character is not a decimal point. 
		 */
		bool Skip() const noexcept
		{
			return ( m_Value == '.' );
		}
		/**
		 * @brief Implicity convert to a char.
		 * 
		 * @return char The converted character.
		 */
		operator char() const noexcept
		{
			return m_Value;
		}
	};
	/**
	 * @brief An RawInput Kernal that allows alphanumeric inputs, only mutable through up and down.
	 */
	struct StringInputKernal
	{
	private:
		char m_Value;
	public:
		using type = void;
		/**
		 * @brief Whether or not to allow number inputs 0-9.
		 */
		static constexpr auto AllowNumbers   = false;
		/**
		 * @brief Whether or not to allow decimal point inputs.
		 */
		static constexpr auto AllowDecimal   = false;
		/**
		 * @brief Whether or not to allow letters as inputs a-z or A-Z.
		 */
		static constexpr auto AllowLetters   = false;
		/**
		 * @brief Whether or not to treat the input as a single block
		 * 
		 * A block is something that can have a single button result in multple characters,
		 *  such as boolean True and False strings.
		 */
		static constexpr auto IsBlock        = false;
		/**
		 * @brief Whether or not to allow up and down keypresses.
		 */
		static constexpr auto AllowUpDown    = false;
		/**
		 * @brief Whether or not to allow cursor movement using left and right.
		 */
		static constexpr auto AllowLeftRight = true;
		/**
		 * @brief Construct a new String Input Kernal object
		 * 
		 * @param value The value to initalise the current character with.
		 */
		StringInputKernal( char value = 'A' ) noexcept:
		    m_Value{ General::ToUpper( value ) }
		{}
		/**
		 * @brief Increment the current letter.
		 * 
		 * @return StringInputKernal& For chained operations.
		 */
		StringInputKernal& operator++() noexcept
		{
			m_Value = General::FromLetterIndex( General::ToLetterIndex( m_Value ) + (std::int8_t)1 );
			return *this;
		}
		/**
		 * @brief Decrement the current letter.
		 * 
		 * @return StringInputKernal& For chained operations.
		 */
		StringInputKernal& operator--() noexcept
		{
			m_Value = General::FromLetterIndex( General::ToLetterIndex( m_Value ) - (std::int8_t)1 );
			return *this;
		}
		/**
		 * @brief Nothing is skippable in this kernal.
		 * 
		 * @return true Never.
		 * @return false Always.
		 */
		bool Skip() const noexcept
		{
			return false;
		}
		/**
		 * @brief Implicitly convert to the current character.s
		 * 
		 * @return char The character.
		 */
		operator char() const noexcept
		{
			return m_Value;
		}
	};
	/**
	 * @brief A alphanumeric input is one that accepts letters, numbers and cursor left and right.
	 */
	struct AlphanumericInputKernal
	{
	private:
		char m_Value;
	public:
		using type                           = void;
		/**
		 * @brief Whether or not to allow number inputs 0-9.
		 */
		static constexpr auto AllowNumbers   = true;
		/**
		 * @brief Whether or not to allow decimal point inputs.
		 */
		static constexpr auto AllowDecimal   = false;
		/**
		 * @brief Whether or not to allow letters as inputs a-z or A-Z.
		 */
		static constexpr auto AllowLetters   = true;
		/**
		 * @brief Whether or not to treat the input as a single block
		 * 
		 * A block is something that can have a single button result in multple characters,
		 *  such as boolean True and False strings.
		 */
		static constexpr auto IsBlock        = false;
		/**
		 * @brief Whether or not to allow up and down keypresses.
		 */
		static constexpr auto AllowUpDown    = false;
		/**
		 * @brief Whether or not to allow cursor movement using left and right.
		 */
		static constexpr auto AllowLeftRight = true;
		/**
		 * @brief Construct a new Number Input Kernal object
		 * 
		 * @param value The inital value of the number kernal
		 */
		AlphanumericInputKernal( char value = '0' ) :
		    m_Value( value ) {}
		/**
		 * @brief Increment the number/letter that is being processed (a single character)
		 * 
		 * The order is:
		 * abcdefghijklmnopqrstuvwxyz0123456789
		 * 
		 * @return NumberInputKernal& For chained operations.
		 */
		AlphanumericInputKernal & operator++() noexcept
		{
			m_Value = General::FromBase36( General::ToBase36( m_Value ) + (std::int8_t)1 );
			return *this;
		}
		/**
		 * @brief Decrement the number/letter that is being processed.
		 * 
		 * The order is:
		 * abcdefghijklmnopqrstuvwxyz0123456789
		 * 
		 * @return NumberInputKernal& For chained operations.
		 */
		AlphanumericInputKernal & operator--() noexcept
		{
			m_Value = General::FromBase36( General::ToBase36( m_Value ) - (std::int8_t)1 );
			return *this;
		}
		/**
		 * @brief Nothing is skippable in a numeric input.
		 * 
		 * @return true Never.
		 * @return false Always.
		 */
		bool Skip() const noexcept
		{
			return false;
		}
		/**
		 * @brief Implictly convert to the underlying character.
		 * 
		 * @return char 
		 */
		operator char() const noexcept
		{
			return m_Value;
		}
	};
	/**
	 * @brief An input kernal that represents Boolean types, see Boolean.hpp.
	 * 
	 * @tparam BooleanType The Boolean type (not bool).
	 */
	template <typename BooleanType>
	class BooleanInputKernal
	{
	private:
		bool m_Value;

	public:
		using type                           = BooleanType;
		/**
		 * @brief Whether or not to allow number inputs 0-9.
		 */
		static constexpr auto AllowNumbers   = false;
		/**
		 * @brief Whether or not to allow decimal point inputs.
		 */
		static constexpr auto AllowDecimal   = false;
		/**
		 * @brief Whether or not to allow letters as inputs a-z or A-Z.
		 */
		static constexpr auto AllowLetters   = false;
		/**
		 * @brief Whether or not to treat the input as a single block
		 * 
		 * A block is something that can have a single button result in multple characters,
		 *  such as boolean True and False strings.
		 */
		static constexpr auto IsBlock        = true;
		/**
		 * @brief Whether or not to allow up and down keypresses.
		 */
		static constexpr auto AllowUpDown    = true;
		/**
		 * @brief Whether or not to allow cursor movement using left and right.
		 */
		static constexpr auto AllowLeftRight = false;
		/**
		 * @brief Construct a new Boolean Input Kernal object
		 * 
		 * @param value Default boolean value.
		 */
		BooleanInputKernal( bool value = false ) noexcept:
		    m_Value( value )
		{}
		/**
		 * @brief Toggle between the true and false strings.
		 * 
		 * @return BooleanInputKernal& For chained operations.
		 */
		BooleanInputKernal& operator++() noexcept
		{
			m_Value = !m_Value;
			return *this;
		}
		/**
		 * @brief Toggle between the true and false strings.
		 * 
		 * @return BooleanInputKernal& For chained operations.
		 */
		BooleanInputKernal& operator--() noexcept
		{
			m_Value = !m_Value;
			return *this;
		}
		/**
		 * @brief No skippable characters.
		 * 
		 * @return true Never.
		 * @return false Always.
		 */
		bool Skip() const noexcept
		{
			return false;
		}
		/**
		 * @brief Implicity convert the underlying bool to a bool.
		 * 
		 * @return bool Value in the underlying bool.
		 */
		operator bool() const noexcept
		{
			return m_Value;
		}
	};

}