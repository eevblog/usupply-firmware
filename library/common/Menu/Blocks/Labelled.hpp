#pragma once

#include "Label.hpp"
#include "MenuItem.hpp"

namespace Menu
{
	/**
	 * @brief Convert an unlabelled menu item to a labelled menu item.
	 * 
	 * When the item is not selected, the label is displayed.
	 * When the item is selected, the menu item is displayed.
	 * 
	 * @tparam BufferSize The render target length.
	 * @tparam Kernal The underlying menu item.
	 * @tparam N The length of the label used.
	 */
	template <unsigned BufferSize, typename Kernal, unsigned N>
	class Labelled : public SelectableMenuItem<BufferSize>
	{
	private:
		using base_t = SelectableMenuItem<BufferSize>;
		static_assert( std::is_base_of_v<RawMenuItem<BufferSize>, Kernal>, "Labelled kernal must be a RawMenuItem." );
		using buffer_t = typename base_t::buffer_t;
		Label<BufferSize, N> m_Label;
		Kernal               m_Kernal;

	public:
		/**
		 * @brief Construct a new Labelled object
		 * 
		 * @tparam Args The arguments used to construct the kernal.
		 * @param label The menu item's label.
		 * @param args The constructor arguments for Kernal.
		 */
		template <typename... Args>
		Labelled( std::array<char, N> const& label, Args&&... args ) noexcept:
		    m_Label( label ),
		    m_Kernal( std::forward<Args>( args )... )
		{}
		/**
		 * @brief Use the kernals navigation function when selected.
		 * 
		 * @param input The keycode to handle.
		 * @return NavigateReturn The result of Kernal::Navigate( input ).
		 */
		virtual NavigateReturn SelectedNavigate( KeyCodes input ) noexcept
		{
			return m_Kernal.Navigate( input );
		}
		/**
		 * @brief Use the kernals renderer function when selected.
		 * 
		 * @param output The render target.
		 * @param use_cursor Should we use cursors?
		 * @return unsigned The number of characters rendered.
		 */
		virtual unsigned SelectedRender( buffer_t& output, bool use_cursor ) noexcept
		{
			return m_Kernal.Render( output, use_cursor );
		}
		/**
		 * @brief Render the menu's label when the menu isn't selected.
		 * 
		 * @param output The render target.
		 * @param use_cursor Should we use cursors?
		 * @return unsigned The number of characters rendered.
		 */
		virtual unsigned DeselectedRender( buffer_t& output, bool use_cursor ) noexcept
		{
			return m_Label.Render( output, use_cursor );
		}
	};
}