#pragma once
#include "Convert.hpp"
#include <cstdint>

namespace Menu
{
	/**
	 * @brief The supported possible keycodes.
	 */
	enum class KeyCodes : std::uint8_t
	{
		Cancel   = 0x3,
		Back     = 0x8,
		Tab      = 0x9,
		Return   = 0x0D,
		Enter    = Return,
		Shift    = 0x10,
		Control  = 0x11,
		Menu     = 0x12,
		Pause    = 0x13,
		Escape   = 0x1B,
		Space    = 0x20,
		Delete   = 0x2E,
		Left     = 0x25,
		Up       = 0x26,
		Right    = 0x27,
		Down     = 0x28,
		A        = 0x41,
		B        = 0x42,
		C        = 0x43,
		D        = 0x44,
		E        = 0x45,
		F        = 0x46,
		G        = 0x47,
		H        = 0x48,
		I        = 0x49,
		J        = 0x4A,
		K        = 0x4B,
		L        = 0x4C,
		M        = 0x4D,
		N        = 0x4E,
		O        = 0x4F,
		P        = 0x50,
		Q        = 0x51,
		R        = 0x52,
		S        = 0x53,
		T        = 0x54,
		U        = 0x55,
		V        = 0x56,
		W        = 0x57,
		X        = 0x58,
		Y        = 0x59,
		Z        = 0x5A,
		Num0     = 0x30,
		Num1     = 0x31,
		Num2     = 0x32,
		Num3     = 0x33,
		Num4     = 0x34,
		Num5     = 0x35,
		Num6     = 0x36,
		Num7     = 0x37,
		Num8     = 0x38,
		Num9     = 0x39,
		Numpad0  = 0x60,
		Numpad1  = 0x61,
		Numpad2  = 0x62,
		Numpad3  = 0x63,
		Numpad4  = 0x64,
		Numpad5  = 0x65,
		Numpad6  = 0x66,
		Numpad7  = 0x67,
		Numpad8  = 0x68,
		Numpad9  = 0x69,
		Add      = 0x6B,
		Divide   = 0x6F,
		Decimal  = 0x6E,
		Multiply = 0x6A,
		Subtract = 0x6C,
		Numlock  = 0x90,

		Power,
		Config,
		Lock,
		Presets,
		PresetsLong,
		Voltage,
		Current
	};
	/**
	 * @brief The type of the return of a menu item Navigate function.
	 * 
	 * @note Handled indicates that the menu item handled the input and nothing else should attempt to handle it.
	 * @note Unhandled indicates that the menu item did not handle the input.
	 * @note Finished indicates that the menu item is completed processing AND it handled the input.
	 */
	enum NavigateReturn : unsigned
	{
		Handled   = 1,
		Unhandled = 0,
		Finished  = 2,
	};
	/**
	 * @brief Checks whether an input is a number.
	 * 
	 * @param input The keycode to check.
	 * @return true The input is a number.
	 * @return false The input isn't a number.
	 */
	bool constexpr IsKeycodeNumber( KeyCodes input ) noexcept
	{
		auto const keycode   = (std::uint8_t)input;
		const bool is_num    = General::Between( keycode, (std::uint8_t)KeyCodes::Num0, (std::uint8_t)KeyCodes::Num9 );
		const bool is_numpad = General::Between( keycode, (std::uint8_t)KeyCodes::Numpad0, (std::uint8_t)KeyCodes::Numpad9 );
		return is_num || is_numpad;
	}
	/**
	 * @brief Checks whether the keycode is a cancel character.
	 * 
	 * @param input The keycode to check.
	 * @return true The input is cancel, back or escape.
	 * @return false The input isn't a cancel character.
	 */
	bool constexpr IsCancel(KeyCodes input) noexcept
	{
		return General::IsOneOf(input, KeyCodes::Cancel, KeyCodes::Back, KeyCodes::Escape);
	}
	/**
	 * @brief The base class of all menu items, this defines all the required and optional virtual methods
	 * 
	 * @tparam BufferSize The length in characters of the render target.
	 */
	template <unsigned BufferSize>
	class RawMenuItem
	{
	private:
		static_assert( BufferSize > 0, "Cannot create a menu item with a buffer size of zero, this would not be possible to display." );
	public:
		/**
		 * @brief The render target character array type.
		 */
		using buffer_t = std::array<char, BufferSize>;
		/**
		 * @brief Construct a new Raw Menu Item object
		 */
		RawMenuItem() noexcept = default;
		/**
		 * @brief This class should reset any state of the menu item.
		 */
		virtual void Reset() noexcept {};
		/**
		 * @brief This should setup the state of a manu item based on the input array.
		 */
		virtual void Load( buffer_t const & ) noexcept {}
		/**
		 * @brief This should perform any navigation needed for the menu item.
		 * 
		 * @return NavigateReturn See specific child class implementation.
		 */
		virtual NavigateReturn Navigate( KeyCodes ) noexcept { return Unhandled; }
		/**
		 * @brief This should render the menu item into the output character array.
		 * 
		 * @param output The render target.
		 * @param use_cursor Whether or not the menu item should process cursors.
		 * @return unsigned The length of the rendering.
		 */
		virtual unsigned Render( buffer_t &output, bool use_cursor ) noexcept = 0;
	};
	/**
	 * @brief A menu item that is selectable, see .
	 * 
	 * @tparam BufferSize The length in characters of the render target.
	 */
	template <unsigned BufferSize>
	class SelectableMenuItem : public RawMenuItem<BufferSize>
	{
	public:
		using base_t   = RawMenuItem<BufferSize>;
		using buffer_t = typename base_t::buffer_t;

	protected:
		/**
		 * @brief The flag that indicates whether the menu item is selected. 
		 */
		bool m_Selected = false;
		void Select() noexcept			{ m_Selected = true;		}
		void Deselect()	noexcept		{ m_Selected = false;		}
		void ToggleSelection() noexcept	{ m_Selected = !m_Selected; }
		bool Selected() const noexcept	{ return m_Selected;		}

		/**
		 * @brief This selected navigate is always called when Navigate is called.
		 * 
		 * If the SelectedNavigate:
		 * - Is Finished; or,
		 * - Didn't handle the input
		 * and,
		 * - The input is cancel
		 * Then the menu item is deselected and the menu item reset.
		 * 
		 * @param input 
		 * @return NavigateReturn 
		 */
		NavigateReturn InternalSelectedNavigate( KeyCodes input ) noexcept
		{
			auto output = SelectedNavigate( input );
			if ( ( output == Finished ) || ( ( output == Unhandled ) && IsCancel( input ) ) )
			{
				base_t::Reset();
				Deselect();
			}
			return output;
		}
	public:
		/**
		 * @brief Construct a new Selectable Menu Item object
		 * 
		 * @param selected The intial selection state.
		 */
		SelectableMenuItem( bool selected = false ) noexcept: m_Selected( selected ) {}
		/**
		 * @brief The navigation function to run when the item is selected.
		 * 
		 * @param input The keycode to process.
		 * @return NavigateReturn See child class implementation docs.
		 */
		virtual NavigateReturn SelectedNavigate( KeyCodes input ) noexcept = 0;
		/**
		 * @brief The function to handle navigation when the control is not selected.
		 * 
		 * If input is the Enter KeyCode then the selected state is set to true.
		 * 
		 * @note This is overridable, always check with child class implmentation.
		 * 
		 * @param input The keycode to process.
		 * @return NavigateReturn Handled if Enter is pressed, unhandled otherwise.
		 */
		virtual NavigateReturn DeselectedNavigate( KeyCodes input ) noexcept
		{
			if ( input == KeyCodes::Enter )
			{
				Select();
				return Handled;
			}
			return Unhandled;
		}
		/**
		 * @brief Route the navigation of the keycodes to the selected or deselected handler.
		 * 
		 * @param input 
		 * @return NavigateReturn 
		 */
		virtual NavigateReturn Navigate( KeyCodes input ) noexcept
		{
			return ( Selected() ) ? InternalSelectedNavigate( input ) : DeselectedNavigate( input );
		}
		/**
		 * @brief This function called when the menu item is selected.
		 * 
		 * @param output The buffer to render into.
		 * @param use_cursor Whether or not to use cursors.
		 * @return unsigned The length of the rendered result.
		 */
		virtual unsigned SelectedRender( buffer_t &output, bool use_cursor ) noexcept= 0;
		/**
		 * @brief This function is called when the menu item is not selected.
		 * 
		 * @param output Unused.
		 * @param use_cursor Unused.
		 * @return unsigned The length of the rendered result.
		 */
		virtual unsigned DeselectedRender( buffer_t & output, bool use_cursor ) noexcept
		{
			return 0;
		}
		/**
		 * @brief Routes the render function based on the selection state of the menu item.
		 * 
		 * @param output The render target.
		 * @param use_cursor Whether or not to use cursors.
		 * @return unsigned The length of the rendered result.
		 */
		virtual unsigned Render( buffer_t &output, bool use_cursor ) noexcept
		{
			return ( Selected() ) ? 
				SelectedRender	( output, use_cursor ) :
				DeselectedRender( output, use_cursor );
		}
	};
	/**
	 * @brief This defines a menu item that doesn't render anything and doesn't handle inputs.
	 * 
	 * This can be used in combination with hotkeys, they don't nessarily need to display anything.
	 *  
	 * @tparam BufferSize The render target size.
	 */
	template <unsigned BufferSize>
	struct BlankMenuItem : RawMenuItem<BufferSize>
	{
		using buffer_t = typename RawMenuItem<BufferSize>::buffer_t;
		/**
		 * @brief Construct a new Blank Menu Item object
		 */
		BlankMenuItem() noexcept = default;
		/**
		 * @brief Construct a new Blank Menu Item object
		 */
		BlankMenuItem(General::SizeT<BufferSize>) noexcept {};
		/**
		 * @brief Render nothing.
		 * 
		 * @return unsigned Always returns 0.
		 */
		virtual unsigned Render( buffer_t &, bool ) noexcept { return 0; }
	};
	
	template <unsigned BufferSize>
	BlankMenuItem(General::SizeT<BufferSize>)->BlankMenuItem<BufferSize>;
}