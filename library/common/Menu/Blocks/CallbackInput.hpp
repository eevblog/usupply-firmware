#pragma once
#include "Labelled.hpp"
#include "RawInput.hpp"
#include "RawValue.hpp"

namespace Menu
{
	/**
	 * @brief A menu item that supports an input menu item and calls a callback when a result is avaliable.
	 * 
	 * @tparam BufferSize The target rendering length.
	 * @tparam T The type of the input. (could be a bool, int, float etc...)
	 * @tparam Callback The callback to run if the menu input is completed.
	 */
	template <unsigned BufferSize, typename T, typename Callback>
	class RawCallbackInput : public RawMenuItem<BufferSize>
	{
	private:
		using base_t   = RawMenuItem<BufferSize>;
		using buffer_t = typename base_t::buffer_t;
		using kernal_t = typename GetKernal<std::decay_t<T>>::type;
		
		Callback						m_Callback;
		T								m_Buffer = {};
		RawInput<BufferSize, kernal_t>	m_Input;
	public:
		/**
		 * @brief Construct a new Raw Callback Input object
		 * 
		 * @param callback 
		 */
		RawCallbackInput( Callback && callback ) noexcept : m_Callback	( std::forward<Callback>(callback) ) {}
		RawCallbackInput( General::SizeT<BufferSize>, General::Type<T>, Callback && callback ) noexcept : RawCallbackInput(std::forward<Callback>(callback)) {};
		/**
		 * @brief Saves the input by running the callback with the result buffer.
		 */
		void Save() noexcept
		{
			buffer_t buffer{};
			if (m_Input.Render(buffer, false) > 0)
			{
				if constexpr ( std::is_floating_point_v<T> )
					General::atof<BufferSize>( buffer, m_Buffer );

				if constexpr ( std::is_unsigned_v<T> )
					General::atou<BufferSize>( buffer, m_Buffer );

				if constexpr ( std::is_integral_v<T> )
					General::atoi<BufferSize>( buffer, m_Buffer );

				if constexpr ( General::is_boolean_v<T> )
					m_Buffer.FromString( buffer );

				m_Callback( m_Buffer );
			}
		}
		/**
		 * @brief Clear the input and reset cursor (if its used).
		 */
		virtual void Reset() noexcept
		{
			m_Input.Reset();
		}
		/**
		 * @brief Handle a keycode input.
		 * 
		 * Enter always calls the Save() function above.
		 * The keycode handling is dependant on the type of T.
		 * 
		 * @param input 
		 * @return NavigateReturn 
		 */
		virtual NavigateReturn Navigate( KeyCodes input ) noexcept
		{
			if ( m_Input.Navigate( input ) )
				return Handled;
			else if (input == KeyCodes::Enter)
			{
				Save();
				return Finished;
			}

			return Unhandled;
		}
		/**
		 * @brief Render the input into the output buffer.
		 * 
		 * @param output The output buffer.
		 * @param use_cursor Should we allow cursor?
		 * @return unsigned The length rendered.
		 */
		virtual unsigned Render( buffer_t& output, bool use_cursor ) noexcept
		{
			return m_Input.Render( output, use_cursor );
		}
		/**
		 * @brief Allow implicit convertion of the buffer to the underlying type T.
		 * 
		 * @return T const & 
		 */
		operator T const &() const noexcept
		{
			return m_Buffer;
		}
	};
	/**
	 * @brief A template type deduction guide
	 * 
	 * The means you don't need to manually type template arguments and you don't need to 
	 * risk typing the arguments incorrectly.
	 * 
	 * @tparam BufferSize The size of the render target.
	 * @tparam T The type of the input.
	 * @tparam Callback The callback to run when the input is completed.
	 */
	template <unsigned BufferSize, typename T, typename Callback>
	RawCallbackInput( General::SizeT<BufferSize>, T&, Callback )->RawCallbackInput<BufferSize, T, Callback>;
	/**
	 * @brief A callback input that is also labels when it isn't selected.
	 * 
	 * @tparam BufferSize The size of the render target.
	 * @tparam T The type of the input.
	 * @tparam Callback The callback to run when the input is completed.
	 * @tparam N The length of the label.
	 */
	template <unsigned BufferSize, typename T, typename Callback, unsigned N>
	struct LabelledCallbackInput : Labelled<BufferSize ,RawCallbackInput<BufferSize, T, std::decay_t<Callback>>, N>
	{
		using base_t = Labelled<BufferSize ,RawCallbackInput<BufferSize, T, std::decay_t<Callback>>, N>;
		using base_t::base_t;
		/**
		 * @brief Construct a new Labelled Callback Input object
		 * 
		 * @tparam Args The argument types to use when initialising the RawCallbackInput (see docs).
		 * @param args The arguments to initalise the RawCallbackInput (see docs).
		 */
		template <typename... Args>
		LabelledCallbackInput( General::SizeT<BufferSize>, General::Type<T>, Args&&... args ) : base_t( std::forward<Args>( args )... ) {}
	};
	/**
	 * @brief A deduction guide for the LabelledCallbackInput
	 * 
	 * The means you don't need to manually type template arguments and you don't need to 
	 * risk typing the arguments incorrectly.
	 * 
	 * @tparam BufferSize The size of the render target.
	 * @tparam T The type of the input.
	 * @tparam Callback The callback to run when the input is completed.
	 * @tparam N The length of the label.
	 */
	template <unsigned BufferSize, typename T, typename Callback, unsigned N>
	LabelledCallbackInput( General::SizeT<BufferSize>, General::Type<T>, std::array<char, N> const&, Callback&& )->LabelledCallbackInput<BufferSize, T, Callback, N>;
}