#pragma once

namespace General
{
	#pragma pack(push, 1)
    /**
     * @brief A class that constructs an object with the bytes reversed.
     * @warning Only for standard layout types with trivial destructors.
     * @warning This structure does not check for alignment it is the callers responsibility.
     * @warning This structure does not account for padding, it is the callers responsibility.
     * @tparam T The type to reverse.
     */
    template <typename T>
    class ReversedByteOrder
    {
    private:
        static constexpr size_t size = sizeof(T);
        std::array<uint8_t, size> m_Data;
        /**
         * Access all memory in reverse order
         */
        template <size_t...I>
        ReversedByteOrder( uint8_t const * input, std::index_sequence<I...> ) noexcept:
            m_Data{ input[size - I - 1u] ...  }
        {}
    public:
        /**
         * @brief Constructor reverses byte order
         * 
         * @param input The storage to reverse byte order.
         */
        ReversedByteOrder( T const & input ) noexcept :
			ReversedByteOrder{ (uint8_t const *)&input, std::make_index_sequence<size>{} }
		{}
		/**
		 * @brief Returns the original byte order.
         * @return The unreversed data.
		 */
		T Original() const noexcept
		{
			return (T const &)ReversedByteOrder<ReversedByteOrder>{ m_Data };
		}
		/**
		 * @brief Returns the reversed byte order.
         * @return The reversed data
		 */
		T Reversed() const noexcept
		{
			return (T const &)m_Data;
		}
    };
	#pragma pack(pop)
}