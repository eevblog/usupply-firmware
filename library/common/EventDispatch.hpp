#pragma once

#include "FILO.hpp"
#include "Macros.hpp"

namespace General
{
	/**
	 * @brief This is a class that allows events to be queued from an interrupt and exeuted in the main thread.
	 * 
	 * This system only allows void functions, if you need a more sophisticaed system use a message system.
	 * 
	 * @tparam Key This is a type unique to this interrupt.
	 * @tparam BufferSize The number of queued events that are allowed.
	 */
	template <typename Key, unsigned BufferSize = 4u>
	class EventDispatch
	{
	public:
		using void_function_t = void(*)();
		using Buffer_t = std::conditional_t<(BufferSize > 1), Containers::FILO<void_function_t, BufferSize>, void_function_t>;
	private:
		static inline Buffer_t g_Buffer{};
		using this_t = EventDispatch<Key, BufferSize>;
	protected:
		/**
		 * @brief Pushes a single event function onto the event storage.
		 * 
		 * NOTE: That if the queue size is one, unresolved pending events are overriden.
		 * If the queue size is more than 1 then this function can fail if the storage is full.
		 * 
		 * @param input The function to push into the storage.
		 * @return bool True when the push was successful.
		 */
		ALWAYS_INLINE static bool Push( void_function_t input ) noexcept
		{
			Buffer_t * const volatile ptr = &g_Buffer;
			if constexpr (BufferSize == 1)
			{
				if (*ptr == nullptr) 
				{
					*ptr = input;
					return true;
				}
				return false;
			}
			else
			{
				return ptr->Push( input );
			}
		}
		/**
		 * @brief Removes the latest item from the event queue.
		 * 
		 * If the queue size is 1, this function never fails.
		 * If the queue size is >1, then the queue could have zero items and be unable to remove an item.
		 * 
		 * @return True when item was removed.
		 */
		ALWAYS_INLINE static bool Pop() noexcept
		{
			if constexpr (BufferSize == 1)
			{
				g_Buffer = nullptr;
				return true;
			}
			else
			{
				return g_Buffer.Pop();
			}
		}
		/**
		 * @brief Tests whether there are any items in the queue.
		 * 
		 * @return true when the queue is empty.
		 * @return false when the queue has events.
		 */
		ALWAYS_INLINE static bool Empty() noexcept
		{
			if constexpr (BufferSize == 1)
			{
				return ( g_Buffer == nullptr );
			}
			else
			{
				return g_Buffer.Empty();
			}
		}
	public:
		/**
		 * @brief Executes any pending events.
		 */
		ALWAYS_INLINE static void Dispatch() noexcept
		{
			if constexpr (BufferSize == 1)
			{
				if ( g_Buffer ) g_Buffer();
			}
			else
			{
				for ( auto & item : g_Buffer ) item();
			}
		}
	};
}