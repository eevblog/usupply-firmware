#pragma once
#include "containers/DataView.hpp"
#include <cstdint>
#include <cstddef>
#include <array>

namespace USB::HID
{
    /**
     * @brief A data structure that represents a stack data structure.
     * 
     * The data structure is HID specific, it contains an id, which correlates to the 
     * value sent with the HID report descriptor. 
     * 
     * @tparam N The maximum number of bytes to store in the stack.
     */
    template <std::size_t N>
    class Packet
    {
        std::uint8_t                 m_Id;
        std::uint8_t                 m_Count = 0;
        std::array<std::uint8_t, N>  m_Data;
    public:
        /**
         * @brief Construct a new Packet object
         * 
         * @param id The packet id defined in the HID Report descriptor
         */
        constexpr Packet( uint8_t id ) noexcept :
            m_Id{ id },
            m_Count{ 0 }
        {}
        /**
         * @brief Construct a new Packet object
         * 
         * @param id The packet id defined in the HID Report descriptor
         * @param data The intial data to store in the packet
         */
        constexpr Packet( uint8_t id, std::array<std::uint8_t, N> data ) noexcept :
            m_Id    { id },
            m_Count { N },
            m_Data  { data }
        {}
        /**
         * @brief Inserts an item into the back of the array. 
         * 
         * The internal representation for inserted data is a stack data structure.
         * 
         * @param data 
         * @return true Successfully inserted byte into the packet.
         * @return false Failed to insert byte into the packet.
         */
        bool PushBack( std::uint8_t data ) noexcept
        {
            if ( m_Count < N )
            {
                m_Data[ m_Count++ ] = data;
                return true;
            }
            else
            {
                return false;
            }
        }
        /**
         * @brief Assigns the bytes represented by the DataView into the packets stack data structure.
         * 
         * @param data      A view of some data to insert into the packet.
         * @return true     When ALL of the data successfully inserted into the data structure.
         * @return false    When the dataview represents more data than the packet has space. 
         */
        bool Assign( Containers::DataView<std::uint8_t> data ) noexcept
        {
            if ( data.Size() and ( data.Size() <= Capacity() ) )
            {
                m_Count = data.Size();
                memcpy( m_Data.data(), data.Data(), data.Size() );
                return true;
            }
            else
            {
                return false;
            }
        }
        /**
         * @brief Clears all the data in the packet.
         * 
         */
        void Clear() noexcept
        {
            m_Count = 0;
        }
        /**
         * @brief Gets a copy of the packet with a different id.
         * 
         * @param id 
         * @return Packet 
         */
        Packet Withdraw(uint8_t id) noexcept
        {
            Packet copy = *this;
            copy.m_Id = id;
            Clear();
            return copy;
        }
        /**
         * @brief Returns the number of bytes in the packet data.
         * 
         * This does not include the sizeof(m_Id).
         * 
         * @return constexpr std::size_t
         */
        static constexpr std::size_t Length() noexcept
        {
            return sizeof(Packet) - 1;
        }
        /**
         * @brief The total number of bytes stored in the stack structure.
         * 
         * @return constexpr std::uint8_t 
         */
        constexpr std::uint8_t Size() const noexcept
        {
            return m_Count;
        }
        /**
         * @brief Gets the packet id for the data structure.
         * 
         * @return constexpr std::uint8_t 
         */
        constexpr std::uint8_t Id() const noexcept
        {
            return m_Id;
        }
        /**
         * @brief Gets the maximum number of bytes stored in the stack. 
         * 
         * @return constexpr std::uint8_t 
         */
        static constexpr std::uint8_t Capacity() noexcept
        {
            return N;
        }
        /**
         * @brief Removes an item from the back of the stack
         * 
         * @return uint8_t Returns the value poped from the back or 0 if there is no data.
         */
        uint8_t PopBack() noexcept
        {
            return (m_Count) ? m_Data[ --m_Count ] : 0;
        }
        /**
         * @brief Gets the first byte in the stack.
         * 
         * @return constexpr std::uint8_t 
         */
        constexpr std::uint8_t Front() const noexcept
        {
            return m_Data[0];
        }
        /**
         * @brief Gets the last byte in the stack.
         * 
         * @return constexpr std::uint8_t 
         */
        constexpr std::uint8_t Back() const noexcept
        {
            return (m_Count) ? m_Data[m_Count - 1] : 0;
        }
        /**
         * @brief Gets the byte at the requested index in the stack.
         * 
         * @param index
         * @return std::uint8_t& A reference to the item at the requested index.
         */
        std::uint8_t & operator[](size_t index) noexcept
        {
            return m_Data[ index ];
        }
        /**
         * @brief Gets the byte at the requested index in the stack.
         * 
         * @param index
         * @return std::uint8_t& A reference to the item at the requested index.
         */
        constexpr std::uint8_t operator[](size_t index) const noexcept
        {
            return m_Data[ index ];
        }
    };
    /**
     * @brief A deduction guide for the Packet class.
     * 
     * @tparam N The maximum number of bytes to store in the stack.
     */
    template <std::size_t N>
    Packet(std::uint8_t, std::array<std::uint8_t, N>) -> Packet<N>;
}