#pragma once
#include "Meta.hpp"
#include "Pin.hpp"
#include "Math.hpp"
#include "Register.hpp"
#include "DataView.hpp"
#include <cstdint>
#include <cstring>

namespace Peripherals::USBGeneral
{
    /**
     * @brief 
     */
    enum class Direction : uint32_t
    {
        IN  = 0,
        OUT = 1,
        Unknown
    };
    /**
     * 
     */
    constexpr size_t Endpoint_Base_Address  = 0x40005C00u;
    constexpr size_t Registers_Base_Address = 0x40005C00u;
    constexpr size_t SRAM_Base_Address      = Endpoint_Base_Address + 0x400u;
    /**
     * USB Register Offsets
     */
    constexpr size_t EP_Offset      = 0x00u;
    constexpr size_t CNTR_Offset    = 0x40u;
    constexpr size_t ISTR_Offset    = 0x44u;
    constexpr size_t FNR_Offset     = 0x48u;
    constexpr size_t DADDR_Offset   = 0x4Cu;
    constexpr size_t BTABLE_Offset  = 0x50u;
    constexpr size_t LPMCSR_Offset  = 0x54u;
    constexpr size_t BCDR_Offset    = 0x58u;
    /**
     * 
     */
    template <unsigned endpoint>
    using EP_REG = General::u16_reg<(endpoint * 4u) + Endpoint_Base_Address + EP_Offset>;
    constexpr uint16_t EP_CTR_RX_MASK   = (0b1000000000000000u);
    constexpr uint16_t EP_DTOG_RX_MASK  = (0b0100000000000000u);
    constexpr uint16_t EP_STAT_RX_MASK  = (0b0011000000000000u);
    constexpr uint16_t EP_SETUP_MASK    = (0b0000100000000000u);
    constexpr uint16_t EP_EP_TYPE_MASK  = (0b0000011000000000u);
    constexpr uint16_t EP_EP_KIND_MASK  = (0b0000000100000000u);
    constexpr uint16_t EP_CTR_TX_MASK   = (0b0000000010000000u);
    constexpr uint16_t EP_DTOG_TX_MASK  = (0b0000000001000000u);
    constexpr uint16_t EP_STAT_TX_MASK  = (0b0000000000110000u);
    constexpr uint16_t EP_EA_MASK       = (0b0000000000001111u);
    /**
     * 
     */
    constexpr uint16_t EP_NO_TOGGLE_MASK    = ~( EP_DTOG_RX_MASK | EP_STAT_RX_MASK | EP_DTOG_TX_MASK | EP_STAT_TX_MASK );
    constexpr uint16_t EP_NO_ZERO_MASK      = EP_CTR_TX_MASK | EP_CTR_RX_MASK;
    /**
     * 
     */
    template <unsigned N>
	struct EP : EP_REG<N>
    {
		using base_t = EP_REG<N>;
		using base_t::base_t;
        /**
         * This is the endpoint number
         */
        static constexpr unsigned Number = N;
        /**
         * 
         */
        ALWAYS_INLINE auto CTR_RX (){ return base_t::template Actual<EP_CTR_RX_MASK,    EP_NO_TOGGLE_MASK,                    EP_CTR_TX_MASK,     false   >(); }
        ALWAYS_INLINE auto DTOG_RX(){ return base_t::template Actual<EP_DTOG_RX_MASK,   EP_NO_TOGGLE_MASK | EP_DTOG_RX_MASK,  EP_NO_ZERO_MASK,    true    >(); }
        ALWAYS_INLINE auto STAT_RX(){ return base_t::template Actual<EP_STAT_RX_MASK,   EP_NO_TOGGLE_MASK | EP_STAT_RX_MASK,  EP_NO_ZERO_MASK,    true    >(); }
        ALWAYS_INLINE auto SETUP  (){ return base_t::template Actual<EP_SETUP_MASK,     EP_NO_TOGGLE_MASK,                    EP_NO_ZERO_MASK,    false   >(); }
        ALWAYS_INLINE auto EP_TYPE(){ return base_t::template Actual<EP_EP_TYPE_MASK,   EP_NO_TOGGLE_MASK,                    EP_NO_ZERO_MASK,    false   >(); }
        ALWAYS_INLINE auto EP_KIND(){ return base_t::template Actual<EP_EP_KIND_MASK,   EP_NO_TOGGLE_MASK,                    EP_NO_ZERO_MASK,    false   >(); }
        ALWAYS_INLINE auto CTR_TX (){ return base_t::template Actual<EP_CTR_TX_MASK ,   EP_NO_TOGGLE_MASK,                    EP_CTR_RX_MASK,     false   >(); }
        ALWAYS_INLINE auto DTOG_TX(){ return base_t::template Actual<EP_DTOG_TX_MASK,   EP_NO_TOGGLE_MASK | EP_DTOG_TX_MASK,  EP_NO_ZERO_MASK,    true    >(); }
        ALWAYS_INLINE auto STAT_TX(){ return base_t::template Actual<EP_STAT_TX_MASK,   EP_NO_TOGGLE_MASK | EP_STAT_TX_MASK,  EP_NO_ZERO_MASK,    true    >(); }
        ALWAYS_INLINE auto EA     (){ return base_t::template Actual<EP_EA_MASK,        EP_NO_TOGGLE_MASK,                    EP_NO_ZERO_MASK,    false   >(); }
    };
    /**
     * 
     */
    using CNTR_REG = General::u32_reg<Registers_Base_Address + CNTR_Offset>;
    constexpr uint16_t CNTR_CTRM_MASK     = (0b1000000000000000u);
    constexpr uint16_t CNTR_PMAOVRM_MASK  = (0b0100000000000000u);
    constexpr uint16_t CNTR_ERRM_MASK     = (0b0010000000000000u);
    constexpr uint16_t CNTR_WKUPM_MASK    = (0b0001000000000000u);
    constexpr uint16_t CNTR_SUSPM_MASK    = (0b0000100000000000u);
    constexpr uint16_t CNTR_RESETM_MASK   = (0b0000010000000000u);
    constexpr uint16_t CNTR_SOFM_MASK     = (0b0000001000000000u);
    constexpr uint16_t CNTR_ESOFM_MASK    = (0b0000000100000000u);
    constexpr uint16_t CNTR_L1REQM_MASK   = (0b0000000010000000u);
    constexpr uint16_t CNTR_L1RESUME_MASK = (0b0000000000100000u);
    constexpr uint16_t CNTR_RESUME_MASK   = (0b0000000000010000u);
    constexpr uint16_t CNTR_FSUSP_MASK    = (0b0000000000001000u);
    constexpr uint16_t CNTR_LPMODE_MASK   = (0b0000000000000100u);
    constexpr uint16_t CNTR_PDWN_MASK     = (0b0000000000000010u);
    constexpr uint16_t CNTR_FRES_MASK     = (0b0000000000000001u);
    /**
     * 
     */
	struct CNTR : CNTR_REG
    {
		using base_t = CNTR_REG;
		using base_t::base_t;
        /**
         * Correct transfer interrupt mask
         * 0:   Correct Transfer (CTR) Interrupt disabled.
         * 1:   CTR Interrupt enabled, an interrupt request is generated 
         *      when the corresponding bit in the USB_ISTR register is set.
         */
        ALWAYS_INLINE auto CTRM()     { return base_t::template Actual<CNTR_CTRM_MASK>();    }
        /**
         * Packet memory area over / underrun interrupt mask
         * 0:   PMAOVR Interrupt disabled.
         * 1:   PMAOVR Interrupt enabled, an interrupt request is generated 
         *      when the corresponding bit in the USB_ISTR register is set.
         */
        ALWAYS_INLINE auto PMAOVRM()  { return base_t::template Actual<CNTR_PMAOVRM_MASK>(); } 
        /**
         * Error interrupt mask
         * 0:   ERR Interrupt disabled.
         * 1:   ERR Interrupt enabled, an interrupt request is generated when 
         *      the corresponding bit in the USB_ISTR register is set.
         */  
        ALWAYS_INLINE auto ERRM()     { return base_t::template Actual<CNTR_ERRM_MASK>();    }  
        /**
         * Wakeup interrupt mask
         * 0:   WKUP Interrupt disabled.
         * 1:   WKUP Interrupt enabled, an interrupt request is generated when the
         *      corresponding bit in the USB_ISTR register is set.
         */   
        ALWAYS_INLINE auto WKUPM()    { return base_t::template Actual<CNTR_WKUPM_MASK>();    }
        /**
         * Suspend mode interrupt mask
         * 0:   Suspend Mode Request (SUSP) Interrupt disabled.
         * 1:   SUSP Interrupt enabled, an interrupt request is generated when the
         *      corresponding bit in the USB_ISTR register is set.
         */
        ALWAYS_INLINE auto SUSPM()    { return base_t::template Actual<CNTR_SUSPM_MASK>();    }
        /**
         * USB reset interrupt mask
         * 0:   RESET Interrupt disabled.
         * 1:   RESET Interrupt enabled, an interrupt request is generated when the
         *      corresponding bit in the USB_ISTR register is set.
         */
        ALWAYS_INLINE auto RESETM()   { return base_t::template Actual<CNTR_RESETM_MASK>();   }
        /**
         * Start of frame interrupt mask
         * 0:   SOF Interrupt disabled.
         * 1:   SOF Interrupt enabled, an interrupt request is generated when the
         *      corresponding bit in the USB_ISTR register is set.
         */
        ALWAYS_INLINE auto SOFM()     { return base_t::template Actual<CNTR_SOFM_MASK>();     } 
        /**
         * Expected start of frame interrupt mask
         * 0:   Expected Start of Frame (ESOF) Interrupt disabled.
         * 1:   ESOF Interrupt enabled, an interrupt request is generated when the 
         *      corresponding bit in the USB_ISTR register is set.
         */
        ALWAYS_INLINE auto ESOFM()    { return base_t::template Actual<CNTR_ESOFM_MASK>();    }
        /**
         * LPM L1 state request interrupt mask
         * 0:   LPM L1 state request (L1REQ) Interrupt disabled.
         * 1:   L1REQ Interrupt enabled, an interrupt request is generated when the 
         *      corresponding bit in the USB_ISTR register is set.
         */
        ALWAYS_INLINE auto L1REQM()   { return base_t::template Actual<CNTR_L1REQM_MASK>();   }
        /**
         *
         */
        ALWAYS_INLINE auto L1RESUME() { return base_t::template Actual<CNTR_L1RESUME_MASK>(); }
        ALWAYS_INLINE auto RESUME()   { return base_t::template Actual<CNTR_RESUME_MASK>();   }
        ALWAYS_INLINE auto FSUSP()    { return base_t::template Actual<CNTR_FSUSP_MASK>();    }
        ALWAYS_INLINE auto LPMODE()   { return base_t::template Actual<CNTR_LPMODE_MASK>();   }
        ALWAYS_INLINE auto PDWN()     { return base_t::template Actual<CNTR_PDWN_MASK>();     }
        ALWAYS_INLINE auto FRES()     { return base_t::template Actual<CNTR_FRES_MASK>();     }
    };
    /**
     * 
     */
    using ISTR_REG = General::u32_reg<Registers_Base_Address + ISTR_Offset>;
    constexpr uint16_t ISTR_CTR_MASK     = (0b1000000000000000u);
    constexpr uint16_t ISTR_PMAOVR_MASK  = (0b0100000000000000u);
    constexpr uint16_t ISTR_ERR_MASK     = (0b0010000000000000u);
    constexpr uint16_t ISTR_WKUP_MASK    = (0b0001000000000000u);
    constexpr uint16_t ISTR_SUSP_MASK    = (0b0000100000000000u);
    constexpr uint16_t ISTR_RESET_MASK   = (0b0000010000000000u);
    constexpr uint16_t ISTR_SOF_MASK     = (0b0000001000000000u);
    constexpr uint16_t ISTR_ESOF_MASK    = (0b0000000100000000u);
    constexpr uint16_t ISTR_L1REQ_MASK   = (0b0000000010000000u);
    constexpr uint16_t ISTR_DIR_MASK     = (0b0000000000010000u);
    constexpr uint16_t ISTR_EP_ID_MASK   = (0b0000000000001111u);
    /**
     * 
     */
	struct ISTR : ISTR_REG
    {
		using base_t = ISTR_REG;
		using base_t::base_t;
        using base_t::operator=;
        /**
         * 
         */
        ALWAYS_INLINE auto CTR    () { return base_t::template Actual<ISTR_CTR_MASK   >(); }
        ALWAYS_INLINE auto PMAOVR () { return base_t::template Actual<ISTR_PMAOVR_MASK>(); }
        ALWAYS_INLINE auto ERR    () { return base_t::template Actual<ISTR_ERR_MASK   >(); }
        ALWAYS_INLINE auto WKUP   () { return base_t::template Actual<ISTR_WKUP_MASK  >(); }
        ALWAYS_INLINE auto SUSP   () { return base_t::template Actual<ISTR_SUSP_MASK  >(); }
        ALWAYS_INLINE auto RESET  () { return base_t::template Actual<ISTR_RESET_MASK >(); }
        ALWAYS_INLINE auto SOF    () { return base_t::template Actual<ISTR_SOF_MASK   >(); }
        ALWAYS_INLINE auto ESOF   () { return base_t::template Actual<ISTR_ESOF_MASK  >(); }
        ALWAYS_INLINE auto L1REQ  () { return base_t::template Actual<ISTR_L1REQ_MASK >(); }
        ALWAYS_INLINE auto DIR    () { return base_t::template Actual<ISTR_DIR_MASK   >(); }
        ALWAYS_INLINE auto EP_ID  () { return base_t::template Actual<ISTR_EP_ID_MASK >(); }
    };
    /**
     * 
     */
    using FNR_REG = General::u32_reg<Registers_Base_Address + FNR_Offset>;
    constexpr uint16_t FNR_RXDP = 0b1000000000000000u;
    constexpr uint16_t FNR_RXDM = 0b0100000000000000u;
    constexpr uint16_t FNR_LCK  = 0b0010000000000000u;
    constexpr uint16_t FNR_LSOF = 0b0001100000000000u;
    constexpr uint16_t FNR_FN   = 0b0000011111111111u;
    /**
     * 
     */
	struct FNR : FNR_REG
    {
		using base_t = FNR_REG;
		using base_t::base_t;
        /**
         * 
         */
        ALWAYS_INLINE auto RX()  { return base_t::template Actual<FNR_RXDP | FNR_RXDM>();   }
        ALWAYS_INLINE auto RXDP() { return base_t::template Actual<FNR_RXDP>();   }
        ALWAYS_INLINE auto RXDM() { return base_t::template Actual<FNR_RXDM>();   }
        ALWAYS_INLINE auto LCK () { return base_t::template Actual<FNR_LCK>();    }
        ALWAYS_INLINE auto LSOF() { return base_t::template Actual<FNR_LSOF>();   }
        ALWAYS_INLINE auto FN  () { return base_t::template Actual<FNR_FN>();     }
    };
    /**
     * 
     */
    using DADDR_REG = General::u32_reg<Registers_Base_Address + DADDR_Offset>;
    constexpr uint16_t DADDR_ADD_MASK  = 0b01111111;
    constexpr uint16_t DADDR_EF_MASK   = 0b10000000;
    /**
     * 
     */
	struct DADDR : DADDR_REG
    {
		using base_t = DADDR_REG;
		using base_t::base_t;
        /**
         * 
         */
        ALWAYS_INLINE auto ADD()  { return base_t::template Actual<DADDR_ADD_MASK>();  }
        ALWAYS_INLINE auto EF()   { return base_t::template Actual<DADDR_EF_MASK>();   }
    };
    /**
     * 
     */
    using BTABLE_REG = General::u32_reg<Registers_Base_Address + BTABLE_Offset>;
    constexpr uint16_t BTABLE_BTABLE = 0b1111111111111000u;
    /**
     * BTABLE position
     */
	struct BTABLE : private BTABLE_REG
    {
		using base_t = BTABLE_REG;
		using base_t::base_t;
        /**
         * 
         */
        ALWAYS_INLINE operator uint32_t () const noexcept
        {
            return base_t::template Actual<BTABLE_BTABLE>();
        }
        ALWAYS_INLINE BTABLE & operator = (uint32_t value) noexcept
        {
            base_t::template Actual<BTABLE_BTABLE>() = value;
            return *this;
        }
    };
    /**
     * [P]acket [M]emory [A]rea
     */
    #pragma pack(0)
    constexpr size_t PMALength = 0x40006400 - 0x40006000;
    struct PacketMemoryArea
    {
        struct D
        {
            //
            //  [USB_BTABLE] + n*8 + 0
            uint16_t ADDRn_TX;
            //
            //  [USB_BTABLE] + n*8 + 2
            uint16_t COUNTn_TX  : 10u;
            uint16_t            : 6u;
            //
            //  [USB_BTABLE] + n*8 + 4
            uint16_t ADDRn_RX;
            //
            //  [USB_BTABLE] + n*8 + 6
            uint16_t COUNTn_RX  : 10u;
            uint16_t NUM_BLOCK  : 5u;
            uint16_t BL_SIZE    : 1u;
        }
        BufferDescriptionTable[ 8u ];
        /**
         * 
         */
        static_assert( sizeof( D ) == 8,                        "PMA table wrong size." );
        static_assert( sizeof( BufferDescriptionTable ) == 64,  "PMA table wrong size." );
        /**
         * The actual PMA memory
         */
        static constexpr uint16_t BufferOffset    = sizeof( BufferDescriptionTable );
        static constexpr uint16_t BufferLength    = ( PMALength - BufferOffset );
        /**
         * The buffer, this should be partitioned into various sections using DataView
         */
        std::array<uint8_t, BufferLength> Buffer;
        /**
         * Returns the amount of memory allocated to the buffer associated with the endpoint.
         */
        template <unsigned Endpoint>
        ALWAYS_INLINE std::size_t ReceiveAllocation( EP<Endpoint>, std::size_t buffer_size ) noexcept
        {
            auto & desc = BufferDescriptionTable[ Endpoint ];
            //
            // Determine the courseness of the buffer allocation
            // 62 is the maximum bytes allocatable with BL_SIZE = 0
            if ( buffer_size <= 62u )
            {
                // bytes = NUM_BLOCK * 2u
                // NUM_BLOCK = ( bytes + 1u ) /  2u
                //
                volatile uint16_t num_block = ( ( buffer_size + 1u ) / 2u ) & 0x1fu;
                volatile uint16_t count = num_block * 2u;
                //
                desc.BL_SIZE = 0u;
                desc.NUM_BLOCK = num_block;
                //
                // Round up so that sufficient space is allocated
                return count;
            }
            else
            {
                // bytes = ( NUM_BLOCK + 1u ) * 32u
                // NUM_BLOCK = ( bytes + 31u ) / 32u - 1u
                //
                volatile uint16_t num_block = ( ( buffer_size + 31u ) / 32u - 1u ) & 0x1f;
                volatile uint16_t count = ( num_block + 1u ) * 32u;
                //
                desc.BL_SIZE = 1u;
                desc.NUM_BLOCK = num_block;
                //
                // Round up so that sufficient space is allocated
                return count;
            }
        }
        /**
         * 
         */
        template <unsigned Endpoint>
        ALWAYS_INLINE std::size_t ReceiveAllocation( EP<Endpoint>, uint16_t address, std::size_t buffer_size ) noexcept
        {
            auto & desc = BufferDescriptionTable[ Endpoint ];
            //
            desc.ADDRn_RX = address;
            //
            return ReceiveAllocation(EP<Endpoint>{}, buffer_size);
        }
        /**
         * Gets the bytes Received into the buffer
         */
        template <unsigned Endpoint>
        ALWAYS_INLINE Containers::DataView<uint8_t, true> ReceiveData( EP<Endpoint> ) const noexcept
        {
            auto & desc = BufferDescriptionTable[ Endpoint ];
            return { &Buffer[ desc.ADDRn_RX - BufferOffset ], desc.COUNTn_RX };
        }
        /**
         * 
         */
        template <typename T = uint8_t, unsigned Endpoint>
        ALWAYS_INLINE Containers::DataView<T volatile, false> TransmitData( EP<Endpoint> ) noexcept
        {
            auto * volatile const  desc = &BufferDescriptionTable[ Endpoint ];
            //
            auto count = (((size_t)desc->ADDRn_RX - (size_t)desc->ADDRn_TX) / sizeof(T));
            T volatile * ptr = (T volatile *)&Buffer[ desc->ADDRn_TX - BufferOffset ];
            //
            return { ptr, count };
        }
        /**
         * Sets the transmit address
         */
        template <unsigned Endpoint>
        ALWAYS_INLINE void TransmitAllocation( EP<Endpoint>, std::uint16_t address, std::uint16_t length = 0 ) noexcept
        {
            BufferDescriptionTable[ Endpoint ].ADDRn_TX = address;
            BufferDescriptionTable[ Endpoint ].COUNTn_TX = length;
        }
        template <unsigned Endpoint>
        ALWAYS_INLINE std::uint16_t TransmitLength( EP<Endpoint> ) noexcept
        {
            return BufferDescriptionTable[ Endpoint ].COUNTn_TX;
        }
        template <unsigned Endpoint>
        ALWAYS_INLINE void TransmitLength( EP<Endpoint>, std::uint16_t length ) noexcept
        {
            BufferDescriptionTable[ Endpoint ].COUNTn_TX = length;
        }
        /**
         * Write to the PMA for the endpoint
         */
        template <unsigned Endpoint>
        ALWAYS_INLINE Containers::DataView<uint8_t> WriteTransmitPMA( EP<Endpoint> ep, Containers::DataView<uint8_t> data ) noexcept
        {
            auto pma = TransmitData<uint16_t volatile>( ep );
            /**
             * Case for more than x byte transmissions
             * odd sizes have the last byte padded with a zero.
             * that byte is not transmitted.
             */
            auto [ tx, remaining ] = data.Split( pma.Size() * 2u );
            Containers::DataView<uint16_t> tx_16{ (uint16_t const *)tx.Data(), ( tx.Size() / 2u ) };
            /**
             */
            {
                volatile size_t count_16 = tx_16.Size();
                for ( std::size_t i = 0u; i < count_16; ++i )
                {
                    pma[ i ] = tx_16[i];
                }
                /***
                 * If an odd number of bytes are being transmitted handle the last byte
                 */
                if (tx.Size() & 1)
                {
                    pma[ count_16 ] = static_cast<uint16_t>(tx.Back());
                }
            }
            /**
             * Write the count of the data to the approprate register
             */
            TransmitLength(ep, tx.Size());
            /**
             * This is how the transaction handles odd numbers of bytes, 
             * the transaction stops when the counter is zero (counted in bytes.)
             */
            return remaining;
        }
    };
    #pragma pack()
    /**
     * These ensure that the PMA is defined correctly
     */
    static_assert( PacketMemoryArea::BufferOffset == 64,            "PacketMemoryArea structure does not have the correct padding/packing.");
    static_assert( sizeof(PacketMemoryArea)       == PMALength,     "PacketMemoryArea size is incorrect and will not function.");
    /**
     * This function assigns values to the BTABLE register and then returns a reference to the PacketMemoryArea
     */
    ALWAYS_INLINE PacketMemoryArea * volatile GetPacketMemoryArea() noexcept
    {
        return (PacketMemoryArea * volatile)SRAM_Base_Address;
    }
    /**
     * 
     */
    using LPMCSR_REG = General::u32_reg<Registers_Base_Address + LPMCSR_Offset>;
    constexpr uint16_t LPMCSR_LPMEN_MASK     = 0b0000'0001u;
    constexpr uint16_t LPMCSR_LPMACK_MASK    = 0b0000'0010u; 
    constexpr uint16_t LPMCSR_REMWAKE_MASK   = 0b0000'1000u;
    constexpr uint16_t LPMCSR_BESL_MASK      = 0b1111'0000u;
    /**
     * 
     */
	struct LPMCSR : LPMCSR_REG
    {
		using base_t = LPMCSR_REG;
		using base_t::base_t;
        /**
         * 
         */
        ALWAYS_INLINE auto LPMEN  ()  { return base_t::template Actual<LPMCSR_LPMEN_MASK  >();  }
        ALWAYS_INLINE auto LPMACK ()  { return base_t::template Actual<LPMCSR_LPMACK_MASK >();  }
        ALWAYS_INLINE auto REMWAKE()  { return base_t::template Actual<LPMCSR_REMWAKE_MASK>();  }
        ALWAYS_INLINE auto BESL   ()  { return base_t::template Actual<LPMCSR_BESL_MASK   >();  }
    };
    /**
     * 
     */
    using BCDR_REG = General::u32_reg<Registers_Base_Address + BCDR_Offset>;
    constexpr uint16_t BCDR_DPPU_MASK    = 0b1000000000000000u;
    constexpr uint16_t BCDR_PS2DET_MASK  = 0b0000000010000000u;
    constexpr uint16_t BCDR_SDET_MASK    = 0b0000000001000000u;
    constexpr uint16_t BCDR_PDET_MASK    = 0b0000000000100000u;
    constexpr uint16_t BCDR_DCDET_MASK   = 0b0000000000010000u;
    constexpr uint16_t BCDR_SDEN_MASK    = 0b0000000000001000u;
    constexpr uint16_t BCDR_PDEN_MASK    = 0b0000000000000100u;
    constexpr uint16_t BCDR_DCDEN_MASK   = 0b0000000000000010u;
    constexpr uint16_t BCDR_BCDEN_MASK   = 0b0000000000000001u;
    /**
     * 
     */
	struct BCDR : BCDR_REG
    {
		using base_t = BCDR_REG;
		using base_t::base_t;
        /**
         * 
         */
        ALWAYS_INLINE auto DPPU  ()  { return base_t::template Actual<BCDR_DPPU_MASK  >(); }
        ALWAYS_INLINE auto PS2DET()  { return base_t::template Actual<BCDR_PS2DET_MASK>(); }
        ALWAYS_INLINE auto SDET  ()  { return base_t::template Actual<BCDR_SDET_MASK  >(); }
        ALWAYS_INLINE auto PDET  ()  { return base_t::template Actual<BCDR_PDET_MASK  >(); }
        ALWAYS_INLINE auto DCDET ()  { return base_t::template Actual<BCDR_DCDET_MASK >(); }
        ALWAYS_INLINE auto SDEN  ()  { return base_t::template Actual<BCDR_SDEN_MASK  >(); }
        ALWAYS_INLINE auto PDEN  ()  { return base_t::template Actual<BCDR_PDEN_MASK  >(); }
        ALWAYS_INLINE auto DCDEN ()  { return base_t::template Actual<BCDR_DCDEN_MASK >(); }
        ALWAYS_INLINE auto BCDEN ()  { return base_t::template Actual<BCDR_BCDEN_MASK >(); }
    };
}