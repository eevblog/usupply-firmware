#pragma once
#include "USB_Types.hpp"
#include "Meta.hpp"

namespace USB
{
    #pragma pack(push, 1)
    /**
     * @brief The endpoint address in a more typesafe form.
     */
    struct EndpointAddress
    {
        U<1>    EndpointNumber  : 4u;
        U<1>    Reserved6_4     : 3u;
        U<1>    Direction       : 1u;
        /**
         * 
         */
        constexpr EndpointAddress( uint8_t number, ::USB::Direction direction = ::USB::Direction::IN ) noexcept :
            EndpointNumber{ number },
            Reserved6_4{ 0 },
            Direction{ General::UnderlyingValue( direction ) }
        {}
    };
    /**
     * @brief A more typesafe endpoint attributes structure.
     * 
     * @note Warnings will appear in compiler for enum bitfields.
     */
    struct EndpointAttributes
    {
        enum : U<1>
        {
            Control             = 0b00,
            Isochronous         = 0b01,
            Bulk                = 0b10,
            Interrupt           = 0b11
        }
        TransferType : 2u = Control;
        enum : U<1>
        {
            NoSynchronization   = 0b00,
            Asynchronous        = 0b10,
            Adaptive            = 0b10,
            Synchronous         = 0b11
        }
        SyncronizationType : 2u = NoSynchronization;
        enum : U<1>
        {
            Data                = 0b00,
            Feedback            = 0b01,
            ImplicitFeedback    = 0b10
        }
        UsageType : 2u = Data;
        U<1> : 2u;
    };
    /**
     * @brief A standard endpoint descriptor.
     * 
     * @note see usb 2.0 specifications.
     */
    struct StandardEndpointDescriptor : 
        StandardHeader<StandardEndpointDescriptor, DescriptorTypes::ENDPOINT>
    {
        EndpointAddress     bEndpointAddress;
        EndpointAttributes  bmAttributes;
        U<2>                wMaxPacketSize;
        U<1>                bInterval;
    };
    #pragma pack(pop)
}