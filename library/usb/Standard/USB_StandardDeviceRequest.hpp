#pragma once
#include "USB_Types.hpp"
#include "USB_Languages.hpp"
#include "USB_HIDCommon.hpp"
#include <cstdint>

namespace USB
{
    #pragma pack(push, 1)
    enum FeatureSelector : uint8_t
    {
        DEVICE_REMOTE_WAKEUP    = 1,
        ENDPOINT_HALT           = 0,
        TEST_MODE               = 2,
    };
    /**
     * @brief A clear feature descriptor.
     * 
     * @note Refer to USB 2.0 specifications.
     */
    struct ClearFeature
    {
        RequestType const   bmRequestType;
        RequestCodes const  bRequest;
        FeatureSelector     wValue;
        std::uint16_t       wIndex;
        std::uint16_t       wLength;
    };
    /**
     * @brief A get configuration descriptor.
     * 
     * @note Refer to USB 2.0 specifications.
     */
    struct GetConfiguration
    {
        RequestType const   bmRequestType;
        RequestCodes const  bRequest;
        std::uint16_t       wValue;
        std::uint16_t       wIndex;
        std::uint16_t       wLength;
        /**
         * 1 byte data section
         * Containing Configuration Value
         */
    };
    /**
     * @brief A get descriptor, descriptor.
     * 
     * @note Refer to USB 2.0 specifications.
     */
    struct GetDescriptor
    {
        RequestType const   bmRequestType;
        RequestCodes const  bRequest;
        struct
        {
            std::uint8_t    DescriptorIndex;
            DescriptorTypes DescriptorType;
        }                   wValue;
        std::uint16_t       wIndex;
        std::uint16_t       wLength;
        /**
         * The device recieves a OUT transction with the descriptor
         */
    };
    /**
     * @brief A get interface descriptor.
     * 
     * @note Refer to USB 2.0 specifications.
     */
    struct GetInterface
    {
        RequestType const   bmRequestType;
        RequestCodes const  bRequest;
        uint16_t            wValue;
        struct
        {
            std::uint16_t   Interface;
        }                   wIndex;
        std::uint16_t       wLength;
        /**
         * The device transmits a 1 byte IN traction with the alternate setting
         */
    };
    /**
     * @brief A get status descriptor.
     * 
     * @note Refer to USB 2.0 specifications.
     */
    struct GetStatus
    {
        RequestType const   bmRequestType;
        RequestCodes const  bRequest;
        uint16_t            wValue;
        std::uint16_t       wIndex;
        std::uint16_t       wLength;
        /**
         * The device transmits a 2 byte IN traction with the device interface or endpoint status
         */
    };
    /**
     * @brief A device status descriptor.
     * 
     * @note Refer to USB 2.0 specifications.
     */
    struct DeviceStatus
    {
        std::uint16_t SelfPowered   : 1u;
        std::uint16_t RemoteWakeup  : 1u;
        std::uint16_t Reserved      : 14u;
        /**
         */
        constexpr DeviceStatus( bool self_powered = false, bool remote_wakeup = false ) noexcept :
            SelfPowered{ self_powered },
            RemoteWakeup{ remote_wakeup },
            Reserved{ 0 }
        {}
    };
    /**
     * @brief A interface status descriptor.
     * 
     * @note Refer to USB 2.0 specifications.
     */
    struct InterfaceStatus
    {
        std::uint16_t Reserved = 0;
    };
    /**
     * @brief A endpoint status descriptor.
     * 
     * @note Refer to USB 2.0 specifications.
     */
    struct EndpointStatus
    {
        std::uint16_t Halt      : 1u;
        std::uint16_t Reserved  : 15u;
    };
    /**
     * @brief A set address descriptor.
     * 
     * @note Refer to USB 2.0 specifications.
     */
    struct SetAddress
    {
        RequestType const   bmRequestType;
        RequestCodes const  bRequest;
        struct
        {
            std::uint16_t   DeviceAddress;
        }                   wValue;
        std::uint16_t       wIndex;
        std::uint16_t       wLength;
        /**
         * No data
         */
    };
    /**
     * @brief A clear feature descriptor.
     * 
     * @note Refer to USB 2.0 specifications.
     */
    struct SetConfiguration
    {
        RequestType const   bmRequestType;
        /**
         */
        RequestCodes const bRequest;
        /**
         */
        struct
        {
            std::uint16_t ConfigurationValue;
        }
        wValue;
        /**
         */
        std::uint16_t wIndex;
        /**
         */
        std::uint16_t wLength;
    };
    /**
     * @brief A set descriptor, descriptor.
     * 
     * @note Refer to USB 2.0 specifications.
     */
    struct SetDescriptor
    {
        RequestType const   bmRequestType;
        /**
         */
        RequestCodes const bRequest;
        /**
         */
        struct
        {
            std::uint8_t DescriptorIndex;
            DescriptorTypes DescriptorType;
        } wValue;
        /**
         * Language ID or Zero
         */
        LanguageID wIndex;
        /**
         * This is the descriptor length
         */
        std::uint16_t wLength;
        /**
         * The device recieves an OUT transaction from the host containing the new descriptor
         */
    };
    /**
     * @brief A set feature descriptor.
     * 
     * @note Refer to USB 2.0 specifications.
     */
    struct SetFeature
    {
        RequestType const   bmRequestType;
        RequestCodes const bRequest;
        FeatureSelector wValue;
        struct
        {
            enum : std::uint8_t 
            {
                Test_J               =  0x01, 
                Test_K               =  0x02, 
                Test_SE0_NAK         =  0x03, 
                Test_Packet          =  0x04, 
                Test_Force_Enable    =  0x05, 
            } 
            TestSelector;
            /**
             */
            std::uint8_t Index;
        } wIndex;
        /**
         * Length is always zero
         */
        std::uint16_t wLength;
    };
    /**
     * @brief A set interface descriptor.
     * 
     * @note Refer to USB 2.0 specifications.
     */
    struct SetInterface
    {
        RequestType const   bmRequestType;
        RequestCodes const  bRequest;
        struct 
        {
            uint16_t        AlternateSetting;
        }                   wValue;
        struct
        {
            std::uint16_t   Interface;
        }                   wIndex;
        std::uint16_t       wLength;
        /**
         * No data section
         */
    };
    /**
     * @brief A synch frame descriptor.
     * 
     * @note Refer to USB 2.0 specifications.
     */
    struct SynchFrame
    {
        RequestType const   bmRequestType;
        RequestCodes const  bRequest;
        uint16_t            wValue;
        struct
        {
            std::uint16_t   Endpoint;
        }                   wIndex;
        std::uint16_t       wLength;
        /**
         * Data is a 16 bit number containing the frame number
         */
    };
    /**
     * @brief Converts a generic request into the correct descriptor type and dispatches it to the result_callback.
     */
    template <typename F>
    bool Transform( uint8_t endpoint, StandardDeviceRequest request, F && result_callback ) noexcept
    {
        union Alias
        {
            StandardDeviceRequest   m_Setup;
            ClearFeature            m_ClearFeature;
            GetConfiguration        m_GetConfiguration;
            GetDescriptor           m_GetDescriptor;
            GetInterface            m_GetInterface;
            GetStatus               m_GetStatus;
            SetAddress              m_SetAddress;
            SetConfiguration        m_SetConfiguration;
            SetDescriptor           m_SetDescriptor;
            SetFeature              m_SetFeature;
            SetInterface            m_SetInterface;
            SynchFrame              m_SynchFrame;
        } const request_alias{ request };
        /**
         */
        static_assert( sizeof(Alias) == sizeof(StandardDeviceRequest), "Size of atleast one of the 'Standard Device Requests' is wrong." );
        auto f{ std::forward<F>(result_callback) };
        /**
         */
        if ( request.bmRequestType.Recipient == RequestRecipient::Device )
        {
            /**
             * Handles the standard requests
             */
            switch ( request.bRequest )
            {
            case RequestCodes::CLEAR_FEATURE:     f( endpoint, request_alias.m_ClearFeature       );   return true; 
            case RequestCodes::GET_STATUS:        f( endpoint, request_alias.m_GetStatus          );   return true;       
            case RequestCodes::SET_FEATURE:       f( endpoint, request_alias.m_SetFeature         );   return true;  
            case RequestCodes::SET_ADDRESS:       f( endpoint, request_alias.m_SetAddress         );   return true; 
            case RequestCodes::GET_DESCRIPTOR:    f( endpoint, request_alias.m_GetDescriptor      );   return true;  
            case RequestCodes::SET_DESCRIPTOR:    f( endpoint, request_alias.m_SetDescriptor      );   return true;  
            case RequestCodes::GET_CONFIGURATION: f( endpoint, request_alias.m_GetConfiguration   );   return true;        
            case RequestCodes::SET_CONFIGURATION: f( endpoint, request_alias.m_SetConfiguration   );   return true;        
            case RequestCodes::GET_INTERFACE:     f( endpoint, request_alias.m_GetInterface       );   return true;      
            case RequestCodes::SET_INTERFACE:     f( endpoint, request_alias.m_SetInterface       );   return true;        
            case RequestCodes::SYNCH_FRAME:       f( endpoint, request_alias.m_SynchFrame         );   return true;
            /**
             */
            default: break;
            }
        }
        return false;
    }
    #pragma pack(pop)
}