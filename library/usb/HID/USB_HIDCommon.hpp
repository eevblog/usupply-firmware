#pragma once
#include "USB_Types.hpp"
#include <cstdint>

namespace USB::HID
{
	using TransmitData 	= USB::TransmitData;
	using ReceiveData 	= USB::ReceiveData;
    /**
     * @brief The common header for all HID descriptors.
     */
    struct Header
    {
        RequestType         bmRequestType;
        RequestCodes const  bRequest;
    };
    /**
     * @brief The footer of all HID descriptors.
     */
    struct Footer
    {
        U<2> wIndex;    // Interface index
        U<2> wLength;   // Data stage length
    };
    /**
     * @brief Represents a single empty 8 bit byte.
     */
    struct EmptyByte
    {
        U<1> : 8u;
    };
    /**
     * @brief The protocol byte of a HID descriptor.
     */
    struct Protocol
    {
        enum : U<1>
        {
            BootProtocol,
            ReportProtocol
        } Protocol;
        U<1> : 7u;
    };
    /**
     * @brief The HID idle rate representation.
     */
    struct IdleRate
    {
        U<1> Data;
    };
    /**
     * @brief The data contained within a Get or Set idle descriptors.
     */
    struct IdleValue
    {
        U<1>        ReportID;
        IdleRate    Duration;
    };
    /**
     * @brief The data contained within the Get and Set descriptor descriptors.
     */
    struct DescriptorValue
    {
        union
        {
            U<2> wValue;
            struct
            {
                U<1> Index;
                DescriptorTypes Type;
            
            } Data;
        };
    };
    /**
     * @brief The report type.
     */
    enum class NoIdReportType : U<1>
    {
        Input = 0x01,
        Output = 0x02,
        Feature = 0x03
    };
    /**
     * @brief The basic body of a Get report descriptor.
     */
    struct ReportIDBasic
    {
        std::uint8_t    ReportID;
        NoIdReportType  ReportType;
    };
    /**
     * @brief A more generic version of the body of a Get report descriptor.
     */
    struct ReportIDSpecific
    {
        std::uint8_t    ReportID;
        U<1>            ReportType;
    };
    /**
     * @brief Different types are used for TAG dispatch, this avoids multiple redundant switch cases
     * it allows the use of a single switch in the below Transform function.
     */
    struct GetDescriptor        : Header, DescriptorValue,  Footer {};
    struct SetDescriptor        : Header, DescriptorValue,  Footer {};
    struct GetReportBasic       : Header, ReportIDBasic,    Footer {};  //if the report id is 0 then GetReportBasic
    struct GetReportSpecific    : Header, ReportIDSpecific, Footer {};  //else type is GetReportSpecific
    struct SetReport            : Header, ReportIDSpecific, Footer {};
    struct GetIdle              : Header, IdleValue,        Footer {};
    struct SetIdle              : Header, IdleValue,        Footer {};
    struct GetProtocol          : Header, EmptyByte,        Footer {};
    struct SetProtocol          : Header, Protocol,         Footer {};
    /**
     * @brief Transform Standard Request
     */
    template <typename F>
    bool Transform(uint8_t endpoint, StandardDeviceRequest request, F && result_callback) noexcept
    {
        union Alias
        {
            StandardDeviceRequest   m_Setup;
            GetDescriptor           m_GetDescriptor;
            SetDescriptor           m_SetDescriptor;
            GetReportBasic          m_GetReportBasic;
            GetReportSpecific       m_GetReportSpecific;
            SetReport               m_SetReport;
            GetIdle                 m_GetIdle;
            SetIdle                 m_SetIdle;
            GetProtocol             m_GetProtocol;
            SetProtocol             m_SetProtocol;
        }
        request_alias{ request };
        /**
         * @brief Ensure that padding hasn't caused issues with the union.
         * 
         * This essentially checks that the structures fully overlap in memory.
         * This is nessary because it is undefined behaviour to access a non-active member of a union in C++.
         */
        static_assert( sizeof(Alias) == sizeof(StandardDeviceRequest), "Size of atleast one of the 'Standard Device Requests' is wrong." );
        /**
         */
        auto f{ std::forward<F>(result_callback) };
        /**
         */
        switch ( request.bRequest )
        {
        /**
         * Standard requests
         */
        case RequestCodes::GET_DESCRIPTOR:      f( endpoint, request_alias.m_GetDescriptor );   return true;
        case RequestCodes::SET_DESCRIPTOR:      f( endpoint, request_alias.m_SetDescriptor );   return true;
        /** 
         * HID Get commands 
         */ 
        case RequestCodes::HID_GET_REPORT:      f( endpoint, request_alias.m_GetReportBasic );  return true;
        case RequestCodes::HID_GET_IDLE:        f( endpoint, request_alias.m_GetIdle );         return true;
        case RequestCodes::HID_GET_PROTOCOL:    f( endpoint, request_alias.m_GetProtocol );     return true;
        /** 
         * HID Set commands 
         */ 
        case RequestCodes::HID_SET_REPORT:      f( endpoint, request_alias.m_SetReport );       return true;
        case RequestCodes::HID_SET_IDLE:        f( endpoint, request_alias.m_SetIdle );         return true;
        case RequestCodes::HID_SET_PROTOCOL:    f( endpoint, request_alias.m_SetProtocol );     return true;
        /**
         * No recognised request
         */
        default: break;
        }
        return false;

    }
}