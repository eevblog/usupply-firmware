#pragma once
#include "USB_StandardInterfaceDescriptor.hpp"
#include "USB_HIDDescriptor.hpp"

namespace USB::HID
{
    /**
     * @brief 
     */
    enum class SubClassCodes : U<1>
    {
        None            = 0x00,
        BootInterface   = 0x01
    };
    /**
     * @brief 
     */
    enum class ProtocolCodes : U<1>
    {
        None            = 0x00,
        Keyboard        = 0x01,
        Mouse           = 0x02
    };
    /**
     * @brief The base class of a HID interface descriptor.
     * 
     * This contains common components of any HID interface descriptor.
     */
    template <std::size_t Count>
    struct DescriptorBase : BaseInterfaceDescriptor<InterfaceClass::HID, DescriptorBase<Count>>
    {
        static constexpr std::size_t EndpointCount = Count;
        /**
         */
        SubClassCodes    bInterfaceSubClass;
        ProtocolCodes    bInterfaceProtocol;
        U<1>             iInterface;
    };
    /**
     * @brief Used to generate an interface descriptor from a  set of component descriptors.
     * 
     * The class requires at least 1:
     * @li HID descriptor.
     * @li Endpoint descriptor.
     * 
     * @tparam HIDDescriptor The endpoint descriptor type.
     * @tparam Endpoints The endpoint descriptors.
     */
    template <typename HIDDescriptor, typename ... Endpoints>
    struct AutoInterface :
        DescriptorBase<sizeof...(Endpoints)>,
        HIDDescriptor,
        PackedTuple<Endpoints...>
    {
        using base_t = DescriptorBase<sizeof...(Endpoints)>;
        using interface_t = BaseInterfaceDescriptor<InterfaceClass::HID, base_t>;
        /**
         * @brief Construct a HID interface descriptor.
         * 
         * @param number The interface number.
         * @param sub_class The descriptor sub class, see SubClassCodes.
         * @param interface_protocol The protocol code, see ProtocolCodes.
         * @param interface The string index number.
         * @param hid_descriptor The HID descriptor.
         * @param endpoints The endpoint descriptors.
         */
        constexpr AutoInterface( U<1> number, SubClassCodes sub_class, ProtocolCodes interface_protocol, U<1> interface, HIDDescriptor hid_descriptor, Endpoints ... endpoints ) noexcept :
            base_t{ { number }, sub_class, interface_protocol, interface },
            HIDDescriptor{ hid_descriptor },
            PackedTuple<Endpoints...>  { endpoints   ... }
        {}
    };
}