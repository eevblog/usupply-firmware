#pragma once
#include "Ratio.hpp"
#include "Integer.hpp"
#include <utility>

namespace Parts::AD5260
{
	/**
	 * @brief A manager class of the AD5260 chip.
	 * 
	 * @tparam SPIKernal The SPI interface to use for comms.
	 */
	template <typename SPIKernal>
	class AD5260 : SPIKernal
	{
	private:
		static constexpr unsigned Bits = 8u;
		using word_t = General::UnsignedInt<Bits>;
		//
		unsigned const m_StepSize;
		static constexpr unsigned Maximum  = word_t::max;
		static constexpr unsigned Midscale = word_t::max >> 1u;
	public:
		/**
		 * @brief Construct a AD5260 manager class.
		 * 
		 * @tparam Args The types used to construct the underlying SPI implementation.
		 * @param max The maximum value of the digipot.
		 * @param args The arguments used to construct the AD5260.
		 */
		template <typename ... Args>
		AD5260( unsigned max, Args && ... args ) noexcept:
			SPIKernal	{ std::forward<Args>( args ) ... },
		    m_StepSize	{ ( max + Midscale ) / Maximum }
		{}
		/**
		 * @brief Overload for raw potentiometer register assignment.
		 * 
		 * @param input The unsigned integer input.
		 * @return AD5260& For chained operations.
		 */
		AD5260 & operator=( word_t::type input ) noexcept
		{
			SPIKernal::template Write<Bits>( input );
			return *this;
		}
		/**
		 * @brief Assign a resistance to the AD5260.
		 * 
		 * The resistance is the low side resistance.
		 * 
		 * @param input The resistance to assign.
		 * @return AD5260& For chained operations.
		 */
		AD5260 & operator=( float input ) noexcept
		{
			word_t::type value = ( input / (float)m_StepSize ) + 0.5f;
			return ( *this = value );
		}
		/**
		 * @brief Assign a resistance to the AD5260.
		 * 
		 * The resistance is the low side resistance.
		 * 
		 * @param input The resistance to assign.
		 * @return AD5260& For chained operations.
		 */
		AD5260 & operator=( double input ) noexcept
		{
			word_t::type value = ( input / (double)m_StepSize ) + 0.5;
			return ( *this = value );
		}
		/**
		 * @brief Assign a ratio to the AD5260.
		 * 
		 * @tparam T The type of the underlying type in the ratio.
		 * @param input The ratio.
		 * @return AD5260& For chained operations.
		 */
		template <typename T>
		AD5260 & operator=( General::Ratio<T> input ) noexcept
		{
			return ( *this = ( word_t::type )( (T)input * (T)Maximum ) );
		}
	};
}
