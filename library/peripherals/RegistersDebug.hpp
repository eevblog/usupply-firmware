#pragma once

#include "Meta.hpp"
#include "Pin.hpp"
#include "Register.hpp"
#include <cstdint>

namespace Peripherals::DebugGeneral
{
	template <std::uint32_t Channel>
	constexpr bool IsTimerAPB1 =  General::IsOneOf( Channel, 2u, 3u, 6u, 7u, 14u );
	template <std::uint32_t Channel>
	constexpr bool IsTimerAPB2 =  General::IsOneOf( Channel, 15u, 1u, 16u, 17u );
	template <std::uint32_t Channel>
	constexpr bool IsTimerChannel = ( IsTimerAPB1<Channel> || IsTimerAPB2<Channel> );
	
	template <std::size_t A>
	struct IDCODE : General::u32_reg<A>
	{
		using base_t = General::u32_reg<A>;
		using base_t::base_t;
		
		auto REV_ID () { return base_t::template Actual<DBGMCU_IDCODE_REV_ID>(); }
		auto DEV_ID () { return base_t::template Actual<DBGMCU_IDCODE_DEV_ID>(); }
	};
	
	template <std::size_t A>
	struct CR : General::u32_reg<A>
	{
		using base_t = General::u32_reg<A>;
		using base_t::base_t;
		
		auto STANDBY() 	{ return base_t::template Actual<DBGMCU_STANDBY>(); }		
		auto STOP() 	{ return base_t::template Actual<DBGMCU_STOP>(); }		
	};
	
	template <std::size_t A>
	struct APB1FZ : General::u32_reg<A>
	{
		using base_t = General::u32_reg<A>;
		using base_t::base_t;
		
        auto CAN_STOP          () { return base_t::template Actual<DBGMCU_CAN_STOP         	>(); }
        auto I2C1_SMBUS_TIMEOUT() { return base_t::template Actual<DBGMCU_I2C1_SMBUS_TIMEOUT>(); }
        auto IWDG_STOP         () { return base_t::template Actual<DBGMCU_IWDG_STOP        	>(); }
        auto WWDG_STOP         () { return base_t::template Actual<DBGMCU_WWDG_STOP        	>(); }
        auto RTC_STOP          () { return base_t::template Actual<DBGMCU_RTC_STOP         	>(); }
        auto TIM14_STOP        () { return base_t::template Actual<DBGMCU_TIM14_STOP       	>(); }
        auto TIM7_STOP         () { return base_t::template Actual<DBGMCU_TIM7_STOP        	>(); }
        auto TIM6_STOP         () { return base_t::template Actual<DBGMCU_TIM6_STOP        	>(); }
        auto TIM3_STOP         () { return base_t::template Actual<DBGMCU_TIM3_STOP        	>(); }
        auto TIM2_STOP	       () { return base_t::template Actual<DBGMCU_TIM2_STOP	       	>(); }
	};
	
	template <std::size_t A>
	struct APB2FZ : General::u32_reg<A>
	{
		using base_t = General::u32_reg<A>;
		using base_t::base_t;
		
		auto TIM17_STOP() { return base_t::template Actual<DBGMCU_TIM17_STOP>(); }
		auto TIM16_STOP() { return base_t::template Actual<DBGMCU_TIM16_STOP>(); }
		auto TIM15_STOP() { return base_t::template Actual<DBGMCU_TIM15_STOP>(); }
		auto TIM1_STOP () { return base_t::template Actual<DBGMCU_TIM1_STOP >(); }
	};
	
	IDCODE()	-> IDCODE	<DBGMCU_BASE + offsetof(DBGMCU_TypeDef, IDCODE)>;
	CR() 		-> CR		<DBGMCU_BASE + offsetof(DBGMCU_TypeDef, CR)>;
	APB1FZ() 	-> APB1FZ	<DBGMCU_BASE + offsetof(DBGMCU_TypeDef, APB1FZ)>;
	APB2FZ() 	-> APB2FZ	<DBGMCU_BASE + offsetof(DBGMCU_TypeDef, APB2FZ)>;
}