#pragma once

#include "Types.hpp"
#include "Register.hpp"
#include "stm32f0xx.h"
#include "stm32f0xx_flash.h"
#include <cstdint>

namespace Peripherals::FlashGeneral
{
	enum class ReadProtectionLevel : std::uint32_t
	{
		Level0 = 0b00,
		Level1 = 0b01,
		Level2 = 0b11
	};
	enum class Latency : std::uint32_t
	{
		ZeroWaitStates	=	0b000,
		OneWaitState	=	0b001
	};
	template <std::size_t A>
	struct ACR : public General::u32_reg<A>
	{
		using base_t = General::u32_reg<A>;
		using base_t::base_t;
		
		auto PRFTBS () { return base_t::template Actual<FLASH_ACR_PRFTBS >(); }
		auto PRFTBE () { return base_t::template Actual<FLASH_ACR_PRFTBE >(); }
		auto LATENCY() { return base_t::template Actual<FLASH_ACR_LATENCY>(); }
		
		void SetupLatency()
		{
			LATENCY() = ( ::System::SystemClock_t::SYSCLK() > General::MHz(24) );
		}
	};
	template <std::size_t A>
	struct KEYR : public General::u32_reg<A>
	{
		using base_t = General::u32_reg<A>;
		using base_t::base_t;
		using base_t::operator=;
	};
	
	template <std::size_t A>
	struct OPTKEYR : public General::u32_reg<A>
	{
		using base_t = General::u32_reg<A>;
		using base_t::base_t;
		using base_t::operator=;
	};
	
	template <std::size_t A >
	struct SR : public General::u32_reg<A>
	{
		using base_t = General::u32_reg<A>;
		using base_t::base_t;
		
		auto EOP     () { return base_t::template Actual<FLASH_SR_EOP 		>(); }
		auto WRPRTERR() { return base_t::template Actual<FLASH_SR_WRPRTERR	>(); }
		auto PGERR   () { return base_t::template Actual<FLASH_SR_PGERR 	>(); }
		auto BSY     () { return base_t::template Actual<FLASH_SR_BSY 		>(); }
	};

	template <std::size_t A>
	struct CR : public General::u32_reg<A>
	{
		using base_t = General::u32_reg<A>;
		using base_t::base_t;
		
		auto OBL_LAUNCH() { return base_t::template Actual<FLASH_CR_OBL_LAUNCH>(); }
		auto EOPIE     () { return base_t::template Actual<FLASH_CR_EOPIE     >(); }
		auto ERRIE     () { return base_t::template Actual<FLASH_CR_ERRIE     >(); }
		auto OPTWRE    () { return base_t::template Actual<FLASH_CR_OPTWRE    >(); }
		auto LOCK      () { return base_t::template Actual<FLASH_CR_LOCK      >(); }
		auto STRT      () { return base_t::template Actual<FLASH_CR_STRT      >(); }
		auto OPTER     () { return base_t::template Actual<FLASH_CR_OPTER     >(); }
		auto OPTPG     () { return base_t::template Actual<FLASH_CR_OPTPG     >(); }
		auto MER       () { return base_t::template Actual<FLASH_CR_MER       >(); }
		auto PER       () { return base_t::template Actual<FLASH_CR_PER       >(); }
		auto PG        () { return base_t::template Actual<FLASH_CR_PG        >(); }
	};
	
	template <std::size_t A>
	struct AR : public General::u32_reg<A>
	{
		using base_t = General::u32_reg<A>;
		using base_t::base_t;
		using base_t::operator=;
	};
	
	template <std::size_t A>
	struct OBR : public General::u32_reg<A>
	{
		using base_t = General::u32_reg<A>;
		using base_t::base_t;
		
		auto DATA1           () { return base_t::template Actual<FLASH_OBR_DATA1           >(); }
		auto DATA0           () { return base_t::template Actual<FLASH_OBR_DATA0           >(); }
		auto RAM_PARITY_CHECK() { return base_t::template Actual<FLASH_OBR_RAM_PARITY_CHECK>(); }
		auto VDDA_MONITOR    () { return base_t::template Actual<FLASH_OBR_VDDA_MONITOR    >(); }
		auto nBOOT1          () { return base_t::template Actual<FLASH_OBR_nBOOT1          >(); }
		auto nBOOT0          () { return base_t::template Actual<FLASH_OBR_nBOOT0          >(); }
		auto nRST_STDBY      () { return base_t::template Actual<FLASH_OBR_nRST_STDBY      >(); }
		auto nRST_STOP       () { return base_t::template Actual<FLASH_OBR_nRST_STOP       >(); }
		auto WDG_SW          () { return base_t::template Actual<FLASH_OBR_IWDG_SW         >(); }
		auto RDPRT           () { return base_t::template Actual<FLASH_OBR_RDPRT1 | FLASH_OBR_RDPRT2           >(); }
		auto OPTERR          () { return base_t::template Actual<FLASH_OBR_OPTERR          >(); }
		
		void SetReadProtectionLevel(ReadProtectionLevel const pInput)
		{
			RDPRT() = (std::uint32_t)pInput;
		}
	};
	
	template <std::size_t A>
	struct WRPR : public General::u32_reg<A>
	{
		using base_t = General::u32_reg<A>;
		using base_t::base_t;
	};
	
#define DEFAULTS(x) x() -> x<FLASH_R_BASE + offsetof(FLASH_TypeDef, x)>
	DEFAULTS(ACR);
	DEFAULTS(KEYR);
	DEFAULTS(OPTKEYR);
	DEFAULTS(SR);
	DEFAULTS(CR);
	DEFAULTS(AR);
	DEFAULTS(OBR);
	DEFAULTS(WRPR);
#undef DEFAULTS
}