#pragma once

#include "Meta.hpp"
#include "Power.hpp"
#include "RegistersUSART.hpp"
#include "FIFO.hpp"
#include "BlindFIFO.hpp"
#include "ArrayConvert.hpp"
#include "StaticLambdaWrapper.hpp"
#include "Convert.hpp"
#include <cstdint>

namespace Peripherals
{
	enum class CommandCompletion
	{
		No,
		Submit,
		Silent
	};
	//This is a class which contains all the data for a particular channel, this seperates the function template from
	// the usart class, enabling it to be passed to child classes
	template <typename Kernal>
	struct USARTSender
	{
		using RXPin = typename Kernal::RXPin;
		static constexpr auto Channel = USARTGeneral::USARTChannelRX_v<typename RXPin::AltFunctions>;
        
		using TDR = USARTGeneral::template TDR<Channel>;
		using RDR = USARTGeneral::template RDR<Channel>;
		using BRR = USARTGeneral::template BRR<Channel>;
		using CR3 = USARTGeneral::template CR3<Channel>;
		using CR2 = USARTGeneral::template CR2<Channel>;
		using CR1 = USARTGeneral::template CR1<Channel>;
		using ISR = USARTGeneral::template ISR<Channel>;
		using ICR = USARTGeneral::template ICR<Channel>;
        
		using rx_buffer_t	= Containers::FILO<char, Kernal::BufferSize>;
		using rx_cbuffer_t 	= Containers::BlindFIFO<char, 3>;
		using tx_buffer_t	= Containers::FIFO<char, Kernal::BufferSize>;
        
		static inline rx_buffer_t   m_RBuffer{};
		static inline rx_cbuffer_t	m_RCBuffer{};
		static inline tx_buffer_t   m_TBuffer{};
        
		USARTSender() = default;
        
		static void KickstartTx() noexcept
		{
			if ( !CR1().TCIE() )
			{
				CR1().TCIE() = true;
				if ( auto data = m_TBuffer.Pop(); data.has_value() )
					TDR() = data.value();
			}
		}

		static void BlockingTx(char const pInput)
		{
			while(!ISR().TC());
			TDR() = pInput;
		}

		static void TXPush(char const pInput) noexcept
		{
			if (m_TBuffer.Push( pInput ))
				KickstartTx();
		}
        
		template <typename ... Args>
		static std::size_t SendInternal( Args && ... pArgs ) noexcept
		{
			return ( (General::xtoa( std::forward<Args>(pArgs), m_TBuffer ) > 0) + ... );
		}

		static CommandCompletion RxComplete(char input) noexcept
		{
			return Kernal::CommandComplete(input);
		}

		static void BlockTilClear() noexcept
		{
			while(!m_TBuffer.Empty());
		}

		template <typename ... Args>
		static void Send( Args && ... args ) noexcept
		{
			SendInternal( std::forward<Args>(args) ... );
			KickstartTx();
		}
        
		template <std::size_t N>
		static void BlockingSend(std::array<char, N> const & pBlock)
		{
			BlockTilClear();
			//
			for(char c : pBlock) BlockingTx(c);
		}

		static void BlockingSend(const char * input)
		{
			BlockTilClear();
			//
			if(input)
			{
				while(*input != '\0')
				{
					BlockingTx(*input);
					++input;
				}
			}
		}
	};

	//This class manages a particular USART channel, it contains a complex type Callback which means that the
	// deduction guide should be used for the classes construction
	template<typename Kernal, typename Callback>
	class USARTModule : Kernal::RXPin, Kernal::TXPin
	{
	public:
		using RXPin = typename Kernal::RXPin;
		using TXPin = typename Kernal::TXPin;
		//
		static constexpr auto Baudrate		= Kernal::Baudrate;
		static constexpr auto Databits 		= Kernal::Databits;
		static constexpr auto Stopbits 		= Kernal::Stopbits;
		static constexpr auto Parity 		= Kernal::Parity;
		static constexpr auto FlowControl 	= Kernal::FlowControl;
		static constexpr auto BufferSize	= Kernal::BufferSize;
		static constexpr auto Echo			= Kernal::Echo;
		static constexpr auto RXChannel		= USARTGeneral::USARTChannelRX_v<typename RXPin::AltFunctions>;
		static constexpr auto TXChannel		= USARTGeneral::USARTChannelTX_v<typename TXPin::AltFunctions>;
		static constexpr auto Channel 		= TXChannel;
		//
		static_assert(RXChannel > 0, 			"USARTModule<...> : The pin selected for RX does not support USART.");
		static_assert(TXChannel > 0, 			"USARTModule<...> : The pin selected for TX does not support USART.");
		static_assert(RXChannel == TXChannel, 	"USARTModule<...> : The pins selected for RX and TX do not support the same USART channel (they must).");
		//
		using kernal_t 		= USARTGeneral::template USARTPowerKernal<Channel>;
		using power_t 		= General::ModulePower<kernal_t>;
		using interrupt_t 	= System::Interrupt<USARTModule, USARTGeneral::template USARTChannelInterrupt<Channel>()>;
		using function_t 	= General::StaticLambdaWrapper<Callback>;
		using sender_t 		= USARTSender<Kernal>;
	private:
		using TDR = USARTGeneral::template TDR<Channel>;
		using RDR = USARTGeneral::template RDR<Channel>;
		using BRR = USARTGeneral::template BRR<Channel>;
		using CR3 = USARTGeneral::template CR3<Channel>;
		using CR2 = USARTGeneral::template CR2<Channel>;
		using CR1 = USARTGeneral::template CR1<Channel>;
		using ISR = USARTGeneral::template ISR<Channel>;
		using ICR = USARTGeneral::template ICR<Channel>;
		//
		//
		Kernal 					m_Kernal; //Should initialise pins
		interrupt_t 			m_ISR;
		power_t 				m_PSR;
	protected:
		function_t m_Function;
		inline static sender_t m_Sender{};
	public:
		template<typename C> 
		USARTModule( C && callback ) :
			RXPin{ IO::Mode::Alternate, USARTGeneral::USARTAltFunctionRX_v<typename RXPin::AltFunctions, RXChannel> },
			TXPin{ IO::Mode::Alternate, USARTGeneral::USARTAltFunctionTX_v<typename TXPin::AltFunctions, TXChannel> },
			m_Function( std::forward<C>( callback ) )
		{
            RCCGeneral::CFGR3().SelectUSARTClock<Channel>( RCCGeneral::USARTClock::HSI );
            //
			// Send some blank data to initialise any statics in the sender function
			function_t::Run( CommandCompletion::Silent );
			//
			// These are always enabled
			CR1().OVER8() = true;
			CR3().ONEBIT() = true;
			CR3().OVRDIS() = true;
			//
			// Setup the baudrate based on paramters
			BRR().SetBaudrate( Baudrate );
			//
			// Configure the protocol
			CR2().SetStopbits( Stopbits );
			CR1().SetWordLength( Databits );
			CR1().SetParity( Parity );
			//
			// Enable the peripherial
			CR1().UE() = true;
			//
			// Wait for peripheral to enable
			while (!ISR().TC());
			ICR().TCCF() = true;
			//
			// Only enable DMA if it is requested by the kernal
			//	This must be done after UE is enabled.
			if constexpr (Kernal::UseDMA)
			{
				CR3().DMAT() = true;
				CR3().DMAR() = true;
			}
			//
			// Enable transmit and recieve
			CR1().RE() = true;
			CR1().TE() = true;
			//
			//
			CR1().RXNEIE() = true;
		}
		USARTModule( General::Type<Kernal>, Callback && callback ) noexcept : USARTModule{ std::forward<Callback>(callback) } {}
		~USARTModule(){}
		//
		//
		static auto & RBuffer() noexcept
		{
			return m_Sender.m_RBuffer;
		}
		static auto & RCBuffer() noexcept
		{
			return m_Sender.m_RCBuffer;
		}
		//
		//
		template <typename ... Args>
		static void Send( Args && ... args ) noexcept
		{
			m_Sender.Send( std::forward<Args>(args) ... );
		}
		template <typename T>
		static void Transmit( T const & data ) noexcept
		{
			for( auto c : data )
			{
				m_Sender.m_TBuffer.Push(c);
			}
			m_Sender.KickstartTx();
		}
		//
		//
		template <typename ... Args>
		static void BlockingSend( Args && ... args ) noexcept
		{
			( m_Sender.BlockingSend( std::forward<Args>( args ) ), ... );
		}
		//
		//
		static void Interrupt() noexcept
		{
			// Transmit complete indicates another byte can be sent
			if ( ISR().TC() )
			{
				//
				// This is a transmit only interrupt in this context
				ICR().TCCF() = true;
				if ( auto data{ m_Sender.m_TBuffer.Pop() }; data.has_value() )
				{
					TDR() = data.value();
				}
				//
				//If buffer is now empty disable the interrupt
				else CR1().TCIE() = false;
			}
			//
			// Idle indicates that the DMA transaction can be cancelled as the line is idle
			// 	take data that has been aquired and run the callback with it
			else if ( ISR().RXNE() )
			{
				char const value = RDR().Get();
				//
				if ( Kernal::ShouldPush( value, RCBuffer() ) )
				{
					RCBuffer().Clear();
					RBuffer().Push( value );
					//
					if constexpr ( Echo )
						m_Sender.TXPush( value );
					//
					// Remove last character with backspace
					if ( Kernal::IsBackspace( value ) )
						RBuffer().Pop(2);	//Remove backspace char and the previous
					//
					// Reset state when recieve command is complete
					if ( auto s{ m_Sender.RxComplete( value ) }; s != CommandCompletion::No )
					{
						function_t::Run( s );
						RCBuffer().Clear();
						RBuffer().Clear();
					}
				}
			}
		}
	};

	//This allows deduction of the template parameters of the USARTModule class.
	//	the type of C is complex and may be of a lambda type, this is why a deduction guide is
	// 	nessary
	template <typename K, typename C>
	USARTModule(General::Type<K>, C) -> USARTModule<K, C>;
}