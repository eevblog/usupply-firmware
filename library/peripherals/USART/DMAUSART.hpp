#pragma once
#include "Meta.hpp"
#include "Power.hpp"
#include "DMA.hpp"
#include "StaticLambdaWrapper.hpp"
#include "RegistersUSART.hpp"
#include "DMAUSARTBuffer.hpp"
#include <cstdint>
#include <string.h>

namespace Peripherals
{
	//This class manages a particular USART channel, it contains a complex type Callback which means that the
	// deduction guide should be used for the classes construction
	template<typename Kernal, typename RecieveCallback>
	class USARTDMAModule : Kernal::RXPin, Kernal::TXPin
	{
	public:
		//
		// The pins used by the USART port
		using RXPin         = typename Kernal::RXPin;
		using TXPin         = typename Kernal::TXPin;
		//
		//
		static constexpr auto Baudrate		= Kernal::Baudrate;
		static constexpr auto Databits 		= Kernal::Databits;
		static constexpr auto Stopbits 		= Kernal::Stopbits;
		static constexpr auto Parity 		= Kernal::Parity;
		static constexpr auto FlowControl 	= Kernal::FlowControl;
		static constexpr auto RXChannel		= USARTGeneral::USARTChannelRX_v<typename RXPin::AltFunctions>;
		static constexpr auto TXChannel		= USARTGeneral::USARTChannelTX_v<typename TXPin::AltFunctions>;
		/**
		 * 
		 */
		static_assert(RXChannel > 0, 			"USARTDMAModule<...> : The pin selected for RX does not support USART.");
		static_assert(TXChannel > 0, 			"USARTDMAModule<...> : The pin selected for TX does not support USART.");
		static_assert(RXChannel == TXChannel, 	"USARTDMAModule<...> : The pins selected for RX and TX do not support the same USART channel (they must)."); 
		static constexpr auto Channel = Kernal::Channel;
		//
		//
		using kernal_t 				= USARTGeneral::template USARTPowerKernal<Channel>;
		//
		//
		using interrupt_t 			= System::Interrupt<USARTDMAModule, USARTGeneral::template USARTChannelInterrupt<Channel>()>;
		using power_t 				= General::ModulePower<kernal_t>;
		using recieve_callback_t	= General::StaticLambdaWrapper<RecieveCallback>;
	private:
		using TXDMAProp = DMAProperties<1, Kernal::TXDMAChannel>;
		using RXDMAProp = DMAProperties<1, Kernal::RXDMAChannel>;
		//
		//
		using TDR = USARTGeneral::template TDR<Channel>;
		using RDR = USARTGeneral::template RDR<Channel>;
		using BRR = USARTGeneral::template BRR<Channel>;
		using CR3 = USARTGeneral::template CR3<Channel>;
		using CR2 = USARTGeneral::template CR2<Channel>;
		using CR1 = USARTGeneral::template CR1<Channel>;
		using ISR = USARTGeneral::template ISR<Channel>;
		using ICR = USARTGeneral::template ICR<Channel>;
		//
		//
		interrupt_t     		m_ISR;
		power_t         		m_PSR;
		recieve_callback_t		m_RecieveCallback;
		//
		// DataView : An observer of a block of memory.
		// 	Data is send through packets defined by dataviews, these represent an 
		//	arbitary contigious data structure. They allow complete memory flexibility.
		using dataview_t 		= Containers::DataView<uint8_t>;
		using recieve_data_t 	= dataview_t;
		using send_data_t 		= dataview_t;
		/**
		 * 
		 */
		static void RecieveBlock( recieve_data_t block ) noexcept
		{
			recieve_callback_t::Run( block );
		}
		/**
		 * This handles the recieve DMA setup. this is a circular DMA.
		 * 	The above RecieveBlock function will be called every time a block is recieved.
		 * 	This is **NOT** the only place the RecieveBlock is called it is also called from the USART
		 * 	interrupt when an idle recieve line is detected.
		 * 	This function should only be called once.
		 * 
		 * 	NOTE: 	A partial section recieve MUST reset the DMA recieve counter, this is critical because
		 * 			the section will retrigger a RecieveBlock function once the complete partial section is recieve.
		 * 			this results in duplication of data.
		 */
		inline static std::array<uint8_t, Kernal::BufferSize>	ReceiveBuffer{};
		inline static CircularBuffer<Kernal::BufferSize> 		ReceiveManager{};
		//
		// This function handles the transmit DMA setup, this is **NOT** a circular DMA
		// 	this can be problematic if the DMA is left to get out of hand because it will go completely 
		//	bananas retranmitting already transmitted data. This is why it is important to clear CIRC after
		//	the last section of data is transmitted.
		//
		// Related data and types
		inline static std::array<uint8_t, Kernal::BufferSize> 						TransmitBuffer{};
		inline static Containers::DataView<uint8_t, false>					TransmitView{0, 0};
		static constexpr size_t BufferBoundary{ Kernal::BufferSize / 2u };
	private:
		/**
		 * @brief Enables the dma receive functionality
		 */
		static void ContinueReceive(DMAInterruptFlag flag) noexcept
		{
			switch ( flag )
			{
			case DMAInterruptFlag::Error: break;
			case DMAInterruptFlag::HalfComplete:
			case DMAInterruptFlag::Complete:
				/**
				 * Load the half of data into the buffer
				 */
				ReceiveManager( RXDMAProp::Remaining(), ReceiveBuffer, &RecieveBlock );
				break;
			};
		}
		static auto const & SetupDMAReceive() noexcept
		{
			ReceiveManager.Reset( Kernal::BufferSize );
			/**
			 * If the send queue not empty and this function was called (pre-condition)
			 * then the DMA module should be avaliable.
			 * 	NOTE:	If this function is called when the DMA module hasn't completed its previous transation
			 * 			it is invoking undefined behaviour. Very likely corruption or totally missed callbacks.
			 */
			static DMAModule dma
			{
				RXDMAProp{},
				[](DMAInterruptFlag f){ ContinueReceive (f); },	// callback
				RDR().Addr,										// peripheral					Read from the [R]ead [D]ata [R]egister.
				ReceiveBuffer.data(),							// memory						Pointer to the data to be written into.
				ReceiveBuffer.size(),							// count						Number of items to be written into.
				DMAGeneral::Direction::ReadFromPeripheral,		// direction					Read from peripheral into memory.
				DMAGeneral::Priority::Medium,					// priority						Medium priority, might need high.
				true,											// circular						Circular, this means it loops back to start once done.
																// 								 This means there is no delay between sections and
																// 								 it enables continious transfers to take place
				false,											// interrupt_on_error			No way to handle error, so don't.
				true,											// interrupt_on_complete		Interrupt on complete.
				true,											// interrupt_on_half_complete	Interrupt on half-complete.
				true,											// default_enabled				Enable immediately.
				true,											// memory_increment				Increment memory.
				false											// peripheral_increment			Do not increment peripheral
			};
			return dma;
		}
		/**
		 * @brief Initiates a transfer operation using DMA
		 * 
		 * @param data The data to transmit.
		 * @return Containers::DataView<uint8_t> 
		 */
		static Containers::DataView<uint8_t> TriggerTransmit( Containers::DataView<uint8_t> data ) noexcept
		{
			if ( TransmitView.Size() or data.Empty() )
			{
				return data;
			}
			else
			{
				auto [tx, remaining] = data.Split( Kernal::BufferSize );
				/**
				 * @brief Copy the data into the output buffer
				 */
				memcpy( &TransmitBuffer[0], &tx[0], tx.Size() );
				TransmitView = { TransmitBuffer.data(), tx.Size() };
				/**
				 * @brief The following functino sets up the DMA with the tx data
				 */
				TXDMAProp::SetupMemory( tx.Data(), tx.Size() );
				/**
				 * @brief The result is the untransmitted, remaining data.
				 */
				return remaining;
			}
		}
		//
		//
		static void ContinueTransmit(DMAInterruptFlag) noexcept
		{
			TXDMAProp::Disable();
			TransmitView = {};
		}
		//
		// This function should only be called from 1 thread, it is not re-entrent
		//	A race condition exists for the enabling of the DMA module
		static auto const & SetupDMATransmit() noexcept
		{
			static DMAModule dma
			{
				TXDMAProp{},
				[](DMAInterruptFlag f){ ContinueTransmit(f); },
				TDR().Addr,									// Write to the [T]ransmit [D]ata [R]egister.
				TransmitBuffer.data(),						// Pointer to the data to be read from.
				TransmitBuffer.size(),						// Number of items to be read from.
				DMAGeneral::Direction::ReadFromMemory,		// Read from memory into peripheral.
				DMAGeneral::Priority::Medium,				// Medium priority, might need high.
				false,										// Not circular, needs to terminate after transaction ( or be reset ).
				false,										// No way to handle error, so don't.
				true,										// Interrupt on complete.
				false,										// Don't interrupt on half-complete.
				false,										// Don't enable immediately.
				true,										// Increment memory.
				false										// Don't increment peripheral ( when is that useful ).
			};
			return dma;
		}
		static void DisableUSART() 	noexcept { CR1().UE() = false; 	}
		static void EnableUSART() 	noexcept
		{
			CR1().UE() = true;
			//
			// Wait for peripheral to enable
			while ( !ISR().TC() );
		}
		//
		//
	public:
		/**
		 * Initialisation occurs based on the statically determined kernal parameters.
		 * 	the advantage of this is that it is garenteed to not incur a cost of stack pushing and poping, 
		 * 	it it also organises the interfaces in a way that is useful for code organisation.
		 */
		template <typename C>
		USARTDMAModule( C && recieve_callback ) noexcept:
			RXPin				{ IO::Mode::Alternate, USARTGeneral::USARTAltFunctionRX_v<typename RXPin::AltFunctions, RXChannel> },
			TXPin				{ IO::Mode::Alternate, USARTGeneral::USARTAltFunctionTX_v<typename TXPin::AltFunctions, TXChannel> },
			m_RecieveCallback	{ std::forward<C>( recieve_callback ) }
		{
			DisableUSART();
			//
			// Configure the protocol
			CR2().SetStopbits	( Stopbits	 );
			CR1().SetWordLength	( Databits	 );
			CR1().SetParity		( Parity	 );
			//
			//
			CR1().TE().Set		( true );
			CR1().RE().Set		( true );
			CR3().CTSE().Set	( false );
			CR3().RTSE().Set	( false );
			//
			//
			BRR().SetBaudrate	( Baudrate );
			//
			// Enable the DMA in the USART peripheral
			CR3().DMAR().Set	( true );
			CR3().DMAT().Set	( true );
			//
			// These are always enabled
			CR1().OVER8().Set	( true );
			CR3().ONEBIT().Set	( true );
			CR3().OVRDIS().Set	( true );
			//
			// Enable the peripherial
			EnableUSART();
			//
			// Only enable DMA if it is requested by the kernal
			//	This must be done after UE is enabled.
			SetupDMAReceive();
			SetupDMATransmit();
			//
			// Enable the required interrupts
			// 		IDLEIE 	- Used to detect an idle recieve line
			// 		TCIE 	- Used to determine whether a transmission is complete
			// 		RXNEIE 	- This is handled by idle line and DMA
			CR1().IDLEIE().	Set	( true );
			CR1().TCIE().	Set	( false );
			CR1().RXNEIE().	Set	( false );
			//
			ICR().Clear();
		}
		~USARTDMAModule() = default;
		/**
		 * This asynchroniously sends data using the DMA over the USART port.
		 */
		static Containers::DataView<uint8_t> Transmit( Containers::DataView<uint8_t> data ) noexcept
		{
			return TriggerTransmit( data );
		}
		/**
		 * Only two interrupts are needed here, the others are
		 * handled by the DMA module.
		 * 
		 * Recieve complete (or atleast a handler should be called)
		 * Idle indicates that the DMA transaction can be cancelled as the line is idle
		 */
		static void Interrupt() noexcept
		{
			if ( ISR().IDLE() )
			{
				ICR().IDLECF() = true;
				/**
				 * Calculate the recieved block, it doesn't matter if this is behind the actual data
				 *  as long as its not ahead no corruption occurs.
				 */
				ReceiveManager( RXDMAProp::Remaining(), ReceiveBuffer, &RecieveBlock );
			}
		}
	};
}