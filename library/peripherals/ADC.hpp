#pragma once
#include "IndexOf.hpp"
#include "RegistersADC.hpp"
#include "Meta.hpp"
#include "Pin.hpp"
#include "Power.hpp"
#include "Interrupt.hpp"
#include "DMA.hpp"
#include "Constants.hpp"
#include "AlternateFunction.hpp"
#include <cstdint>

namespace Peripherals
{
	/**
	 * @brief A power kernal manages the activation and deactivation of the ADC.
	 * 
	 * @tparam Pins The pins to setup the ADC with.
	 */
	template< typename ... Pins >
	struct ADCPowerKernal
	{
		/**
		 * @brief Configures all the pins as analog
		 */
		struct ADCPins : Pins...
		{
			ADCPins() noexcept : Pins{ IO::Mode::Analog } ... {}
		};
		/**
		 * @brief The type that manages initialisation of the ADC pins.
		 */
		static inline ADCPins m_Pins{};
		/**
		 * @brief Enables the ADC clock and blocks until its ready.
		 */
		static void EnableClock() noexcept
		{
			RCC->APB2ENR |= RCC_APB2ENR_ADC1EN;
			RCC->CR2 |= RCC_CR2_HSI14ON;
			while ((RCC->CR2 & RCC_CR2_HSI14RDY) == 0);
		}
		/**
		 * @brief Disables the ADC clock.
		 */
		static void DisableClock() noexcept
		{
		    RCC->APB2ENR &= ~RCC_APB2ENR_ADC1EN;
		}
		/**
		 * @brief Gets the ADC ready for use.
		 * 
		 * @li Enables the clock.
		 * @li Setups up the ADC for DMA use by default.
		 * @li Calibrates the ADC.
		 * @li Enables the ADC module.
		 * @li Selects the ADC modules.
		 * 
		 * This also initialises the ADC for DMA use.
		 */
		static void Construct() noexcept
		{
			using namespace ADCGeneral;

			// Enable Peripheral clock
			EnableClock();

			// 
			ADCGeneral::CFGR1{}.Resolution		( Resolution::Bits12 	 );
			ADCGeneral::CFGR1{}.Alignment		( Alignment::LSB 		 );
			ADCGeneral::CFGR1{}.ScanDirection	( ADCGeneral::Direction::Upward );
			ADCGeneral::CFGR1{}.DMAConfiguration( true, DMAMode::OneShot );
			ADCGeneral::SMPR{}.SetSamplingTime(ADCClockSampling::CLK_239_5);
			ADCGeneral::CFGR2{}.SetClockMode(ADCGeneral::ADCClockMode::PCLK_4);

			// Auto-Calibration
			Calibrate();

			// Enable Module
			EnableSequence();

			//
			CHSELR{}.Select( General::Type_t<typename Pins::AltFunctions>::ADCChannel... );
		}
		/**
		 * @brief This disables the clock and the module.
		 * 
		 * @li Disables the module.
		 * @li Disable the module clock.
		 */
		static void Destruct() noexcept
		{
			using namespace ADCGeneral;
			DisableSequence();
			DisableClock();
		}
	};
	/**
	 * @brief Manages the ADC peripheral.
	 * 
	 * This by default uses the DMA module to reduce the overhead induced by 
	 * sampling multiple ADC channels in sequence.
	 * 
	 * @tparam Pins The pins to use with the ADC.
	 */
	template< typename ... Pins >
	class ADCModule
	{
	public:
		static constexpr auto count = sizeof...(Pins);
		static constexpr auto contains_temperature = General::IsOneOf( System::TemperatureADCChannel, General::Type_t< typename Pins::AltFunctions >::ADCChannel ... );
		using array_t = std::uint16_t[count];
		//
		static constexpr auto 	Bits = 12u;
		static constexpr auto 	Mask = General::MaskRange(0u, Bits);
		static constexpr float 	Scale = System::AnalogReference / (float)Mask;
	private:
		using pins_t 		= General::TypeList<Pins...>;
		using kernal_t		= ADCPowerKernal< Pins ... >;
		using power_t		= General::ModulePower<kernal_t>;
		//
	protected:
		power_t m_PSR{};
		static inline volatile array_t g_Buffer{};
	public:
		/**
		 * @brief Construct a new ADCModule object
		 * 
		 * @tparam Callback The callback type.
		 * @param callback The callback to run when a sample sequence is complete.
		 */
		template <typename Callback>
		ADCModule( Callback && callback ) noexcept
		{
			static DMAModule adc_dma
			{
				DMAProperties<1, 1>{},
				[c{std::forward<Callback>(callback)}](DMAInterruptFlag)
				{
					c();
				},
				&ADC1->DR,
				std::data( g_Buffer ),
				std::size( g_Buffer ),
				DMAGeneral::Direction::ReadFromPeripheral,
				DMAGeneral::Priority::Low,
				true
			};
			
			if constexpr ( contains_temperature )
			{
				ADCGeneral::CCR{}.TSEN() = true;
			}
		}
		/**
		 * @brief Triggers an ADC conversion and doesn't wait for completion.
		 */
		static void Trigger() noexcept
		{
			ADCGeneral::CR{}.ADSTART() = true;
		}
		/**
		 * @brief Checks whether a the ADC is busy.
		 * 
		 * @return true The adc is sampling or off.
		 * @return false The adc is idle.
		 */
		static bool Busy() noexcept
		{
			return ADCGeneral::CR{}.ADSTART();
		}
		/**
		 * @brief Triggers an ADC conversion and blocks until it completes.
		 */
		static void TriggerBlock() noexcept
		{
			Trigger();
			while( Busy() );
		}
		/**
		 * @brief Cast a sample array to a structure type of the same size.
		 * 
		 * @tparam Pin The pin to get the coressponding ADC sample.
		 * @return uint16_t The converted value for the given ADC pin.
		 */
		template<typename Pin>
		uint16_t Get() const noexcept
		{
			constexpr size_t index = General::IndexOf_v<Pin, pins_t>;
			static_assert(index < sizeof...(Pins), "The requested pin sample doesn't exist (wrong pin).");
            LOG_POINT(index);
			return g_Buffer[index];
		}
	};
}