#pragma once
/**
 * @file AlternateFunction.hpp
 * @author David Ledger
 * @brief This file sets up the avaliable functionality of each pin on a microcontroller.
 * 
 * It is important users do not attempt to generate these by hand. This is done by code generators.
 * 
 * @date 2019-11-29
 * 
 * @copyright Copyright (c) 2019
 */
#include "PinCommon.hpp"
#ifdef CEC
#undef CEC
#endif

namespace IO
{
	template <unsigned channel>
	struct ADCPin
	{
		struct AltFunctions
		{
			static constexpr unsigned ADCChannel = channel;
		};
	};


#ifdef STM32F070x6
	template <>
	struct AlternateFunction<Port::A, 0>
	{
		enum Type
		{
			Default = 0,
			USART2_CTS = 1,
			USART4_TX = 4,
		} m_Value;
		
		static constexpr unsigned ADCChannel = 0u;
	};

	template <>
	struct AlternateFunction<Port::A, 1>
	{
		enum Type
		{
			Default = 0,
			EVENTOUT = 0,
			USART2_RTS = 1,
			USART4_RX = 4,
			TIM15_CH1N = 5,
		} m_Value;
		
		static constexpr unsigned ADCChannel = 1u;
	};

	template <>
	struct AlternateFunction<Port::A, 2>
	{
		enum Type
		{
			Default = 0,
			TIM15_CH1 = 0,
			USART2_TX = 1,
		} m_Value;
		
		static constexpr unsigned ADCChannel = 2u;
	};

	template <>
	struct AlternateFunction<Port::A, 3>
	{
		enum Type
		{
			Default = 0,
			TIM15_CH2 = 0,
			USART2_RX = 1,
		} m_Value;
		
		static constexpr unsigned ADCChannel = 3u;
	};

	template <>
	struct AlternateFunction<Port::A, 4>
	{
		enum Type
		{
			Default = 0,
			SPI1_NSS = 0,
			USART2_CK = 1,
			USB_NOE = 2,
			TIM14_CH1 = 4,
		} m_Value;
		
		static constexpr unsigned ADCChannel = 4u;
	};

	template <>
	struct AlternateFunction<Port::A, 5>
	{
		enum Type
		{
			Default = 0,
			SPI1_SCK = 0,
		} m_Value;
		
		static constexpr unsigned ADCChannel = 5u;
	};

	template <>
	struct AlternateFunction<Port::A, 6>
	{
		enum Type
		{
			Default = 0,
			SPI1_MISO = 0,
			TIM3_CH1 = 1,
			TIM1_BKIN = 2,
			USART3_CTS = 4,
			TIM16_CH1 = 5,
			EVENTOUT = 6,
		} m_Value;
		
		static constexpr unsigned ADCChannel = 6u;
	};

	template <>
	struct AlternateFunction<Port::A, 7>
	{
		enum Type
		{
			Default = 0,
			SPI1_MOSI = 0,
			TIM3_CH2 = 1,
			TIM1_CH1N = 2,
			TIM14_CH1 = 4,
			TIM17_CH1 = 5,
			EVENTOUT = 6,
		} m_Value;
		
		static constexpr unsigned ADCChannel = 7u;
	};

	template <>
	struct AlternateFunction<Port::A, 8>
	{
		enum Type
		{
			Default = 0,
			MCO = 0,
			USART1_CK = 1,
			TIM1_CH1 = 2,
			EVENTOUT = 3,
		} m_Value;
		
	};

	template <>
	struct AlternateFunction<Port::A, 9>
	{
		enum Type
		{
			Default = 0,
			TIM15_BKIN = 0,
			USART1_TX = 1,
			TIM1_CH2 = 2,
			I2C1_SCL = 4,
		} m_Value;
		
	};

	template <>
	struct AlternateFunction<Port::A, 10>
	{
		enum Type
		{
			Default = 0,
			TIM17_BKIN = 0,
			USART1_RX = 1,
			TIM1_CH3 = 2,
			I2C1_SDA = 4,
		} m_Value;
		
	};

	template <>
	struct AlternateFunction<Port::A, 11>
	{
		enum Type
		{
			Default = 0,
			EVENTOUT = 0,
			USART1_CTS = 1,
			TIM1_CH4 = 2,
		} m_Value;
	};

	template <>
	struct AlternateFunction<Port::A, 12>
	{
		enum Type
		{
			Default = 0,
			EVENTOUT = 0,
			USART1_RTS = 1,
			TIM1_ETR = 2,
		} m_Value;
		
	};

	template <>
	struct AlternateFunction<Port::A, 13>
	{
		enum Type
		{
			Default = 0,
			SWDIO = 0,
			IR_OUT = 1,
			USB_NOE = 2,
		} m_Value;
		
	};

	template <>
	struct AlternateFunction<Port::A, 14>
	{
		enum Type
		{
			Default = 0,
			SWCLK = 0,
			USART2_TX = 1,
		} m_Value;
		
	};

	template <>
	struct AlternateFunction<Port::A, 15>
	{
		enum Type
		{
			Default = 0,
			SPI1_NSS = 0,
			USART2_RX = 1,
			EVENTOUT = 3,
			USART4_RTS = 4,
		} m_Value;
		
	};

	template <>
	struct AlternateFunction<Port::F, 0>
	{
		enum Type
		{
			Default = 0,
			I2C1_SDA = 1,
		} m_Value;
		
	};

	template <>
	struct AlternateFunction<Port::F, 1>
	{
		enum Type
		{
			Default = 0,
			I2C1_SCL = 1,
		} m_Value;
		
	};
#endif

#ifdef STM32F072
	template <>
	struct AlternateFunction<Port::A, 0>
	{
		enum Type
		{
			Default = 0,
			USART2_CTS = 1,
			TIM2_CH1_ETR = 2,
			TSC_G1_IO1 = 3,
			USART4_TX = 4,
			COMP1_OUT = 7,
		} m_Value;
		static constexpr unsigned ADCChannel = 0u;
	};

	template <>
	struct AlternateFunction<Port::A, 1>
	{
		enum Type
		{
			Default = 0,
			EVENTOUT = 0,
			USART2_RTS = 1,
			TIM2_CH2 = 2,
			TSC_G1_IO2 = 3,
			USART4_RX = 4,
			TIM15_CH1N = 5,
		} m_Value;
		static constexpr unsigned ADCChannel = 1u;
	};

	template <>
	struct AlternateFunction<Port::A, 2>
	{
		enum Type
		{
			Default = 0,
			TIM15_CH1 = 0,
			USART2_TX = 1,
			TIM2_CH3 = 2,
			TSC_G1_IO3 = 3,
			COMP2_OUT = 7,
		} m_Value;
		static constexpr unsigned ADCChannel = 2u;
	};

	template <>
	struct AlternateFunction<Port::A, 3>
	{
		enum Type
		{
			Default = 0,
			TIM15_CH2 = 0,
			USART2_RX = 1,
			TIM2_CH4 = 2,
			TSC_G1_IO4 = 3,
		} m_Value;
		static constexpr unsigned ADCChannel = 3u;
	};

	template <>
	struct AlternateFunction<Port::A, 4>
	{
		enum Type
		{
			Default = 0,
			SPI1_NSS = 0,
			I2S1_WS = 0,
			USART2_CK = 1,
			TSC_G2_IO1 = 3,
			TIM14_CH1 = 4,
		} m_Value;
		static constexpr unsigned ADCChannel = 4u;
		static constexpr unsigned DACChannel = 1u;
	};

	template <>
	struct AlternateFunction<Port::A, 5>
	{
		enum Type
		{
			Default = 0,
			SPI1_SCK = 0,
			I2S1_CK = 0,
			CEC = 1,
			TIM2_CH1_ETR = 2,
			TSC_G2_IO2 = 3,
		} m_Value;
		static constexpr unsigned ADCChannel = 5u;
		static constexpr unsigned DACChannel = 2u;
	};

	template <>
	struct AlternateFunction<Port::A, 6>
	{
		enum Type
		{
			Default = 0,
			SPI1_MISO = 0,
			I2S1_MCK = 0,
			TIM3_CH1 = 1,
			TIM1_BKIN = 2,
			TSC_G2_IO3 = 3,
			USART3_CTS = 4,
			TIM16_CH1 = 5,
			EVENTOUT = 6,
			COMP1_OUT = 7,
		} m_Value;
		static constexpr unsigned ADCChannel = 6u;
	};

	template <>
	struct AlternateFunction<Port::A, 7>
	{
		enum Type
		{
			Default = 0,
			SPI1_MOSI = 0,
			I2S1_SD = 0,
			TIM3_CH2 = 1,
			TIM1_CH1N = 2,
			TSC_G2_IO4 = 3,
			TIM14_CH1 = 4,
			TIM17_CH1 = 5,
			EVENTOUT = 6,
			COMP2_OUT = 7,
		} m_Value;
		static constexpr unsigned ADCChannel = 7u;
	};

	template <>
	struct AlternateFunction<Port::A, 8>
	{
		enum Type
		{
			Default = 0,
			MCO = 0,
			USART1_CK = 1,
			TIM1_CH1 = 2,
			EVENTOUT = 3,
			CRS_SYNC = 4,
		} m_Value;
	};

	template <>
	struct AlternateFunction<Port::A, 9>
	{
		enum Type
		{
			Default = 0,
			TIM15_BKIN = 0,
			USART1_TX = 1,
			TIM1_CH2 = 2,
			TSC_G4_IO1 = 3,
		} m_Value;
	};

	template <>
	struct AlternateFunction<Port::A, 10>
	{
		enum Type
		{
			Default = 0,
			TIM17_BKIN = 0,
			USART1_RX = 1,
			TIM1_CH3 = 2,
			TSC_G4_IO2 = 3,
		} m_Value;
	};

	template <>
	struct AlternateFunction<Port::A, 11>
	{
		enum Type
		{
			Default = 0,
			EVENTOUT = 0,
			USART1_CTS = 1,
			TIM1_CH4 = 2,
			TSC_G4_IO3 = 3,
			CAN_RX = 4,
			COMP1_OUT = 7,
		} m_Value;
	};

	template <>
	struct AlternateFunction<Port::A, 12>
	{
		enum Type
		{
			Default = 0,
			EVENTOUT = 0,
			USART1_RTS = 1,
			TIM1_ETR = 2,
			TSC_G4_IO4 = 3,
			CAN_TX = 4,
			COMP2_OUT = 7,
		} m_Value;
	};

	template <>
	struct AlternateFunction<Port::A, 13>
	{
		enum Type
		{
			Default = 0,
			SWDIO = 0,
			IR_OUT = 1,
			USB_NOE = 2,
		} m_Value;
	};

	template <>
	struct AlternateFunction<Port::A, 14>
	{
		enum Type
		{
			Default = 0,
			SWCLK = 0,
			USART2_TX = 1,
		} m_Value;
	};

	template <>
	struct AlternateFunction<Port::A, 15>
	{
		enum Type
		{
			Default = 0,
			SPI1_NSS = 0,
			I2S1_WS = 0,
			USART2_RX = 1,
			TIM2_CH1_ETR = 2,
			EVENTOUT = 3,
			USART4_RTS = 4,
		} m_Value;
	};

	//PORT B SPECIALISATIONS
	template <>
	struct AlternateFunction<Port::B, 0>
	{
		enum Type
		{
			Default = 0,
			EVENTOUT = 0,
			TIM3_CH3 = 1,
			TIM1_CH2N = 2,
			TSC_G3_IO2 = 3,
			USART3_CK = 4,
		} m_Value;
		static constexpr unsigned ADCChannel = 8u;
	};

	template <>
	struct AlternateFunction<Port::B, 1>
	{
		enum Type
		{
			Default = 0,
			TIM14_CH1 = 0,
			TIM3_CH4 = 1,
			TIM1_CH3N = 2,
			TSC_G3_IO3 = 3,
			USART3_RTS = 4,
		} m_Value;
		static constexpr unsigned ADCChannel = 9u;
	};

	template <>
	struct AlternateFunction<Port::B, 2>
	{
		enum Type
		{
			Default = 0,
			TSC_G3_IO4 = 3,
		} m_Value;
	};

	template <>
	struct AlternateFunction<Port::B, 3>
	{
		enum Type
		{
			Default = 0,
			SPI1_SCK = 0,
			I2S1_CK = 0,
			EVENTOUT = 1,
			TIM2_CH2 = 2,
			TSC_G5_IO1 = 3,
		} m_Value;
	};

	template <>
	struct AlternateFunction<Port::B, 4>
	{
		enum Type
		{
			Default = 0,
			SPI1_MISO = 0,
			I2S1_MCK = 0,
			TIM3_CH1 = 1,
			EVENTOUT = 2,
			TSC_G5_IO2 = 3,
			TIM17_BKIN = 5,
		} m_Value;
	};

	template <>
	struct AlternateFunction<Port::B, 5>
	{
		enum Type
		{
			Default = 0,
			SPI1_MOSI = 0,
			I2S1_SD = 0,
			TIM3_CH2 = 1,
			TIM16_BKIN = 2,
			I2C1_SMBA = 3,
		} m_Value;
	};

	template <>
	struct AlternateFunction<Port::B, 6>
	{
		enum Type
		{
			Default = 0,
			USART1_TX = 0,
			I2C1_SCL = 1,
			TIM16_CH1N = 2,
			TSC_G5_IO3 = 3,
		} m_Value;
	};

	template <>
	struct AlternateFunction<Port::B, 7>
	{
		enum Type
		{
			Default = 0,
			USART1_RX = 0,
			I2C1_SDA = 1,
			TIM17_CH1N = 2,
			TSC_G5_IO4 = 3,
			USART4_CTS = 4,
		} m_Value;
	};

	template <>
	struct AlternateFunction<Port::B, 8>
	{
		enum Type
		{
			Default = 0,
			CEC = 0,
			I2C1_SCL = 1,
			TIM16_CH1 = 2,
			TSC_SYNC = 3,
			CAN_RX = 4,
		} m_Value;
	};

	template <>
	struct AlternateFunction<Port::B, 9>
	{
		enum Type
		{
			Default = 0,
			IR_OUT = 0,
			I2C1_SDA = 1,
			TIM17_CH1 = 2,
			EVENTOUT = 3,
			CAN_TX = 4,
			SPI2_NSS = 5,
			I2S2_WS = 5,
		} m_Value;
	};

	template <>
	struct AlternateFunction<Port::B, 10>
	{
		enum Type
		{
			Default = 0,
			CEC = 0,
			I2C2_SCL = 1,
			TIM2_CH3 = 2,
			TSC_SYNC = 3,
			USART3_TX = 4,
			SPI2_SCK = 5,
			I2S2_CK = 5,
		} m_Value;
	};

	template <>
	struct AlternateFunction<Port::B, 11>
	{
		enum Type
		{
			Default = 0,
			EVENTOUT = 0,
			I2C2_SDA = 1,
			TIM2_CH4 = 2,
			TSC_G6_IO1 = 3,
			USART3_RX = 4,
		} m_Value;
	};

	template <>
	struct AlternateFunction<Port::B, 12>
	{
		enum Type
		{
			Default = 0,
			SPI2_NSS = 0,
			I2S2_WS = 0,
			EVENTOUT = 1,
			TIM1_BKIN = 2,
			TSC_G6_IO2 = 3,
			USART3_CK = 4,
			TIM15_BKIN = 5,
		} m_Value;
	};

	template <>
	struct AlternateFunction<Port::B, 13>
	{
		enum Type
		{
			Default = 0,
			SPI2_SCK = 0,
			I2S2_CK = 0,
			TIM1_CH1N = 2,
			TSC_G6_IO3 = 3,
			USART3_CTS = 4,
			I2C2_SCL = 5,
		} m_Value;
	};

	template <>
	struct AlternateFunction<Port::B, 14>
	{
		enum Type
		{
			Default = 0,
			SPI2_MISO = 0,
			I2S2_MCK = 0,
			TIM15_CH1 = 1,
			TIM1_CH2N = 2,
			TSC_G6_IO4 = 3,
			USART3_RTS = 4,
			I2C2_SDA = 5,
		} m_Value;
	};

	template <>
	struct AlternateFunction<Port::B, 15>
	{
		enum Type
		{
			Default = 0,
			SPI2_MOSI = 0,
			I2S2_SD = 0,
			TIM15_CH2 = 1,
			TIM1_CH3N = 2,
			TIM15_CH1N = 3,
		} m_Value;
	};

	//PORT C Specialisatoins
	template <>
	struct AlternateFunction<Port::C, 0>
	{
		enum Type
		{
			Default = 0,
			EVENTOUT = 0,
		} m_Value;
	};

	template <>
	struct AlternateFunction<Port::C, 1>
	{
		enum Type
		{
			Default = 0,
			EVENTOUT = 0,
		} m_Value;
	};

	template <>
	struct AlternateFunction<Port::C, 2>
	{
		enum Type
		{
			Default = 0,
			EVENTOUT = 0,
			SPI2_MISO = 1,
			I2S2_MCK = 1,
		} m_Value;
	};

	template <>
	struct AlternateFunction<Port::C, 3>
	{
		enum Type
		{
			Default = 0,
			EVENTOUT = 0,
			SPI2_MOSI = 1,
			I2S2_SD = 1,
		} m_Value;
	};

	template <>
	struct AlternateFunction<Port::C, 4>
	{
		enum Type
		{
			Default = 0,
			EVENTOUT = 0,
			USART3_TX = 1,
		} m_Value;
		static constexpr unsigned ADCChannel = 14u;
	};

	template <>
	struct AlternateFunction<Port::C, 5>
	{
		enum Type
		{
			Default = 0,
			TSC_G3_IO1 = 0,
			USART3_RX = 1,
		} m_Value;
		static constexpr unsigned ADCChannel = 15u;
	};

	template <>
	struct AlternateFunction<Port::C, 6>
	{
		enum Type
		{
			Default = 0,
			TIM3_CH1 = 0,
		} m_Value;
	};

	template <>
	struct AlternateFunction<Port::C, 7>
	{
		enum Type
		{
			Default = 0,
			TIM3_CH2 = 0,
		} m_Value;
	};

	template <>
	struct AlternateFunction<Port::C, 8>
	{
		enum Type
		{
			Default = 0,
			TIM3_CH3 = 0,
		} m_Value;
	};

	template <>
	struct AlternateFunction<Port::C, 9>
	{
		enum Type
		{
			Default = 0,
			TIM3_CH4 = 0,
		} m_Value;
	};

	template <>
	struct AlternateFunction<Port::C, 10>
	{
		enum Type
		{
			Default = 0,
			USART4_TX = 0,
			USART3_TX = 1,
		} m_Value;
	};

	template <>
	struct AlternateFunction<Port::C, 11>
	{
		enum Type
		{
			Default = 0,
			USART4_RX = 0,
			USART3_RX = 1,
		} m_Value;
	};

	template <>
	struct AlternateFunction<Port::C, 12>
	{
		enum Type
		{
			Default = 0,
			USART4_CK = 0,
			USART3_CK = 1,
		} m_Value;
	};
	template <>
	struct AlternateFunction<Port::C, 13>
	{
		enum Type
		{
			Default = 0,
		} m_Value;
	};

	template <>
	struct AlternateFunction<Port::C, 14>
	{
		enum Type
		{
			Default = 0,
		} m_Value;
	};

	template <>
	struct AlternateFunction<Port::C, 15>
	{
		enum Type
		{
			Default = 0,
		} m_Value;
	};

	//Port D Specialisations
	template <>
	struct AlternateFunction<Port::D, 0>
	{
		enum Type
		{
			Default = 0,
			CAN_RX = 0,
			SPI2_NSS = 1,
			I2S2_WS = 1,
		} m_Value;
	};

	template <>
	struct AlternateFunction<Port::D, 1>
	{
		enum Type
		{
			Default = 0,
			CAN_TX = 0,
			SPI2_SCK = 1,
			I2S2_CK = 1,
		} m_Value;
	};

	template <>
	struct AlternateFunction<Port::D, 2>
	{
		enum Type
		{
			Default = 0,
			TIM3_ETR = 0,
			USART3_RTS = 1,
		} m_Value;
	};

	template <>
	struct AlternateFunction<Port::D, 3>
	{
		enum Type
		{
			Default = 0,
			USART2_CTS = 0,
			SPI2_MISO = 1,
			I2S2_MCK = 1,
		} m_Value;
	};

	template <>
	struct AlternateFunction<Port::D, 4>
	{
		enum Type
		{
			Default = 0,
			USART2_RTS = 0,
			SPI2_MOSI = 1,
			I2S2_SD = 1,
		} m_Value;
	};

	template <>
	struct AlternateFunction<Port::D, 5>
	{
		enum Type
		{
			Default = 0,
			USART2_TX = 0,
		} m_Value;
	};

	template <>
	struct AlternateFunction<Port::D, 6>
	{
		enum Type
		{
			Default = 0,
			USART2_RX = 0,
		} m_Value;
	};

	template <>
	struct AlternateFunction<Port::D, 7>
	{
		enum Type
		{
			Default = 0,
			USART2_CK = 0,
		} m_Value;
	};

	template <>
	struct AlternateFunction<Port::D, 8>
	{
		enum Type
		{
			Default = 0,
			USART3_TX = 0,
		} m_Value;
	};

	template <>
	struct AlternateFunction<Port::D, 9>
	{
		enum Type
		{
			Default = 0,
			USART3_RX = 0,
		} m_Value;
	};

	template <>
	struct AlternateFunction<Port::D, 10>
	{
		enum Type
		{
			Default = 0,
			USART3_CK = 0,
		} m_Value;
	};

	template <>
	struct AlternateFunction<Port::D, 11>
	{
		enum Type
		{
			Default = 0,
			USART3_CTS = 0,
		} m_Value;
	};

	template <>
	struct AlternateFunction<Port::D, 12>
	{
		enum Type
		{
			Default = 0,
			USART3_RTS = 0,
			TSC_G8_IO1 = 1,
		} m_Value;
	};

	template <>
	struct AlternateFunction<Port::D, 13>
	{
		enum Type
		{
			Default = 0,
			TSC_G8_IO2 = 1,
		} m_Value;
	};

	template <>
	struct AlternateFunction<Port::D, 14>
	{
		enum Type
		{
			Default = 0,
			TSC_G8_IO3 = 1,
		} m_Value;
	};

	template <>
	struct AlternateFunction<Port::D, 15>
	{
		enum Type
		{
			Default = 0,
			CRS_SYNC = 0,
			TSC_G8_IO4 = 1,
		} m_Value;
	};

	//PORT E Specialisations
	template <>
	struct AlternateFunction<Port::E, 0>
	{
		enum Type
		{
			Default = 0,
			TIM16_CH1 = 0,
			EVENTOUT = 1,
		} m_Value;
	};

	template <>
	struct AlternateFunction<Port::E, 1>
	{
		enum Type
		{
			Default = 0,
			TIM17_CH1 = 0,
			EVENTOUT = 1,
		} m_Value;
	};

	template <>
	struct AlternateFunction<Port::E, 2>
	{
		enum Type
		{
			Default = 0,
			TIM3_ETR = 0,
			TSC_G7_IO1 = 1,
		} m_Value;
	};

	template <>
	struct AlternateFunction<Port::E, 3>
	{
		enum Type
		{
			Default = 0,
			TIM3_CH1 = 0,
			TSC_G7_IO2 = 1,
		} m_Value;
	};

	template <>
	struct AlternateFunction<Port::E, 4>
	{
		enum Type
		{
			Default = 0,
			TIM3_CH2 = 0,
			TSC_G7_IO3 = 1,
		} m_Value;
	};

	template <>
	struct AlternateFunction<Port::E, 5>
	{
		enum Type
		{
			Default = 0,
			TIM3_CH3 = 0,
			TSC_G7_IO4 = 1,
		} m_Value;
	};

	template <>
	struct AlternateFunction<Port::E, 6>
	{
		enum Type
		{
			Default = 0,
			TIM3_CH4 = 0,
		} m_Value;
	};

	template <>
	struct AlternateFunction<Port::E, 7>
	{
		enum Type
		{
			Default = 0,
			TIM1_ETR = 0,
		} m_Value;
	};

	template <>
	struct AlternateFunction<Port::E, 8>
	{
		enum Type
		{
			Default = 0,
			TIM1_CH1N = 0,
		} m_Value;
	};

	template <>
	struct AlternateFunction<Port::E, 9>
	{
		enum Type
		{
			Default = 0,
			TIM1_CH1 = 0,
		} m_Value;
	};

	template <>
	struct AlternateFunction<Port::E, 10>
	{
		enum Type
		{
			Default = 0,
			TIM1_CH2N = 0,
		} m_Value;
	};

	template <>
	struct AlternateFunction<Port::E, 11>
	{
		enum Type
		{
			Default = 0,
			TIM1_CH2 = 0,
		} m_Value;
	};

	template <>
	struct AlternateFunction<Port::E, 12>
	{
		enum Type
		{
			Default = 0,
			TIM1_CH3N = 0,
			SPI1_NSS = 1,
			I2S1_WS = 1,
		} m_Value;
	};

	template <>
	struct AlternateFunction<Port::E, 13>
	{
		enum Type
		{
			Default = 0,
			TIM1_CH3 = 0,
			SPI1_SCK = 1,
			I2S1_CK = 1,
		} m_Value;
	};

	template <>
	struct AlternateFunction<Port::E, 14>
	{
		enum Type
		{
			Default = 0,
			TIM1_CH4 = 0,
			SPI1_MISO = 1,
			I2S1_MCK = 1,
		} m_Value;
	};

	template <>
	struct AlternateFunction<Port::E, 15>
	{
		enum Type
		{
			Default = 0,
			TIM1_BKIN = 0,
			SPI1_MOSI = 1,
			I2S1_SD = 1,
		} m_Value;
	};

	//Port F Specialisatoins
	template <>
	struct AlternateFunction<Port::F, 0>
	{
		enum Type
		{
			Default = 0,
			CRS_SYNC = 0,
		} m_Value;
	};

	template <>
	struct AlternateFunction<Port::F, 1>
	{
		enum Type
		{
			Default = 0,
		} m_Value;
	};

	template <>
	struct AlternateFunction<Port::F, 2>
	{
		enum Type
		{
			Default = 0,
			EVENTOUT = 0,
		} m_Value;
	};

	template <>
	struct AlternateFunction<Port::F, 3>
	{
		enum Type
		{
			Default = 0,
			EVENTOUT = 0,
		} m_Value;
	};

	template <>
	struct AlternateFunction<Port::F, 6>
	{
		enum Type
		{
			Default = 0,
		} m_Value;
	};

	template <>
	struct AlternateFunction<Port::F, 9>
	{
		enum Type
		{
			Default = 0,
			TIM15_CH1 = 0,
		} m_Value;
	};

	template <>
	struct AlternateFunction<Port::F, 10>
	{
		enum Type
		{
			Default = 0,
			TIM15_CH2 = 0,
		} m_Value;
	};
#endif
}