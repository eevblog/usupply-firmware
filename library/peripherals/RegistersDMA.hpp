#pragma once

#include "Meta.hpp"
#include "Pin.hpp"
#include "Register.hpp"
#include <cstdint>

namespace Peripherals::DMAGeneral
{
	enum class MemorySize : std::uint32_t
	{
		Bits8 = 0b00,
		Bits16 = 0b01,
		Bits32 = 0b10,
		Reserved = 0b11
	};
	enum class Priority : std::uint32_t
	{
		Low			= 0b00,
		Medium		= 0b01,
		High		= 0b10,
		VeryHigh    = 0b11
	};
	enum class Direction : std::uint32_t
	{
		ReadFromPeripheral = 0, 
		ReadFromMemory = 1,
		MemoryToMemory = 2,
	};
	
	template <size_t Module, size_t Channel>
	constexpr DMA_Channel_TypeDef * GetDMAChannel()
	{
		static_assert(Channel <= 7, "STM32F07x only has 7 DMA channels.");
		
		switch (Channel)
		{
		case 1: return DMA1_Channel1;
		case 2: return DMA1_Channel2;
		case 3: return DMA1_Channel3;
		case 4: return DMA1_Channel4;
		case 5: return DMA1_Channel5;
		case 6: return DMA1_Channel6;
		case 7: return DMA1_Channel7;
		};
	}
	constexpr auto DMAModuleBase(size_t Module)
	{
		return DMA1_BASE;
	}
	constexpr auto DMAChannelBase(size_t Channel)
	{
		switch (Channel)
		{
		case 1: return DMA1_Channel1_BASE;
		case 2: return DMA1_Channel2_BASE;
		case 3: return DMA1_Channel3_BASE;
		case 4: return DMA1_Channel4_BASE;
		case 5: return DMA1_Channel5_BASE;
		case 6: return DMA1_Channel6_BASE;
		case 7: return DMA1_Channel7_BASE;
		};
	}
	
	constexpr DMA_TypeDef * GetDMAModule()
	{
		return DMA1;
	}
	
	//Interrupt flags
	template <size_t Module, size_t Channel, std::size_t A = DMAModuleBase(Module) + offsetof(DMA_TypeDef, ISR)>
	struct ISR : public General::u32_reg<A>
	{
		using base_t = General::u32_reg<A>;
		using base_t::base_t;
		
	private:
		auto TEIF7  (){ return base_t::template Actual<DMA_ISR_TEIF7>(); }
		auto HTIF7  (){ return base_t::template Actual<DMA_ISR_HTIF7>(); }
		auto TCIF7  (){ return base_t::template Actual<DMA_ISR_TCIF7>(); }
		auto GIF7   (){ return base_t::template Actual<DMA_ISR_GIF7 >(); }
		auto TEIF6  (){ return base_t::template Actual<DMA_ISR_TEIF6>(); }
		auto HTIF6  (){ return base_t::template Actual<DMA_ISR_HTIF6>(); }
		auto TCIF6  (){ return base_t::template Actual<DMA_ISR_TCIF6>(); }
		auto GIF6   (){ return base_t::template Actual<DMA_ISR_GIF6 >(); }
		auto TEIF5  (){ return base_t::template Actual<DMA_ISR_TEIF5>(); }
		auto HTIF5  (){ return base_t::template Actual<DMA_ISR_HTIF5>(); }
		auto TCIF5  (){ return base_t::template Actual<DMA_ISR_TCIF5>(); }
		auto GIF5   (){ return base_t::template Actual<DMA_ISR_GIF5 >(); }
		auto TEIF4  (){ return base_t::template Actual<DMA_ISR_TEIF4>(); }
		auto HTIF4  (){ return base_t::template Actual<DMA_ISR_HTIF4>(); }
		auto TCIF4  (){ return base_t::template Actual<DMA_ISR_TCIF4>(); }
		auto GIF4   (){ return base_t::template Actual<DMA_ISR_GIF4 >(); }
		auto TEIF3  (){ return base_t::template Actual<DMA_ISR_TEIF3>(); }
		auto HTIF3  (){ return base_t::template Actual<DMA_ISR_HTIF3>(); }
		auto TCIF3  (){ return base_t::template Actual<DMA_ISR_TCIF3>(); }
		auto GIF3   (){ return base_t::template Actual<DMA_ISR_GIF3 >(); }
		auto TEIF2  (){ return base_t::template Actual<DMA_ISR_TEIF2>(); }
		auto HTIF2  (){ return base_t::template Actual<DMA_ISR_HTIF2>(); }
		auto TCIF2  (){ return base_t::template Actual<DMA_ISR_TCIF2>(); }
		auto GIF2   (){ return base_t::template Actual<DMA_ISR_GIF2 >(); }
		auto TEIF1  (){ return base_t::template Actual<DMA_ISR_TEIF1>(); }
		auto HTIF1  (){ return base_t::template Actual<DMA_ISR_HTIF1>(); }
		auto TCIF1  (){ return base_t::template Actual<DMA_ISR_TCIF1>(); }
		auto GIF1   (){ return base_t::template Actual<DMA_ISR_GIF1 >(); }
	public:
		auto TEIF()
		{
			if constexpr (Channel == 1) return TEIF1();
			if constexpr (Channel == 2) return TEIF2();
			if constexpr (Channel == 3)	return TEIF3();
			if constexpr (Channel == 4) return TEIF4();
			if constexpr (Channel == 5) return TEIF5();
			if constexpr (Channel == 6) return TEIF6();
			if constexpr (Channel == 7) return TEIF7();
		}
		auto TCIF()
		{
			if constexpr (Channel == 1) return TCIF1();
			if constexpr (Channel == 2) return TCIF2();
			if constexpr (Channel == 3) return TCIF3();
			if constexpr (Channel == 4) return TCIF4();
			if constexpr (Channel == 5) return TCIF5();
			if constexpr (Channel == 6) return TCIF6();
			if constexpr (Channel == 7) return TCIF7();
		}
		auto HTIF()
		{
			if constexpr (Channel == 1) return HTIF1();
			if constexpr (Channel == 2) return HTIF2();
			if constexpr (Channel == 3) return HTIF3();
			if constexpr (Channel == 4) return HTIF4();
			if constexpr (Channel == 5) return HTIF5();
			if constexpr (Channel == 6) return HTIF6();
			if constexpr (Channel == 7) return HTIF7();
		}
		auto GIF()
		{
			if constexpr (Channel == 1) return GIF1();
			if constexpr (Channel == 2) return GIF2();
			if constexpr (Channel == 3) return GIF3();
			if constexpr (Channel == 4) return GIF4();
			if constexpr (Channel == 5) return GIF5();
			if constexpr (Channel == 6) return GIF6();
			if constexpr (Channel == 7) return GIF7();
		}
	};
	
	//Interrupt flag clear register (Set to clear)
	template <size_t Module, size_t Channel, std::size_t A = DMAModuleBase(Module) + offsetof(DMA_TypeDef, IFCR)>
	struct IFCR : General::u32_reg<A>
	{
		using base_t = General::u32_reg<A>;
		using base_t::base_t;
		
	private:
		auto CTEIF7 (){	return base_t::template Actual<DMA_IFCR_CTEIF7>();}
		auto CHTIF7 (){	return base_t::template Actual<DMA_IFCR_CHTIF7>();}
		auto CTCIF7 (){	return base_t::template Actual<DMA_IFCR_CTCIF7>();}
		auto CGIF7  (){	return base_t::template Actual<DMA_IFCR_CGIF7>();}
		auto CTEIF6 (){	return base_t::template Actual<DMA_IFCR_CTEIF6>();}
		auto CHTIF6 (){	return base_t::template Actual<DMA_IFCR_CHTIF6>();}
		auto CTCIF6 (){	return base_t::template Actual<DMA_IFCR_CTCIF6>();}
		auto CGIF6  (){	return base_t::template Actual<DMA_IFCR_CGIF6>();}
		auto CTEIF5 (){	return base_t::template Actual<DMA_IFCR_CTEIF5>();}
		auto CHTIF5 (){	return base_t::template Actual<DMA_IFCR_CHTIF5>();}
		auto CTCIF5 (){	return base_t::template Actual<DMA_IFCR_CTCIF5>();}
		auto CGIF5  (){	return base_t::template Actual<DMA_IFCR_CGIF5>();}
		auto CTEIF4 (){	return base_t::template Actual<DMA_IFCR_CTEIF4>();}
		auto CHTIF4 (){	return base_t::template Actual<DMA_IFCR_CHTIF4>();}
		auto CTCIF4 (){	return base_t::template Actual<DMA_IFCR_CTCIF4>();}
		auto CGIF4  (){	return base_t::template Actual<DMA_IFCR_CGIF4>();}
		auto CTEIF3 (){	return base_t::template Actual<DMA_IFCR_CTEIF3>();}
		auto CHTIF3 (){	return base_t::template Actual<DMA_IFCR_CHTIF3>();}
		auto CTCIF3 (){	return base_t::template Actual<DMA_IFCR_CTCIF3>();}
		auto CGIF3  (){	return base_t::template Actual<DMA_IFCR_CGIF3>();}
		auto CTEIF2 (){	return base_t::template Actual<DMA_IFCR_CTEIF2>();}
		auto CHTIF2 (){	return base_t::template Actual<DMA_IFCR_CHTIF2>();}
		auto CTCIF2 (){	return base_t::template Actual<DMA_IFCR_CTCIF2>();}
		auto CGIF2  (){	return base_t::template Actual<DMA_IFCR_CGIF2>();}
		auto CTEIF1 (){	return base_t::template Actual<DMA_IFCR_CTEIF1>();}
		auto CHTIF1 (){	return base_t::template Actual<DMA_IFCR_CHTIF1>();}
		auto CTCIF1 (){	return base_t::template Actual<DMA_IFCR_CTCIF1>();}
		auto CGIF1  (){	return base_t::template Actual<DMA_IFCR_CGIF1>();}
	public:
		auto CTEIF()
		{
			if constexpr (Channel == 1) return CTEIF1();
			if constexpr (Channel == 2) return CTEIF2();
			if constexpr (Channel == 3)	return CTEIF3();
			if constexpr (Channel == 4) return CTEIF4();
			if constexpr (Channel == 5) return CTEIF5();
			if constexpr (Channel == 6) return CTEIF6();
			if constexpr (Channel == 7) return CTEIF7();
		}
		auto CHTIF()
		{
			if constexpr (Channel == 1) return CHTIF1();
			if constexpr (Channel == 2) return CHTIF2();
			if constexpr (Channel == 3)	return CHTIF3();
			if constexpr (Channel == 4) return CHTIF4();
			if constexpr (Channel == 5) return CHTIF5();
			if constexpr (Channel == 6) return CHTIF6();
			if constexpr (Channel == 7) return CHTIF7();
		}
		auto CTCIF()
		{
			if constexpr (Channel == 1) return CTCIF1();
			if constexpr (Channel == 2) return CTCIF2();
			if constexpr (Channel == 3)	return CTCIF3();
			if constexpr (Channel == 4) return CTCIF4();
			if constexpr (Channel == 5) return CTCIF5();
			if constexpr (Channel == 6) return CTCIF6();
			if constexpr (Channel == 7) return CTCIF7();
		}
		auto CGIF()
		{
			if constexpr (Channel == 1) return CGIF1();
			if constexpr (Channel == 2) return CGIF2();
			if constexpr (Channel == 3)	return CGIF3();
			if constexpr (Channel == 4) return CGIF4();
			if constexpr (Channel == 5) return CGIF5();
			if constexpr (Channel == 6) return CGIF6();
			if constexpr (Channel == 7) return CGIF7();
		}
		void Clear()
		{
			base_t::Set( 0xffffffff );
		}
	};

	//Configuration register
	template <size_t Module, size_t Channel, std::size_t A = DMAChannelBase(Channel) + offsetof(DMA_Channel_TypeDef, CCR)>
	struct CCR : General::u32_reg<A>
	{
		using base_t = General::u32_reg<A>;
		using base_t::base_t;
		
		auto MEM2MEM(){return base_t::template Actual<DMA_CCR_MEM2MEM>();} 
		auto PL     (){return base_t::template Actual<DMA_CCR_PL>();} 
		auto MSIZE  (){return base_t::template Actual<DMA_CCR_MSIZE>();} 
		auto PSIZE  (){return base_t::template Actual<DMA_CCR_PSIZE>();} 
		auto MINC   (){return base_t::template Actual<DMA_CCR_MINC>();} 
		auto PINC   (){return base_t::template Actual<DMA_CCR_PINC>();} 
		auto CIRC   (){return base_t::template Actual<DMA_CCR_CIRC>();} 
		auto DIR    (){return base_t::template Actual<DMA_CCR_DIR>();} 
		auto TEIE   (){return base_t::template Actual<DMA_CCR_TEIE>();} 
		auto HTIE   (){return base_t::template Actual<DMA_CCR_HTIE>();} 
		auto TCIE   (){return base_t::template Actual<DMA_CCR_TCIE>();} 
		auto EN     (){return base_t::template Actual<DMA_CCR_EN>();} 
	};	
	
	//Channel number of data register (Count)
	template <size_t Module, size_t Channel, std::size_t A = DMAChannelBase(Channel) + offsetof(DMA_Channel_TypeDef, CNDTR)>
	struct CNDTR : General::u32_reg<A>
	{
		using base_t = General::u32_reg<A>;
		using base_t::base_t;

		static constexpr uint32_t Maximum = DMA_CNDTR_NDT + 1u;
		
		auto NDT(){ return base_t::template Actual<DMA_CNDTR_NDT>(); } 
	};
	
	//Peripheral address register
	template <size_t Module, size_t Channel, std::size_t A = DMAChannelBase(Channel) + offsetof(DMA_Channel_TypeDef, CPAR)>
	struct CPAR : General::u32_reg<A>
	{
		using base_t = General::u32_reg<A>;
		using base_t::base_t;
		
		auto PA(){ return base_t::template Actual<DMA_CPAR_PA>(); }
	};

	//Memory address register
	template <size_t Module, size_t Channel, std::size_t A = DMAChannelBase(Channel) + offsetof(DMA_Channel_TypeDef, CMAR)>
	struct CMAR : General::u32_reg<A>
	{
		using base_t = General::u32_reg<A>;
		using base_t::base_t;
	
		auto MA(){ return base_t::template Actual<DMA_CMAR_MA>(); }
	};

	template <size_t Module, size_t Channel>
	struct Registers
	{
		using ISR_t		= ISR	<Module, Channel>;
		using IFCR_t 	= IFCR	<Module, Channel>;
		using CCR_t 	= CCR	<Module, Channel>;
		using CNDTR_t 	= CNDTR	<Module, Channel>;
		using CPAR_t 	= CPAR	<Module, Channel>;
		using CMAR_t	= CMAR	<Module, Channel>;
	};
}