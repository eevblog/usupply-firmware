#pragma once

#include "RegistersRTC.hpp"
#include "Types.hpp"
#include "Time.hpp"
#include "Power.hpp"
#include "Constants.hpp"
#include "Meta.hpp"
#include "Interrupt.hpp"
#include "StaticLambdaWrapper.hpp"
#include "stm32f0xx_rcc.h"

namespace Peripherals
{
	/**
	 * @brief An RTC power kernal.
	 */
    template<RCCGeneral::RTCClockSource source>
	struct RTCPowerKernal
	{
		static constexpr auto ChannelInterrupt = (IRQn_Type)System::InterruptSource::eRTC;
		/**
		 * @brief should be run to enable the RTC module.
		 */
		static void Construct()
		{
			using namespace System::InterruptGeneral;
            using namespace RCCGeneral;
            //
			RTSR<20u>().RT() 	= true;
			IMR<20u>().IM() 	= true;
            //
			RCC_APB1PeriphClockCmd(RCC_APB1Periph_PWR, ENABLE);
			PWR->CR |= PWR_CR_DBP;
            //
            BDCR{}.EnableLSE();
            BDCR{}.EnableRTC();
            //
			NVIC_EnableIRQ(ChannelInterrupt); // Configure NVIC for RTC
			NVIC_SetPriority(ChannelInterrupt,2); // Set priority for RTC (=2, low priority)
		}
		/**
		 * @brief should be run to disable the RTC module.
		 */
		static void Destruct()
		{
			NVIC_DisableIRQ(ChannelInterrupt);
		}
	};
	/**
	 * @brief A manager class for the RTC peripheral.
	 * 
	 * @tparam C The type of the callback for the interrupt.
	 */
	template <typename C>
	class RTCModule
	{
	private:
		using interrupt_t 	= System::Interrupt<RTCModule, System::InterruptSource::eRTC>;
		using power_t 		= General::ModulePower<RTCPowerKernal<RCCGeneral::RTCClockSource::LSE>>;
		using callback_t    = General::StaticLambdaWrapper<C>;
		//
		power_t const 		m_PWR;
		interrupt_t const 	m_ISR;
		/**
		 * @brief Configures the wake up timer.
		 * 
		 * @param frequency The frequency for the wake-up timer.
		 */
		static void ConfigureWUT( std::uint32_t frequency ) noexcept
		{
			if (frequency > 0)
			{
				using namespace RTCGeneral;

				CR{}.WUTE() = false;
				while (!ISR{}.WUTWF());

				std::array<unsigned, 4u> constexpr 	divide_lookup = { 16u, 8u, 4u, 2u };
				for (unsigned index = 0; index < divide_lookup.size(); ++index)
				{
					auto const prescaler = divide_lookup[index];
					auto const result = System::RTCClock / prescaler;
                    //
					if ( result >= frequency )
					{
						//Compiler turns these into a single combination operation
						// don't move it into the branch
						auto const div = result / frequency; // Always > 0, result >= frequency
						auto const rem = result % frequency;
                        //
						if ( rem == 0 )
						{
							CR{}.SetupWakeup( true, (WakeupClockSelection)prescaler );
							WUTR{}.WUT() = div - 1u;	//Underflow save result >= frequency
							ISR{}.WUTWF() = false;
							return;
						}
					}
				}
			}
		}
		/**
		 * @brief Clears the interrupt flags for the RTC.
		 */
		static void ClearFlags() noexcept
		{
			using namespace RTCGeneral;
			using namespace System::InterruptGeneral;
            ISR{}.Set( 0 );
			PR<20u>().PIF() = true;
		}
		//
		callback_t m_Callback;
        /**
         * 
         */
        using RTCClockSource = RCCGeneral::RTCClockSource;
	public:
		/**
		 * @brief setup the real time clock.
		 * 
		 * @param frequency The frequency of the real time clock.
		 * @param callback The callback to run when interrupts occur.
		 */
		RTCModule( std::uint32_t frequency, RTCClockSource source, C && callback) noexcept :
			m_Callback{ std::move( callback ) }
		{
			using namespace RTCGeneral;
            ScopedControl ctrl;
            PRER{}.SetupPrescale( System::RTCClock ); //Configures it for 1 Hz (F / F = 1)
            TR{} = 0;
            DR{} = 0;
            CR{}.HourFormat( Notation::Hours24 );
            ConfigureWUT( frequency );
		}
		/**
		 * @brief Resets the time and date of the RTC.
		 */
		static void Reset() noexcept
		{
			RTCGeneral::ResetTimeAndDate();
			RTCGeneral::Pause();
			ClearFlags();
		}
		/**
		 * @brief Gets the time currently in the RTC.
		 */
		static auto Time() noexcept
		{
			return RTCGeneral::TR{}.ToTime();
		}
		/**
		 * @brief The interrupt handler.
		 */
		static void Interrupt() noexcept
		{
			//Normal handling
			if ( RTCGeneral::ISR{}.WUTF() )
			{
				callback_t::Run();
				ClearFlags();
			}
		}
	};

	template <typename C>
	RTCModule(unsigned, C) -> RTCModule<C>;
}