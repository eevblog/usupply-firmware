#pragma once

#include "Interrupt.hpp"
#include "Power.hpp"
#include "Meta.hpp"
#include "Config.hpp"
#include "RegistersTimer.hpp"
#include "PinDefinitions.hpp"
#include "RCC.hpp"
#include "StaticLambdaWrapper.hpp"
#include <cstdlib>
#include <cstdint>

namespace Peripherals
{
	/**
	 * @brief Whether DMA is triggered from Capture compare or update.
	 */
	enum class TimerDMAOn : bool
	{
		CaptureCompare	=	false,
		Update			=	true
	};
	/**
	 * @brief Manages the power on and off a timer peripheral
	 * 
	 * @param Channel The timer channel to manage.
	 * @param Priority THe priority of interrupts from the timer.
	 */
	template <unsigned Channel, uint16_t Priority>
	struct TimerPowerKernal : GeneralPowerKernal<TimerGeneral::GetPeripheral<Channel>()>
	{
		using base_t = GeneralPowerKernal<TimerGeneral::GetPeripheral<Channel>()>;
		static constexpr auto ChannelInterrupt = (IRQn_Type)TimerGeneral::TimerChannelInterrupt<Channel>();
		/**
		 * @brief Power on the timer peripheral.
		 * 
		 * This performs the following:
		 * @li Enable the interrupt.
		 * @li Enable the peripheral power.
		 * @li Setup the interrupt priority.
		 */
		static void Construct() noexcept
		{
			base_t::Construct();
			NVIC_EnableIRQ(ChannelInterrupt);
			NVIC_SetPriority(ChannelInterrupt,Priority);
		}
		/**
		 * @brief Powers off the timer peripheral
		 * 
		 * The performs the following:
		 * @li Disables the peripheral.
		 * @li Disables the channel interrupts.
		 */
		static void Destruct() noexcept
		{
			base_t::Destruct();
			NVIC_DisableIRQ(ChannelInterrupt);
		}
	};
	/**
	 * @brief A metaclass for use in template deduction of a timer.
	 * 
	 * @param Channel The timer channel to use.
	 * @param Priority The priority of the interrupt to call.
	 */
	template <std::uint16_t Channel, uint16_t Priority = 1>
	struct TimerConfig{};
	/**
	 * @brief A manager class for a timer.
	 * 
	 * @tparam Channel the timer channel to manage.
	 * @tparam Priority The priority of the interrupts of the timer.
	 * @tparam C The callback type for the timer.
	 */
	template <std::uint16_t Channel, uint16_t Priority, typename C>
	class BasicTimer
	{
		static_assert(TimerGeneral::IsTimerChannel<Channel>, 	"Timer channel is not a valid module."	);
		static_assert(TimerGeneral::IsBasicTimer<Channel> || TimerGeneral::IsGeneralPurposeTimer<Channel>, 		"Timer channel is not a basic or general purpose timer."	);
		
	protected:
		using kernal_t 		= TimerPowerKernal<Channel, Priority>;
		using power_t		= General::ModulePower<kernal_t>;
		using interrupt_t 	= System::Interrupt<BasicTimer, 
		Peripherals::TimerGeneral::TimerChannelInterrupt<Channel>()>;
		using callback_t 	= General::StaticLambdaWrapper<C>;
		
		using CR1   		= TimerGeneral::template CR1  <Channel>;
		using CR2   		= TimerGeneral::template CR2  <Channel>;
		using SMCR  		= TimerGeneral::template SMCR <Channel>;
		using DIER  		= TimerGeneral::template DIER <Channel>;
		using SR    		= TimerGeneral::template SR   <Channel>;
		using EGR   		= TimerGeneral::template EGR  <Channel>;
		using CCMR1 		= TimerGeneral::template CCMR1<Channel>;
		using CCER  		= TimerGeneral::template CCER <Channel>;
		using CNT   		= TimerGeneral::template CNT  <Channel>;
		using PSC   		= TimerGeneral::template PSC  <Channel>;
		using ARR   		= TimerGeneral::template ARR  <Channel>;
		using CCR1  		= TimerGeneral::template CCR1 <Channel>;
		using CCR2  		= TimerGeneral::template CCR2 <Channel>;
		using CCR3  		= TimerGeneral::template CCR3 <Channel>;
		using CCR4  		= TimerGeneral::template CCR4 <Channel>;
		using DCR   		= TimerGeneral::template DCR  <Channel>;
		using DMAR  		= TimerGeneral::template DMAR <Channel>;
		
	private:
		const power_t 		m_PSR;
		const interrupt_t	m_ISR;
		
		callback_t m_Callback;
	public:
		/**
		 * @brief Sets up a timer for a given frequency.
		 * 
		 * @param deduced_settings The value used to deduce the Channel of the timer and the priority of the interrupt.
		 * @param frequency The frequency to setup the timer.
		 * @param callback The callback to run when interrupts are triggerd.
		 * @param dma_on When to trigger DMA transactions (if they are setup at all).
		 * @param one_pulse Whether or not the DMA only runs once.
		 */
		constexpr BasicTimer
			(
				TimerConfig<Channel, Priority> deduced_settings,
				unsigned const 		frequency,
				C&& 				callback,
				TimerDMAOn const 	dma_on		= TimerDMAOn::Update,
				bool const 			one_pulse 	= false
			) noexcept : m_Callback( std::move( callback ) )
		{
			using namespace TimerGeneral;
			
			CR1().CEN() 			= false;		//Disable counter
			CR1().UDIS() 			= true;			//Disable updates
			CR1().URS()				= true;			//Only update on overflow or underflow or DMA request
			CR1().OPM() 			= one_pulse;	//One pulse mode, only runs once
			
			auto const prescales 	= CalculatePrescale( System::SystemClock_t::Timer(), frequency );
			ARR() 					= prescales.ARR;
			PSC() 					= prescales.PSC;
			CR1().ARPE() 			= true;			//Buffered auto-reload
			DIER().UIE() 			= true;
			DIER().UDE() 			= (bool)dma_on;
		}
		/**
		 * @brief Starts the timer.
		 */
		static void Start() noexcept
		{
			SR().Clear();
			CR1().UDIS()  	= false; 	//Enable update
			CR1().CEN()		= true;		//Enable counter
		}
		/**
		 * @brief Stops the timer.
		 */
		static void Stop() noexcept
		{
			CR1().CEN() = false;
			CR1().UDIS() = true;
		}
		/**
		 * @brief Resets the timer.
		 */
		static void Reset() noexcept
		{
			EGR().UG() = true;
		}
		/**
		 * @brief The interrupt handler function.
		 */
		static void Interrupt() noexcept
		{
			if (SR().UIF()) 
				callback_t::Run();
			SR().Clear();
		}
	};
	/**
	 * @brief A template deduction guide for the BasicTimer class.
	 * 
	 * @tparam C The timer channel.
	 * @tparam P The interrupt priority.
	 * @tparam F The callback function.
	 */
	template <std::uint16_t C, uint16_t P, typename F>
	BasicTimer(TimerConfig<C, P>, unsigned, F, TimerDMAOn, bool) -> BasicTimer<C, P, F>;
	/**
	 * @brief A template deduction guide for the BasicTimer class.
	 * 
	 * @tparam C The timer channel.
	 * @tparam P The interrupt priority.
	 * @tparam F The callback function.
	 */
	template <std::uint16_t C, uint16_t P, typename F>
	BasicTimer(TimerConfig<C, P>, unsigned, F) -> BasicTimer<C, P, F>;
}