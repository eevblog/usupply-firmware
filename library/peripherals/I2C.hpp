#pragma once

#include "FIFO.hpp"
#include "Power.hpp"
#include "DataView.hpp"
#include "Interrupt.hpp"
#include "I2CRegisters.hpp"
#include "I2CPowerKernal.hpp"
#include "StaticLambdaWrapper.hpp"
#include "Meta.hpp"
#include <variant>

namespace Peripherals
{
	enum class I2CRole
	{
		Master, 
		Slave
	};
	enum class I2CDirection : uint8_t
	{
		Write,
		WriteWrite,
		Read
	};
	enum class I2CEvent
	{
		ReadComplete,
		WriteComplete,
		Stop,
		Busy,
		NACK,
		Alert,
		Timeout,
		PacketErrorChecking,
		BusError,
		OverrunUnderrun,
		ArbitationLost,
		DataStarvation,
		ActionStarvation,
		CorruptAction,
		StateLost
	};
	/**
	 * @brief Represents a single stage of an i2c action.
	 */
	class I2CAction
	{
	private:
		using Read 			= Containers::DataView<uint8_t, false>;
		using Write 		= Containers::DataView<uint8_t, true>;
		using Write2Part 	= std::pair<Write, Write>;
		//
		struct 
		{
			std::uint16_t m_Address 	: 10;	//10
			std::uint16_t m_IsEnd		: 1;	//11
			std::uint16_t m_Resetable 	: 1;	//12
			std::uint16_t 				: 4;	//16
		} Fields;
		std::variant<int, Read, Write> 	m_ReadWrite = 0;
		//
	public:
		//
		I2CAction() = default;
		/**
		 * @brief Construct a new I2CAction object
		 * 
		 * @param address The address of the slave/master.
		 * @param data The data to write
		 * @param length The number of bytes to write.
		 * @param is_end Whether or not the transation is the end of a transaction.
		 */
		I2CAction(std::uint16_t address, uint8_t * data, size_t length, bool is_end) : 
			Fields
			{
				address,
				uint16_t((is_end) ? 1u : 0u),
				0
			},
			m_ReadWrite{ Read{ data, length } }
		{}
		/**
		 * @brief Construct a new I2CAction object
		 * 
		 * @param address The address of the slave/master.
		 * @param data The data to write
		 * @param length The number of bytes to write.
		 * @param is_end Whether or not the transation is the end of a transaction.
		 */
		I2CAction(std::uint16_t address, uint8_t const * data, size_t length, bool is_end) : 
			Fields
			{
				address,
				uint16_t((is_end) 	? 1u : 0u),
				0
			},
			m_ReadWrite{ Write{ data, length } }
		{}
		/**
		 * @brief Gets the address of the slave/master.
		 * 
		 * @return std::uint16_t 
		 */
		std::uint16_t Address() const noexcept
		{
			return Fields.m_Address;
		}
		/**
		 * @brief Gets the data as a read transaction.
		 * 
		 * @return Read* A pointer to the read data.
		 */
		Read * AsRead() noexcept
		{
			return std::get_if<Read>(&m_ReadWrite);
		}
		/**
		 * @brief Gets the data as writable data.
		 * 
		 * @return Write* A pointer to write data.
		 */
		Write * AsWrite() noexcept
		{
			return std::get_if<Write>(&m_ReadWrite);
		}
		/**
		 * @brief Whether or not the transaction is a read transaction.
		 * 
		 * @return true The data is a read transaction.
		 * @return false The data is a write transaction.
		 */
		bool IsRead() noexcept
		{
			return !!AsRead();
		}
		/**
		 * @brief Whether or not the transaction is a write transaction.
		 * 
		 * @return true The data is a write transaction.
		 * @return false The data is a read transaction.
		 */
		bool IsWrite() noexcept
		{
			return !!AsWrite();
		}
		/**
		 * @brief Whether or not to continue with more I2C transactions after this transaction.
		 * 
		 * @return true Should continue with more transactions.
		 * @return false The action is the last transaction.
		 */
		bool Continue() const noexcept
		{
			return !Fields.m_IsEnd;
		}
		/**
		 * @brief Whether or not this is the last I2C action.
		 * 
		 * @return true The action is the last transaction.
		 * @return false Should continue with more transactions.
		 */
		bool IsEnd() const noexcept
		{
			return !Continue();
		}
		/**
		 * @brief Whether or not the transaction is resettable.
		 * 
		 * @return true The transaction is resettable.
		 * @return false Not resettable.
		 */
		bool Resetable() const noexcept
		{
			return Fields.m_Resetable;	
		}
		/**
		 * @brief Runs a callback with an argument that is the currently active data.
		 * 
		 * @li If this is a read action, then the read data will be in the parameter of the callback.
		 * @li If this were a write action, then the write data would be in the parameters of the callback.
		 * 
		 * @tparam F The function type.
		 * @param function The callback function/
		 */
		template <typename F>
		void Visit(F && function) const noexcept
		{
			//Don't visit int...
			std::visit([f{std::forward<F>(function)}](auto && d)
			{
				using type = std::decay_t<decltype(d)>;
				// This removes the int case from compilation
				if constexpr (!std::is_same_v<type, int>)
					f( std::forward<decltype(d)>(d) );
			}, m_ReadWrite);
		}
		/**
		 * @brief Checks whether the action has anything in it.
		 * 
		 * @return true The action is empty.
		 * @return false The action is not empty.
		 */
		bool ActionEmpty() const noexcept
		{
			bool output = false;
			Visit( [&](auto const & v){ output = v.Empty(); } );
			return output;
		}
		/**
		 * @brief Returns the number of bytes written or received in this action.
		 * 
		 * @return std::size_t The number of bytes writen or received.
		 */
		std::size_t ActionSize() const noexcept
		{
			std::size_t output = 0;
			Visit( [&](auto const & v){ output = v.Size(); } );
			return output;
		}
	};
	/**
	 * @brief 
	 * 
	 * @tparam Kernal 
	 * @tparam SDAPin 
	 * @tparam SCLPin 
	 * @tparam EventCallback 
	 */
	template <typename Kernal, typename SDAPin, typename SCLPin, typename EventCallback>
	class I2CModule
	{
		static constexpr auto Channel = Kernal::Channel;
	public:
		using kernal_t			= I2CGeneral::I2CPowerKernal<Channel, SDAPin, SCLPin>;
		using power_t			= General::ModulePower<kernal_t>;
		using event_callback_t 	= General::StaticLambdaWrapper<EventCallback>;
		using action_fifo_t		= Containers::FIFO<I2CAction, Kernal::BufferSize>;
		using interrupt_t 		= System::Interrupt<I2CModule, I2CGeneral::GetInterruptSource(Channel)>;
	
		using ISR_t 		= I2CGeneral::ISR	<Channel>;
		using ICR_t 		= I2CGeneral::ICR	<Channel>;
		using RX_t 			= I2CGeneral::RXDR	<Channel>;
		using TX_t 			= I2CGeneral::TXDR	<Channel>;
		using CR1_t			= I2CGeneral::CR1	<Channel>;
		using CR2_t			= I2CGeneral::CR2	<Channel>;
	private:
		//
		// Manage the module power
		power_t const m_PSR{};
		//
		// Interrupt callbacks
		interrupt_t const 	m_ISR;
		//
		// Callback called when N bytes are recieved.
		event_callback_t m_Event;
		static action_fifo_t m_Actions;
        static inline action_fifo_t * volatile m_ActionsPtr = &m_Actions;
		//
		//
		inline static void APBDelay() noexcept
		{
			//
			// Delay by 3 analog peripheral clock cycles
			for( volatile unsigned i = 3; i > 0; i--);
		}
		static constexpr bool IsMaster() noexcept
		{
			return Kernal::Role == I2CRole::Master;
		}
		static void SetReload(I2CAction & action)
		{
			if ( auto count = action.ActionSize(); count > 0xFFu )
			{
				CR2_t{}.Protocol().NBYTES() = 0xFFu;
				CR2_t{}.Protocol().RELOAD() = true;
			}
			else
			{
				CR2_t{}.Protocol().NBYTES() = (uint8_t)count;
				CR2_t{}.Protocol().RELOAD() = action.Continue();
			}
		}
		static bool SetReload() noexcept
		{
			if ( m_Actions.Empty() ) return false;
			SetReload(m_Actions.Front());
			return true;
		}
		static bool Start() noexcept
		{
			if ( m_Actions.Empty() )
				return false;
			/**
			 */
			auto & 	action  = m_Actions.Front();
			auto 	address = action.Address();
			/**
			 * At the start address needs to be initialised
			 * 	Master write, this always uses NBYTES
			 */
			CR2_t{}.Config().ADD10() 	= ( address > 0xFFu );
			CR2_t{}.Protocol().SADD() 	= address;
			/**
			 *  Direction change can occur during a transaction as part of a 
			 *  restart condition.
			 */
			CR2_t{}.Protocol().RD_WRN() = action.IsRead(); //0 is a write
			/**
			 * 
			 */
			SetReload( action );
			/**
			 *	This could be a start or restart condition.
			 */
			CR2_t{}.Protocol().START() = true;
			/**
			 */
			return true;
		}
		void WaitReady() const noexcept
		{
			while(m_Actions.Size());
		}
	public:
		/**
		 * @brief Specific default constructor
		 * 
		 * @tparam E The callback type.
		 * @param event_callback The event callback (called on supported interrupt events).
		 */
		template <typename E>
		I2CModule( E && event_callback ) noexcept : 
			m_Event	{ std::forward<E>( event_callback ) }
		{
			using namespace I2CGeneral;
			//
			static_assert(Kernal::FilterCount < 0xf, "Filter count must be between 0x00 and 0x0f.");
			static_assert(General::IsAnyOf( Kernal::AddressBits, 7u, 10u ), "Only supports 7 and 10 bit address mode.");
			//
			// Disable the peripheral, this resets peripheral
			CR1_t{}.Config().PE() = false;
			APBDelay();
			//
			// Setup the filter
			CR1_t{}.Filter().ANFOFF() 	= !Kernal::AnalogFilter;
			CR1_t{}.Filter().DNF() 		= Kernal::FilterCount;
			//
			// Configure the peripheral timing requirements
			if ( !TIMINGR<Channel>{}.DataHoldTime( Kernal::DataHoldTime ) && 
				 !TIMINGR<Channel>{}.DataSetupTime( Kernal::DataSetupTime ) && 
				 !TIMINGR<Channel>{}.HighTime( Kernal::HighTime ) && 
				 !TIMINGR<Channel>{}.LowTime( Kernal::LowTime ) )
			{
				return;
			}
			//
			// Configure clock stretching
			CR1_t{}.Config().NOSTRETCH() 	= false;
			//
			// Disable the relevant DMA requests
			CR1_t{}.Config().RXDMAEN() 		= false;		// No DMA currently supported, but this wouldn't be hard to add.
			CR1_t{}.Config().TXDMAEN() 		= false;		// 	^	^	^	^
			//
			// Enable the requested interrupts
			CR1_t{}.Interrupt().TCIE()		= true;			// Always enabled
			CR1_t{}.Interrupt().RXIE() 		= true;			// 	^	^	^	^
			CR1_t{}.Interrupt().TXIE() 		= true;			// 	^	^	^	^
			CR1_t{}.Interrupt().STOPIE()	= true;			// 	^	^	^	^
			CR1_t{}.Interrupt().ERRIE() 	= false;		// 	^	^	^	^
			CR1_t{}.Interrupt().NACKIE()	= IsMaster();	// Only master devices "recieve" NACK, slave devices issue NACK.
			CR1_t{}.Interrupt().ADDRIE()	= !IsMaster();
			CR2_t{}.Protocol().AUTOEND() 	= false;
			//
			// Enable the peripheral
			CR1_t{}.Config().PE() 			= true;
		}
		/**
		 * @brief Construct a new I2CModule object
		 * 
		 * @tparam E The callback type.
		 * @param event_callback The event callback.
		 */
		template <typename E>
		I2CModule( Kernal, SDAPin, SCLPin, E && event_callback ) noexcept :
			I2CModule{ std::forward<E>( event_callback ) }
		{}
		/**
		 * @brief Whether or not the peripheral is ready for transactions.
		 * 
		 * @return true I2C module is valid and ready for transactions.
		 * @return false I2C module is not valid.
		 */
		static bool Valid() noexcept
		{
			return CR1_t{}.Config().PE(); 
		}				
		/**
		 * @brief Read data from an I2C slave.
		 * 
		 * @warning These functions are completely async and do not block, for this reason 
		 * it is critically important that the buffer is not externally modified until
		 * the read is completed.
		 * 
		 * @tparam Arg The data type.
		 * @param address The address of the I2C device. This address the lowest bit is don't care, 
		 * it is usually replaced with don't care, it should be 8 or 10 bits.
		 * 0xA0 and 0xA1 are treated as the same address in 8 bit mode.
         * @param trigger Whether or not to start the I2C transaction.
		 * @param data A pointer to a contigious block of data.
		 * 	The output of the read function is into the buffer.
		 * @return true 
		 * @return false 
		 */
		template<typename Arg>
		bool Read(std::uint16_t address, bool trigger, Arg data) noexcept
		{
			// Short circuit if push cannot be performed, setup doesn't call if push fails
			return m_Actions.Push( I2CAction{ address, data.Data(), data.Size(), true } ) and (not trigger or Start());
		}
		/**
		 * @brief Writes a sequence of views to the address.
		 * 
		 * @tparam Args A set of types to write to the i2c bus.
		 * @param address The address to write to.
         * @param trigger Whether or not to start the I2C transaction.
		 * @param views The data to write.
		 * @return true Write/s succeeded
		 * @return false Write/s failed.
		 */
		template<typename ... Args>
		bool Write(std::uint16_t address, bool trigger, Args const & ... views) noexcept
		{
			//
			// Short circuit if push cannot be performed, setup doesn't call if push fails
			using tuple_t = std::tuple<Args const &...>;
			//
			constexpr auto 	size	{ sizeof...(Args) };
			tuple_t 		tuple	{ views ... };
			//
			// Only insert when there is sufficient room
			if ( size >= m_Actions.Capacity() - m_Actions.Size() )
				return false;
			//
			General::ForEachAndTuple
			(
				tuple,
				[&]( auto const & item, auto index )
				{
					constexpr bool is_end = ( ( decltype( index )::value + 1u ) >= size );
					return m_ActionsPtr->Push( I2CAction{ address, item.Data(), item.Size(), is_end } );
				}
			);
			//
			return not trigger or Start();
		}
		/*
		 * @brief The error interrupt handler.
		 */
		static void MasterErrorInterrupt() noexcept
		{
			if (CR1_t{}.Interrupt().ERRIE())
			{
				if (ISR_t{}.ALERT())
				{
					ICR_t{}.ALERTCF();
					event_callback_t::Run(I2CEvent::Alert);
				}
				if (ISR_t{}.TIMEOUT())
				{
					ICR_t{}.TIMOUTCF();
					event_callback_t::Run(I2CEvent::Timeout);
				}
				if (ISR_t{}.PECERR())
				{
					ICR_t{}.PECCF();
					event_callback_t::Run(I2CEvent::PacketErrorChecking);
				}
				if (ISR_t{}.ARLO())
				{
					ICR_t{}.ARLOCF();
					event_callback_t::Run(I2CEvent::ArbitationLost);
				}
				if (ISR_t{}.BERR())
				{
					ICR_t{}.BERRCF();
					event_callback_t::Run(I2CEvent::BusError);
				}
			}
		}
		/**
		 * @brief The master interrupt handler.
		 */
		static void MasterInterrupt() noexcept
		{
			if (!!ISR_t{}.NACKF())
			{
				ICR_t{}.NACKCF();
			}
			/**
			 * 
			 */
			if (!!ISR_t{}.STOPF())
			{
				ICR_t{}.STOPCF();
				/**
				 * If there are more actions a start condition can be issued.
				 */
				Start();
			}
			/**
			 * 
			 */
			if ( m_ActionsPtr->Size() )
			{
				/**
				 * Transmit Interrupt Status (transmitters)
				 * 	This bit is set by hardware when the I2C_TXDR register is empty and the data to be
				 * 	transmitted must be written in the I2C_TXDR register. It is cleared when the next data to be
				 * 	sent is written in the I2C_TXDR register.
				 */
				if ( !!ISR_t{}.TXIS() )
				{
					if ( auto action_ptr{ m_ActionsPtr->Front().AsWrite() }; action_ptr && action_ptr->Size() )
					{
						/**
						 * We data is the copy of the front, its no longer needed
						 */
						TX_t{}.TXDATA() = action_ptr->Front();
						action_ptr->PopFront();
					}
					else event_callback_t::Run( I2CEvent::StateLost );
				}
				/**
				 * Receive data register Not Empty
				 * 	This bit is set by hardware when the received data is copied into the I2C_RXDR register, and is
				 * 	ready to be read. It is cleared when I2C_RXDR is read.
				 */
				else if ( !!ISR_t{}.RXNE() )
				{
					if ( auto action_ptr{ m_ActionsPtr->Front().AsRead() }; action_ptr && action_ptr->Size() )
					{
						/**
						 *  Write incomming data into buffer
						 * 	Front of buffer no longer needed
						 */
						action_ptr->Front() = RX_t{}.Get();
						action_ptr->PopFront();
					}
					else event_callback_t::Run( I2CEvent::StateLost );
				}
				/**
				 * Transfer Complete Reload
				 * 	This flag is set by hardware when RELOAD=1 and NBYTES data have been transferred. It is
				 * 	cleared by software when NBYTES is written to a non-zero value.
				 */
				else if ( !!ISR_t{}.TCR() )
				{
					/**
					 * Remove current item if there is no more data to handle
					 */
					if ( m_ActionsPtr->Front().ActionEmpty() )
						m_ActionsPtr->Pop();
					/**
					 * Setup the next block of bytes
					 *  If nothing was done about TCR then the only solution is
					 * 	to disable the peripheral, but the user is responsible for this
					 * 	because it is critical that they understand at this point the peripheral must be turned off
					 * 	this is not appropriate code hiding.
					 */
					if ( !SetReload() )
						event_callback_t::Run( I2CEvent::CorruptAction );
				}
				/**
				 * Transfer Complete (master mode)
				 * 	This flag is set by hardware when RELOAD=0, AUTOEND=0 and NBYTES data have been
				 * 	transferred. It is cleared by software when START bit or STOP bit is set.
				 */
				else if ( !!ISR_t{}.TC() )
				{
					auto & old_front = m_ActionsPtr->Front();
					/**
					 * Notify the requester
					 */
					if ( old_front.IsWrite() )
						event_callback_t::Run( I2CEvent::WriteComplete );
					//
					else if ( old_front.IsRead() )
						event_callback_t::Run( I2CEvent::ReadComplete );
					/**
					 * If the new action has the same address we can issue a restart instead
					 *  of another start condition.
					 * @todo Re-add restart conditon if needed
					 */

					/**
					 * Issue stop condition
					 */
					CR2_t{}.Protocol().STOP() = true;
					m_ActionsPtr->Pop();
				}
			}
			else
			{
				event_callback_t::Run( I2CEvent::ActionStarvation );
			}
			/**
			 * Handle errors
			 */
			MasterErrorInterrupt();
		}
		/**
		 * @brief Handles interrupts when the micro is a slave I2C device.
		 * @note Not yet implemented, this will be done one day when I have time...
		 */
		static void SlaveInterrupt() noexcept {}
		/**
		 * @brief The interrupt handler.
		 */
		static void Interrupt() noexcept
		{	
			// The interrupts for slave and master devices are different
			if constexpr ( IsMaster() )	MasterInterrupt();
			else 						SlaveInterrupt();
		}
	};
	//
	//
	template <typename Kernal, typename SDAPin, typename SCLPin, typename EventCallback>
	I2CModule( Kernal, SDAPin, SCLPin,EventCallback ) -> I2CModule<Kernal, SDAPin, SCLPin, EventCallback>;
	//
	// Initial state of Actions is empty.
	template <typename Kernal, typename SDAPin, typename SCLPin, typename EventCallback>
	typename I2CModule<Kernal, SDAPin, SCLPin, EventCallback>::action_fifo_t I2CModule<Kernal, SDAPin, SCLPin, EventCallback>::m_Actions;
}