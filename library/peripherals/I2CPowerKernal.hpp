#pragma once
#include "RegistersRCC.hpp"
#include <cstdint>

namespace Peripherals::I2CGeneral
{
	/**
	 * @brief A helper class to assist in powering up and down the I2C peripheral
	 * 
	 * @tparam Channel The I2C channel to manage.
	 * @tparam SDA The SDA pin.
	 * @tparam SCL The SCL pin.
	 */
	template< std::uint32_t Channel, typename SDA, typename SCL >
	struct I2CPowerKernal
	{
		//	Automated hardware design review
		//  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
		template<class T, class = void>
		struct HasSDA
		{
			static constexpr bool value = false;
		};
		template<class T>
		struct HasSDA<T, std::void_t<decltype(T::AltFunctions::I2C1_SDA)>>
		{
			static constexpr bool value = true;
			static constexpr std::uint32_t channel = 1;

			static_assert(channel == Channel, "Wrong I2C channel for SDA.");
		};
		template<class T>
		struct HasSDA<T, std::void_t<decltype(T::AltFunctions::I2C2_SDA)>>
		{
			static constexpr bool value = true;
			static constexpr std::uint32_t channel = 2;

			static_assert(channel == Channel, "Wrong I2C channel for SDA.");
		};
		//  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
		template<class T, class = void>
		struct HasSCL
		{
			static constexpr bool value = false;
		};
		template<class T>
		struct HasSCL<T, std::void_t<decltype(T::AltFunctions::I2C1_SCL)>>
		{
			static constexpr bool value = true;
			static constexpr std::uint32_t channel = 1;

			static_assert(channel == Channel, "Wrong I2C channel for SCL.");
		};
		template<class T>
		struct HasSCL<T, std::void_t<decltype(T::AltFunctions::I2C2_SCL)>>
		{
			static constexpr bool value = true;
			static constexpr std::uint32_t channel = 2;

			static_assert(channel == Channel, "Wrong I2C channel for SCL.");
		};
		//  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
		static constexpr bool HasSDA_v = HasSDA<SDA>::value;
		static constexpr bool HasSCL_v = HasSCL<SCL>::value;
		//  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
		static_assert(HasSDA_v, "SDA pin doesn't have required alternate function.");
		static_assert(HasSCL_v, "SCL pin doesn't have required alternate function.");
		//  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
		using mode_t = IO::Mode;
		/**
		 * @brief Connects the pins to the I2C peripheral.
		 */
		static void EnablePins() noexcept
		{
			SDA m_SDA;
			SCL m_SCL;
			// Need alternate functionailty
			m_SDA = mode_t::Alternate;
			m_SCL = mode_t::Alternate;
			//
			using sda_altfunctions_t = typename SDA::AltFunctions;
			using scl_altfunctions_t = typename SCL::AltFunctions;
			//
			if constexpr ( Channel == 1 )
			{	// Setup pins for I2C
				m_SDA = sda_altfunctions_t::I2C1_SDA;
				m_SCL = scl_altfunctions_t::I2C1_SCL;
			}
			if constexpr ( Channel == 2 )
			{	// Setup pins for I2C
				m_SDA = sda_altfunctions_t::I2C2_SDA;
				m_SCL = scl_altfunctions_t::I2C2_SCL;
			}
		}
		/**
		 * @brief Disconnects the pins from the I2C peripheral.
		 */
		static void DisablePins() noexcept
		{
			SDA m_SDA;
			SCL m_SCL;
			// Open circuit, the pull up will raise the line
			m_SDA = mode_t::Input;
			m_SCL = mode_t::Input;
		}
		/**
		 * @brief Enable the clock for the I2C peripheral
		 * 
		 */
		static void EnableClock() noexcept
		{
			if constexpr (Channel == 1)
			{
				RCCGeneral::CFGR3{}.I2C1SW() 	= false;// Use HSI
				RCCGeneral::APB1ENR{}.I2C1EN() 	= true;	// Enable clock for I2C1
			}
			if constexpr (Channel == 2)
				RCCGeneral::APB1ENR{}.I2C2EN() 	= true;	// Enable clock for I2C2
		}
		/**
		 * @brief Disables the I2C peripheral clock.
		 * 
		 */
		static void DisableClock() noexcept
		{
			if constexpr (Channel == 1)
				RCCGeneral::APB1ENR{}.I2C1EN() = false;	// Disable clock for I2C1
			if constexpr (Channel == 2)
				RCCGeneral::APB1ENR{}.I2C2EN() = false;	// Disable clock for I2C2
		}
		/**
		 * @brief Gets the interrupt source for the I2C channel.
		 * 
		 * @return constexpr IRQn_Type The interrupt source.
		 */
		static constexpr IRQn_Type ChannelInterrupt() noexcept
		{
			#ifdef STM32F072
			if constexpr (Channel == 1) return (IRQn_Type)System::InterruptSource::eI2C1;
			if constexpr (Channel == 2) return (IRQn_Type)System::InterruptSource::eI2C2;
			#endif

			#ifdef STM32F070x6
			if constexpr (Channel == 1) return (IRQn_Type)System::InterruptSource::eI2C1;
			#endif
		}
		/**
		 * @brief Enables the I2C perippherals interrupts.
		 */
		static void EnableInterrupts() noexcept
		{
			NVIC_EnableIRQ(ChannelInterrupt()); 	// Configure NVIC for I2C
			NVIC_SetPriority(ChannelInterrupt(),2); // Set priority for I2C (=2, low priority)
		}
		/**
		 * @brief Disables the I2C perpherals interrupts.
		 */
		static void DisableInterrupts() noexcept
		{
			NVIC_DisableIRQ(ChannelInterrupt());
		}
		/**
		 * @brief Should be called before using I2C peripheral.
		 */
		static void Construct() noexcept
		{
			EnableClock();
			EnablePins();
			EnableInterrupts();
		}
		/**
		 * @brief Disables the I2C peripheral after use.
		 */
		static void Destruct() noexcept
		{
			DisableInterrupts();
			DisablePins();
			DisableClock();
		}
	};
}