#pragma once

#include "Meta.hpp"
#include "Pin.hpp"
#include "PinDefinitions.hpp"
#include "AlternateFunction.hpp"
#include "Register.hpp"
#include "Interrupt.hpp"
#include "BlindFIFO.hpp"
#include "Types.hpp"
#include <cstdint>

namespace Peripherals::USARTGeneral
{	
	//
	//
	constexpr auto ImplUSARTChannel_TX(...)->std::integral_constant<unsigned, 0u>;
	template <typename T>	constexpr auto ImplUSARTChannel_TX(T) -> decltype((T::USART1_TX, std::integral_constant<unsigned, 1u>{}));
	template <typename T>	constexpr auto ImplUSARTChannel_TX(T) -> decltype((T::USART2_TX, std::integral_constant<unsigned, 2u>{}));
	template <typename T>	constexpr auto ImplUSARTChannel_TX(T) -> decltype((T::USART3_TX, std::integral_constant<unsigned, 3u>{}));
	template <typename T>	constexpr auto ImplUSARTChannel_TX(T) -> decltype((T::USART4_TX, std::integral_constant<unsigned, 4u>{}));
	template <typename T>	constexpr auto ImplUSARTChannel_TX(T) -> decltype((T::USART5_TX, std::integral_constant<unsigned, 5u>{}));
	template <typename T>	constexpr auto ImplUSARTChannel_TX(T) -> decltype((T::USART6_TX, std::integral_constant<unsigned, 6u>{}));
	template <typename T>	constexpr auto ImplUSARTChannel_TX(T) -> decltype((T::USART7_TX, std::integral_constant<unsigned, 7u>{}));
	template <typename T>	constexpr auto ImplUSARTChannel_TX(T) -> decltype((T::USART8_TX, std::integral_constant<unsigned, 8u>{}));
	//
	template <typename T>
	constexpr unsigned USARTChannelTX_v = decltype( ImplUSARTChannel_TX( T{} ) ){};
	//
	auto ImplUSARTChannel_RX(...)->std::integral_constant<unsigned, 0u>;
	template <typename T>	constexpr auto ImplUSARTChannel_RX(T) -> decltype((T::USART1_RX, std::integral_constant<unsigned, 1u>{}));
	template <typename T>	constexpr auto ImplUSARTChannel_RX(T) -> decltype((T::USART2_RX, std::integral_constant<unsigned, 2u>{}));
	template <typename T>	constexpr auto ImplUSARTChannel_RX(T) -> decltype((T::USART3_RX, std::integral_constant<unsigned, 3u>{}));
	template <typename T>	constexpr auto ImplUSARTChannel_RX(T) -> decltype((T::USART4_RX, std::integral_constant<unsigned, 4u>{}));
	template <typename T>	constexpr auto ImplUSARTChannel_RX(T) -> decltype((T::USART5_RX, std::integral_constant<unsigned, 5u>{}));
	template <typename T>	constexpr auto ImplUSARTChannel_RX(T) -> decltype((T::USART6_RX, std::integral_constant<unsigned, 6u>{}));
	template <typename T>	constexpr auto ImplUSARTChannel_RX(T) -> decltype((T::USART7_RX, std::integral_constant<unsigned, 7u>{}));
	template <typename T>	constexpr auto ImplUSARTChannel_RX(T) -> decltype((T::USART8_RX, std::integral_constant<unsigned, 8u>{}));
	//
	//
	constexpr auto HasCommandCompleteImpl(...)->std::false_type;
	template <typename T>
	constexpr auto HasCommandCompleteImpl(T v)->decltype((v.CommandComplete('\0'), std::true_type{}));
	//
	template <typename T>
	constexpr bool HasCommandComplete_v = decltype( HasCommandCompleteImpl( std::declval<T>() ) ){};
	//
	//
	constexpr auto HasIsBackspaceImpl(...)->std::false_type;
	template <typename T>
	constexpr auto HasIsBackspaceImpl(T v)->decltype((v.IsBackspace('\0'), std::true_type{}));
	//
	template <typename T>
	constexpr bool HasIsBackspace_v = decltype( HasIsBackspaceImpl( std::declval<T>() ) ){};
	//
	//
	constexpr auto HasShouldPushImpl(...)->std::false_type;
	//
	// char value, Containers::BlindFIFO<char, N> & buffer
	template <typename T>
	constexpr auto HasShouldPushImpl(T v)->decltype( ( v.ShouldPush('\0', std::declval<Containers::BlindFIFO<char, 2>&>()), std::true_type{} ) );
	//
	template <typename T>
	constexpr bool HasShouldPush_v = decltype( HasShouldPushImpl( std::declval<T>() ) ){};
	//
	//
	template <typename T>
	constexpr unsigned USARTChannelRX_v = decltype( ImplUSARTChannel_RX( T{} ) ){};
	//
	template <typename T, unsigned Channel>
	struct USARTAltFunctionRX{ };
	template <typename T>	struct USARTAltFunctionRX<T, 1> { static constexpr auto value = T::USART1_RX; };
	template <typename T>	struct USARTAltFunctionRX<T, 2> { static constexpr auto value = T::USART2_RX; };
	template <typename T>	struct USARTAltFunctionRX<T, 3> { static constexpr auto value = T::USART3_RX; };
	template <typename T>	struct USARTAltFunctionRX<T, 4> { static constexpr auto value = T::USART4_RX; };
	template <typename T>	struct USARTAltFunctionRX<T, 5> { static constexpr auto value = T::USART5_RX; };
	template <typename T>	struct USARTAltFunctionRX<T, 6> { static constexpr auto value = T::USART6_RX; };
	template <typename T>	struct USARTAltFunctionRX<T, 7> { static constexpr auto value = T::USART7_RX; };
	template <typename T>	struct USARTAltFunctionRX<T, 8> { static constexpr auto value = T::USART8_RX; };
	//
	template <typename T, unsigned C>
	constexpr typename T::Type USARTAltFunctionRX_v = USARTAltFunctionRX<T, C>::value;
	//
	template <typename T, unsigned Channel>
	struct USARTAltFunctionTX{ };
	template <typename T>	struct USARTAltFunctionTX<T, 1> { static constexpr auto value = T::USART1_TX; };
	template <typename T>	struct USARTAltFunctionTX<T, 2> { static constexpr auto value = T::USART2_TX; };
	template <typename T>	struct USARTAltFunctionTX<T, 3> { static constexpr auto value = T::USART3_TX; };
	template <typename T>	struct USARTAltFunctionTX<T, 4> { static constexpr auto value = T::USART4_TX; };
	template <typename T>	struct USARTAltFunctionTX<T, 5> { static constexpr auto value = T::USART5_TX; };
	template <typename T>	struct USARTAltFunctionTX<T, 6> { static constexpr auto value = T::USART6_TX; };
	template <typename T>	struct USARTAltFunctionTX<T, 7> { static constexpr auto value = T::USART7_TX; };
	template <typename T>	struct USARTAltFunctionTX<T, 8> { static constexpr auto value = T::USART8_TX; };
	//
	template <typename T, unsigned C>
	constexpr typename T::Type USARTAltFunctionTX_v = USARTAltFunctionTX<T, C>::value;
	//
	enum class Parity : std::uint32_t
	{
		None	= 0b00,
		Odd		= 0b11,
		Even	= 0b01
	};
	enum class FlowControl : std::uint32_t
	{
		None,
		XOnXOff,
		RtsCts,
		RsrDtr
	};
	enum class Stopbits : std::uint32_t
	{
		One				=	0b00,
		Half			=	0b01,
		Two				=	0b10,
		OneAndHalf		=	0b11,
	};
	enum class Databits : std::uint32_t
	{
		Bits8			=	0b00,
		Bits9			=	0b01,
		Bits7			=	0b10,
	};
	
	template <unsigned Channel>
	constexpr USART_TypeDef * USART()
	{
		static_assert((1 <= Channel) && (Channel <= 4), "The selected USART channel doesn't exist. Please only use channels 1-4.");
		switch(Channel)
		{
		case 1: return USART1;
		case 2: return USART2;
		case 3: return USART3;
		case 4: return USART4;
		};
	}
	constexpr auto USARTBase(unsigned Channel) noexcept
	{
		switch(Channel)
		{
		case 1: return USART1_BASE;
		case 2: return USART2_BASE;
		case 3: return USART3_BASE;
		case 4: return USART4_BASE;
		};
	}
	
	template <std::uint32_t Channel>
	constexpr auto USARTChannelInterrupt()
	{
#ifdef STM32F070x6
		switch(Channel)
		{
		case 1: 	return System::InterruptSource::eUSART1;
		case 2: 	return System::InterruptSource::eUSART2;
		};
#endif
#ifdef STM32F072
		switch(Channel)
		{
		case 1: 	return System::InterruptSource::eUSART1;
		case 2: 	return System::InterruptSource::eUSART2;
		case 3:
		case 4:		return System::InterruptSource::eUSART3_4;
		};
#endif
	}

	//This defines how the USART modules power and interrupt priority is setup, it doesn't handle anything else.
	template <std::uint32_t Channel>
	struct USARTPowerKernal
	{
		static constexpr IRQn_Type ChannelInterrupt = (IRQn_Type)USARTChannelInterrupt<Channel>();
		
		static auto & APB()
		{
			if constexpr (Channel == 1) return (RCC->APB2ENR);
			else 						return (RCC->APB1ENR);
		}
		static constexpr auto EnableFlag()
		{
			switch(Channel)
			{
			case 1:	return RCC_APB2ENR_USART1EN;
			case 2:	return RCC_APB1ENR_USART2EN;
			case 3:	return RCC_APB1ENR_USART3EN;
			case 4:	return RCC_APB1ENR_USART4EN;
			};
			return (uint32_t)0u;
		}

        static auto SelectClock() noexcept
        {
            using namespace RCCGeneral;
            CFGR3{}.SelectUSARTClock<Channel>( USARTClock::SYSCLK );
        }

		static void Construct()
		{
            SelectClock();
			APB() |= EnableFlag();
		}
		static void Destruct()
		{
			APB() &= ~EnableFlag();
		}
	};

	template<unsigned Channel, std::size_t A = USARTBase(Channel) + offsetof(USART_TypeDef, CR1)>
	struct CR1 : General::u32_reg<A>
	{
		using base_t = General::u32_reg<A>;
		using base_t::base_t;
		
		auto M1		()	{ return base_t::template Actual<USART_CR1_M_1	  >(); }
		auto EOBIE	()	{ return base_t::template Actual<USART_CR1_EOBIE  >(); }
		auto RTOIE	()	{ return base_t::template Actual<USART_CR1_RTOIE  >(); }
		auto DEAT4	()	{ return base_t::template Actual<USART_CR1_DEAT_4 >(); }
		auto DEAT3	()	{ return base_t::template Actual<USART_CR1_DEAT_3 >(); }
		auto DEAT2	()	{ return base_t::template Actual<USART_CR1_DEAT_2 >(); }
		auto DEAT1	()	{ return base_t::template Actual<USART_CR1_DEAT_1 >(); }
		auto DEAT0	()	{ return base_t::template Actual<USART_CR1_DEAT_0 >(); }
		auto DEDT4	()	{ return base_t::template Actual<USART_CR1_DEDT_4 >(); }
		auto DEDT3	()	{ return base_t::template Actual<USART_CR1_DEDT_3 >(); }
		auto DEDT2	()	{ return base_t::template Actual<USART_CR1_DEDT_2 >(); }
		auto DEDT1	()	{ return base_t::template Actual<USART_CR1_DEDT_1 >(); }
		auto DEDT0	()	{ return base_t::template Actual<USART_CR1_DEDT_0 >(); }
		auto OVER8	()	{ return base_t::template Actual<USART_CR1_OVER8  >(); }
		auto CMIE	()	{ return base_t::template Actual<USART_CR1_CMIE   >(); }
		auto MME	()	{ return base_t::template Actual<USART_CR1_MME    >(); }
		auto M0		()	{ return base_t::template Actual<USART_CR1_M_0	  >(); }
		auto WAKE	()	{ return base_t::template Actual<USART_CR1_WAKE   >(); }
		auto PCE	()	{ return base_t::template Actual<USART_CR1_PCE    >(); }
		auto PS		()	{ return base_t::template Actual<USART_CR1_PS	  >(); }
		auto PEIE	()	{ return base_t::template Actual<USART_CR1_PEIE   >(); }
		auto TXEIE	()	{ return base_t::template Actual<USART_CR1_TXEIE  >(); }
		auto TCIE	()	{ return base_t::template Actual<USART_CR1_TCIE   >(); }
		auto RXNEIE	()	{ return base_t::template Actual<USART_CR1_RXNEIE >(); }
		auto IDLEIE	()	{ return base_t::template Actual<USART_CR1_IDLEIE >(); }
		auto TE		()	{ return base_t::template Actual<USART_CR1_TE	  >(); }
		auto RE		()	{ return base_t::template Actual<USART_CR1_RE	  >(); }
		auto UESM	()	{ return base_t::template Actual<USART_CR1_UESM   >(); }
		auto UE		()	{ return base_t::template Actual<USART_CR1_UE	  >(); }

		void SetWordLength(Databits pInput)
		{
			M0() = (std::uint32_t)pInput & 1;
			M1() = ((std::uint32_t)pInput >> 1) & 1;
		}
		void SetParity(Parity const & pInput)
		{
			std::uint32_t value{ (std::uint32_t)pInput };
			PCE()	= (value & 1u);
			PS()	= (value >> 1) & 1u;
		}
	};
 
	template<unsigned Channel, std::size_t A = USARTBase(Channel) + offsetof(USART_TypeDef, CR2)>
	struct CR2 : General::u32_reg<A>
	{
		using base_t = General::u32_reg<A>;
		using base_t::base_t;

		auto ADD()		{ return base_t::template Actual<USART_CR2_ADD>(); 		}
		auto RTOEN()	{ return base_t::template Actual<USART_CR2_RTOEN>(); 	}
		auto ABRMOD1()	{ return base_t::template Actual<USART_CR2_ABRMODE_1>();}
		auto ABRMOD0()	{ return base_t::template Actual<USART_CR2_ABRMODE_0>();}
		auto ABREN()	{ return base_t::template Actual<USART_CR2_ABREN>(); 	}
		auto MSBFIRST()	{ return base_t::template Actual<USART_CR2_MSBFIRST>(); }
		auto DATAINV()	{ return base_t::template Actual<USART_CR2_DATAINV>(); 	}
		auto TXINV()	{ return base_t::template Actual<USART_CR2_TXINV>(); 	}
		auto RXINV()	{ return base_t::template Actual<USART_CR2_RXINV>(); 	}
		auto SWAP()		{ return base_t::template Actual<USART_CR2_SWAP>(); 	}
		auto LINEN()	{ return base_t::template Actual<USART_CR2_LINEN>(); 	}
		auto STOP()		{ return base_t::template Actual<USART_CR2_STOP>(); 	}
		auto CLKEN()	{ return base_t::template Actual<USART_CR2_CLKEN>(); 	}
		auto CPOL()		{ return base_t::template Actual<USART_CR2_CPOL>(); 	}
		auto CPHA()		{ return base_t::template Actual<USART_CR2_CPHA>(); 	}
		auto LBCL()		{ return base_t::template Actual<USART_CR2_LBCL>(); 	}
		auto LBDIE()	{ return base_t::template Actual<USART_CR2_LBDIE>(); 	}
		auto LBDL()		{ return base_t::template Actual<USART_CR2_LBDL>(); 	}
		auto ADDM7()	{ return base_t::template Actual<USART_CR2_ADDM7>(); 	}
		
		void SetStopbits(Stopbits const & pInput)
		{
			STOP() = (std::uint32_t)pInput;
		}
	};

	template<unsigned Channel, std::size_t A = USARTBase(Channel) + offsetof(USART_TypeDef, CR3)>
	struct CR3 : General::u32_reg<A>
	{
		using base_t = General::u32_reg<A>;
		using base_t::base_t;

		auto WUFIE()	{ return base_t::template Actual<USART_CR3_WUFIE>(); 	}
		auto WUS()		{ return base_t::template Actual<USART_CR3_WUS>(); 		}
		auto SCARCNT()	{ return base_t::template Actual<USART_CR3_SCARCNT>(); 	}
		auto DEP()		{ return base_t::template Actual<USART_CR3_DEP>(); 		}
		auto DEM()		{ return base_t::template Actual<USART_CR3_DEM>(); 		}
		auto DDRE()		{ return base_t::template Actual<USART_CR3_DDRE>(); 	}
		auto OVRDIS()	{ return base_t::template Actual<USART_CR3_OVRDIS>(); 	}
		auto ONEBIT()	{ return base_t::template Actual<USART_CR3_ONEBIT>(); 	}
		auto CTSIE()	{ return base_t::template Actual<USART_CR3_CTSIE>(); 	}
		auto CTSE()		{ return base_t::template Actual<USART_CR3_CTSE>(); 	}
		auto RTSE()		{ return base_t::template Actual<USART_CR3_RTSE>(); 	}
		auto DMAT()		{ return base_t::template Actual<USART_CR3_DMAT>(); 	}
		auto DMAR()		{ return base_t::template Actual<USART_CR3_DMAR>(); 	}
		auto SCEN()		{ return base_t::template Actual<USART_CR3_SCEN>(); 	}
		auto NACK()		{ return base_t::template Actual<USART_CR3_NACK>(); 	}
		auto HDSEL()	{ return base_t::template Actual<USART_CR3_HDSEL>(); 	}
		auto IRLP()		{ return base_t::template Actual<USART_CR3_IRLP>(); 	}
		auto IREN()		{ return base_t::template Actual<USART_CR3_IREN>(); 	}
		auto EIE()		{ return base_t::template Actual<USART_CR3_EIE>(); 		}
		
		void EnableDMA(bool pRecieve, bool pTransmit)
		{
			DMAR() = pRecieve;
			DMAT() = pTransmit;
		}
	};

	template<unsigned Channel, std::size_t A = USARTBase(Channel) + offsetof(USART_TypeDef, BRR)>
	struct BRR : General::u32_reg<A>
	{
		using base_t = General::u32_reg<A>;
		using base_t::base_t;
		
		BRR & operator = ( std::uint32_t input ) noexcept
		{
			base_t::template Actual<USART_BRR_BRR>() = input;
			return *this;
		}
		
		void SetBaudrate( std::uint32_t input ) noexcept
		{
			auto div = ::System::SystemClock_t::USARTCLK<Channel>() / input;
			if ( CR1<Channel>().OVER8() )
			{
				div *= 2u;
				auto lower = (div & 0xf) >> 1u;
				div = ((div & 0xfff0) | lower);
			}
			(*this) = div;
		}
	};

	template<unsigned Channel, std::size_t A = USARTBase(Channel) + offsetof(USART_TypeDef, GTPR)>
	struct GTPR : General::u32_reg<A>
	{
		using base_t = General::u32_reg<A>;
		using base_t::base_t;
		
		auto GT() { return base_t::template Actual<USART_GTPR_GT>(); }
		auto PSC() { return base_t::template Actual<USART_GTPR_PSC>(); }
	};

	template<unsigned Channel, std::size_t A = USARTBase(Channel) + offsetof(USART_TypeDef, RTOR)>
	struct RTOR : General::u32_reg<A>
	{
		using base_t = General::u32_reg<A>;
		using base_t::base_t;
		
		auto RTO() { return base_t::template Actual<USART_RTOR_RTO>(); }
		auto BLEN() { return base_t::template Actual<USART_RTOR_BLEN>(); }
	};
    
	template<unsigned Channel, std::size_t A = USARTBase(Channel) + offsetof(USART_TypeDef, RQR)>
	struct RQR : General::u32_reg<A>
	{
		using base_t = General::u32_reg<A>;
		using base_t::base_t;

		auto TXFRQ() { return base_t::template Actual<USART_RQR_TXFRQ>(); }
		auto RXFRQ() { return base_t::template Actual<USART_RQR_RXFRQ>(); }
		auto MMRQ()	 { return base_t::template Actual<USART_RQR_MMRQ>(); }
		auto SBKRQ() { return base_t::template Actual<USART_RQR_SBKRQ>(); }
		auto ABRRQ() { return base_t::template Actual<USART_RQR_ABRRQ>(); }
	};

	template<unsigned Channel, std::size_t A = USARTBase(Channel) + offsetof(USART_TypeDef, ISR)>
	struct ISR : General::u32_reg<A>
	{
		using base_t = General::u32_reg<A>;
		using base_t::base_t;

		auto REACK() { return base_t::template Actual<USART_ISR_REACK>();}
		auto TEACK() { return base_t::template Actual<USART_ISR_TEACK>();}
		auto WUF  () { return base_t::template Actual<USART_ISR_WUF>();}
		auto RWU  () { return base_t::template Actual<USART_ISR_RWU>();}
		auto SBKF () { return base_t::template Actual<USART_ISR_SBKF>();}
		auto CMF  () { return base_t::template Actual<USART_ISR_CMF>();}
		auto BUSY () { return base_t::template Actual<USART_ISR_BUSY>();}
		auto ABRF () { return base_t::template Actual<USART_ISR_ABRF>();}
		auto ABRE () { return base_t::template Actual<USART_ISR_ABRE>();}
		auto EOBF () { return base_t::template Actual<USART_ISR_EOBF>();}
		auto RTOF () { return base_t::template Actual<USART_ISR_RTOF>();}
		auto CTS  () { return base_t::template Actual<USART_ISR_CTS>();}
		auto CTSIF() { return base_t::template Actual<USART_ISR_CTSIF>();}
		auto TXE  () { return base_t::template Actual<USART_ISR_TXE>();}
		auto TC	  () { return base_t::template Actual<USART_ISR_TC>();}
		auto RXNE () { return base_t::template Actual<USART_ISR_RXNE>();}
		auto IDLE () { return base_t::template Actual<USART_ISR_IDLE>();}
		auto ORE  () { return base_t::template Actual<USART_ISR_ORE>();}
		auto NE	  () { return base_t::template Actual<USART_ISR_NE>();}
		auto FE	  () { return base_t::template Actual<USART_ISR_FE>();}
		auto PE	  () { return base_t::template Actual<USART_ISR_PE>();}
	};

	template<unsigned Channel, std::size_t A = USARTBase(Channel) + offsetof(USART_TypeDef, ICR)>
	struct ICR : General::u32_reg<A>
	{
		using base_t = General::u32_reg<A>;
		using base_t::base_t;

		auto CMCF  () { return base_t::template Actual<USART_ICR_CMCF  >(); }
		auto RTOCF () { return base_t::template Actual<USART_ICR_RTOCF >(); }
		auto CTSCF () { return base_t::template Actual<USART_ICR_CTSCF >(); }
		auto TCCF  () { return base_t::template Actual<USART_ICR_TCCF  >(); }
		auto IDLECF() { return base_t::template Actual<USART_ICR_IDLECF>(); }
		auto ORECF () { return base_t::template Actual<USART_ICR_ORECF >(); }
		auto NCF   () { return base_t::template Actual<USART_ICR_NCF   >(); }
		auto FECF  () { return base_t::template Actual<USART_ICR_FECF  >(); }
		auto PECF  () { return base_t::template Actual<USART_ICR_PECF  >(); }
		/**
		 * @brief Clears all the interrupt flags
		 */
		void Clear() noexcept
		{
			base_t::Set
			(
				USART_ICR_CMCF |  
				USART_ICR_RTOCF | 
				USART_ICR_CTSCF | 
				USART_ICR_TCCF |  
				USART_ICR_IDLECF |
				USART_ICR_ORECF | 
				USART_ICR_NCF |   
				USART_ICR_FECF |  
				USART_ICR_PECF
			);
		}
	};
	
	template<unsigned Channel, std::size_t A = USARTBase(Channel) + offsetof(USART_TypeDef, RDR)>
	struct RDR : General::u32_reg<A>
	{
		using base_t = General::u32_reg<A>;
		using base_t::base_t;
		
		auto & operator = (std::uint32_t input) noexcept
		{
			base_t::template Actual<USART_RDR_RDR>() = input;
			return *this;
		}
	};

	template<unsigned Channel, std::size_t A = USARTBase(Channel) + offsetof(USART_TypeDef, TDR)>
	struct TDR : General::u32_reg<A>
	{
		using base_t = General::u32_reg<A>;
		using base_t::base_t;
	
		auto & operator = (std::uint32_t input) noexcept
		{
			base_t::template Actual<USART_TDR_TDR>() = input;
			return *this;
		}
	};
}