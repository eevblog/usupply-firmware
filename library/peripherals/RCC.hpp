#pragma once

#include "Math.hpp"
#include "Config.hpp"
#include "Constants.hpp"
#include "RegistersRCC.hpp"

#include <cstdint>

namespace Peripherals
{	
	enum class Peripheral : std::uint32_t
	{
		ClockTSC,
		ClockGPIOF,
		ClockGPIOE,
		ClockGPIOD,
		ClockGPIOC,
		ClockGPIOB,
		ClockGPIOA,
		ClockCRC,
		ClockFLITF,
		ClockSRAM,
		ClockDMA2,
		ClockDMA1,
		ClockDMA = ClockDMA1,
		ClockDebug,
		ClockTIM17,
		ClockTIM16,
		ClockTIM15,
		ClockSPI1,
		ClockTIM1,
		ClockADC,
		ClockUSART1,
		ClockUSART6,
		ClockUSART7,
		ClockUSART8,
		ClockSYSCFGCOMP,
		ClockHDMI_CEC,
		ClockDAC,
		ClockPWR,
		ClockCRS,
		ClockCAN,
		ClockUSB,
		ClockI2C2,
		ClockI2C1,
		ClockUSART5,
		ClockUSART4,
		ClockUSART3,
		ClockUSART2,
		ClockSPI2,
		ClockWWDG,
		ClockTIM14,
		ClockTIM7,
		ClockTIM6,
		ClockTIM3,
		ClockTIM2,
	};
	/**
	 * @brief A manager class for the RCC peripheral.
	 * 
	 * This class is used to enable or disable any peripheral on the micro.
	 * 
	 * @tparam module 
	 */
	template <Peripheral module>
	struct RCCPeripheral
	{
		/**
		 * @brief Turns on or off the clock to a peripheral.
		 * 
		 * @param power Whether the state is on or off.
		 */
		static void Power(RCCGeneral::ClockState const power = RCCGeneral::ClockState::On) noexcept
		{
			using namespace RCCGeneral;
			
			if constexpr ( module == Peripheral::ClockTSC )
				AHBENR{}.TSCEN() = (bool)power;
			if constexpr ( module == Peripheral::ClockGPIOF        )
				AHBENR{}.GPIOFEN() = (bool)power;
			if constexpr ( module == Peripheral::ClockGPIOE        )
				AHBENR{}.GPIOEEN() = (bool)power;
			if constexpr ( module == Peripheral::ClockGPIOD        )
				AHBENR{}.GPIODEN() = (bool)power;
			if constexpr ( module == Peripheral::ClockGPIOC        )
				AHBENR{}.GPIOCEN() = (bool)power;
			if constexpr ( module == Peripheral::ClockGPIOB        )
				AHBENR{}.GPIOBEN() = (bool)power;
			if constexpr ( module == Peripheral::ClockGPIOA        )
				AHBENR{}.GPIOBEN() = (bool)power;
			if constexpr ( module == Peripheral::ClockCRC          )
				AHBENR{}.CRCEN() = (bool)power;
			if constexpr ( module == Peripheral::ClockFLITF        )
				AHBENR{}.FLITFEN() = (bool)power;
			if constexpr ( module == Peripheral::ClockSRAM         )
				AHBENR{}.SRAMEN() = (bool)power;
			if constexpr ( module == Peripheral::ClockDMA2         )
				AHBENR{}.DMA2EN() = (bool)power;
			if constexpr ( module == Peripheral::ClockDMA1         )
				AHBENR{}.DMAEN() = (bool)power;
			if constexpr ( module == Peripheral::ClockDebug        )
				APB2ENR{}.DBGMCUEN() = (bool)power;
			if constexpr ( module == Peripheral::ClockTIM17        )
				APB2ENR{}.TIM17EN() = (bool)power;
			if constexpr ( module == Peripheral::ClockTIM16        )
				APB2ENR{}.TIM16EN() = (bool)power;
			if constexpr ( module == Peripheral::ClockTIM15        )
				APB2ENR{}.TIM15EN() = (bool)power;
			if constexpr ( module == Peripheral::ClockSPI1         )
				APB2ENR{}.SPI1EN() = (bool)power;
			if constexpr ( module == Peripheral::ClockTIM1         )
				APB2ENR{}.TIM1EN() = (bool)power;
			if constexpr ( module == Peripheral::ClockADC          )
				APB2ENR{}.ADCEN() = (bool)power;
			if constexpr ( module == Peripheral::ClockUSART1       )
				APB2ENR{}.USART1EN() = (bool)power;
			if constexpr ( module == Peripheral::ClockUSART6       )
				APB2ENR{}.USART6EN() = (bool)power;
			if constexpr ( module == Peripheral::ClockUSART7       )
				APB2ENR{}.USART7EN() = (bool)power;
			if constexpr ( module == Peripheral::ClockUSART8       )
				APB2ENR{}.USART8EN() = (bool)power;
			if constexpr ( module == Peripheral::ClockSYSCFGCOMP   )
				APB2ENR{}.SYSCFGCOMPEN() = (bool)power;
			if constexpr ( module == Peripheral::ClockHDMI_CEC     )
				APB1ENR{}.CECEN() = (bool)power;
			if constexpr ( module == Peripheral::ClockDAC          )
				APB1ENR{}.DACEN() = (bool)power;
			if constexpr ( module == Peripheral::ClockPWR          )
				APB1ENR{}.PWREN() = (bool)power;
			if constexpr ( module == Peripheral::ClockCRS          )
				APB1ENR{}.CRSEN() = (bool)power;
			if constexpr ( module == Peripheral::ClockCAN          )
				APB1ENR{}.CANEN() = (bool)power;
			if constexpr ( module == Peripheral::ClockUSB          )
				APB1ENR{}.USBEN() = (bool)power;
			if constexpr ( module == Peripheral::ClockI2C2         )
				APB1ENR{}.I2C2EN() = (bool)power;
			if constexpr ( module == Peripheral::ClockI2C1         )
				APB1ENR{}.I2C1EN() = (bool)power;
			if constexpr ( module == Peripheral::ClockUSART5       )
				APB1ENR{}.USART5EN() = (bool)power;
			if constexpr ( module == Peripheral::ClockUSART4       )
				APB1ENR{}.USART4EN() = (bool)power;
			if constexpr ( module == Peripheral::ClockUSART3       )
				APB1ENR{}.USART3EN() = (bool)power;
			if constexpr ( module == Peripheral::ClockUSART2       )
				APB1ENR{}.USART2EN() = (bool)power;
			if constexpr ( module == Peripheral::ClockSPI2         )
				APB1ENR{}.SPI2EN() = (bool)power;
			if constexpr ( module == Peripheral::ClockWWDG         )
				APB1ENR{}.WWDGEN() = (bool)power;
			if constexpr ( module == Peripheral::ClockTIM14        )
				APB1ENR{}.TIM14EN() = (bool)power;
			if constexpr ( module == Peripheral::ClockTIM7         )
				APB1ENR{}.TIM7EN() = (bool)power;
			if constexpr ( module == Peripheral::ClockTIM6         )
				APB1ENR{}.TIM6EN() = (bool)power;
			if constexpr ( module == Peripheral::ClockTIM3         )
				APB1ENR{}.TIM3EN() = (bool)power;
			if constexpr ( module == Peripheral::ClockTIM2         )
				APB1ENR{}.TIM2EN() = (bool)power;
		}
		/**
		 * @brief Resets a peripheral to its default state.
		 */
		static void Reset() noexcept
		{
			using namespace RCCGeneral;
			
			if constexpr ( module == Peripheral::ClockGPIOF        )
				AHBRSTR{}.GPIOFRST() = true;
			if constexpr ( module == Peripheral::ClockGPIOE        )
				AHBRSTR{}.GPIOERST() = true;
			if constexpr ( module == Peripheral::ClockGPIOD        )
				AHBRSTR{}.GPIODRST() = true;
			if constexpr ( module == Peripheral::ClockGPIOC        )
				AHBRSTR{}.GPIOCRST() = true;
			if constexpr ( module == Peripheral::ClockGPIOB        )
				AHBRSTR{}.GPIOBRST() = true;
			if constexpr ( module == Peripheral::ClockGPIOA        )
				AHBRSTR{}.GPIOBRST() = true;
			if constexpr ( module == Peripheral::ClockCRC          )
				APB1RSTR{}.CRSRST() = true;
			if constexpr ( module == Peripheral::ClockDebug        )
				APB2RSTR{}.DBGMCURST() = true;
			if constexpr ( module == Peripheral::ClockTIM17        )
				APB2RSTR{}.TIM17RST() = true;
			if constexpr ( module == Peripheral::ClockTIM16        )
				APB2RSTR{}.TIM16RST() = true;
			if constexpr ( module == Peripheral::ClockTIM15        )
				APB2RSTR{}.TIM15RST() = true;
			if constexpr ( module == Peripheral::ClockSPI1         )
				APB2RSTR{}.SPI1RST() = true;
			if constexpr ( module == Peripheral::ClockTIM1         )
				APB2RSTR{}.TIM1RST() = true;
			if constexpr ( module == Peripheral::ClockADC          )
				APB2RSTR{}.ADCRST() = true;
			if constexpr ( module == Peripheral::ClockUSART1       )
				APB2RSTR{}.USART1RST() = true;
			if constexpr ( module == Peripheral::ClockUSART6       )
				APB2RSTR{}.USART6RST() = true;
			if constexpr ( module == Peripheral::ClockUSART7       )
				APB2RSTR{}.USART7RST() = true;
			if constexpr ( module == Peripheral::ClockUSART8       )
				APB2RSTR{}.USART8RST() = true;
			if constexpr ( module == Peripheral::ClockSYSCFGCOMP   )
				APB2RSTR{}.SYSCFGRST() = true;
			if constexpr ( module == Peripheral::ClockHDMI_CEC     )
				APB1RSTR{}.CECRST() = true;
			if constexpr ( module == Peripheral::ClockDAC          )
				APB1RSTR{}.DACRST() = true;
			if constexpr ( module == Peripheral::ClockPWR          )
				APB1RSTR{}.PWRRST() = true;
			if constexpr ( module == Peripheral::ClockCRS          )
				APB1RSTR{}.CRSRST() = true;
			if constexpr ( module == Peripheral::ClockCAN          )
				APB1RSTR{}.CANRST() = true;
			if constexpr ( module == Peripheral::ClockUSB          )
				APB1RSTR{}.USBRST() = true;
			if constexpr ( module == Peripheral::ClockI2C2         )
				APB1RSTR{}.I2C2RST() = true;
			if constexpr ( module == Peripheral::ClockI2C1         )
				APB1RSTR{}.I2C1RST() = true;
			if constexpr ( module == Peripheral::ClockUSART5       )
				APB1RSTR{}.USART5RST() = true;
			if constexpr ( module == Peripheral::ClockUSART4       )
				APB1RSTR{}.USART4RST() = true;
			if constexpr ( module == Peripheral::ClockUSART3       )
				APB1RSTR{}.USART3RST() = true;
			if constexpr ( module == Peripheral::ClockUSART2       )
				APB1RSTR{}.USART2RST() = true;
			if constexpr ( module == Peripheral::ClockSPI2         )
				APB1RSTR{}.SPI2RST() = true;
			if constexpr ( module == Peripheral::ClockWWDG         )
				APB1RSTR{}.WWDGRST() = true;
			if constexpr ( module == Peripheral::ClockTIM14        )
				APB1RSTR{}.TIM14RST() = true;
			if constexpr ( module == Peripheral::ClockTIM7         )
				APB1RSTR{}.TIM7RST() = true;
			if constexpr ( module == Peripheral::ClockTIM6         )
				APB1RSTR{}.TIM6RST() = true;
			if constexpr ( module == Peripheral::ClockTIM3         )
				APB1RSTR{}.TIM3RST() = true;
			if constexpr ( module == Peripheral::ClockTIM2         )
				APB1RSTR{}.TIM2RST() = true;
		}
	};
	/**
	 * @brief 
	 * 
	 * @tparam Item 
	 */
	template <Peripheral Item>
	struct GeneralPowerKernal
	{
		static constexpr Peripheral Current = Item;
		using RCCPeripheral_t = RCCPeripheral<Item>;
				
		static void Construct() noexcept
		{
			RCCPeripheral_t::Power(RCCGeneral::ClockState::On);
		}
		static void Destruct() noexcept
		{
			RCCPeripheral_t::Power(RCCGeneral::ClockState::Off);
		}
	};
	/**
	 * @brief 
	 * 
	 * @tparam module 
	 */
	template <RCCGeneral::RTCClockSource source>
	struct RTCClockEnabler
	{
		static void On() noexcept
		{
			using namespace RCCGeneral;
            if constexpr (source == RTCClockSource::None) 	    return;
            if constexpr (source == RTCClockSource::LSE)	    BDCR{}.EnableLSE();
            if constexpr (source == RTCClockSource::LSI) 	    CSR{}.EnableLSI();
            if constexpr (source == RTCClockSource::HSE_32)	    CR{}.EnableHSE();
		}
        static void Off() noexcept
        {
			using namespace RCCGeneral;
            if constexpr (source == RTCClockSource::None) 	    return;
            if constexpr (source == RTCClockSource::LSE)	    BDCR{}.DisableLSE();
            if constexpr (source == RTCClockSource::LSI) 	    CSR{}.DisableLSI();
            if constexpr (source == RTCClockSource::HSE_32)	    CR{}.DisableHSE();
        }
	};
	template <RCCGeneral::SystemClockSource source>
	struct SystemClockEnabler
	{
		static void On() noexcept
		{
			using namespace RCCGeneral;
            if constexpr (source == SystemClockSource::HSI) CR{}.EnableHSI();
            if constexpr (source == SystemClockSource::HSE) CR{}.EnableHSE();
            if constexpr (source == SystemClockSource::PLL) CR{}.EnablePLL();
		}
		static void Off() noexcept
		{
			using namespace RCCGeneral;
            if constexpr (source == SystemClockSource::HSI) CR{}.DisableHSI();
            if constexpr (source == SystemClockSource::HSE) CR{}.DisableHSE();
            if constexpr (source == SystemClockSource::PLL) CR{}.DisablePLL();
        }
	};
	/**
	 * @brief A manager class for the system clock.
	 * 
	 * This sets up:
	 * @li PLL
	 * @li Clock sources.
	 * @li Clock tree calculations.
	 * 
	 * @tparam Source The main clock source.
	 * @tparam PCLKDivision The division for PCLK.
	 * @tparam HCLKDivision The division for HCLK.
     * @tparam PLLSource The clock source for the PLL system (before divider and multiplier).
	 * @tparam PLLMultiple The multiplier for the PLL.
	 * @tparam PLLDivision The divider for the PLL.
	 */
	template
	<
		RCCGeneral::SystemClockSource   SystemSource,
		RCCGeneral::PCLKDivision 	    PCLKDivision,
		RCCGeneral::HCLKDivision 	    HCLKDivision,
        RCCGeneral::PLLSource           PLLSource,
		RCCGeneral::PLLMultiply		    PLLMultiply,
		RCCGeneral::PLLDivision		    PLLDivision
	>
	class SystemClock
	{
	private:
		using Clocks           	= RCCGeneral::Clocks;
		using SystemClockSource = RCCGeneral::SystemClockSource;
		/**
         */
        static void SelectClock(SystemClockSource source) noexcept
        {
            RCCGeneral::CFGR{}.SelectSystemClock( source );
        }
		/**
         */
		static void SetupPLL() noexcept
		{
			using namespace RCCGeneral;
			/**
			 * PLL must be off before modifying it.
			 * 
             * To modify the PLL configuration, proceed as follows:
             *  1. Disable the PLL by setting PLLON to 0.
             *  2. Wait until PLLRDY is cleared. The PLL is now fully stopped.
             */
			CR{}.DisablePLL();
            /**
             * 3. Change the desired parameter.
             *  Enable HCLK, PCLK
             */
            CFGR{}.SetupPrescalers( HCLKDivision, PCLKDivision );
            /**
             * Configure the PLL ( Division / Multiply )
             */
            CFGR2{}.SetPLLDivision( PLLDivision );
            CFGR{}.SetPLLMultiply( PLLMultiply );
            /**
             * 4. Enable the PLL again by setting PLLON to 1.
             * 5. Wait until PLLRDY is set.
             */
			CR{}.EnablePLL();
		}
		/**
		 */
		static constexpr std::uint64_t SourceFrequency() noexcept
		{
            using namespace RCCGeneral;
            if constexpr ( PLLSource == PLLSource::HSI_2 )   return ::System::HSIClock;
            if constexpr ( PLLSource == PLLSource::HSI )     return ::System::HSIClock;
            if constexpr ( PLLSource == PLLSource::HSE )     return ::System::HSEClock;
            if constexpr ( PLLSource == PLLSource::HSI48 )   return ::System::HSI48Clock;
		}
        /**
         */
        template <typename T>
        static constexpr std::uint64_t GetUint64( T const & input ) noexcept
        {
            return static_cast<std::uint64_t>( input );
        }
		/**
         */
		static constexpr std::uint64_t GetMultiple() noexcept
		{
			return GetUint64( PLLMultiply ) + GetUint64( 2u );
		}
		/**
         */
		static constexpr std::uint64_t GetDivider() noexcept
		{
            if constexpr ( PLLSource == RCCGeneral::PLLSource::HSI_2 )
            {
			    return General::Minimum<uint64_t>( GetUint64( PLLDivision ) + GetUint64( 1u ), GetUint64( 2u ) );
            }
            else
            {
			    return GetUint64( PLLDivision ) + GetUint64( 1u );
            }
		}
		/**
		 */
		static constexpr std::uint64_t HPREDiv() noexcept
		{
            using namespace RCCGeneral;
            //
			switch( HCLKDivision )
			{
			default:                        return 0u;
			case HCLKDivision::SYSCLK_1: 	return 1u;
			case HCLKDivision::SYSCLK_2: 	return 2u;
			case HCLKDivision::SYSCLK_4: 	return 4u;
			case HCLKDivision::SYSCLK_8: 	return 8u;
			case HCLKDivision::SYSCLK_16: 	return 16u;
			case HCLKDivision::SYSCLK_64: 	return 64u;
			case HCLKDivision::SYSCLK_128: 	return 128u;
			case HCLKDivision::SYSCLK_256: 	return 256u;
			}
		}
		/**
         */
		static constexpr std::uint64_t PPREDiv() noexcept
		{
            using namespace RCCGeneral;
            //
			switch ( PCLKDivision )
			{
			default:                    return 0u;
			case PCLKDivision::HCLK_1: 	return 1u;
			case PCLKDivision::HCLK_2: 	return 2u;
			case PCLKDivision::HCLK_4: 	return 4u;
			case PCLKDivision::HCLK_8: 	return 8u;
			case PCLKDivision::HCLK_16:	return 16u;
			}
		}
	public:
		/**
		 * @brief Sets up the system clock.
		 */
		SystemClock() noexcept
		{
			using namespace RCCGeneral;
            /**
			 * Set latency for high speed clock
			 * @todo Make this dependant on the clock.
             */
			FLASH->ACR = FLASH_ACR_LATENCY;
            CR{}.CSSON() = false;
            CR{}.DisableHSE();
			/**
			 * Enable PLL, no point if it is at the boot value of 1
             */
			SetupPLL();
            /**
             * Select the requested clock source.
             */
            SelectClock( SystemSource );
		}
        /**
		 * @brief The frequency of the system clock.
		 * 
		 * @return The PLLCLK clock frequency.
         */
        static constexpr std::uint64_t PLLCLK() noexcept
        {
			return ::General::RoundedDivide<std::uint64_t>( ( SourceFrequency() * GetMultiple() ), GetDivider() );
        }
		/**
		 * @brief The frequency of the system clock.
		 * 
		 * @return The SYSCLK clock frequency.
		 */
		static constexpr std::uint64_t SYSCLK() noexcept
		{
            using RCCGeneral::SystemClockSource;
            //
            switch ( SystemSource )
            {
            case SystemClockSource::HSI:    return ::System::HSIClock;
            case SystemClockSource::HSE:    return ::System::HSEClock;
            case SystemClockSource::HSI48:  return ::System::HSI48Clock;
            case SystemClockSource::PLL:    return PLLCLK();
            default:                        return 0;
            }
		}
        /**
         * @brief 
         * 
		 * @return The HCLK clock frequency.
         */
        static constexpr std::uint64_t HCLK() noexcept
        {
            return ::General::RoundedDivide<std::uint64_t>( SYSCLK(), HPREDiv() );
        }
        /**
         * @brief 
         * 
		 * @return The PCLK clock frequency.
         */
        static constexpr std::uint64_t PCLK() noexcept
        {
            return ::General::RoundedDivide<std::uint64_t>( HCLK(), PPREDiv() );
        }
		/**
		 * @brief Get the frequency that feeds into timers.
		 * 
		 * @return The timer clock frequency.
		 */
		static constexpr std::uint64_t Timer() noexcept
		{
			return PCLK();
		}
        /**
         * @brief 
         */
        template <unsigned Channel>
        static std::uint64_t USARTCLK() noexcept
        {
            using RCCGeneral::USARTClock;
            USARTClock source_selection = []() noexcept
            {
                if constexpr (Channel == 1)
                {
                    return static_cast<USARTClock>( RCCGeneral::CFGR3{}.USART1SW().Get() );
                }
                else if constexpr (Channel == 2)
                {
                    return static_cast<USARTClock>( RCCGeneral::CFGR3{}.USART2SW().Get() );
                }
                else if constexpr (Channel == 3)
                {
                    return static_cast<USARTClock>( RCCGeneral::CFGR3{}.USART3SW().Get() );
                }
                else
                {
                    return 0;
                }
            }();
            //
            switch ( source_selection )
            {
            case USARTClock::PCLK:      return PCLK();
            case USARTClock::SYSCLK:    return SYSCLK();
            case USARTClock::HSI:       return ::System::HSIClock;
            case USARTClock::LSE:       return ::System::LSEClock;
            default:                    return 0;
            }
        }
	};
}