#pragma once

#include "Meta.hpp"
#include "Register.hpp"
#include "Date.hpp"
#include "Time.hpp"
#include "stm32f0xx_rtc.h"
#include <cstdint>
#include <array>
#include <type_traits>

namespace Peripherals::RTCGeneral
{
	enum class Notation : std::uint32_t
	{
		AM			= 0u,
		Hours24		= 0u,
		PM			= 1u,
		AM_PM		= 1u
	};
	enum class WakeupClockSelection : std::uint32_t
	{
		RTC_16	=	0,
		RTC_8	=	1,
		RTC_4	=	2,
		RTC_2	=	3,
		CK_SPRE	=	4
	};
	enum class CalibrationOutput : std::uint32_t
	{
		Hz_512	=	0,
		Hz_1	=	1,
	};
	enum class OutputSelection : std::uint32_t
	{
		Disabled	=	0b00,
		AlarmA		=	0b01,
		Wakeup		=	0b11
	};
	enum class OutputPolarity : bool
	{
		Normal		=	false,
		Inverted	=	true,
	};
	enum class TimestampEvent : bool
	{
		RisingEdge 	= 	false,
		FallingEdge = 	true,
	};
	
	template <typename T, unsigned N>
	T BCDToBinary(std::array<T, N> const & pInput)
	{
		static_assert(N > (T)0, "Cannot convert an empty array to a binary number.");
		
		T output{ 0 };
		auto i = N;
		while ( (i--) > (T)0 )
		{
			output *= (T)10;
			output += (T)pInput[i];
		}
		return output;
	}
	template <typename ... T>
	auto BCDToBinary(T && ... pInput)
	{
		constexpr auto N = sizeof...(T);
		using first = std::decay_t<typename General::TypeList<T...>::head>;
		return BCDToBinary<first, N>(std::array<first, N>({{std::forward<T>(pInput)...}}));
	}
	
	template <std::size_t A>
	struct TR : General::u32_reg<A>
	{
		using base_t = General::u32_reg<A>;
		using base_t::base_t;
		using base_t::operator=;
		
		auto PM (){ return base_t::template Actual<RTC_TR_PM >(); }
		auto HT (){ return base_t::template Actual<RTC_TR_HT >(); }
		auto HU (){ return base_t::template Actual<RTC_TR_HU >(); }
		auto MNT(){ return base_t::template Actual<RTC_TR_MNT>(); }
		auto MNU(){ return base_t::template Actual<RTC_TR_MNU>(); }
		auto ST (){ return base_t::template Actual<RTC_TR_ST >(); }
		auto SU (){ return base_t::template Actual<RTC_TR_SU >(); }
		
		auto ToTime()
		{
			return General::Time
			(
				(std::int8_t)BCDToBinary( HU().Get(), 	HT().Get() 	), 
				(std::int8_t)BCDToBinary( MNU().Get(),	MNT().Get() ), 
				(std::int8_t)BCDToBinary( SU().Get(), 	ST().Get()  )
			);
		}
		
		void FromTime( General::Time const & pInput )
		{
			auto const seconds 	= (std::uint8_t)( pInput.Seconds() );
			auto const minutes 	= (std::uint8_t)( pInput.Minutes() );
			auto const hours 	= (std::uint8_t)( pInput.Hours() 	);
			
			HT() 	= hours 	%	 10u;
			HU() 	= hours 	/	 10u;
			MNT() 	= minutes 	%	 10u;
			MNU() 	= minutes 	/	 10u;
			ST() 	= seconds 	%	 10u;
			SU() 	= seconds 	/	 10u;
		}
	};
	
	template <std::size_t A>
	struct DR : General::u32_reg<A>
	{
		using base_t = General::u32_reg<A>;
		using base_t::base_t;
		using base_t::operator=;
		
		auto WDU(){ return base_t::template Actual<RTC_DR_WDU>(); }
		auto YT (){ return base_t::template Actual<RTC_DR_YT >(); }
		auto YU (){ return base_t::template Actual<RTC_DR_YU >(); }
		auto MT (){ return base_t::template Actual<RTC_DR_MT >(); }
		auto MU (){ return base_t::template Actual<RTC_DR_MU >(); }
		auto DT (){ return base_t::template Actual<RTC_DR_DT >(); }
		auto DU (){ return base_t::template Actual<RTC_DR_DU >(); }
		
		auto ToDate()
		{
			return General::Date
			(
				(std::int8_t)BCDToBinary( YT().Get(), YU().Get() ), 
				(std::int8_t)BCDToBinary( MT().Get(), MU().Get() ), 
				(std::int8_t)BCDToBinary( DT().Get(), DU().Get() )
			);
		}
		void FromDate(General::Date const & pInput)
		{
			std::uint8_t const year = pInput.Year();
			std::uint8_t const month = pInput.Month();
			std::uint8_t const day = pInput.Day();
			
			YT() 	= year 		%	 10u;
			YU() 	= year 		/	 10u;
			MT() 	= month 	%	 10u;
			MU() 	= month 	/	 10u;
			DT() 	= day 		%	 10u;
			DU() 	= day 		/	 10u;
		}
		
		auto & operator = (General::Date const & pInput)
		{
			FromDate(pInput);
			return *this;
		}
		operator General::Date()
		{
			return ToDate();
		}
	};
	
	template <std::size_t A>
	struct CR : General::u32_reg<A>
	{
		using base_t = General::u32_reg<A>;
		using base_t::base_t;
		
		auto COE    () { return base_t::template Actual<RTC_CR_COE    >(); }
		auto OSEL   () { return base_t::template Actual<RTC_CR_OSEL   >(); }
		auto POL    () { return base_t::template Actual<RTC_CR_POL    >(); }
		auto COSEL  () { return base_t::template Actual<RTC_CR_COSEL  >(); }
		auto BKP    () { return base_t::template Actual<RTC_CR_BKP    >(); }
		auto SUB1H  () { return base_t::template Actual<RTC_CR_SUB1H  >(); }
		auto ADD1H  () { return base_t::template Actual<RTC_CR_ADD1H  >(); }
		auto TSIE   () { return base_t::template Actual<RTC_CR_TSIE   >(); }
		auto WUTIE  () { return base_t::template Actual<RTC_CR_WUTIE  >(); }
		auto ALRAIE () { return base_t::template Actual<RTC_CR_ALRAIE >(); }
		auto TSE    () { return base_t::template Actual<RTC_CR_TSE    >(); }
		auto WUTE   () { return base_t::template Actual<RTC_CR_WUTE   >(); }
		auto ALRAE  () { return base_t::template Actual<RTC_CR_ALRAE  >(); }
		auto FMT    () { return base_t::template Actual<RTC_CR_FMT    >(); }
		auto BYPSHAD() { return base_t::template Actual<RTC_CR_BYPSHAD>(); }
		auto REFCKON() { return base_t::template Actual<RTC_CR_REFCKON>(); }
		auto TSEDGE () { return base_t::template Actual<RTC_CR_TSEDGE >(); }
		auto WUCKSEL() { return base_t::template Actual<RTC_CR_WUCKSEL>(); }
		
		void SetupCalibration( bool const pOutputEnabled, CalibrationOutput const pCalOut)
		{
			COE() = pOutputEnabled;
			COSEL() = (std::uint32_t)pCalOut;
		}
		void SetupOutput( OutputSelection const pOutputSelection, OutputPolarity const pPolarity )
		{
			OSEL() = (std::uint32_t)pOutputSelection;
			POL() = (std::uint32_t)pPolarity;
		}
		void SetupWakeup(bool const pInterrupt, WakeupClockSelection const pClock)
		{
			WUCKSEL() = (std::uint32_t)pClock;
			WUTE() = true;
			WUTIE() = pInterrupt;
		}
		void SetupTimestamp(bool const pInterrupt, TimestampEvent const pEdge)
		{
			TSIE() = pInterrupt;
			TSEDGE() = (std::uint32_t)pEdge;
			TSE() = true;
		}
		void SetupAlarm(bool const pInterrupt)
		{
			ALRAIE() = pInterrupt;
			ALRAE() = true;
		}
		void HourFormat(Notation const pInput)
		{
			FMT() = (std::uint32_t)pInput;
		}
		
		void AddHour() { ADD1H() = true; }
		void SubHour() { SUB1H() = true; }
	};
	
	template <std::size_t A>
	struct ISR : General::u32_reg<A>
	{
		using base_t = General::u32_reg<A>;
		using base_t::base_t;
		
		auto RECALPF() { return base_t::template Actual<RTC_ISR_RECALPF>(); }
		auto TAMP3F () { return base_t::template Actual<RTC_ISR_TAMP3F >(); }
		auto TAMP2F () { return base_t::template Actual<RTC_ISR_TAMP2F >(); }
		auto TAMP1F () { return base_t::template Actual<RTC_ISR_TAMP1F >(); }
		auto TSOVF  () { return base_t::template Actual<RTC_ISR_TSOVF  >(); }
		auto TSF    () { return base_t::template Actual<RTC_ISR_TSF    >(); }
		auto WUTF   () { return base_t::template Actual<RTC_ISR_WUTF   >(); }
		auto ALRAF  () { return base_t::template Actual<RTC_ISR_ALRAF  >(); }
		auto INIT   () { return base_t::template Actual<RTC_ISR_INIT   >(); }
		auto INITF  () { return base_t::template Actual<RTC_ISR_INITF  >(); }
		auto RSF    () { return base_t::template Actual<RTC_ISR_RSF    >(); }
		auto INITS  () { return base_t::template Actual<RTC_ISR_INITS  >(); }
		auto SHPF   () { return base_t::template Actual<RTC_ISR_SHPF   >(); }
		auto WUTWF  () { return base_t::template Actual<RTC_ISR_WUTWF  >(); }
		auto ALRAWF () { return base_t::template Actual<RTC_ISR_ALRAWF >(); }
	};
	
	template <std::size_t A>
	struct PRER : General::u32_reg<A>
	{
		using base_t = General::u32_reg<A>;
		using base_t::base_t;
		
		auto PREDIV_A() {return base_t::template Actual<RTC_PRER_PREDIV_A>();}
		auto PREDIV_S() {return base_t::template Actual<RTC_PRER_PREDIV_S>();}
		
		void SetupPrescale(std::uint32_t const pInput)
		{
			//Prefer A
			//127 is A's maximum value
			std::uint32_t Al = 128u, S = 0u;
			
			while (Al > 0)
			{
				auto input = pInput;
				if ((input % Al) == 0)
				{
					input /= Al;
					S = input;
					break;
				}
				--Al;
			}
			
			PREDIV_A() = Al - 1u;
			PREDIV_S() = S - 1u;
		}
	};
	
	template <std::size_t A>
	struct WUTR : General::u32_reg<A>
	{
		using base_t = General::u32_reg<A>;
		using base_t::base_t;
		
		auto WUT() { return base_t::template Actual<RTC_WUTR_WUT>(); }
	};
	
	template <std::size_t A>
	struct ALRMAR : General::u32_reg<A>
	{
		using base_t = General::u32_reg<A>;
		using base_t::base_t;
		
		auto MSK4 () { return base_t::template Actual<RTC_ALRMAR_MSK4>(); }
		auto WDSEL() { return base_t::template Actual<RTC_ALRMAR_WDSEL>(); }
		auto DT   () { return base_t::template Actual<RTC_ALRMAR_DT>(); }
		auto DU   () { return base_t::template Actual<RTC_ALRMAR_DU>(); }
		auto MSK3 () { return base_t::template Actual<RTC_ALRMAR_MSK3>(); }
		auto PM   () { return base_t::template Actual<RTC_ALRMAR_PM>(); }
		auto HT   () { return base_t::template Actual<RTC_ALRMAR_HT>(); }
		auto HU   () { return base_t::template Actual<RTC_ALRMAR_HU>(); }
		auto MSK2 () { return base_t::template Actual<RTC_ALRMAR_MSK2>(); }
		auto MNT  () { return base_t::template Actual<RTC_ALRMAR_MNT>(); }
		auto MNU  () { return base_t::template Actual<RTC_ALRMAR_MNU>(); }
		auto MSK1 () { return base_t::template Actual<RTC_ALRMAR_MSK1>(); }
		auto ST   () { return base_t::template Actual<RTC_ALRMAR_ST>(); }
		auto SU   () { return base_t::template Actual<RTC_ALRMAR_SU>(); }
		
		void FromTime( General::Time const & pInput )
		{
			auto const seconds 	= (std::uint8_t)( pInput.Seconds() );
			auto const minutes 	= (std::uint8_t)( pInput.Minutes() );
			auto const hours 	= (std::uint8_t)( pInput.Hours() 	);
			
			HT() 	= hours 	%	 10u;
			HU() 	= hours 	/	 10u;
			MNT() 	= minutes 	%	 10u;
			MNU() 	= minutes 	/	 10u;
			ST() 	= seconds 	%	 10u;
			SU() 	= seconds 	/	 10u;
		}

		void FromWeekday(General::Weekday const & pInput)
		{
			if (pInput == General::Weekday::Today)
				MSK4() = true; // Don't care about date/day match
			else
				DU() = (std::uint32_t)(pInput);
		}
        
		// Note month and year is completely ignored
		void SetupAlarm(General::Weekday const & pDay, General::Time const & pTime)
		{ 
			WDSEL() = true;		// DU represents week day not date.
			FromWeekday(pDay);
			FromTime(pTime);
		}
		
	};
	
	template <std::size_t A>
	struct WPR : General::u32_reg<A>
	{
		using base_t = General::u32_reg<A>;
		using base_t::base_t;
		using base_t::operator=;

		void Unlock()
		{
			*this = 0xCA;
			*this = 0x53;
		}

		void Lock()
		{
			*this = 0xFE;
			*this = 0x64;
		}
	};
	
	template <std::size_t A>
	struct SSR : General::u32_reg<A>
	{
		using base_t = General::u32_reg<A>;
		using base_t::base_t;
		
		auto SS() { return base_t::template Actual<RTC_SSR_SS>(); }
	};
	
	template <std::size_t A>
	struct SHIFTR : General::u32_reg<A>
	{
		using base_t = General::u32_reg<A>;
		using base_t::base_t;
		
		auto ADD1S() { return base_t::template Actual<RTC_SHIFTR_ADD1S>(); }
		auto SUBFS() { return base_t::template Actual<RTC_SHIFTR_SUBFS>(); }
		
		void AddSecond()
		{
			ADD1S() = true;
		}
	};
	
	template <std::size_t A>
	struct TSTR : General::u32_reg<A>
	{
		using base_t = General::u32_reg<A>;
		using base_t::base_t;
		
		auto PM () { return base_t::template Actual<RTC_TSTR_PM >(); }
		auto HT () { return base_t::template Actual<RTC_TSTR_HT >(); }
		auto HU () { return base_t::template Actual<RTC_TSTR_HU >(); }
		auto MNT() { return base_t::template Actual<RTC_TSTR_MNT>(); }
		auto MNU() { return base_t::template Actual<RTC_TSTR_MNU>(); }
		auto ST () { return base_t::template Actual<RTC_TSTR_ST >(); }
		auto SU () { return base_t::template Actual<RTC_TSTR_SU >(); }
		
		auto ToTime()
		{
			return General::Time
			(
				(std::int8_t)BCDToBinary( (std::int8_t)HT().Get(),	(std::int8_t)HU().Get()  ) + (( PM() == true ) ? 12u : 0u), 
				(std::int8_t)BCDToBinary( (std::int8_t)MNT().Get(), (std::int8_t)MNU().Get() ), 
				(std::int8_t)BCDToBinary( (std::int8_t)ST().Get(),	(std::int8_t)SU().Get()  )
			);
		}

		operator General::Time()
		{
			return ToTime();
		}
	};
	
	template <std::size_t A>
	struct TSDR : General::u32_reg<A>
	{
		using base_t = General::u32_reg<A>;
		using base_t::base_t;
		
		auto WDU() { return base_t::template Actual<RTC_TSDR_WDU>(); }
		auto MT () { return base_t::template Actual<RTC_TSDR_MT >(); }
		auto MU () { return base_t::template Actual<RTC_TSDR_MU >(); }
		auto DT () { return base_t::template Actual<RTC_TSDR_DT >(); }
		auto DU () { return base_t::template Actual<RTC_TSDR_DU >(); }
		

		auto ToDate()
		{
			return General::Date
			(
				(std::int8_t)BCDToBinary( 0, 0, 0 ),
				(std::int8_t)BCDToBinary( MT().Get(), 	MU().Get() ), 
				(std::int8_t)BCDToBinary( DT().Get(),	DU().Get()  )
			);
		}
		operator General::Date()
		{
			return ToDate();
		}
	};
	
	template <std::size_t A>
	struct TSSSR : General::u32_reg<A>
	{
		using base_t = General::u32_reg<A>;
		using base_t::base_t;
		
		auto SS() { return base_t::template Actual<RTC_TSSSR_SS>(); }
	};
	
	template <std::size_t A>
	struct CALR : General::u32_reg<A>
	{
		using base_t = General::u32_reg<A>;
		using base_t::base_t;
		
		auto CALP  () { return base_t::template Actual<RTC_CALR_CALP  >(); }
		auto CALW8 () { return base_t::template Actual<RTC_CALR_CALW8 >(); }
		auto CALW16() { return base_t::template Actual<RTC_CALR_CALW16>(); }
		auto CALM  () { return base_t::template Actual<RTC_CALR_CALM  >(); }
	};
	
	template <std::size_t A>
	struct TAFCR : General::u32_reg<A>
	{
		using base_t = General::u32_reg<A>;
		using base_t::base_t;
		
		auto PC15VALUE() { return base_t::template Actual<RTC_TAFCR_PC15VALUE>(); }
		auto PC15MODE () { return base_t::template Actual<RTC_TAFCR_PC15MODE >(); }
		auto PC14VALUE() { return base_t::template Actual<RTC_TAFCR_PC14VALUE>(); }
		auto PC14MODE () { return base_t::template Actual<RTC_TAFCR_PC14MODE >(); }
		auto PC13VALUE() { return base_t::template Actual<RTC_TAFCR_PC13VALUE>(); }
		auto PC13MODE () { return base_t::template Actual<RTC_TAFCR_PC13MODE >(); }
		auto TAMPPUDIS() { return base_t::template Actual<RTC_TAFCR_TAMPPUDIS>(); }
		auto TAMPPRCH () { return base_t::template Actual<RTC_TAFCR_TAMPPRCH >(); }
		auto TAMPFLT  () { return base_t::template Actual<RTC_TAFCR_TAMPFLT  >(); }
		auto TAMPFREQ () { return base_t::template Actual<RTC_TAFCR_TAMPFREQ >(); }
		auto TAMPTS   () { return base_t::template Actual<RTC_TAFCR_TAMPTS   >(); }
		auto TAMP3TRG () { return base_t::template Actual<RTC_TAFCR_TAMP3EDGE>(); }
		auto TAMP2TRG () { return base_t::template Actual<RTC_TAFCR_TAMP2EDGE>(); }
		auto TAMP1TRG () { return base_t::template Actual<RTC_TAFCR_TAMP1TRG >(); }
		auto TAMP3E   () { return base_t::template Actual<RTC_TAFCR_TAMP3E   >(); }
		auto TAMP2E   () { return base_t::template Actual<RTC_TAFCR_TAMP2E   >(); }
		auto TAMP1E   () { return base_t::template Actual<RTC_TAFCR_TAMP1E   >(); }
		auto TAMPIE   () { return base_t::template Actual<RTC_TAFCR_TAMPIE   >(); }
	};
	
	template <std::size_t A>
	struct ALRMASSR : General::u32_reg<A>
	{
		using base_t = General::u32_reg<A>;
		using base_t::base_t;
		
		auto SS() 		{ return base_t::template Actual<RTC_ALRMASSR_SS>(); 	}
		auto MASKSS() 	{ return base_t::template Actual<RTC_ALRMASSR_MASKSS>(); }
	};
	
	template <std::size_t A>
	struct BKP0R : General::u32_reg<A>
	{
		using base_t = General::u32_reg<A>;
		using base_t::base_t;
	};
	
	template <std::size_t A>
	struct BKP4R : General::u32_reg<A>
	{
		using base_t = General::u32_reg<A>;
		using base_t::base_t;
	};
	
	TR() 		-> TR		< RTC_BASE + offsetof(RTC_TypeDef, TR)		>;
	DR() 		-> DR		< RTC_BASE + offsetof(RTC_TypeDef, DR)		>;
	CR() 		-> CR		< RTC_BASE + offsetof(RTC_TypeDef, CR)		>;
	ISR() 		-> ISR		< RTC_BASE + offsetof(RTC_TypeDef, ISR)		>;
	PRER() 		-> PRER		< RTC_BASE + offsetof(RTC_TypeDef, PRER)	>;
	WUTR() 		-> WUTR		< RTC_BASE + offsetof(RTC_TypeDef, WUTR)	>;
	ALRMAR() 	-> ALRMAR	< RTC_BASE + offsetof(RTC_TypeDef, ALRMAR)	>;
	WPR() 		-> WPR		< RTC_BASE + offsetof(RTC_TypeDef, WPR)		>;
	SSR() 		-> SSR		< RTC_BASE + offsetof(RTC_TypeDef, SSR)		>;
	SHIFTR() 	-> SHIFTR	< RTC_BASE + offsetof(RTC_TypeDef, SHIFTR)	>;
	TSTR() 		-> TSTR		< RTC_BASE + offsetof(RTC_TypeDef, TSTR)	>;
	TSDR() 		-> TSDR		< RTC_BASE + offsetof(RTC_TypeDef, TSDR)	>;
	TSSSR() 	-> TSSSR	< RTC_BASE + offsetof(RTC_TypeDef, TSSSR)	>;
	CALR() 		-> CALR		< RTC_BASE + offsetof(RTC_TypeDef, CALR)	>;
	TAFCR() 	-> TAFCR	< RTC_BASE + offsetof(RTC_TypeDef, TAFCR)	>;
	ALRMASSR() 	-> ALRMASSR	< RTC_BASE + offsetof(RTC_TypeDef, ALRMASSR)>;
	BKP0R() 	-> BKP0R	< RTC_BASE + offsetof(RTC_TypeDef, BKP0R)	>;
	BKP4R() 	-> BKP4R	< RTC_BASE + offsetof(RTC_TypeDef, BKP4R) 	>;
	

	struct ScopedAccess
	{
		static volatile inline int RefCount = 0;
        //
		ScopedAccess()
		{
			if ( not RefCount++ )
			{
				WPR{}.Unlock();
			}
		}
		~ScopedAccess()
		{
			if ( not --RefCount )
			{
				WPR{}.Lock();
			}
		}
	};
	
	struct ScopedInit
	{
		static volatile inline int RefCount = 0;
        //
		ScopedInit() noexcept
		{
            if ( not RefCount++ )
            {
                if ( !ISR{}.INITF() )
                {
                    //Enter INIT mode if nessary.
                    ISR{}.INIT() = true;
                    while (!ISR{}.INITF());
                }
            }
		}
		~ScopedInit()
		{
            if ( not --RefCount )
            {
			    ISR{}.INIT() = false;
            }
		}
	};
	
	class ScopedControl : ScopedAccess, ScopedInit{};
	
	void ResetTimeAndDate() noexcept
	{
		ScopedControl control;
 		TR{} = {};
		DR{} = {};
	}
    
	void Pause() noexcept
	{
		ScopedAccess m_Access;
		ISR{}.INIT() = true;
	}
	void Resume() noexcept
	{
		ScopedAccess m_Access;
		ISR{}.INIT() = false;
	}
	auto Time() noexcept
	{
		return TR{}.ToTime();
	}
	auto Date() noexcept
	{
		return DR{}.ToDate();
	}
}