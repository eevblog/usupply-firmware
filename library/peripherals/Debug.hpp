#pragma once
#include "RegistersDebug.hpp"

namespace Peripherals
{
	/**
	 * @brief Enum for whether or not to stop a timer when a breakpoint is encountered.
	 */
	enum class TimerDebugMode : bool
	{
		Continue = false,
		Stop = true
	};
	/**
	 * @brief Configure a timer channal to pause or not when a breakpoint is encountered.
	 * 
	 * @tparam Channel The channel to configure.
	 * @param input The timer mode to configure the channel.
	 */
	template <unsigned Channel>
	void SetTimerDebugMode(TimerDebugMode input)
	{
		using namespace Peripherals::DebugGeneral;
		if constexpr (IsTimerAPB1<Channel>)
		{
			switch (Channel)
			{
			case 14: 	APB1FZ{}.TIM14_STOP()	= (bool)input; 
			case 7: 	APB1FZ{}.TIM7_STOP() 	= (bool)input;  
			case 6: 	APB1FZ{}.TIM6_STOP() 	= (bool)input;  
			case 3: 	APB1FZ{}.TIM3_STOP() 	= (bool)input;  
			case 2: 	APB1FZ{}.TIM2_STOP() 	= (bool)input;	
			}; 
		}
		if constexpr (IsTimerAPB2<Channel>)
		{
			switch (Channel)
			{
			case 17: 	APB2FZ{}.TIM17_STOP()= (bool)input; 
			case 16: 	APB2FZ{}.TIM16_STOP()= (bool)input;  
			case 15: 	APB2FZ{}.TIM15_STOP()= (bool)input;  
			case 1: 	APB2FZ{}.TIM1_STOP ()= (bool)input;  

			};
		}
	}
}