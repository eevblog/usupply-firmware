#pragma once
#include "RCC.hpp"
#include "Interrupt.hpp"
#include "RegistersSysTick.hpp"
#include "Meta.hpp"
#include "StaticLambdaWrapper.hpp"
#include <atomic>
#include <mutex>

/**
 * Defined as per std::byte
 */
enum Seconds : uint64_t{};
enum Milliseconds : uint64_t {};
enum Microseconds : uint64_t {};

namespace Peripherals
{
    /**
     * @brief SysTick manager classes can use a callback or not,
     * this enum is used to determine the implementation.
     */
    enum class SysTickType
    {
        None,
        Callback,   ///< Manage a tick counter and run callback.
        Ticker      ///< Only manage a Tick counter.
    };
    /**
     * @brief Metaprogramming tool.
     */
    struct NoInterrupt
    {
        NoInterrupt() = default;
        NoInterrupt(NoInterrupt &&) = default;
        NoInterrupt(NoInterrupt const &) = default;
        NoInterrupt & operator=(NoInterrupt &&) = default;
        NoInterrupt & operator=(NoInterrupt const &) = default;

        template <typename ... Args>
        NoInterrupt(Args && ...) noexcept {}

        template <typename ... Args>
        static void Run(Args && ...) noexcept {}
    };
    //
    template <typename T>
    bool constexpr IsNoInterrupt_v = false;
    template <>
    bool constexpr IsNoInterrupt_v<NoInterrupt> = true;
    /**
     * @brief A peripheral manager class.
     * 
     * @tparam F The type of the callback to run for systick.
     * @tparam Type The type of the system tick manager.
     */
    template <typename F, SysTickType Type>
    class SysTickModule;
    /**
     * @brief This is the common class for all systick instantiations.
     * 
     */
    class SysTickCommon
    {
    private:
        inline volatile static std::uint64_t m_Count = 0;
    public:
        /**
         * @brief Setup sys tick with a requested frequency.
         */
        SysTickCommon( uint64_t frequency ) noexcept
        {
            using namespace SysTickGeneral;
            //
            // Setup the reload registers
            RELOAD{} = (uint32_t)(::System::SystemClock / frequency - 1ull);
            //
            // System tick counter reset
            CURRENT{}.Clear();
            //
            // Setup the system control and status registers
            CTRL{}.TICKINT()    =   true;  // interrupts, always on to avoid need to handle wrapping
            CTRL{}.CLKSOURCE()  =   true;  // timer use processor clock
            CTRL{}.ENABLE()     =   true;  // enables the SysTick clock
        }
        /**
         * @brief Incremenets the systick counter.
         * 
         * @return The current system tick.
         */
        static uint64_t Inc() noexcept
        {
            return ++m_Count;
        }
        /**
         * @brief Note, as this is 64 bit CURRENT conversion to 64 bit
         * takes more than a single instruction and therefore
         * can be intercepted.
         *
         * If using the Tick function interrupts should not be used.
         * 
         * @return The current system tick.
         */
        static std::uint64_t Tick() noexcept
        {
            return m_Count;
        }
        /**
         * @brief This waits "ticks" number of ticks.
         * Ticks increment at the frequency defined by the constructor.
         */
        static void Wait( uint64_t ticks ) noexcept
        {
            volatile const uint64_t end{ Tick() + ticks };
            while( Tick() < end );
        }
        /**
         * @brief The number of milliseconds to wait.
         */
        static void Wait( Milliseconds ms ) noexcept
        {
            auto count = ( General::UnderlyingValue( ms ) * ::System::SystemClock) / 1000u;
            Wait( count );
        }
        /**
         * @brief The number of microseconds to wait.
         */
        static void Wait( Microseconds us ) noexcept
        {
            auto count = (General::UnderlyingValue( us ) * ::System::SystemClock) / 1000000u;
            Wait( count );
        }
    };
    /**
     * @brief A systick manager class that runs a callback each tick interrupt.
     * 
     * @tparam F The callback type.
     */
    template <typename F>
    class SysTickModule<F, SysTickType::Callback> : public SysTickCommon
    {
    private:
		using interrupt_t 	= System::Interrupt<SysTickModule, System::InterruptSource::eSysTick>;
		using callback_t 	= General::StaticLambdaWrapper<F>;
        /**
         * @brief The interrupt handler object, the construction of this object connects the interrupt event
         * to the class Interrupt function. 
         * 
         * @todo The increment and reads are not atomic (64 bits) this means data corruption is possible.
         */
        interrupt_t m_ISR;
        /**
         * @brief The callback to run each tick event.
         */
        callback_t m_Callback;
    public:
        /**
         * @brief Setup the systick to run at a particular frequency.
         * 
         * @param frequency The frequency of the systick peripheral.
         * @param callback The callback to run.
         */
        SysTickModule( std::uint64_t frequency, F && callback ) noexcept:
            SysTickCommon{ frequency },
            m_Callback{ std::move( callback ) }
        {}
        /**
         * @brief This is the interrupt callback for systick.
         */
        static void Interrupt() noexcept
        {
            callback_t::Run( SysTickCommon::Inc() );
        }
    };
    /**
     * @brief A systick manager class that just managers a uint64 counter.
     */
    template <>
    class SysTickModule<NoInterrupt, SysTickType::Ticker> : public SysTickCommon
    {
    private:
        using interrupt_t = System::Interrupt<SysTickModule, System::InterruptSource::eSysTick>;
        /**
         * @todo The increment and reads are not atomic (64 bits) this means data corruption is possible.
         */
        interrupt_t m_ISR;
    public:
        /**
         * @brief The frequency to initalise the system tick.
         */
        SysTickModule( std::uint64_t frequency ) noexcept :
            SysTickCommon{ frequency }
        {}
        /**
         * @brief The systick interrupt handler.
         */
        static void Interrupt() noexcept
        {
            SysTickCommon::Inc();
        }
    };
    /**
     * 
     */
    template <typename F>
    SysTickModule(std::uint64_t, F) -> SysTickModule<F, SysTickType::Callback>;
    SysTickModule(std::uint64_t) -> SysTickModule<NoInterrupt, SysTickType::Ticker>;
}