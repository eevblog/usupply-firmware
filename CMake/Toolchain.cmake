message("Toolchain.cmake running.")

# Target operating system
message("Setting : CMAKE_SYSTEM_NAME/VERSION/PROCESSOR")

set(TARGET STM32F072)
add_compile_definitions( ${TARGET} )
add_compile_definitions( USE_STDPERIPH_DRIVER HSE_VALUE=48000000 )

set(STM32_FAMILY 			STM32F07x)
set(CMAKE_SYSTEM_NAME		Generic)
set(CMAKE_SYSTEM_VERSION	1)
set(CMAKE_SYSTEM_PROCESSOR	arm-eabi)

# Compilers to use for ASM, C and C++
message("Setting : Toolchain")
set(CMAKE_C_COMPILER		arm-none-eabi-gcc)
set(CMAKE_CXX_COMPILER		arm-none-eabi-g++)
set(CMAKE_ASM_COMPILER		arm-none-eabi-as)
set(CMAKE_OBJCOPY			arm-none-eabi-objcopy)
set(CMAKE_OBJDUMP			arm-none-eabi-objdump)

#Setup shared compiler flags, these are used later
set(ARCH			armv6-m)
set(CORE			cortex-m0)
set(ARM_ASM			mthumb)
set(MSG_LEN			0)
set(LTO			    "")

#Setup compiler and linker flags
message("${CMAKE_BUILD_TYPE}")
if ((CMAKE_BUILD_TYPE STREQUAL "Debug") OR (CMAKE_BUILD_TYPE STREQUAL "RelWithDebInfo"))
    message("Debug mode")
    if (CMAKE_BUILD_TYPE STREQUAL "RelWithDebInfo")
        set(OPTIMISATION Os)
        set(DEBUG "-g2 -ggdb")
    else()
        set(OPTIMISATION Og)
        set(DEBUG "-g3 -ggdb")
    endif()
    set(COMMON_FLAGS    "-march=${ARCH} -mcpu=${CORE} -D${TARGET} -${OPTIMISATION} -${ARM_ASM} ${DEBUG} -Wall -Wl,--gc-sections -fmessage-length=${MSG_LEN} -ffunction-sections -fdata-sections -ffreestanding -fno-builtin")
else()
    message("Release mode")
    set(OPTIMISATION	O3)
    set(DEBUG "")
    # set(LTO "-flto -fdevirtualize-speculatively") # This lowers stack usage substantially during execution of menu
    set(COMMON_FLAGS    "-march=${ARCH} -mcpu=${CORE} -D${TARGET} -${OPTIMISATION} -${ARM_ASM} ${DEBUG} -Wall -fomit-frame-pointer -Wl,--gc-sections -Wl,--strip-all -fmessage-length=${MSG_LEN} -ffunction-sections -fdata-sections -ffreestanding -fno-builtin")
endif()

# 
set(CMAKE_ASM_FLAGS	        "${CMAKE_ASM_FLAGS} ${COMMON_FLAGS}" CACHE INTERNAL "asm compiler flags")
set(CMAKE_C_FLAGS           "${COMMON_FLAGS} -std=gnu11" CACHE INTERNAL "c compiler flags")
set(CMAKE_CXX_FLAGS	        "${COMMON_FLAGS} -std=c++1z -fno-rtti -fno-exceptions -fno-use-cxa-atexit -fno-threadsafe-statics -ftemplate-backtrace-limit=0" CACHE INTERNAL "cpp compiler flags")
set(CMAKE_EXE_LINKER_FLAGS  "${COMMON_FLAGS} ${LTO} -nostartfiles -Wl,-Map,\"${TARGET}.map\" --specs=nano.specs" CACHE INTERNAL "exe link flags")

set(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM BOTH)
set(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
set(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)