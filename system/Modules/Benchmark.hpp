#pragma once

#include "SysTick.hpp"
#include "Types.hpp"
#include <utility>

namespace System
{
    template <typename F, typename ... Args>
    volatile std::uint64_t Benchmark(F && function, Args && ... args)
    {
        using namespace Peripherals;
        using systick = SysTickModule<NoInterrupt, SysTickType::Ticker>;

        // Measure the time that the measurement takes
        volatile auto cal0 	= systick::Tick();
        volatile auto cal 	= systick::Tick() - cal0;

        // Measure the time that the flash save takes
        volatile auto start = systick::Tick();

        // Call function with arguments
        std::forward<F>(function)(std::forward<Args>(args)...);

		//
        volatile auto end 	= systick::Tick();

        // Time to output
        return end - start - cal;
    }

    inline float Seconds(std::uint64_t value)
    {
        return float(value) / float(SystemClock);
    }
}