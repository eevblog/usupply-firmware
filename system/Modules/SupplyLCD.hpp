#pragma once

//LCD types and format aliases
#include "LDO.hpp"
#include "LCD.hpp"
#include "FIFO.hpp"
#include "Bargraph.hpp"
#include "LockState.hpp"
#include "ControlMode.hpp"
#include "DisplayMode.hpp"
#include "SegmentDigits.hpp"
#include "MultipurposeUnits.hpp"

//Global supply related data (settings and measurements are singleton)
#include "Measurement.hpp"
#include "SettingsSingleton.hpp"
#include "PinDefinitions.hpp"

namespace System
{
	using BuzzerTone = LCD::BuzzerTone;

	enum ScheduledTone
	{
		High,
		Low
	};

	template <typename SPI_Kernal>
	class SupplyLCD
	{
	protected:
		using MultipurposeUnit = LCD::Block::MultipurposeUnit;
		using LCD_t = LCD::LCD<SPI_Kernal>;
		//
		LCD_t 								m_LCD{};
		volatile bool						m_Change = true;
		Containers::FIFO<ScheduledTone, 12>	m_Tones{};
		//
		using SEG_1A  		= LCD::LCD_Segment<0, 8>;
		using SEG_1B  		= LCD::LCD_Segment<1, 8>;
		using SEG_1F  		= LCD::LCD_Segment<2, 8>;
		using SEG_1G  		= LCD::LCD_Segment<3, 8>;
		using SEG_1C  		= LCD::LCD_Segment<4, 8>;
		using SEG_1E  		= LCD::LCD_Segment<5, 8>;
		using SEG_1D  		= LCD::LCD_Segment<6, 8>;
		using SEG_1DP 		= LCD::LCD_Segment<7, 8>;
		using SEG_2A  		= LCD::LCD_Segment<0, 7>;
		using SEG_2B  		= LCD::LCD_Segment<1, 7>;
		using SEG_2F  		= LCD::LCD_Segment<2, 7>;
		using SEG_2G  		= LCD::LCD_Segment<3, 7>;
		using SEG_2C  		= LCD::LCD_Segment<4, 7>;
		using SEG_2E  		= LCD::LCD_Segment<5, 7>;
		using SEG_2D  		= LCD::LCD_Segment<6, 7>;
		using SEG_2DP 		= LCD::LCD_Segment<7, 7>;
		using SEG_3A  		= LCD::LCD_Segment<0, 6>;
		using SEG_3B  		= LCD::LCD_Segment<1, 6>;
		using SEG_3F  		= LCD::LCD_Segment<2, 6>;
		using SEG_3G  		= LCD::LCD_Segment<3, 6>;
		using SEG_3C  		= LCD::LCD_Segment<4, 6>;
		using SEG_3E  		= LCD::LCD_Segment<5, 6>;
		using SEG_3D  		= LCD::LCD_Segment<6, 6>;
		using SEG_3DP 		= LCD::LCD_Segment<7, 6>;
		using SEG_4A 		= LCD::LCD_Segment<0, 5>;
		using SEG_4B 		= LCD::LCD_Segment<1, 5>;
		using SEG_4F 		= LCD::LCD_Segment<2, 5>;
		using SEG_4G 		= LCD::LCD_Segment<3, 5>;
		using SEG_4C 		= LCD::LCD_Segment<4, 5>;
		using SEG_4E 		= LCD::LCD_Segment<5, 5>;
		using SEG_4D 		= LCD::LCD_Segment<6, 5>;
		using SEG_5DP 		= LCD::LCD_Segment<0, 1>;
		using SEG_5D  		= LCD::LCD_Segment<1, 1>;
		using SEG_5E  		= LCD::LCD_Segment<2, 1>;
		using SEG_5C  		= LCD::LCD_Segment<3, 1>;
		using SEG_5G  		= LCD::LCD_Segment<4, 1>;
		using SEG_5F  		= LCD::LCD_Segment<5, 1>;
		using SEG_5B  		= LCD::LCD_Segment<6, 1>;
		using SEG_5A  		= LCD::LCD_Segment<7, 1>;
		using SEG_6DP 		= LCD::LCD_Segment<0, 2>;
		using SEG_6D  		= LCD::LCD_Segment<1, 2>;
		using SEG_6E  		= LCD::LCD_Segment<2, 2>;
		using SEG_6C  		= LCD::LCD_Segment<3, 2>;
		using SEG_6G  		= LCD::LCD_Segment<4, 2>;
		using SEG_6F  		= LCD::LCD_Segment<5, 2>;
		using SEG_6B  		= LCD::LCD_Segment<6, 2>;
		using SEG_6A  		= LCD::LCD_Segment<7, 2>;
		using SEG_7DP 		= LCD::LCD_Segment<0, 3>;
		using SEG_7D  		= LCD::LCD_Segment<1, 3>;
		using SEG_7E  		= LCD::LCD_Segment<2, 3>;
		using SEG_7C  		= LCD::LCD_Segment<3, 3>;
		using SEG_7G  		= LCD::LCD_Segment<4, 3>;
		using SEG_7F  		= LCD::LCD_Segment<5, 3>;
		using SEG_7B  		= LCD::LCD_Segment<6, 3>;
		using SEG_7A  		= LCD::LCD_Segment<7, 3>;
		using SEG_8D 		= LCD::LCD_Segment<1, 4>;
		using SEG_8E 		= LCD::LCD_Segment<2, 4>;
		using SEG_8C 		= LCD::LCD_Segment<3, 4>;
		using SEG_8G 		= LCD::LCD_Segment<4, 4>;
		using SEG_8F 		= LCD::LCD_Segment<5, 4>;
		using SEG_8B 		= LCD::LCD_Segment<6, 4>;
		using SEG_8A 		= LCD::LCD_Segment<7, 4>;
		using SEG_9DP 		= LCD::LCD_Segment<0, 10>;
		using SEG_9D  		= LCD::LCD_Segment<1, 10>;
		using SEG_9E  		= LCD::LCD_Segment<2, 10>;
		using SEG_9C  		= LCD::LCD_Segment<3, 10>;
		using SEG_9G  		= LCD::LCD_Segment<4, 10>;
		using SEG_9F  		= LCD::LCD_Segment<5, 10>;
		using SEG_9B  		= LCD::LCD_Segment<6, 10>;
		using SEG_9A  		= LCD::LCD_Segment<7, 10>;
		using SEG_10DP 		= LCD::LCD_Segment<0, 11>;
		using SEG_10D  		= LCD::LCD_Segment<1, 11>;
		using SEG_10E  		= LCD::LCD_Segment<2, 11>;
		using SEG_10C  		= LCD::LCD_Segment<3, 11>;
		using SEG_10G  		= LCD::LCD_Segment<4, 11>;
		using SEG_10F  		= LCD::LCD_Segment<5, 11>;
		using SEG_10B  		= LCD::LCD_Segment<6, 11>;
		using SEG_10A  		= LCD::LCD_Segment<7, 11>;
		using SEG_11DP 		= LCD::LCD_Segment<0, 12>;
		using SEG_11D  		= LCD::LCD_Segment<1, 12>;
		using SEG_11E  		= LCD::LCD_Segment<2, 12>;
		using SEG_11C  		= LCD::LCD_Segment<3, 12>;
		using SEG_11G  		= LCD::LCD_Segment<4, 12>;
		using SEG_11F  		= LCD::LCD_Segment<5, 12>;
		using SEG_11B  		= LCD::LCD_Segment<6, 12>;
		using SEG_11A  		= LCD::LCD_Segment<7, 12>;
		using SEG_12D 		= LCD::LCD_Segment<1, 13>;
		using SEG_12E 		= LCD::LCD_Segment<2, 13>;
		using SEG_12C 		= LCD::LCD_Segment<3, 13>;
		using SEG_12G 		= LCD::LCD_Segment<4, 13>;
		using SEG_12F 		= LCD::LCD_Segment<5, 13>;
		using SEG_12B 		= LCD::LCD_Segment<6, 13>;
		using SEG_12A 		= LCD::LCD_Segment<7, 13>;
		using SEG_13DP 		= LCD::LCD_Segment<0, 14>;
		using SEG_13D  		= LCD::LCD_Segment<1, 14>;
		using SEG_13E  		= LCD::LCD_Segment<2, 14>;
		using SEG_13C  		= LCD::LCD_Segment<3, 14>;
		using SEG_13G  		= LCD::LCD_Segment<4, 14>;
		using SEG_13F  		= LCD::LCD_Segment<5, 14>;
		using SEG_13B  		= LCD::LCD_Segment<6, 14>;
		using SEG_13A  		= LCD::LCD_Segment<7, 14>;
		using SEG_14DP 		= LCD::LCD_Segment<0, 15>;
		using SEG_14D  		= LCD::LCD_Segment<1, 15>;
		using SEG_14E  		= LCD::LCD_Segment<2, 15>;
		using SEG_14C  		= LCD::LCD_Segment<3, 15>;
		using SEG_14G  		= LCD::LCD_Segment<4, 15>;
		using SEG_14F  		= LCD::LCD_Segment<5, 15>;
		using SEG_14B  		= LCD::LCD_Segment<6, 15>;
		using SEG_14A  		= LCD::LCD_Segment<7, 15>;
		using SEG_15DP 		= LCD::LCD_Segment<0, 16>;
		using SEG_15D  		= LCD::LCD_Segment<1, 16>;
		using SEG_15E  		= LCD::LCD_Segment<2, 16>;
		using SEG_15C  		= LCD::LCD_Segment<3, 16>;
		using SEG_15G  		= LCD::LCD_Segment<4, 16>;
		using SEG_15F  		= LCD::LCD_Segment<5, 16>;
		using SEG_15B  		= LCD::LCD_Segment<6, 16>;
		using SEG_15A  		= LCD::LCD_Segment<7, 16>;
		using SEG_16D 		= LCD::LCD_Segment<1, 17>;
		using SEG_16E 		= LCD::LCD_Segment<2, 17>;
		using SEG_16C 		= LCD::LCD_Segment<3, 17>;
		using SEG_16G 		= LCD::LCD_Segment<4, 17>;
		using SEG_16F 		= LCD::LCD_Segment<5, 17>;
		using SEG_16B 		= LCD::LCD_Segment<6, 17>;
		using SEG_16A 		= LCD::LCD_Segment<7, 17>;
		using SEG_17C 		= LCD::LCD_Segment<0, 31>;
		using SEG_17B 		= LCD::LCD_Segment<1, 31>;
		using SEG_18DP 		= LCD::LCD_Segment<0, 29>;
		using SEG_18D  		= LCD::LCD_Segment<0, 30>;
		using SEG_18E  		= LCD::LCD_Segment<1, 30>;
		using SEG_18C  		= LCD::LCD_Segment<2, 30>;
		using SEG_18G  		= LCD::LCD_Segment<3, 30>;
		using SEG_18F  		= LCD::LCD_Segment<4, 30>;
		using SEG_18B  		= LCD::LCD_Segment<5, 30>;
		using SEG_18A  		= LCD::LCD_Segment<6, 30>;
		using SEG_19DP 		= LCD::LCD_Segment<0, 28>;
		using SEG_19D  		= LCD::LCD_Segment<1, 28>;
		using SEG_19E  		= LCD::LCD_Segment<2, 28>;
		using SEG_19C  		= LCD::LCD_Segment<3, 28>;
		using SEG_19G  		= LCD::LCD_Segment<4, 28>;
		using SEG_19F  		= LCD::LCD_Segment<5, 28>;
		using SEG_19B  		= LCD::LCD_Segment<6, 28>;
		using SEG_19A  		= LCD::LCD_Segment<7, 28>;
		using SEG_20DP 		= LCD::LCD_Segment<0, 26>;
		using SEG_20D  		= LCD::LCD_Segment<0, 27>;
		using SEG_20E  		= LCD::LCD_Segment<1, 27>;
		using SEG_20C  		= LCD::LCD_Segment<2, 27>;
		using SEG_20G  		= LCD::LCD_Segment<3, 27>;
		using SEG_20F  		= LCD::LCD_Segment<4, 27>;
		using SEG_20B  		= LCD::LCD_Segment<5, 27>;
		using SEG_20A  		= LCD::LCD_Segment<6, 27>;
		using SEG_21DP 		= LCD::LCD_Segment<0, 25>;
		using SEG_21D  		= LCD::LCD_Segment<1, 25>;
		using SEG_21E  		= LCD::LCD_Segment<2, 25>;
		using SEG_21C  		= LCD::LCD_Segment<3, 25>;
		using SEG_21G  		= LCD::LCD_Segment<4, 25>;
		using SEG_21F  		= LCD::LCD_Segment<5, 25>;
		using SEG_21B  		= LCD::LCD_Segment<6, 25>;
		using SEG_21A  		= LCD::LCD_Segment<7, 25>;
		using SEG_22D 		= LCD::LCD_Segment<1, 24>;
		using SEG_22E 		= LCD::LCD_Segment<2, 24>;
		using SEG_22C 		= LCD::LCD_Segment<3, 24>;
		using SEG_22G 		= LCD::LCD_Segment<4, 24>;
		using SEG_22F 		= LCD::LCD_Segment<5, 24>;
		using SEG_22B 		= LCD::LCD_Segment<6, 24>;
		using SEG_22A 		= LCD::LCD_Segment<7, 24>;
		using SEG_CL1 		= LCD::LCD_Segment<1, 29>;
		using SEG_CL2 		= LCD::LCD_Segment<1, 26>;

		//Main voltage segments
		using SEG_1  		= LCD::SegmentDigit<LCD_t, SEG_1A, SEG_1B, SEG_1C, SEG_1D, SEG_1E, SEG_1F, SEG_1G, SEG_1DP>;
		using SEG_2  		= LCD::SegmentDigit<LCD_t, SEG_2A, SEG_2B, SEG_2C, SEG_2D, SEG_2E, SEG_2F, SEG_2G, SEG_2DP>;
		using SEG_3  		= LCD::SegmentDigit<LCD_t, SEG_3A, SEG_3B, SEG_3C, SEG_3D, SEG_3E, SEG_3F, SEG_3G, SEG_3DP>;
		using SEG_4  		= LCD::SegmentDigit<LCD_t, SEG_4A, SEG_4B, SEG_4C, SEG_4D, SEG_4E, SEG_4F, SEG_4G>;
		using V_SEGS 		= LCD::SegmentDigits<LCD_t, SEG_1, SEG_2, SEG_3, SEG_4>;

		//Main current segments
		using SEG_5  		= LCD::SegmentDigit<LCD_t, SEG_5A, SEG_5B, SEG_5C, SEG_5D, SEG_5E, SEG_5F, SEG_5G, SEG_5DP>;
		using SEG_6  		= LCD::SegmentDigit<LCD_t, SEG_6A, SEG_6B, SEG_6C, SEG_6D, SEG_6E, SEG_6F, SEG_6G, SEG_6DP>;
		using SEG_7  		= LCD::SegmentDigit<LCD_t, SEG_7A, SEG_7B, SEG_7C, SEG_7D, SEG_7E, SEG_7F, SEG_7G, SEG_7DP>;
		using SEG_8  		= LCD::SegmentDigit<LCD_t, SEG_8A, SEG_8B, SEG_8C, SEG_8D, SEG_8E, SEG_8F, SEG_8G>;
		using C_SEGS 		= LCD::SegmentDigits<LCD_t, SEG_5, SEG_6, SEG_7, SEG_8>;

		//Set voltage segments
		using SEG_9      	= LCD::SegmentDigit<LCD_t, SEG_9A, SEG_9B, SEG_9C, SEG_9D, SEG_9E, SEG_9F, SEG_9G, SEG_9DP>;
		using SEG_10     	= LCD::SegmentDigit<LCD_t, SEG_10A, SEG_10B, SEG_10C, SEG_10D, SEG_10E, SEG_10F, SEG_10G, SEG_10DP>;
		using SEG_11     	= LCD::SegmentDigit<LCD_t, SEG_11A, SEG_11B, SEG_11C, SEG_11D, SEG_11E, SEG_11F, SEG_11G, SEG_11DP>;
		using SEG_12     	= LCD::SegmentDigit<LCD_t, SEG_12A, SEG_12B, SEG_12C, SEG_12D, SEG_12E, SEG_12F, SEG_12G>;
		using V_SET_SEGS 	= LCD::SegmentDigits<LCD_t, SEG_9, SEG_10, SEG_11, SEG_12>;

		//Set current limit segments
		using SEG_13     	= LCD::SegmentDigit<LCD_t, SEG_13A, SEG_13B, SEG_13C, SEG_13D, SEG_13E, SEG_13F, SEG_13G, SEG_13DP>;
		using SEG_14     	= LCD::SegmentDigit<LCD_t, SEG_14A, SEG_14B, SEG_14C, SEG_14D, SEG_14E, SEG_14F, SEG_14G, SEG_14DP>;
		using SEG_15     	= LCD::SegmentDigit<LCD_t, SEG_15A, SEG_15B, SEG_15C, SEG_15D, SEG_15E, SEG_15F, SEG_15G, SEG_15DP>;
		using SEG_16     	= LCD::SegmentDigit<LCD_t, SEG_16A, SEG_16B, SEG_16C, SEG_16D, SEG_16E, SEG_16F, SEG_16G>;
		using I_SET_SEGS 	= LCD::SegmentDigits<LCD_t, SEG_13, SEG_14, SEG_15, SEG_16>;

		//Time/Energy/Power segments
		using SEG_17     	= LCD::SegmentDigit<LCD_t, SEG_17B, SEG_17C>;
		using SEG_18     	= LCD::SegmentDigit<LCD_t, SEG_18A, SEG_18B, SEG_18C, SEG_18D, SEG_18E, SEG_18F, SEG_18G, SEG_18DP, SEG_CL1>;
		using SEG_19     	= LCD::SegmentDigit<LCD_t, SEG_19A, SEG_19B, SEG_19C, SEG_19D, SEG_19E, SEG_19F, SEG_19G, SEG_19DP>;
		using SEG_20     	= LCD::SegmentDigit<LCD_t, SEG_20A, SEG_20B, SEG_20C, SEG_20D, SEG_20E, SEG_20F, SEG_20G, SEG_20DP, SEG_CL2>;
		using SEG_21     	= LCD::SegmentDigit<LCD_t, SEG_21A, SEG_21B, SEG_21C, SEG_21D, SEG_21E, SEG_21F, SEG_21G, SEG_21DP>;
		using SEG_22     	= LCD::SegmentDigit<LCD_t, SEG_22A, SEG_22B, SEG_22C, SEG_22D, SEG_22E, SEG_22F, SEG_22G>;
		using E_SET_SEGS 	= LCD::SegmentDigits<LCD_t, SEG_17, SEG_18, SEG_19, SEG_20, SEG_21, SEG_22>;
		using SEG_B13 		= LCD::Segment<LCD_t, LCD::LCD_Segment<3, 0>>;
		using SEG_B12 		= LCD::Segment<LCD_t, LCD::LCD_Segment<2, 0>>;
		using SEG_B11 		= LCD::Segment<LCD_t, LCD::LCD_Segment<1, 0>>;
		using SEG_B10 		= LCD::Segment<LCD_t, LCD::LCD_Segment<0, 22>>;
		using SEG_B9  		= LCD::Segment<LCD_t, LCD::LCD_Segment<1, 22>>;
		using SEG_B8  		= LCD::Segment<LCD_t, LCD::LCD_Segment<2, 22>>;
		using SEG_B7  		= LCD::Segment<LCD_t, LCD::LCD_Segment<3, 22>>;
		using SEG_B6  		= LCD::Segment<LCD_t, LCD::LCD_Segment<4, 22>>;
		using SEG_B5  		= LCD::Segment<LCD_t, LCD::LCD_Segment<5, 22>>;
		using SEG_B4  		= LCD::Segment<LCD_t, LCD::LCD_Segment<6, 22>>;
		using SEG_B3  		= LCD::Segment<LCD_t, LCD::LCD_Segment<7, 22>>;
		using SEG_B2  		= LCD::Segment<LCD_t, LCD::LCD_Segment<7, 23>>;
		using SEG_B1  		= LCD::Segment<LCD_t, LCD::LCD_Segment<6, 23>>;
		using SEG_B0  		= LCD::Segment<LCD_t, LCD::LCD_Segment<5, 23>>;
		using BARGRAPH 		= LCD::Block::Bargraph<LCD_t, SEG_B0, SEG_B1, SEG_B2, SEG_B3, SEG_B4, SEG_B5, SEG_B6, SEG_B7, SEG_B8, SEG_B9, SEG_B10, SEG_B11, SEG_B12, SEG_B13>;
		using SEG_CV 		= LCD::LCD_Segment<7, 9>;
		using SEG_CC 		= LCD::LCD_Segment<4, 0>;
		using SEG_ON 		= LCD::LCD_Segment<0, 23>;
		using CONTROLMODE 	= LCD::Block::ControlMode<LCD_t, SEG_CV, SEG_CC, SEG_ON>;
		using SEG_VN       	= LCD::LCD_Segment<5, 9>;
		using SEG_VX       	= LCD::LCD_Segment<6, 9>;
		using VDISPLAYMODE 	= LCD::Block::DisplayMode<LCD_t, SEG_VN, SEG_VX>;
		using SEG_CX       	= LCD::LCD_Segment<5, 0>;
		using SEG_CN       	= LCD::LCD_Segment<6, 0>;
		using CDISPLAYMODE 	= LCD::Block::DisplayMode<LCD_t, SEG_CN, SEG_CX>;
		using SEG_V  		= LCD::Segment<LCD_t, LCD::LCD_Segment<2, 18>>;
		using SEG_C  		= LCD::Segment<LCD_t, LCD::LCD_Segment<0, 21>>;
		using SEG_VS 		= LCD::Segment<LCD_t, LCD::LCD_Segment<0, 13>>;
		using SEG_CS 		= LCD::Segment<LCD_t, LCD::LCD_Segment<0, 17>>;
		using SEG_mV 		= LCD::Segment<LCD_t, LCD::LCD_Segment<1, 18>>;
		using SEG_mA 		= LCD::Segment<LCD_t, LCD::LCD_Segment<1, 21>>;
		using SEG_m 		= LCD::Segment<LCD_t, LCD::LCD_Segment<3, 23>>;
		using SEG_j 		= LCD::LCD_Segment<2, 23>;
		using SEG_h 		= LCD::LCD_Segment<1, 23>;
		using SEG_W 		= LCD::LCD_Segment<0, 24>;
		using MultiUnits 	= LCD::Block::MultipurposeUnits<LCD_t, SEG_j, SEG_W, SEG_h>;
		using SEG_Lock    	= LCD::Segment<LCD_t, LCD::LCD_Segment<0, 18>>;
		using SEG_Remote  	= LCD::Segment<LCD_t, LCD::LCD_Segment<2, 21>>;
		using SEG_Battery 	= LCD::Segment<LCD_t, LCD::LCD_Segment<4, 23>>;


		template <typename T>
		T & Get()
		{
			return reinterpret_cast<T&>( m_LCD );
		}
	protected:

		auto& DisplayVoltage()
		{
			return Get<V_SEGS>();
		}
		auto& DisplayCurrent()
		{
			return Get<C_SEGS>();
		}
		auto& DisplayVoltageMode()
		{
			return Get<VDISPLAYMODE>();
		}
		auto& DisplayCurrentMode()
		{
			return Get<CDISPLAYMODE>();
		}
		auto& DisplayVoltageIcon()
		{
			return Get<SEG_V>();
		}
		auto& DisplayCurrentIcon()
		{
			return Get<SEG_C>();
		}
		auto& DisplayVoltageMilli()
		{
			return Get<SEG_mV>();
		}
		auto& DisplayCurrentMilli()
		{
			return Get<SEG_mA>();
		}
		auto & SetpointVoltage()
		{
			return Get<V_SET_SEGS>();
		}
		auto & SetpointCurrent()
		{
			return Get<I_SET_SEGS>();
		}
		auto& SetpointVoltageIcon()
		{
			return Get<SEG_VS>();
		}
		auto& SetpointCurrentIcon()
		{
			return Get<SEG_CS>();
		}
		auto& Multipurpose()
		{
			return Get<E_SET_SEGS>();
		}
		auto& MultipurposeMilli()
		{
			return Get<SEG_m>();
		}
		auto& MultipurposeUnits()
		{
			return Get<MultiUnits>();
		}
		auto& Bargraph()
		{
			return Get<BARGRAPH>();
		}
		auto& Mode()
		{
			return Get<CONTROLMODE>();
		}
		auto& Remote()
		{
			return Get<SEG_Remote>();
		}
		auto& Lock()
		{
			return Get<SEG_Lock>();
		}
		auto& Battery()
		{
			return Get<SEG_Battery>();
		}
		bool Buzzer( BuzzerTone const& pInput ) noexcept
		{
			if (Singleton::Settings().Buzzer())
			{
				m_LCD.Buzzer( pInput );
				return true;
			}
			return false;
		}
	public:
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		//Sets the value of the buzzer
		static void Delay() noexcept
		{
			for(volatile unsigned i = 0; i < 100000u; ++i);
		}
		void Change() noexcept
		{
			m_Change = true;
		}
		
		bool BuzzerHigh() noexcept
		{
			return Buzzer( BuzzerTone::High );
		}
		bool BuzzerLow() noexcept
		{
			return Buzzer( BuzzerTone::Low );
		}
		
		void BuzzerEnable() noexcept
		{
			Singleton::Settings().Buzzer() = General::On;
		}
		void BuzzerDisable() noexcept
		{
			Singleton::Settings().Buzzer() = General::Off;
		}

		void RunTones() noexcept
		{
			while (m_Tones.Size())
			{
				auto const tone = m_Tones.Front();
				if (m_Tones.Pop())
				{
					if (tone == High)
						if ( !BuzzerHigh() )
							return;

					if (tone == Low)
						if ( !BuzzerLow() )
							return;
					
					Delay();
				}
				else return;
			}
		}
		void PlayTones() noexcept
		{
			RunTones();
			Buzzer( BuzzerTone::Off );
		}

		
		bool SingleBuzzHigh() noexcept
		{
			return m_Tones.Push(ScheduledTone::High) == 1;
		}
		bool SingleBuzzLow() noexcept
		{
			return m_Tones.Push(ScheduledTone::Low) == 1;
		}
		bool SingleBuzzLowHigh() noexcept
		{
			return m_Tones.Push(ScheduledTone::Low, ScheduledTone::High) == 2;
		}
		bool SingleBuzzHighLow() noexcept
		{
			return m_Tones.Push(ScheduledTone::High, ScheduledTone::Low) == 2;
		}
    public:
		//Removing this causes crash, it doesn't initialise m_Tones
		SupplyLCD() = default;

		void Clear() noexcept
		{
			m_LCD.Clear();
			Change();
		}
		
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //TONE SECTION, THIS SECTION OF FUNCTIONS PRODUCES VARIOUS TONES TO INTERACT WITH THE USER
        //The set of alert tones
		auto BuzzerState() const noexcept
		{
			return Singleton::Settings().Buzzer();
		}
		void BuzzerState(General::OnOff pInput) noexcept
		{
			Singleton::Settings().Buzzer() = pInput;
		}
        bool SuccessTone() noexcept
        {
			if (BuzzerState())
        		return 	SingleBuzzHigh();
			return false;
    	}
        bool Success2Tone() noexcept
        {
			if (BuzzerState())
        		return 	SingleBuzzLow();
			return false;
    	}
       	bool AlertTone() noexcept
        {
			if (BuzzerState())
				return 	SingleBuzzHighLow() &&
						SingleBuzzHighLow();
			return false;
        }
        bool ErrorTone() noexcept
        {
			if (BuzzerState())
				return 	SingleBuzzHighLow() &&
						SingleBuzzHighLow() &&
						SingleBuzzHighLow() &&
						SingleBuzzHighLow();
			return false;
        }
		bool ChooseTone(bool pInput) noexcept
		{
			return (pInput) ? SuccessTone() : ErrorTone();
		}
		void UpdateLock(LockState pInput) noexcept
		{
			bool const computer = (pInput == LockState::ComputerLocked);
			bool const locked = computer || (pInput == LockState::UserLocked);
			Lock() = locked;
			Remote() = computer;
			Change();
		}

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        void DisplayUnitsVisible(bool pShow = true) noexcept
        {
            DisplayCurrentIcon() = pShow;
            DisplayVoltageIcon() = pShow;
			Change();
        }
        void SetpointUnitsVisible(bool pShow = true) noexcept
        {
            SetpointCurrentIcon() = pShow;
            SetpointVoltageIcon() = pShow;
			Change();
        }

        //Shows the display units A and V
        void DisplayUnitsHide() 	noexcept { DisplayUnitsVisible( false );    }
        void DisplayUnitsShow() 	noexcept { DisplayUnitsVisible( true  );    }

        //Shows the setpoint units A and V
        void SetpointUnitsHide() 	noexcept { SetpointUnitsVisible( false );   }
        void SetpointUnitsShow() 	noexcept { SetpointUnitsVisible( true  );   }

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //UPDATE SECTION, THESE FUNCTIONS UPDATE THE OUTPUT BUFFER
        void UpdateMeasurements(LDO const & pLDO, Measurement & pMeas) noexcept
        {
			bool is_on = pLDO.IsOn();

			if (is_on)
			{
				DisplayVoltage() = pMeas.Voltage();
				DisplayCurrent() = pMeas.Current();
			}
			else
			{
				DisplayVoltage().Clear();
				DisplayCurrent().Clear();
			}

			DisplayVoltageIcon() 	= is_on;
			DisplayCurrentIcon() 	= is_on;
			Mode() 					= pLDO.Mode();
			Change();
        }
		template <typename Base, typename T>
        void UpdateMenu( T & pMenu ) noexcept
        {
			typename T::buffer_t buffer{};
			pMenu.Render(buffer, true);
			Multipurpose() = buffer;
			MultipurposeUnits() = Base::Units();
			Change();
        }
        void UpdateSetpoints() noexcept
        {
            SetpointVoltage() = Singleton::Settings().VoltageSetpoint();
            SetpointCurrent() = Singleton::Settings().CurrentSetpoint();
			Change();
        }

		void BootloaderMessage() noexcept
		{
			BuzzerEnable();
			AlertTone();
			DisplayUnitsHide();
			SetpointUnitsHide();
			SetpointVoltage() = General::MakeArray("boot");
			SetpointCurrent() = General::MakeArray("loAd");
			MultipurposeUnits() = MultipurposeUnit::Off;
			Lock() = false;
			Remote() = false;

			DisplayVoltage().Clear();
			DisplayCurrent().Clear();
			Multipurpose().Clear();
			Mode() = LCD::Block::Control::Off;

			m_Change = true;
			Update();
			for (volatile std::size_t i = 0; i < 5000000; ++i); //A few milliseconds delay
		}

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //UPDATE SECTION, THESE FUNCTIONS UPDATE ELEMENTS OF THE DISPLAY WITH MINIMAL OVERHEAD
		void Update() noexcept
		{
			if ( m_Change )
			{
				PlayTones();
				m_LCD.Update();
				m_Change = false;
			}
		}
	};
}