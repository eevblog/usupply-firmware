#pragma once

#include "Pin.hpp"
#include "Constants.hpp"

namespace System
{
	//Analog In pins
	using ADC_IN0 		= IO::Pin<IO::Port::A, 0>;
	using ADC_IN1 		= IO::Pin<IO::Port::A, 1>;
	using V_BUCK_MON_5 	= IO::Pin<IO::Port::A, 3>;
	using _15V0_MON_5 	= IO::Pin<IO::Port::A, 2>;
	using TEMP_MEAS		= IO::ADCPin<System::TemperatureADCChannel>;
	
	//Analog Out pins
	using DAC_OUT1 		= IO::Pin<IO::Port::A, 4>;
	using DAC_OUT2 		= IO::Pin<IO::Port::A, 5>;

	//Digital pins
	using SWDIO 		= IO::Pin<IO::Port::A, 13>;
	using SWCLK 		= IO::Pin<IO::Port::A, 14>;
	//
	using UART_TX 		= IO::Pin<IO::Port::A, 9>;
	using UART_RX 		= IO::Pin<IO::Port::A, 10>;
	using RTC_CI 		= IO::Pin<IO::Port::C, 14>;
	using RTC_CO 		= IO::Pin<IO::Port::C, 15>;
	using TEST_LED 		= IO::Pin<IO::Port::B, 4>;
	
	using SPI1_NSS 		= IO::Pin<IO::Port::A, 15>;
	using SPI1_SCK 		= IO::Pin<IO::Port::B, 3>;
	using SPI1_MOSI 	= IO::Pin<IO::Port::B, 5>;
	using CC_NCV 		= IO::Pin<IO::Port::B, 9>;

	using SPI2_NSS 		= IO::Pin<IO::Port::B, 12>;
	using SPI2_SCK 		= IO::Pin<IO::Port::B, 13>;
	using SPI2_MISO 	= IO::Pin<IO::Port::B, 14>;
	using SPI2_MOSI 	= IO::Pin<IO::Port::B, 15>;

	//I2C bus
	using A_PWR_EN 		= IO::Pin<IO::Port::A, 11>;
	using I2C_SCL		= IO::Pin<IO::Port::B, 6>;
	using I2C_SDA		= IO::Pin<IO::Port::B, 7>;
	using I2C2_SCL		= IO::Pin<IO::Port::B, 10>;

	//Button matrix pins
	using R0 			= IO::Pin<IO::Port::A, 8>;
	using R1 			= IO::Pin<IO::Port::A, 7>;
	using R2 			= IO::Pin<IO::Port::B, 0>;
	using R3 			= IO::Pin<IO::Port::B, 1>;
	//
	using C0 			= IO::Pin<IO::Port::B, 2>;
	using C1 			= IO::Pin<IO::Port::B, 10>;
	using C2 			= IO::Pin<IO::Port::B, 11>;
	using C3 			= IO::Pin<IO::Port::B, 8>;
	using C4 			= IO::Pin<IO::Port::A, 12>;
	//
	TEST_LED LED = IO::Mode::Output;
	void ToggleLED()
	{
		LED.Toggle();
	}
}