#pragma once

#include "Math.hpp"
#include <cstdint>

namespace System
{
	constexpr std::uint32_t const LSIClock			= 40000;
	constexpr std::uint32_t const LSEClock			= 32768;
	constexpr std::uint32_t const RTCClock			= LSEClock;
	constexpr std::uint32_t const HSIClock 			= General::MHz(8 );
	constexpr std::uint32_t const HSI14Clock		= General::MHz(14);
	constexpr std::uint32_t const HSI48Clock 	 	= General::MHz(48);
	constexpr std::uint32_t const HSEClock			= 0;
	//
	// Timing settings
	static constexpr auto SampleFrequency 			= General::Hz<std::uint32_t>(100);		//Sample frequency
	static constexpr auto UpdateFrequency 			= General::Hz<std::uint32_t>(5);		//Update frequency
	//
	// Analog reference
	constexpr float const AnalogReference			= 3.3f; 	// Volts
	//
	// Self test values
	constexpr float const IsolatedVoltage 			= 15.0f; 	// Volts
	//
	// Tolerances
	constexpr float const IsolatedTolerance			= 5.0f; 	// Percent
	constexpr float const PreregulatorTolerance		= 5.0f; 	// Percent
	constexpr float const OutputVoltageTolerance	= 1.0f;		// Percent
	constexpr float const OutputCurrentTolerance	= 1.0f;		// Percent
	//
	// Component constants
	// Output voltage resistor divider
	constexpr std::uint32_t const R8	= 27000u;
	constexpr std::uint32_t const R23 	= 10000u;

	// Voltage setpoint resistor divider
	constexpr std::uint32_t const R22 	= 27000u;
	constexpr std::uint32_t const R13 	= 10000u;

	// Current sense gain resistors NOTE: R17 = R18, R32 = R33
    constexpr float const         RSHUNT = 0.4;
	constexpr std::uint32_t const R17 	= 10000u;
	constexpr std::uint32_t const R32 	= 33000u;

	// Preregulator monitor divider resistors
	constexpr std::uint32_t const R24 	= 82000u;
	constexpr std::uint32_t const R34 	= 20500u;

	// Isolated convertor monitor divider resistors
	constexpr std::uint32_t const R25	= 82000u;
	constexpr std::uint32_t const R35	= 20500u;

	// Adding unlisted registers
	volatile std::uint16_t & TEMP110_CAL = *((uint16_t*) ((uint32_t) 0x1FFFF7C2));
	volatile std::uint16_t & TEMP30_CAL  = *((uint16_t*) ((uint32_t) 0x1FFFF7B8));

	//ADC input channels
	constexpr std::uint32_t TemperatureADCChannel = 16u;
}