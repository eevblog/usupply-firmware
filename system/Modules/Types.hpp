#pragma once

#include "RCC.hpp"
#include "RegistersRCC.hpp"
#include "Meta.hpp"
#include "Math.hpp"
#include "Button2.hpp"
#include "Matrix.hpp"
#include "Convert.hpp"
#include "AP3513E.hpp"
#include "PinDefinitions.hpp"
#include "SPIBitbanging.hpp"

namespace System
{
	using namespace IO;
	using SPI1_t = SPI::Bitbang<SPI::Polarity::Inverted, SPI::Phase::LeadingEdge, SPI1_NSS, SPI1_SCK, SPI1_MOSI, IO::NoPin, 100>;
	using SPI2_t = SPI::Bitbang<SPI::Polarity::Inverted, SPI::Phase::LeadingEdge, SPI2_NSS, SPI2_SCK, SPI2_MOSI, SPI2_MISO>;
}
namespace System
{
	using namespace IO;
    //
    using SystemClockSource = ::Peripherals::RCCGeneral::SystemClockSource;
    using RTCClockSource    = ::Peripherals::RCCGeneral::RTCClockSource;
    using PCLKDivision      = ::Peripherals::RCCGeneral::PCLKDivision;
    using HCLKDivision      = ::Peripherals::RCCGeneral::HCLKDivision;
    using PLLSource         = ::Peripherals::RCCGeneral::PLLSource;
    using PLLMultiply       = ::Peripherals::RCCGeneral::PLLMultiply;
    using PLLDivision       = ::Peripherals::RCCGeneral::PLLDivision;
    //
	using SystemClock_t = Peripherals::SystemClock
	<
		SystemClockSource::PLL,
		PCLKDivision::HCLK_1,
		HCLKDivision::SYSCLK_1,
        PLLSource::HSI,
		PLLMultiply::x6,
		PLLDivision::Div1
	>;
    //
	constexpr std::uint64_t const   SystemClock	= SystemClock_t::SYSCLK();
	constexpr std::uint64_t const   TimerClock	= SystemClock_t::Timer();
	//
	using Prereg_t = Parts::AP3513E::AP3513E<SPI2_t>;
}