#pragma once

namespace System
{
    enum class LockState
    {
        Unlocked,
        ComputerUnlocked,   //Cannot set to ComputerLocked after this state
        UserLocked,
        ComputerLocked
    };
}