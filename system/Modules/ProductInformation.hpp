#pragma once
#include "ArrayConvert.hpp"

namespace System
{
	struct ProductInformation
	{
		static constexpr auto Manufacturer		=	General::MakeArray("EEVblog");
		static constexpr auto Model           	=	General::MakeArray("uSupply V1");
		static constexpr auto Serial          	=	General::MakeArray("000-001");
		static constexpr auto FirmwareVersion  	=	General::MakeArray("V0.1");    			
	};                                                     				
}