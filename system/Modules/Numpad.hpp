#pragma once

#include "Types.hpp"
#include "Keypad.hpp"
#include "Matrix.hpp"

#define FWD(x) std::forward<decltype(x)>(x)
namespace System
{
	//Note the keypad links to VirtualButtons and must have an instance call .Update periodically to function.
	using Keypad_t 			= IO::Matrix<General::TypeList<R0, R1, R2, R3>, General::TypeList<C0, C1, C2, C3, C4>>;

	using POWER_PIN 		= typename Keypad_t::template pin_t<0, 0>;
	using NUM7_PIN 			= typename Keypad_t::template pin_t<0, 1>;
	using NUM8_PIN 			= typename Keypad_t::template pin_t<0, 2>;
	using NUM9_PIN 			= typename Keypad_t::template pin_t<0, 3>;
	using CANCEL_PIN 		= typename Keypad_t::template pin_t<0, 4>;
	using CONFIG_PIN 		= typename Keypad_t::template pin_t<1, 0>;
	using NUM4_PIN 			= typename Keypad_t::template pin_t<1, 1>;
	using NUM5_PIN 			= typename Keypad_t::template pin_t<1, 2>;
	using NUM6_PIN 			= typename Keypad_t::template pin_t<1, 3>;
	using VOLTS_PIN 		= typename Keypad_t::template pin_t<1, 4>;
	using LOCK_PIN 			= typename Keypad_t::template pin_t<2, 0>;
	using NUM1_PIN 			= typename Keypad_t::template pin_t<2, 1>;
	using NUM2_PIN 			= typename Keypad_t::template pin_t<2, 2>;
	using NUM3_PIN 			= typename Keypad_t::template pin_t<2, 3>;
	using CURRENT_PIN 		= typename Keypad_t::template pin_t<2, 4>;
	using PRESETS_PIN 		= typename Keypad_t::template pin_t<3, 0>;
	using NUM0_PIN 			= typename Keypad_t::template pin_t<3, 1>;
	using DECIMAL_PIN 		= typename Keypad_t::template pin_t<3, 2>;
	using FUNCTION_PIN 		= typename Keypad_t::template pin_t<3, 3>;
	using ENTER_PIN 		= typename Keypad_t::template pin_t<3, 4>;

	using KeyCodes = Menu::KeyCodes;

	template <typename Event>
	auto Numpad( Event && pEvent ) noexcept
	{
		//
		static auto const keypad_event = [&]( KeyCodes pCode )
		{
			static auto event{ FWD(pEvent) };
			return [ &, c{pCode} ]( IO::StateEvent pType )
			{ 
				event( pType, c ); 
			}; 
		};

		//These types represet the type of the buttons, but not the type of the buttons themselves
		using POWER_LIST 		= General::TypeList < IO::SingleKernal2, 		POWER_PIN 	>;
		using LOCK_LIST 		= General::TypeList < IO::HoldKernal2,   		LOCK_PIN 	>;
		using CANCEL_LIST 		= General::TypeList < IO::SingleKernal2, 		CANCEL_PIN 	>;
		using VOLTS_LIST 		= General::TypeList < IO::SingleKernal2, 		VOLTS_PIN 	>;
		using CURRENT_LIST 		= General::TypeList < IO::SingleKernal2, 		CURRENT_PIN	>;
		using ENTER_LIST 		= General::TypeList < IO::SingleKernal2, 		ENTER_PIN 	>;
		using PRESETS_LIST 		= General::TypeList < IO::SingleHoldKernal2, 	PRESETS_PIN	>;
		using CONFIG_LIST 		= General::TypeList < IO::SingleKernal2, 		CONFIG_PIN 	>;
		using FUNCTION_LIST 	= General::TypeList < IO::SingleKernal2, 		FUNCTION_PIN>;
		using DECIMAL_LIST 		= General::TypeList < IO::SingleKernal2, 		DECIMAL_PIN	>;
		using NUM0_LIST 		= General::TypeList < IO::SingleKernal2, 		NUM0_PIN 	>;
		using NUM1_LIST 		= General::TypeList < IO::SingleKernal2, 		NUM1_PIN 	>;
		using NUM2_LIST 		= General::TypeList < IO::SingleKernal2, 		NUM2_PIN 	>;
		using NUM3_LIST 		= General::TypeList < IO::SingleKernal2, 		NUM3_PIN 	>;
		using NUM4_LIST 		= General::TypeList < IO::SingleKernal2, 		NUM4_PIN 	>;
		using NUM5_LIST 		= General::TypeList < IO::SingleKernal2, 		NUM5_PIN 	>;
		using NUM6_LIST 		= General::TypeList < IO::SingleKernal2, 		NUM6_PIN 	>;
		using NUM7_LIST 		= General::TypeList < IO::SingleKernal2, 		NUM7_PIN 	>;
		using NUM8_LIST 		= General::TypeList < IO::SingleKernal2, 		NUM8_PIN 	>;
		using NUM9_LIST 		= General::TypeList < IO::SingleKernal2, 		NUM9_PIN 	>;
	
		return General::Keypad
		{
			General::Type<Keypad_t>{},
			IO::Button2{ POWER_LIST{}, 		keypad_event( KeyCodes::Power 	)	},
			IO::Button2{ LOCK_LIST{}, 		keypad_event( KeyCodes::Lock 	)	},
			IO::Button2{ CANCEL_LIST{}, 	keypad_event( KeyCodes::Cancel 	)	},
			IO::Button2{ VOLTS_LIST{}, 		keypad_event( KeyCodes::Voltage )	},
			IO::Button2{ CURRENT_LIST{}, 	keypad_event( KeyCodes::Current )	},
			IO::Button2{ ENTER_LIST{}, 		keypad_event( KeyCodes::Enter 	)	},
			IO::Button2{ PRESETS_LIST{}, 	keypad_event( KeyCodes::Presets )	},
			IO::Button2{ CONFIG_LIST{}, 	keypad_event( KeyCodes::Config 	)	},
			IO::Button2{ FUNCTION_LIST{}, 	keypad_event( KeyCodes::Shift 	)	},
			IO::Button2{ DECIMAL_LIST{}, 	keypad_event( KeyCodes::Decimal )	},
			IO::Button2{ NUM0_LIST{}, 		keypad_event( KeyCodes::Num0 	)	},
			IO::Button2{ NUM1_LIST{}, 		keypad_event( KeyCodes::Num1 	)	},
			IO::Button2{ NUM2_LIST{}, 		keypad_event( KeyCodes::Num2 	)	},
			IO::Button2{ NUM3_LIST{}, 		keypad_event( KeyCodes::Num3 	)	},
			IO::Button2{ NUM4_LIST{}, 		keypad_event( KeyCodes::Num4 	)	},
			IO::Button2{ NUM5_LIST{}, 		keypad_event( KeyCodes::Num5 	)	},
			IO::Button2{ NUM6_LIST{}, 		keypad_event( KeyCodes::Num6 	)	},
			IO::Button2{ NUM7_LIST{}, 		keypad_event( KeyCodes::Num7 	)	},
			IO::Button2{ NUM8_LIST{}, 		keypad_event( KeyCodes::Num8 	)	},
			IO::Button2{ NUM9_LIST{}, 		keypad_event( KeyCodes::Num9 	)	}
		};
	}
}
#undef FWD