#pragma once

#include "stm32f0xx.h"
#include "stm32f0xx_rcc.h"

#include "LDO.hpp"
#include "Timer.hpp"
#include "LockState.hpp"
#include "SupplyLCD.hpp"
#include "Measurement.hpp"
#include "ProductInformation.hpp"
#include "SettingsSingleton.hpp"
#include "SysTick.hpp"
#include "Serial.hpp"

namespace System
{
	using LCD_t = System::SupplyLCD<SPI1_t>;

	//The above will allow the bootloader to be called from the application
	volatile auto const boot_address 			= 0x1FFFC800;
	volatile auto const boot_jump 				= (void(*)(void))(*((std::uint32_t*)(boot_address + 4u)));
	volatile auto const boot_stack_address 		= *((std::uint32_t*)boot_address);
    
    //
	class SystemBase
	{
	private:
		using SysTickModule_t = Peripherals::SysTickModule<Peripherals::NoInterrupt, Peripherals::SysTickType::Ticker>;
        using MultipurposeUnit = LCD::Block::MultipurposeUnit;
        //
        struct Data
        {
            SystemClock_t 	    m_SysClk;
            LockState 			m_Locked;
            ProductInformation 	m_Info;
            LDO 				m_LDO;
            LCD_t 				m_LCD;
            Measurement 		m_Measure;
            SysTickModule_t 	m_Tick;
		    MultipurposeUnit    m_Units;

            Data() noexcept : 
                m_SysClk{},
                m_Locked{ LockState::Unlocked },
                m_Info{},
                m_LDO{},
                m_LCD{},
                m_Measure{},
                m_Tick{ 100 },  ///< System tick 100 Hz countrate
                m_Units{ MultipurposeUnit::Off }
            {} 
        };
        //
        static inline Data m_Data {};
	public:
		SystemBase() noexcept
		{
			Screen().UpdateSetpoints();     ///<
			Screen().SetpointUnitsShow();   ///< Render intial state of LCD
		}

        static MultipurposeUnit & Units() noexcept
        {
            return m_Data.m_Units;
        }
        
		static void ResetSystem() noexcept
		{
			NVIC_SystemReset();
		}

		static void Bootloader() noexcept
		{
			Setpoint().Off();
			Setpoint().Update();
			for (volatile std::size_t i = 0; i < 5000000; ++i); //A few milliseconds delay

			//Periph library does this fine, a little bit to optimise...
			RCC_APB2PeriphResetCmd(RCC_APB2Periph_USART1, ENABLE);
			RCC_APB2PeriphResetCmd(RCC_APB2Periph_USART1, DISABLE);
			for (volatile std::size_t i = 0; i < 1000000; ++i); //A few milliseconds delay

			RCC_DeInit();

			SysTick->CTRL = 0;
			SysTick->LOAD = 0;
			SysTick->VAL = 0;

			//Disables all peripheral interrupts
			 __disable_irq();
			__DSB();

			Screen().BootloaderMessage();
			__DSB();

			__set_PRIMASK(1);
			__set_MSP( boot_stack_address );

			boot_jump();
		}

		static void ResetEnergyOnPowerEnable() noexcept
		{
			Singleton::Settings().ResetEnergyOnPower() = General::On;
		}

		static void ResetEnergyOnPowerDisable() noexcept
		{
			Singleton::Settings().ResetEnergyOnPower() = General::Off;
		}

		static auto ResetEnergyOnPowerState() noexcept
		{
			return Singleton::Settings().ResetEnergyOnPower();
		}

		static void SetResetEnergyOnPowerState(General::OnOff pInput) noexcept
		{
			Singleton::Settings().ResetEnergyOnPower() = pInput;
		}

        static bool AddVoltageCalibrationPoint(General::Sample pSample) noexcept
        {
            return Singleton::Settings().VoltageCalibration().Push(pSample);
        }

        static bool AddCurrentCalibrationPoint(General::Sample pSample) noexcept
        {
            return Singleton::Settings().CurrentCalibration().Push(pSample);
        }

        static unsigned CountVoltageCalibration() noexcept
        {
            return Singleton::Settings().VoltageCalibration().Count();
        }

        static unsigned CountCurrentCalibration() noexcept
        {
            return Singleton::Settings().CurrentCalibration().Count();
        }

        static void ResetVoltageCalibration() noexcept
        {
            Singleton::Settings().VoltageCalibration().Clear();
        }

        static void ResetCurrentCalibration() noexcept
        {
            Singleton::Settings().CurrentCalibration().Clear();
        }

        static void ResetCalibration() noexcept
        {
            Singleton::Settings().VoltageCalibration().Clear();
            Singleton::Settings().CurrentCalibration().Clear();
        }

		static LockState & LockedState() noexcept
		{
			return m_Data.m_Locked;
		}

		static void ComputerLock() noexcept
		{
			if (m_Data.m_Locked != LockState::ComputerUnlocked)
				Screen().UpdateLock(m_Data.m_Locked = LockState::ComputerLocked);
		}

		static void ComputerUnlock() noexcept
		{
			Screen().UpdateLock(m_Data.m_Locked = LockState::ComputerUnlocked);
		}

		static void UserLock() noexcept
		{
			Screen().UpdateLock(m_Data.m_Locked = LockState::UserLocked);
		}

		static void Unlock() noexcept
		{
			Screen().UpdateLock(m_Data.m_Locked = LockState::Unlocked);
		}

		static std::uint64_t Tick() noexcept
		{
			return m_Data.m_Tick.Tick();
		}

		static void Wait(float seconds) noexcept
		{
			std::uint64_t const end = std::uint64_t( seconds * (float)SystemClock ) + Tick();
			while (Tick() < end);
		}
    private:
        //
        static void CommandSuccess() noexcept
        {
            Screen().UpdateSetpoints();
            Screen().SuccessTone();
        }
        static void CommandError() noexcept
        {
            Screen().ErrorTone();
        }
    public:
        /**
         * 
         */
        static auto Settings() noexcept -> decltype(Singleton::Settings()) &
		{
			return Singleton::Settings();
		}
        /**
         * 
         */
        static auto Measure() noexcept -> Measurement &
        {
        	return m_Data.m_Measure;
        }
        /**
         * 
         */
        static auto Information() noexcept -> ProductInformation &
        {
        	return m_Data.m_Info;
        }
        /**
         * 
         */
        static auto Setpoint() noexcept -> LDO &
        {
        	return m_Data.m_LDO;
        }
        /**
         * 
         */
		static auto Screen() noexcept -> LCD_t &
		{
			return m_Data.m_LCD;
		}
        /**
         * 
         */
        static auto & Serial() noexcept
        {
            static auto serial
            {
                System::Serial<SystemBase>
                (
                    &CommandSuccess,
                    &CommandError
                )
            };
            return serial;
        }
	};
    
	SystemBase CreateSystem() noexcept
	{
        SystemBase base;
        auto & r = base.Serial();
        UNUSED(r);
		return base;
	}
}