#pragma once

#include "Supply/SOUR.hpp"
#include "Supply/MEAS.hpp"
#include "Supply/OUTP.hpp"
#include "Supply/SYST.hpp"
#include "Settings/SETT.hpp"

namespace Parser::SCPI
{
    //  Compilation passes with any of the above flags.
	template <typename Base>
	auto SystemCommands() noexcept
	{
		return Node
        (
            Optional{ ":" },
            States
            (
                SOURce<Base>    (),
                MEASure<Base>	(),
                OUTPut<Base>	(),
                SETTings<Base>  (),
                SYSTem<Base>    ()
            )
        );
	}
}