#pragma once

#include "SCPIShared.hpp"

namespace Parser::SCPI
{
	template <typename Base>
	auto Help() noexcept
    {
        return Command
        (
            Fallback{},
            None{},
            Blank<0>{},
            [&](auto)
            {
                //Get the help message form the file
                const char * raw = 
                #include "HelpMessage.txt"
                ;
                Base::Serial().BlockingSend(raw);
            }
        );
    }
}