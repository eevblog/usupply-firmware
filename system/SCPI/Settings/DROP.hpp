#pragma once

#include "SCPIShared.hpp"

namespace Parser::SCPI
{
	template <typename Base>
	auto DROPout() noexcept
    {
        return Command
        (
            Keyword(":DROP", "out"),
            Param<float>{},
            Blank<0>{},
            [&](auto v)
            {
                if constexpr ( IsParam(v) )	Base::Setpoint().Dropout(v);
                else						Base::Serial().Send(Base::Setpoint().Dropout(), "\n");
            }
        );
    }
}