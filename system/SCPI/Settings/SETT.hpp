#pragma once

#include "CAL.hpp"
#include "DROP.hpp"
#include "FIRM.hpp"
#include "DEB.hpp"

namespace Parser::SCPI
{
	template <typename Base>
	auto SETTings() noexcept
	{
		return Node
		(
			Keyword("SETT", "ings"),
			States
			(
				CALIbration<Base>	(),
				DROPout<Base>		(),
				FIRMware<Base> 	    (),
				DEBug<Base>		    ()
			)
		);
	}
}