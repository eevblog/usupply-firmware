#pragma once

#include "Test.hpp"
#include "Schedulable.hpp"
#include "SCPIShared.hpp"

namespace Parser::SCPI
{
	using test_schedulable_t = System::Schedulable<System::TestState, 16>;

	template <typename Base>
	auto BasicCommands() noexcept
	{
		return Node
		(
			Required("*"),
			States
			(
				Command
				(
					Required("RST"),
					Blank<0>{},
					None{},
					[&](auto)
					{
						Base::ResetSystem();
					}
				),
				Command
				(
					Required("IDN"),
					None{},
					Blank<0>{},
					[&](auto)
					{
						Base::Serial().Send
						(
							Base::Information().Manufacturer, ", ",
							Base::Information().Model, ", ",
							Base::Information().Serial, ", ",
							Base::Information().FirmwareVersion, "\n"
						);
					}
				),
				Command
				(
					Required("TST"),
					Blank<0>{},
					None{},
					[&](auto)
					{
						if (test_schedulable_t::Construct
						(
							[&]
							{
								::System::Test<Base>( 1.0, 10, 0.1, 1.0, 5.0 );
							}
						)) 	Base::Serial().Send("Starting self test:\n");
						else
							Base::Serial().Send("Cannot start self test.\n");
					}
				)
			)
		);
	}
}