#pragma once

#include "Command.hpp"
#include "Keyword.hpp"
#include "Parser.hpp"

namespace Parser::SCPI::Helpers
{
    constexpr	auto voltage = Keyword	{ ":VOLT", "age"	};
    constexpr	auto current = Keyword	{":CURR", "ent"		};
    constexpr	auto power	 = Keyword	{":POW", "er"		};
    constexpr	auto energy	 = Keyword	{":ENER", "gy"		};
    constexpr	auto limit	 = Keyword	{":LIM", "it"		};
    constexpr	auto dc		 = Optional	{ ":DC"				};
    constexpr	auto rst	 = Required	{":RST"				};
    constexpr	auto add	 = Required	{":ADD"				};
    constexpr	auto get	 = Required	{":GET"				};
    constexpr	auto filter	 = Keyword	{":FILT", "er"      };
}