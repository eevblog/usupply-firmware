#pragma once

#include "SCPIShared.hpp"

namespace Parser::SCPI
{
	template <typename Base>
	auto MEASure() noexcept
    {
        using namespace Helpers;
        return Node
        (
            Keyword("MEAS", "ure"),
            States
            (
                Command
                (
                    Chain(voltage, dc),
                    None{},
                    Blank<0>{},
                    [&](auto){ Base::Serial().Send( Base::Measure().Voltage(), "\n" ); }
                )
                ,Command
                (
                    Chain(current, dc),
                    None{},
                    Blank<0>{},
                    [&](auto){ Base::Serial().Send( Base::Measure().Current(), "\n" ); }
                )
                ,Command
                (
                    Chain(power, dc),
                    None{},
                    Blank<0>{},
                    [&](auto){ Base::Serial().Send( Base::Measure().Power(), "\n" ); }
                )
                ,Command
                (
                    Chain(energy, dc),
                    None{},
                    Blank<0>{},
                    [&](auto){ Base::Serial().Send( Base::Measure().Energy(), "\n" ); }
                )
                ,Node
                (
                    Keyword(":INTE", "rnal"),
                    States
                    (
                        Command
                        (
                            Keyword(":TEMP", "erature"),
                            None{},
                            Blank<0>{},
                            [&](auto) { Base::Serial().Send( Base::Measure().Temperature(), "\n"); }
                        ),
                        Command
                        (
                            Keyword(":PRER", "egulator"),
                            None{},
                            Blank<0>{},
                            [&](auto) { Base::Serial().Send( Base::Measure().Preregulator(), "\n" ); }
                        ),
                        Command
                        (
                            Keyword(":ISOL", "ator"),
                            None{},
                            Blank<0>{},
                            [&](auto) { Base::Serial().Send( Base::Measure().Isolated(), "\n" ); }
                        )
                    )
                )
            )
        );
    }
}