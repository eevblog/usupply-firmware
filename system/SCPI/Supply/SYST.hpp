#pragma once

#include "SCPIShared.hpp"

namespace Parser::SCPI
{
	template <typename Base>
	auto SYSTem() noexcept
	{
        using namespace Helpers;
		return Node
        (
            Keyword("SYST", "em"),
            States
            (
                Command
                (
                    Keyword(":VERS", "ion"),
                    None{},
                    Blank<0>{},
                    [&] (auto) { Base::Serial().Send("V1.1\n"); }
                ),
                Command
                (
                    Keyword(":UNLO", "ck"),
                    Blank<0>{},
                    None{},
                    [&] (auto) 
                    {
                        Base::ComputerUnlock();
                        Base::Serial().Send("Unlocked\n");
                    }
                ),
                Command
                (
                    Keyword(":COM", "mand"),
                    Param<General::OnOff>{},
                    Blank{},
                    [&]( auto v )
                    {
                        if constexpr ( IsParam(v) ){ Base::Settings().CommandMode() = v; }
                        else						Base::Serial().Send( Base::Settings().CommandMode().State(), "\n" );
                    }
                )
            )
        );
	}
}