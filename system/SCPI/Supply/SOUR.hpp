#pragma once

#include "SCPIShared.hpp"

namespace Parser::SCPI
{
	template <typename Base>
	auto SOURce() noexcept
	{
        using namespace Helpers;
		return Node
        (
            Keyword("SOUR", "ce"),
            States
            (
                Node
                (
                    voltage,
                    States
                    (
                        Command
                        (
                            limit,
                            Param<float>{},
                            Blank<0>{},
                            [&](auto v)
                            {
                                if constexpr (IsParam(v))	Base::Setpoint().VoltageLimit( v );
                                else						Base::Serial().Send( Base::Setpoint().VoltageLimit(), "\n" );
                            }
                        ),
                        Command
                        (
                            Fallback{},
                            Param<float>{},
                            Blank<0>{},
                            [&](auto v)
                            {
                                if constexpr (IsParam(v))	Base::Setpoint().Voltage( v );
                                else						Base::Serial().Send( Base::Setpoint().Voltage(), "\n" );
                            }
                        )
                    )
                )
                ,Node
                (
                    current,
                    States
                    (
                        Command
                        (
                            limit,
                            Param<float>{},
                            Blank<0>{},
                            [&](auto v)
                            {
                                if constexpr (IsParam(v))	Base::Setpoint().CurrentLimit( v );
                                else						Base::Serial().Send( Base::Setpoint().CurrentLimit(), "\n" );
                            }
                        ),
                        Command
                        (
                            Fallback{},
                            Param<float>{},
                            Blank<0>{},
                            [&](auto v)
                            {
                                if constexpr (IsParam(v))	Base::Setpoint().Current( v );
                                else						Base::Serial().Send( Base::Setpoint().Current(), "\n" );
                            }
                        )
                    )
                )
                ,Node
                (
                    power,
                    Command
                    (
                        limit,
                        Param<float>{},
                        Blank<0>{},
                        [&](auto v)
                        {
                            if constexpr (IsParam(v))	Base::Setpoint().PowerLimit( v );
                            else						Base::Serial().Send( Base::Setpoint().PowerLimit(), "\n" );
                        }
                    )
                )
            )
        );
	}
}