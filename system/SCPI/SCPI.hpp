#pragma once

#include "BasicCommands.hpp"
#include "SupplyCommands.hpp"
#include "Help.hpp"

namespace Parser::SCPI
{
	template <typename Base>
	auto SCPI() noexcept
	{
		return States
		(
			BasicCommands<Base>	(),
			SystemCommands<Base>(),
			Help<Base>          ()
		);
	}
}