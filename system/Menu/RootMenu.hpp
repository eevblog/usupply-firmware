#pragma once

#include "HomeMenu.hpp"
#include "EnumAccess.hpp"
#include "LockState.hpp"
#include "RunOnDestruct.hpp"

namespace System
{
#define FWD( x ) std::forward<decltype( x )>( x )

	template <unsigned BufferSize, typename T, typename BuzzerCallback, typename PowerCallback, typename LockedCallback, typename VoltageCallback, typename CurrentCallback, typename DefaultMenu, typename ChildMenu>
	class RootMenu
	{
	private:
		using MultipurposeUnit = LCD::Block::MultipurposeUnit;
		using base_t = Menu::RawMenuItem<BufferSize>;

	public:
		using buffer_t = typename base_t::buffer_t;
	private:
		using input_t = Menu::RawEditable<BufferSize, T>;
		using value_t = Menu::RawValue<BufferSize, T>;

		enum class State
		{
			Default,
			Presets,
			Voltage,
			Current,
			Unknown,
			Child
		};

		//Storage references
		T					m_Buffer = (T)0;
		MultipurposeUnit &	m_Units;
		LockState & 		m_Locked;

		BuzzerCallback	m_BuzzerCallback;
		General::OnOff	m_Power;
		PowerCallback	m_PowerCallback;
		LockedCallback	m_LockedCallback;
		VoltageCallback m_Voltage;
		CurrentCallback m_Current;

		//Menu structure
		DefaultMenu		m_Default;
		ChildMenu		m_Menu;
		State			m_State = State::Default;
		input_t			m_Input = m_Buffer;

		//Returns the currently selected item
		bool IsDefault() const noexcept
		{
			return m_State == State::Default;
		}
		bool IsLocked() const noexcept
		{
			return 
				(m_Locked == LockState::UserLocked) || 
				(m_Locked == LockState::ComputerLocked);
		}
		LockState ToggleLocked() noexcept
		{
			if (IsLocked()) return m_Locked = LockState::Unlocked;
			else 			return m_Locked = LockState::UserLocked;
		}
		base_t & Item()
		{
			switch ( m_State )
			{
			default:
			case State::Unknown: 	[[fallthrough]];
			case State::Voltage: 	[[fallthrough]];
			case State::Current:	return m_Input;
			case State::Default:	return m_Default;
			case State::Child:		return m_Menu;
			}
		}
		void ResetInput( State const new_state = State::Default )
		{
			m_Input.Reset();
			m_State = new_state;
		}
		void SubmitInput()
		{
			m_Input.Save();
			ResetInput();
		}
		auto NavigateInput( KeyCodes const pInput )
		{
			using namespace Menu;
			auto const submit_voltage = [&]
			{
				SubmitInput();
				m_Voltage( m_Buffer );
			};
			auto const submit_current = [&]
			{
				SubmitInput();
				m_Current( m_Buffer );
			};

			if (auto v = m_Input.Navigate(pInput); v != Unhandled)
			{
				if (v == Finished)
				{
					if (m_State == State::Voltage)
						submit_voltage();
					else if (m_State == State::Current)
						submit_current();
				}
				return v;
			}
			else if ( IsCancel( pInput ) || v == Finished )
			{
				ResetInput();
				return Handled;
			}
			else
			{
				if ( pInput == KeyCodes::Voltage )
				{
					submit_voltage();
					return Handled;
				}
				else if ( pInput == KeyCodes::Current )
				{
					submit_current();
					return Handled;
				}
			}
			return Unhandled;
		}
		auto NavigateChild( KeyCodes const pInput )
		{
			using namespace Menu;
			//Translate the settings button into the up key, so that you can press settings to navigate
			if ( auto v = m_Menu.Navigate( pInput ); v == Handled )
				return v;
			else if ( IsCancel( pInput ) || v == Finished )
			{
				m_State = State::Default;
				return v;
			}
			else if ( pInput == KeyCodes::Config )
				return NavigateChild( KeyCodes::Up );
			else return Unhandled;
		}
	public:
		RootMenu
		(
			MultipurposeUnit & 	pUnits,
			LockState & 		pLockState,
			BuzzerCallback && 	pBuzzer,
		    PowerCallback &&	pPower,
		    LockedCallback &&	pLocked,
		    VoltageCallback &&	pVoltage,
		    CurrentCallback &&	pCurrent,
		    DefaultMenu &&		pDefault,
		    ChildMenu &&		pMenu 
		) :
			m_Units			( pUnits 			),
			m_Locked		( pLockState 		),
			m_BuzzerCallback( FWD( pBuzzer 		) ),
		    m_PowerCallback	( FWD( pPower 		) ),
		    m_LockedCallback( FWD( pLocked 		) ),
		    m_Voltage		( FWD( pVoltage 	) ),
		    m_Current		( FWD( pCurrent 	) ),
		    m_Default		( FWD( pDefault 	) ),
		    m_Menu			( FWD( pMenu 		) )
		{}
		template <typename... Args>
		RootMenu( General::SizeT<BufferSize>, General::Type<T>, MultipurposeUnit & pUnits, LockState & pLockState, Args&&... pArgs ) :
		    RootMenu( pUnits, pLockState, FWD( pArgs )... )
		{}

		//Navigate the home menu.
		void Navigate( KeyCodes const pInput )
		{
			if ( pInput == KeyCodes::Lock )
			{
				m_LockedCallback( ToggleLocked() );
				return;
			}
			else if ( !IsLocked() )
			{
				switch ( pInput )
				{
				case KeyCodes::Power:
					m_PowerCallback(m_Power.Toggle());
					return;
				default:
					switch ( m_State )
					{
					case State::Default:
						switch ( pInput )
						{
						case KeyCodes::Voltage: ResetInput( State::Voltage ); return;
						case KeyCodes::Current: ResetInput( State::Current ); return;

						case KeyCodes::Config:
						[[fallthrough]];
						case KeyCodes::Presets:
						[[fallthrough]];
						case KeyCodes::PresetsLong:
						[[fallthrough]];
						case KeyCodes::Shift:
							m_BuzzerCallback();
							NavigateChild( pInput );
							m_State = State::Child;
							return;
						case KeyCodes::Cancel:
							m_Units = MultipurposeUnit::Off;
							break;
						default:
							if ( IsKeycodeNumber( pInput ) or pInput == KeyCodes::Decimal )
								ResetInput( State::Unknown );
							else return;
						};
						[[fallthrough]];
					case State::Unknown:
						[[fallthrough]];
					case State::Voltage:
						[[fallthrough]];
					case State::Current:
						NavigateInput( pInput );
						return;
					case State::Child:
						NavigateChild( pInput );
						return;
					default: 
						break;
					};
				};
			}
			return;
		}
		unsigned Render( buffer_t& output, bool use_cursor = false ) noexcept
		{
			return Item().Render( output, use_cursor );
		}
	};

	template <unsigned BufferSize, typename T, typename BuzzerCallback, typename PowerCallback, typename LockedCallback, typename VoltageCallback, typename CurrentCallback, typename DefaultMenu, typename ChildMenu>
	RootMenu( General::SizeT<BufferSize>, General::Type<T>, MultipurposeUnit, LockState, BuzzerCallback, PowerCallback&&, LockedCallback&&, VoltageCallback&&, CurrentCallback&&, DefaultMenu&&, ChildMenu && )->RootMenu<BufferSize, T, BuzzerCallback, std::decay_t<PowerCallback>, std::decay_t<LockedCallback>, std::decay_t<VoltageCallback>, std::decay_t<CurrentCallback>, std::decay_t<DefaultMenu>, std::decay_t<ChildMenu>>;

	template 
	<
		unsigned BufferSize, typename T, 
		typename BuzzerCallback,
		typename PowerCallback, 
		typename LockedCallback,
		typename VoltageCallback,
		typename CurrentCallback,
		typename DefaultMenu, 
		typename BeepCallback,
		typename AutoResetEnergyCallback,
		typename SaveSetpointCallback,
		typename SaveLimitsCallback,
		typename UnitCallback,
		typename PresetsCallback,
		typename PresetsLoadCallback,
		typename FactoryReset,
		typename CalReset,
		typename AddVoltageCal,
		typename AddCurrentCal
	>
	auto SystemMenu(
	    General::SizeT<BufferSize>	s,
		General::Type<T>			t,
		MultipurposeUnit &			pUnitRefs,
		LockState & 				pLockState,

	    //Root menu constructor arguments
		BuzzerCallback &&  			pBuzzer, 
	    PowerCallback &&			pPower,
	    LockedCallback &&			pLocked,
	    VoltageCallback &&			pVoltage,
	    CurrentCallback &&			pCurrent,
		DefaultMenu &&  			pDefault,

	    //Home menu constructor arguments
	    BeepCallback	&&			pBeep,
		AutoResetEnergyCallback &&	pAutoResetEnergy,
		SaveSetpointCallback && 	pSaveSetpoints,
		SaveLimitsCallback && 		pSaveLimits,
	    UnitCallback &&				pUnits,
	    PresetsCallback	&&			pPresetsCallback,
	    PresetsLoadCallback	&&		pPresetsLoadCallback,
	    FactoryReset &&				pFactoryReset,
	    CalReset &&					pCalReset,
	    AddVoltageCal &&			pAddVoltageCal,
	    AddCurrentCal &&			pAddCurrentCal )
	{
		return RootMenu
		(
		    s, t, pUnitRefs, pLockState,
			FWD( pBuzzer ),
		    FWD( pPower ),
		    FWD( pLocked ),
		    FWD( pVoltage ),
		    FWD( pCurrent ),
			FWD( pDefault ),
		    HomeMenu(
		        s, t,
		        FWD( pBeep ),
				FWD( pAutoResetEnergy ),
				FWD( pSaveSetpoints ),
				FWD( pSaveLimits ),
				FWD( pUnits ),
				FWD( pPresetsCallback ),
				FWD( pPresetsLoadCallback ),
		        FWD( pFactoryReset ),
		        FWD( pCalReset ),
		        FWD( pAddVoltageCal ),
		        FWD( pAddCurrentCal ) ) 
		);
	}
#undef FWD
}