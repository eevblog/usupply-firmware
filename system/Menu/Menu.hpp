#pragma once

#include "System.hpp"
#include "EnumAccess.hpp"
#include "MultipurposeUnits.hpp"
#include "RootMenu.hpp"
#include "RawCallbackValue.hpp"
#include "RTC.hpp"
#include "Meta.hpp"

namespace System
{
	using MultipurposeUnit = LCD::Block::MultipurposeUnit;

	template <typename Base>
	auto Menu() noexcept
	{
		using namespace General;
		using namespace Menu;
		using namespace Peripherals::RTCGeneral;
        auto constexpr s = General::SizeT<8>{};
		//
		return SystemMenu
        (
            s, 
            General::Type<float>{},
            Base::Units(),
            Base::LockedState(),
            [&]
            {
                Base::Screen().SuccessTone();
            },
            [&]( OnOff power )
            {
                Base::Setpoint().PowerState(power);
                
                //Power was just turned on
                if ( power == On )
                {
                    if (Base::Settings().ResetEnergyOnPower())
                        Base::Measure().ResetTimeAndEnergy();
                    Peripherals::RTCGeneral::Resume();
                }
                else
                {
                    Peripherals::RTCGeneral::Pause();
                }
            },
            [&]( LockState lock ) { Base::Screen().UpdateLock(lock); },
            [&]( float v )
            {
                Base::Screen().ChooseTone( Base::Setpoint().Voltage(v) );
            },
            [&]( float i )
            {
                Base::Screen().ChooseTone( Base::Setpoint().Current(i) );
            },
            EnumAccess
            (
                s,
                Base::Units(),
                KeyedItem( Key<MultipurposeUnit::Off>{},	Label			(s, General::MakeArray("hELLo") ) ),
                KeyedItem( Key<MultipurposeUnit::W>{}, 		RawCallbackValue(s, [&]{ return Base::Measure().Power(); 		})),
                KeyedItem( Key<MultipurposeUnit::J>{}, 		RawCallbackValue(s, [&]{ return Base::Measure().Energy(); 		})),
                KeyedItem( Key<MultipurposeUnit::Wh>{}, 	RawCallbackValue(s, [&]{ return Base::Measure().WattHours();    })),
                KeyedItem( Key<MultipurposeUnit::h>{}, 		RawCallbackValue(s, [&]{ return TR{}.ToTime(); 					}))
            ),
            [&]( OnOff beep )				{ Base::Settings().Buzzer()             = beep; 		},
            [&]( OnOff energy_rst )			{ Base::Settings().ResetEnergyOnPower() = energy_rst; 	},
            [&]( OnOff save_sets )			{ Base::Settings().SaveSetpoints() 		= save_sets; 	},
            [&]( OnOff save_lims )			{ Base::Settings().SaveLimits()         = save_lims; 	},
            [&]( MultipurposeUnit unit ) 	{ Base::Units() = unit; },
            [&]( std::size_t presets )
            {
                auto set{ Base::Settings().Preset( presets ) };
                Base::Setpoint().Voltage( set.Voltage, false );
                Base::Setpoint().Current( set.Current, true );
                Base::Screen().Success2Tone();
            },
            [&]( std::size_t presets )
            {
                Base::Settings().Preset(
                {
                    Base::Setpoint().Voltage(),
                    Base::Setpoint().Current()
                }, presets);
                Base::Screen().SuccessTone();
            },
            [&]								{ Base::ResetSystem();                  },
            [&]                         	{ Base::ResetCalibration();             },
            [&]( Sample v ) 				{ Base::AddVoltageCalibrationPoint(v); 	},
            [&]( Sample i ) 				{ Base::AddCurrentCalibrationPoint(i); 	}
        );
	}
}