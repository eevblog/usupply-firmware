
#pragma once

#include "Action.hpp"
#include "AutoMenu.hpp"
#include "CalibrationInput.hpp"

namespace System
{
#define FWD(x) std::forward<decltype(x)>(x)
	template <unsigned BufferSize, typename AF, typename VC, typename IC>
	decltype( auto ) CalibrationMenu(
	    AF && pResetFunction,
	    VC && pAddVoltageCalibration,
	    IC && pAddCurrentCalibration )
	{
		using namespace Menu;
		auto constexpr s = General::SizeT<BufferSize>{};

		return LabelledAutoMenu( s,
													  General::MakeArray( "CAL" ),
		                         CalibrationInput( s, General::MakeArray( "VOLt" ),	General::MakeArray( "rEF" ), General::MakeArray( "out" ), 	FWD(pAddVoltageCalibration)	),
		                         CalibrationInput( s, General::MakeArray( "curr" ),	General::MakeArray( "rEF" ), General::MakeArray( "out" ), 	FWD(pAddCurrentCalibration)	),
		                         Action			 ( s, General::MakeArray( "C rSt" ),															FWD(pResetFunction) ) );
	}

	template <unsigned BufferSize, typename af, typename vc, typename ic>
	decltype( auto ) CalibrationMenu( 	General::SizeT<BufferSize>,
	                                  	af && pResetFunction,
										vc && pAddVoltageCalibration,
										ic && pAddCurrentCalibration  )
	{
		using namespace Menu;
		return CalibrationMenu<BufferSize>( std::forward<af>(pResetFunction), std::forward<vc>(pAddVoltageCalibration), std::forward<ic>(pAddCurrentCalibration) );
	}
}